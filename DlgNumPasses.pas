unit DlgNumPasses;

interface

uses
  SysUtils, Types, Classes, QGraphics, QControls, QForms, QDialogs,
  QStdCtrls, QComCtrls, QButtons;

type
  TNumPasses = class(TForm)
    GroupBox1: TGroupBox;
    NumOfPasses: TSpinEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    Value:  Integer;
    { Public declarations }
  end;

var
  NumPasses: TNumPasses;

implementation

{$R *.xfm}

procedure TNumPasses.BitBtn1Click(Sender: TObject);
begin
    Value := NumOfPasses.Value;
end;

end.
