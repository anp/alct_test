unit SlowControl;

interface
uses SysUtils, Classes, JTAGlib, ALCT;

function ReadIDCodeStr(alct_ctl: integer): string;
function ReadBoardSNStr(board_type: integer): string;
procedure StrToIDCode(const idstr: string; out id: alct_idreg);


implementation
uses alct_tests;

function ReadBoardSNStr(board_type: integer): string;
var
  sn_str: string;
  cr_str: string;
  sn_int: array[0..1] of longword;
  i,bit: integer;
begin
  sn_str := '';
  case (board_type) of
    BOARD_SN:	begin
         cr_str := ReadRegister(RdCfg);
         cr_str[1] := '0';
//         cr_str := '0020A03806B1193FC1';
         WriteRegister(WrCfg, cr_str);

      end;
	  MEZAN_SN: begin
         cr_str := ReadRegister(RdCfg);
         cr_str[1] := '1';
//         cr_str := '1020A03806B1193FC1';
         WriteRegister(WrCfg, cr_str);
      end;
  end;
  // reset DS2401
  WriteIR (SNreset, V_IR);
  Delay(1);
  WriteIR (SNwrite1, V_IR);
  Delay(50);

  // write read command 0x33
  WriteIR (SNwrite1, V_IR); Delay(1);
  WriteIR (SNwrite1, V_IR); Delay(1);
  WriteIR (SNwrite0, V_IR); Delay(1);
  WriteIR (SNwrite0, V_IR); Delay(1);
  WriteIR (SNwrite1, V_IR); Delay(1);
  WriteIR (SNwrite1, V_IR); Delay(1);
  WriteIR (SNwrite0, V_IR); Delay(1);
  WriteIR (SNwrite0, V_IR); Delay(1);

  // read 64 bits of SN bit by bit
  for i:=0 to 1 do
    begin
      sn_int[i] := 0;
      for bit:=0 to  31 do
      begin
        WriteIR(SNread, V_IR);
        sn_int[i] := sn_int[i] or (StrToInt('$'+ReadDR('0', 1)) shl bit);
        //sn_int[i] := (sn_int[i] shl 1) or (StrToInt('$'+ReadDR('0', 1)) and $1);
        Delay(1);
      end;
      sn_str := IntToHex(sn_int[i], 8) + sn_str;
    end;
  Result := sn_str;
end;

function ReadIDCodeStr(alct_ctl: integer): string;
var
	id:	string;
begin
	id := '0000000000';
  case alct_ctl of
  SLOW_CTL: begin
              WriteIR('00', SC_IR);
	            id := ReadDR(id, CTRL_SC_ID_DR_SIZE);
            end;
  FAST_CTL: begin
              WriteIR('00', V_IR);
	            id := ReadDR(id, USER_V_ID_DR_SIZE);
            end;
  end;
	Result := id;
end;

procedure StrToIDCode(const idstr: string; out id: alct_idreg);
var
	idcode: Int64;
begin
	if Length(idstr) <= sizeof(Int64)*2 then
	begin
		idcode := StrToInt64('$'+idstr);
		id.chip := byte(idcode and $f);
		id.version := byte((idcode shr 4) and $f);
		id.year	:= word((idcode shr 8) and $ffff);
		id.day := byte((idcode shr 24) and $ff);
		id.month := byte((idcode shr 32) and $ff);
	end;
end;

end.
