unit LPTJTAGLib;

interface

uses SysUtils,
{$IFDEF LINUX}
    libprtio,
{$ENDIF}

{$IFDEF MSWINDOWS}
    Windows, HW_32, TVicLib,
{$ENDIF}
Classes;

const TDI  = $01;
const TCK  = $02;
const TMS  = $04;
const TDITMS = $05;
const TCKTMS = $06;
const notTCKTMS = $F9;
const TDO  = $10;
const TRST = $04;

const STRB = $01;
{$IFNDEF LINUX}
const drivername = 'TVicHW32';
{$ELSE}
const drivername = '/dev/parport';
{$ENDIF}

{$IFDEF MSWINDOWS}
function openLPTJTAG(Owner: TComponent): integer;
{$ENDIF}

{$IFDEF LINUX}
function openLPTJTAG(PortNum: byte): integer;
{$ENDIF}

procedure closeLPTJTAG;
procedure setchainLPTJTAG(Chain: byte);
procedure resetLPTJTAG;
procedure enableLPTJTAG;
procedure idleLPTJTAG;
procedure rtiLPTJTAG;
procedure oneclockLPTJTAG;
procedure lightsLPTJTAG;
procedure TMSHighLPTJTAG;
procedure TMSLowLPTJTAG;
procedure jtagioLPTJTAG( const TMSvalue: byte; const TDIvalue: byte; out TDOvalue: byte);

procedure StartIRShiftLPTJTAG();
procedure StartDRShiftLPTJTAG();
procedure ExitIRShiftLPTJTAG();
procedure ExitDRShiftLPTJTAG();
function  ShiftDataLPTJTAG(Data: Longword; DataSize: byte; sendtms: boolean): Longword;

implementation

var
{$IFDEF MSWINDOWS}
    HW32     : TVicHW32;
    LPTPort  : THANDLE;
    nPort    : DWORD;
{$ENDIF}

{$IFDEF LINUX}
    LPTPort  : integer;
{$ENDIF}

{$IFDEF MSWINDOWS}
function openLPTJTAG(Owner: TComponent): integer;
begin

     HW32 := nil;
     HW32 := TVicHW32.Create(Owner);
     LPTPort := 0;
     LPTPort := OpenTVicHW32( LPTPort, PChar(drivername),'TVicDevice0');
     Result := integer(LPTPort);
     if GetActiveHW(LPTPort) then
     begin
          SetHardAccess(LPTPort, False);
          nPort   := GetLPTBAsePort(LPTPort);
          idleLPTJTAG;
     end
     else Result := -1;
end;
{$ENDIF}

{$IFDEF LINUX}
function openLPTJTAG( PortNum: byte): integer;
begin
    LPTPort := openParPort(PChar(drivername + IntToStr(PortNum)));
    if LPTPort > 0 then
    begin
         idleLPTJTAG;
    end;
    Result := LPTPort;
end;
{$ENDIF}

procedure closeLPTJTAG;
begin
{$IFDEF MSWINDOWS}
    CloseTVicHW32(LPTPort);
    HW32.Free;
{$ENDIF}

{$IFDEF LINUX}
    closeParPort(LPTPort);
{$ENDIF}
end;

procedure setchainLPTJTAG(Chain: byte);
begin
{$IFDEF MSWINDOWS}
    SetPortByte(LPTPort, nPort, Chain);
    SetPortByte(LPTPort, nPort+2, 11);
    SetPortByte(LPTPort, nPort+2, 0);
    SetPortByte(LPTPort, nPort, 0);
{$ENDIF}

{$IFDEF LINUX}
    wrDataPort(LPTPort, Chain);
    wrControlPort(LPTPort, 11);
    wrControlPort(LPTPort, 0);
    wrDataPort(LPTPort,0);
{$ENDIF}
end;

procedure resetLPTJTAG;
begin
{$IFDEF MSWINDOWS}
    SetPortByte(LPTPort, nPort, 0);
    SetPortByte(LPTPort, nPort+2, STRB or TRST); // Strobe with TRST high
    SetPortByte(LPTPort, nPort+2, STRB);         // Strobe with TRST low
    SetPortByte(LPTPort, nPort+2, STRB or TRST); // Strobe with TRST high
    SetPortByte(LPTPort, nPort, 0);
{$ENDIF}

{$IFDEF LINUX}
    wrDataPort(LPTPort, 0);
    wrControlPort(LPTPort, STRB or TRST);
    wrControlPort(LPTPort, STRB);
    wrControlPort(LPTPort, STRB or TRST);
    wrDataPort(LPTPort,0);
{$ENDIF}
end;

procedure enableLPTJTAG;
begin
{$IFDEF MSWINDOWS}
    SetPortByte(LPTPort, nPort+2, GetPortByte(LPTPort, nPort+2) or $02);
{$ENDIF}

{$IFDEF LINUX}
    wrControlPort(LPTPort, rdControlPort(LPTPort) or $02);
{$ENDIF}
end;

procedure idleLPTJTAG;
var
    i:  integer;
begin
    for i:= 1 to 5 do TMSHighLPTJTAG;
end;

procedure rtiLPTJTAG;
begin
    idleLPTJTAG;
    TMSLowLPTJTAG;
end;

procedure oneclockLPTJTAG;
begin
    TMSLowLPTJTAG;
end;

procedure lightsLPTJTAG;
begin
{$IFDEF MSWINDOWS}
    SetPortByte(LPTPort, nPort, $ff)
{$ENDIF}

{$IFDEF LINUX}
    wrDataPort(LPTPort, $ff);
{$ENDIF}
end;

procedure TMSHighLPTJTAG;
begin
{$IFDEF MSWINDOWS}
    SetPortByte(LPTPort, nPort, TMS);
    SetPortByte(LPTPort, nPort, TCKTMS);
    SetPortByte(LPTPort, nPort, TMS);
{$ENDIF}

{$IFDEF LINUX}
    wrDataPort(LPTPort, TMS);
    wrDataPort(LPTPort, TCKTMS);
    wrDataPort(LPTPort, TMS);
{$ENDIF}
end;

procedure TMSLowLPTJTAG;
begin
{$IFDEF MSWINDOWS}
    SetPortByte(LPTPort, nPort, 0);
    SetPortByte(LPTPort, nPort, TCK);
    SetPortByte(LPTPort, nPort, 0);
{$ENDIF}

{$IFDEF LINUX}
    wrDataPort(LPTPort, 0);
    wrDataPort(LPTPort, TCK);
    wrDataPort(LPTPort, 0);
{$ENDIF}
end;

procedure jtagioLPTJTAG( const TMSvalue: byte; const TDIvalue: byte; out TDOvalue: byte);
var
   sendbit: byte;
   rcvbit : byte;
begin
{$IFDEF MSWINDOWS}
          sendbit := GetPortByte(LPTPort, nPort);
//          sendbit := 0;
          if (TDIvalue>0) then
            sendbit := (sendbit or TDI)
          else sendbit := (sendbit and (not TDI));
//          SetPortByte(LPTPort, nPort, sendbit);
          if (TMSvalue>0) then
            sendbit := (sendbit or TMS)
          else sendbit := (sendbit and (not TMS));
          sendbit := sendbit or $10;
          SetPortByte(LPTPort, nPort, sendbit);
          sendbit := sendbit or TCK;
          SetPortByte(LPTPort, nPort, sendbit);
          rcvbit := GetPortByte(LPTPort, nPort+1);
          rcvbit := (not rcvbit) and TDO;
          sendbit := sendbit and (not TCK);
          SetPortByte(LPTPOrt, nPort, sendbit);
          if (rcvbit = TDO) then
            TDOvalue := 0
          else
            TDOvalue := 1;
{$ENDIF}

{$IFDEF LINUX}
          sendbit := rdDataPort(LPTPort);
          if (TDIvalue>0) then sendbit := (sendbit or TDI) else sendbit := (sendbit and (not TDI));
          wrDataPort(LPTPort, sendbit);
          if (TMSvalue>0) then sendbit := (sendbit or TMS) else sendbit := (sendbit and (not TMS));
          sendbit := sendbit or $10;
          wrDataPort(LPTPort, sendbit);
          sendbit := sendbit or TCK;
          wrDataPort(LPTPort, sendbit);
          rcvbit := rdStatusPort(LPTPort);
          rcvbit := (not rcvbit) and TDO;
          sendbit := sendbit and (not TCK);
          wrDataPort(LPTPort, sendbit);
          if (rcvbit = TDO) then
            TDOvalue := 0
          else
            TDOvalue := 1;
{$ENDIF}
end;

function  ShiftDataLPTJTAG(Data: Longword; DataSize: byte; sendtms: boolean): Longword;
var
   i:   byte;
   tdo: byte;
   tmp: Longword;
begin
        if (DataSize > 32) then Result:= 0
        else
        begin
                tmp := 0;

                for i:=1 to DataSize do
                begin
                        jtagioLPTJTAG( byte ((i = DataSize) and sendtms), byte (Data and $01), tdo);
                        tmp := tmp or ( (Longword (tdo and $01)) shl (i-1) );
                        Data := Data shr 1;
                end;

                Result := tmp;
        end;
end;

procedure StartIRShiftLPTJTAG();
var
    tdo:    byte;
begin
    jtagioLPTJTAG(0, 0, tdo);
    jtagioLPTJTAG(1, 0, tdo);
    jtagioLPTJTAG(1, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
end;

procedure StartDRShiftLPTJTAG();
var
    tdo:    byte;
begin
    jtagioLPTJTAG(0, 0, tdo);
    jtagioLPTJTAG(1, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
end;

procedure ExitIRShiftLPTJTAG();
var
    tdo:    byte;
begin
    jtagioLPTJTAG(1, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
end;

procedure ExitDRShiftLPTJTAG();
var
    tdo:    byte;
begin
    jtagioLPTJTAG(1, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
    jtagioLPTJTAG(0, 0, tdo);
end;


end.
