unit WriteToFileUnit;

interface

uses
  SysUtils, Types, Classes, QGraphics, QControls, QForms, QDialogs,
  QStdCtrls, alct_tests;

type
  TWriteToFileForm = class(TForm)
    BtnYes: TButton;
    BtnNo: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure BtnYesClick(Sender: TObject);
    procedure BtnNoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WriteToFileForm: TWriteToFileForm;

implementation

{$R *.xfm}

procedure TWriteToFileForm.BtnYesClick(Sender: TObject);
begin
  ModalResult := mrYes;
end;

procedure TWriteToFileForm.BtnNoClick(Sender: TObject);
begin
  ModalResult := mrNo;
end;

procedure TWriteToFileForm.FormShow(Sender: TObject);
begin
  Label5.Caption := alct_tests.Form1.edBoardNumber.Text;
  Label6.Caption := alct_tests.Form1.edBoardNumber.Text;
end;

end.
