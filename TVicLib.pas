{========================================================================}
{=================  TVicHW32  Shareware Version 5.0     =================}
{=====          Copyright (c) 1997-2000 Victor I.Ishikeev           =====}
{==========             mailto:ivi.ufanet.ru                      =======}
{========================================================================}
{=====                 TVicHW32.DLL interface                       =====} 
{========================================================================}

unit TVicLib;

{==} interface {==}

uses Windows, HW_Types;

function  OpenTVicHW32(HW32 : THANDLE;
                       ServiceName : PChar;
                       EntryPoint : PChar) : THANDLE;  stdcall;

function  CloseTVicHW32(HW32 : THANDLE) : THANDLE;     stdcall;

function OpenPciDeviceOnSlot( HW32      : THANDLE;
                              BusNum    : Word;
                              DeviceNum : Word;
                              FuncNum   : Word):THANDLE; stdcall;

procedure ClosePciDevice    ( HW32 : THANDLE; PciDevice : THANDLE); stdcall
function  ReadPciCommandReg ( HW32 : THANDLE; PciDevice : THANDLE): Word; stdcall;
procedure WritePciCommandReg( HW32 : THANDLE;
                              PciDevice : THANDLE;
                              CommandRegValue : Word); stdcall;
procedure ControlPciCommandRegBits ( HW32 : THANDLE;
				     PciDevice : THANDLE;
				     CommandRegBit : Byte;
				     SetClearFlag  : Byte); stdcall;


function  GetActiveHW(HW32 : THANDLE) : BOOL;          stdcall;

function  GetHardAccess(HW32 : THANDLE) : BOOL;        stdcall;

procedure SetHardAccess(HW32 : THANDLE; bNewValue : BOOL); stdcall;

function  GetPortByte( HW32 : THANDLE; PortAddr : DWORD) : Byte;  stdcall;
procedure SetPortByte( HW32 : THANDLE; PortAddr : DWORD; nNewValue : Byte); stdcall;
function  GetPortWord( HW32 : THANDLE; PortAddr : DWORD) : Word;            stdcall;
procedure SetPortWord( HW32 : THANDLE; PortAddr : DWORD; nNewValue : Word); stdcall;
function  GetPortLong( HW32 : THANDLE; PortAddr : DWORD): LongInt;          stdcall;
procedure SetPortLong( HW32 : THANDLE; PortAddr : DWORD; nNewValue : Longint); stdcall;

function  MapPhysToLinear( HW32: THANDLE; PhAddr: DWORD; PhSize: DWORD) : Pointer;  stdcall;
procedure UnmapMemory( HW32: THANDLE; PhAddr : DWORD; PhSize: DWORD); stdcall;
function  GetLockedMemory( HW32 : THANDLE ): Pointer;   stdcall;
function  GetTempVar( HW32 : THANDLE ): Longint;   stdcall;

function  GetMemByte(HW32 : THANDLE; MappedAddr : DWORD ;  Offset : DWORD) : Byte; stdcall;
procedure SetMemByte(HW32 : THANDLE; MappedAddr : DWORD ;  Offset : DWORD; nNewValue : Byte); stdcall;
function  GetMemWord(HW32 : THANDLE;MappedAddr : DWORD ;  Offset : DWORD) : Word; stdcall;
procedure SetMemWord(HW32 : THANDLE;MappedAddr : DWORD ;  Offset : DWORD; nNewValue : Word); stdcall;
function  GetMemLong(HW32 : THANDLE;MappedAddr : DWORD ;  Offset : DWORD) : DWORD;           stdcall;
procedure SetMemLong(HW32 : THANDLE;MappedAddr : DWORD ;  Offset : DWORD; nNewValue : DWORD);stdcall;

function  IsIRQMasked(HW32 : THANDLE; IRQNumber : WORD) : BOOL;         stdcall;
procedure UnmaskIRQ(HW32 : THANDLE; IRQNumber : WORD; HWHandler : TOnHWInterrupt); stdcall;
procedure UnmaskIRQEx(HW32 : THANDLE;
                      IRQNumber : WORD;
                      IrqShared : DWORD;
                      HWHandler : TOnHWInterrupt;
                      ClearRec  : pIrqClearRec
                      ); stdcall;
procedure UnmaskDelphiIRQ(HW32 : THANDLE; TVic: THANDLE; IRQNumber : WORD; HWHandler : TOnDelphiInterrupt); stdcall;
procedure UnmaskDelphiIRQEx(HW32 : THANDLE;
                            TVic: THANDLE;
                            IRQNumber : WORD;
                            IrqShared : DWORD;
                            HWHandler : TOnDelphiInterrupt;
                            ClearRec  : pIrqClearRec
                            ); stdcall;

procedure MaskIRQ(HW32 : THANDLE; IRQNumber : WORD);                 stdcall;
function  GetIRQCounter(HW32: THANDLE; IRQNumber : WORD): DWORD;     stdcall;

procedure PutScanCode(HW32 : THANDLE; b : Byte);                     stdcall;
function  GetScanCode(HW32 : THANDLE):Word;                          stdcall;
procedure HookKeyboard(HW32 : THANDLE; KbdHandler : TOnKeystroke);   stdcall;
procedure HookDelphiKeyboard(HW32 : THANDLE; TVic : THANDLE; KbdHandler : TOnDelphiKeystroke);   stdcall;
procedure UnhookKeyboard(HW32 : THANDLE);                            stdcall;
procedure PulseKeyboard(HW32 : THANDLE);                             stdcall;
procedure PulseKeyboardLocal(HW32 : THANDLE);                        stdcall;

function GetLPTNumber (HW32 : THANDLE) : Byte;                       stdcall;
procedure SetLPTNumber( HW32 : THANDLE; nNewValue : Byte);           stdcall;
function GetLPTNumPorts (HW32 : THANDLE) : Byte;                     stdcall;
function GetLPTBasePort (HW32 : THANDLE) : DWORD;                    stdcall;

function GetPin( HW32 : THANDLE; nPin : Byte) : BOOL;                stdcall;
procedure SetPin( HW32 : THANDLE; nPin : Byte; bNewValue : BOOL);    stdcall;

function GetLPTAckwl (HW32 : THANDLE) : BOOL;                        stdcall;
function GetLPTBusy(HW32 : THANDLE) : BOOL;                          stdcall;
function GetLPTPaperEnd(HW32 : THANDLE) : BOOL;                      stdcall;
function GetLPTSlct(HW32 : THANDLE) : BOOL;                          stdcall;
function GetLPTError(HW32 : THANDLE) : BOOL;                         stdcall;
procedure LPTInit(HW32 : THANDLE);                                   stdcall;
procedure LPTSlctIn(HW32 : THANDLE);                                 stdcall;
procedure LPTStrobe(HW32 : THANDLE);                                 stdcall;
procedure LPTAutofd( HW32 : THANDLE; Flag : BOOL);                   stdcall;

procedure SetLPTReadMode( HW32 : THANDLE );                          stdcall;
procedure SetLPTWriteMode( HW32 : THANDLE );                         stdcall;

procedure ForceIrqLPT( HW32 : THANDLE; IrqEnable : BOOL);            stdcall;

procedure   ReadPortFIFO  ( HW32 : THANDLE; pBuffer : pPortByteFIFO);stdcall;
procedure   ReadPortWFIFO ( HW32 : THANDLE; pBuffer : pPortWordFIFO);stdcall;
procedure   ReadPortLFIFO ( HW32 : THANDLE; pBuffer : pPortLongFIFO);stdcall;
procedure   WritePortFIFO ( HW32 : THANDLE; pBuffer : pPortByteFIFO);stdcall;
procedure   WritePortWFIFO( HW32 : THANDLE; pBuffer : pPortWordFIFO);stdcall;
procedure   WritePortLFIFO( HW32 : THANDLE; pBuffer : pPortLongFIFO);stdcall;

procedure   GetHDDInfo(HW32      : THANDLE;
		           IdeNumber : Word;
                       Master    : Word;
                       Info      : pHDDInfo);  stdcall;

function    GetLastPciBus(HW32      : THANDLE) : Word; stdcall;
function    GetHardwareMechanism(HW32      : THANDLE) : Word; stdcall;
function    GetPciDeviceInfo( HW32      : THANDLE;
                              Bus,Device,Func : Word;
                              CfgInfo         : pPciCfg) : BOOL;  stdcall;

function    GetSysDmaBuffer      ( HW32   : THANDLE;
	 		           BufReq : pDmaBufferRequest) : BOOL; stdcall;

function    GetBusmasterDmaBuffer( HW32   : THANDLE;
	 		           BufReq : pDmaBufferRequest) : BOOL; stdcall;

function    FreeDmaBuffer        ( HW32   : THANDLE;
	 		           BufReq : pDmaBufferRequest) : BOOL; stdcall;

function  AcquireLPT  ( HW32 : THANDLE; LPTNumber : Word) : Word; stdcall;
procedure ReleaseLPT  ( HW32 : THANDLE; LPTNumber : Word);        stdcall;
function IsLPTAcquired( HW32 : THANDLE; LPTNumber : Word) : Word; stdcall;

function  RunRing0Function (HW32                : THANDLE;
                            Ring0FunctionAddress: TRing0Function;
                            pParm               : Pointer) : Longint;
                            stdcall;

function  DebugCode(HW32 : THANDLE): Longint; stdcall;

{==} implementation {==}

function  OpenTVicHW32(HW32 : THANDLE;
                       ServiceName : PChar;
                       EntryPoint : PChar) : THANDLE;
                 stdcall; external 'TVicHW32.dll' name 'OpenTVicHW32';

function  CloseTVicHW32(HW32 : THANDLE) : THANDLE;
                 stdcall; external 'TVicHW32.dll' name 'CloseTVicHW32';

function OpenPciDeviceOnSlot( HW32      : THANDLE;
                              BusNum    : Word;
                              DeviceNum : Word;
                              FuncNum   : Word):THANDLE;
                 stdcall; external 'TVicHW32.dll' name 'OpenPciDeviceOnSlot';
procedure ClosePciDevice    ( HW32 : THANDLE; PciDevice : THANDLE);
                 stdcall; external 'TVicHW32.dll' name 'ClosePciDevice';
function  ReadPciCommandReg ( HW32 : THANDLE; PciDevice : THANDLE): Word;
                 stdcall; external 'TVicHW32.dll' name 'ReadPciCommandReg';
procedure WritePciCommandReg( HW32 : THANDLE;
                              PciDevice : THANDLE;
                              CommandRegValue : Word);
                 stdcall; external 'TVicHW32.dll' name 'WritePciCommandReg';
procedure ControlPciCommandRegBits ( HW32 : THANDLE;
				     PciDevice : THANDLE;
				     CommandRegBit : Byte;
				     SetClearFlag  : Byte);
                 stdcall; external 'TVicHW32.dll' name 'ControlPciCommandRegBits';

function  GetActiveHW(HW32 : THANDLE) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetActiveHW';

function  GetHardAccess(HW32 : THANDLE) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetHardAccess';

procedure SetHardAccess(HW32 : THANDLE; bNewValue : BOOL);
                 stdcall; external 'TVicHW32.dll' name 'SetHardAccess';

function  GetPortByte( HW32 : THANDLE; PortAddr : DWORD) : Byte;
                 stdcall; external 'TVicHW32.dll' name 'GetPortByte';
procedure SetPortByte( HW32 : THANDLE; PortAddr : DWORD; nNewValue : Byte);
                 stdcall; external 'TVicHW32.dll' name 'SetPortByte';
function  GetPortWord( HW32 : THANDLE; PortAddr : DWORD) : Word;
                 stdcall; external 'TVicHW32.dll' name 'GetPortWord';
procedure SetPortWord( HW32 : THANDLE; PortAddr : DWORD; nNewValue : Word);
                 stdcall; external 'TVicHW32.dll' name 'SetPortWord';
function  GetPortLong( HW32 : THANDLE; PortAddr : DWORD): LongInt;
                 stdcall; external 'TVicHW32.dll' name 'GetPortLong';
procedure SetPortLong( HW32 : THANDLE; PortAddr : DWORD; nNewValue : Longint);
                 stdcall; external 'TVicHW32.dll' name 'SetPortLong';

function  MapPhysToLinear( HW32 : THANDLE; PhAddr : DWORD; PhSize: DWORD) : Pointer;
                 stdcall; external 'TVicHW32.dll' name 'MapPhysToLinear';
procedure UnmapMemory( HW32 : THANDLE; PhAddr : DWORD; PhSize: DWORD);
                 stdcall; external 'TVicHW32.dll' name 'UnmapMemory';
function  GetLockedMemory( HW32 : THANDLE ): Pointer;
                 stdcall; external 'TVicHW32.dll' name 'GetLockedMemory';
function  GetTempVar( HW32 : THANDLE ): Longint;
                 stdcall; external 'TVicHW32.dll' name 'GetTempVar';


function  GetMemByte(HW32 : THANDLE; MappedAddr : DWORD ;  Offset : DWORD) : Byte;
                 stdcall; external 'TVicHW32.dll' name 'GetMem';
procedure SetMemByte(HW32 : THANDLE; MappedAddr : DWORD ;  Offset : DWORD; nNewValue : Byte);
                 stdcall; external 'TVicHW32.dll' name 'SetMem';
function  GetMemWord(HW32 : THANDLE;MappedAddr : DWORD ;  Offset : DWORD) : Word;
                 stdcall; external 'TVicHW32.dll' name 'GetMemW';
procedure SetMemWord(HW32 : THANDLE;MappedAddr : DWORD ;  Offset : DWORD; nNewValue : Word);
                 stdcall; external 'TVicHW32.dll' name 'SetMemW';
function  GetMemLong(HW32 : THANDLE;MappedAddr : DWORD ;  Offset : DWORD) : DWORD;
                 stdcall; external 'TVicHW32.dll' name 'GetMemL';
procedure SetMemLong(HW32 : THANDLE;MappedAddr : DWORD ;  Offset : DWORD; nNewValue : DWORD);
                 stdcall; external 'TVicHW32.dll' name 'SetMemL';


function  IsIRQMasked(HW32 : THANDLE; IRQNumber : WORD) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'IsIRQMasked';
procedure UnmaskIRQ(HW32 : THANDLE; IRQNumber : WORD; HWHandler : TOnHWInterrupt);
                 stdcall; external 'TVicHW32.dll' name 'UnmaskIRQ';
procedure UnmaskDelphiIRQ(HW32 : THANDLE; TVic : THANDLE; IRQNumber : WORD; HWHandler : TOnDelphiInterrupt);
                 stdcall; external 'TVicHW32.dll' name 'UnmaskDelphiIRQ';
procedure UnmaskIRQEx(HW32 : THANDLE;
                      IRQNumber : WORD;
                      IrqShared : DWORD;
                      HWHandler : TOnHWInterrupt;
                      ClearRec  : pIrqClearRec
                      );
                 stdcall; external 'TVicHW32.dll' name 'UnmaskIRQEx';

procedure UnmaskDelphiIRQEx(HW32 : THANDLE;
                            TVic: THANDLE;
                            IRQNumber : WORD;
                            IrqShared : DWORD;
                            HWHandler : TOnDelphiInterrupt;
                            ClearRec  : pIrqClearRec
                            );
                 stdcall; external 'TVicHW32.dll' name 'UnmaskDelphiIRQEx';                            


procedure MaskIRQ(HW32 : THANDLE; IRQNumber : WORD);
                 stdcall; external 'TVicHW32.dll' name 'MaskIRQ';
function  GetIRQCounter(HW32: THANDLE; IRQNumber : WORD):DWORD;
                 stdcall; external 'TVicHW32.dll' name 'GetIRQCounter';


procedure PutScanCode(HW32 : THANDLE; b : Byte);
                 stdcall; external 'TVicHW32.dll' name 'PutScanCode';

function  GetScanCode(HW32 : THANDLE):Word;
                 stdcall; external 'TVicHW32.dll' name 'GetScanCode';

procedure HookKeyboard(HW32 : THANDLE; KbdHandler : TOnKeystroke);
                 stdcall; external 'TVicHW32.dll' name 'HookKeyboard';
procedure HookDelphiKeyboard(HW32 : THANDLE; TVic : THANDLE; KbdHandler : TOnDelphiKeystroke);
                 stdcall; external 'TVicHW32.dll' name 'HookDelphiKeyboard';
procedure UnhookKeyboard(HW32 : THANDLE);
                 stdcall; external 'TVicHW32.dll' name 'UnhookKeyboard';
procedure PulseKeyboard(HW32 : THANDLE);
                 stdcall; external 'TVicHW32.dll' name 'PulseKeyboard';
procedure PulseKeyboardLocal(HW32 : THANDLE);
                 stdcall; external 'TVicHW32.dll' name 'PulseKeyboardLocal';

function GetLPTNumber (HW32 : THANDLE) : Byte;
                 stdcall; external 'TVicHW32.dll' name 'GetLPTNumber';
procedure SetLPTNumber( HW32 : THANDLE; nNewValue : Byte);
                 stdcall; external 'TVicHW32.dll' name 'SetLPTNumber';
function GetLPTNumPorts (HW32 : THANDLE) : Byte;
                 stdcall; external 'TVicHW32.dll' name 'GetLPTNumPorts';
function GetLPTBasePort (HW32 : THANDLE) : DWORD;
                 stdcall; external 'TVicHW32.dll' name 'GetLPTBasePort';

function GetPin( HW32 : THANDLE; nPin : Byte) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetPin';
procedure SetPin( HW32 : THANDLE; nPin : Byte; bNewValue : BOOL);
                 stdcall; external 'TVicHW32.dll' name 'SetPin';

function GetLPTAckwl (HW32 : THANDLE) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetLPTAckwl';
function GetLPTBusy(HW32 : THANDLE) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetLPTBusy';
function GetLPTPaperEnd(HW32 : THANDLE) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetLPTPaperEnd';
function GetLPTSlct(HW32 : THANDLE) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetLPTSlct';
function GetLPTError(HW32 : THANDLE) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetLPTError';

procedure LPTInit(HW32 : THANDLE);
                 stdcall; external 'TVicHW32.dll' name 'LPTInit';
procedure LPTSlctIn(HW32 : THANDLE);
                 stdcall; external 'TVicHW32.dll' name 'LPTSlctIn';
procedure LPTStrobe(HW32 : THANDLE);
                 stdcall; external 'TVicHW32.dll' name 'LPTStrobe';
procedure LPTAutofd( HW32 : THANDLE; Flag : BOOL);
                 stdcall; external 'TVicHW32.dll' name 'LPTAutofd';

procedure SetLPTReadMode( HW32 : THANDLE );
                 stdcall; external 'TVicHW32.dll' name 'SetLPTReadMode';

procedure SetLPTWriteMode( HW32 : THANDLE );
                 stdcall; external 'TVicHW32.dll' name 'SetLPTWriteMode';

procedure ForceIrqLPT( HW32 : THANDLE; IrqEnable : BOOL);
                 stdcall; external 'TVicHW32.dll' name 'ForceIrqLPT';

procedure   ReadPortFIFO ( HW32 : THANDLE; pBuffer : pPortByteFIFO);
                 stdcall; external 'TVicHW32.dll' name 'ReadPortFIFO';
procedure   ReadPortWFIFO( HW32 : THANDLE; pBuffer : pPortWordFIFO);
                 stdcall; external 'TVicHW32.dll' name 'ReadPortWFIFO';
procedure   ReadPortLFIFO( HW32 : THANDLE; pBuffer : pPortLongFIFO);
                 stdcall; external 'TVicHW32.dll' name 'ReadPortLFIFO';

procedure   WritePortFIFO( HW32 : THANDLE; pBuffer : pPortByteFIFO);
                 stdcall; external 'TVicHW32.dll' name 'WritePortFIFO';
procedure   WritePortWFIFO( HW32 : THANDLE; pBuffer : pPortWordFIFO);
                 stdcall; external 'TVicHW32.dll' name 'WritePortWFIFO';
procedure   WritePortLFIFO( HW32 : THANDLE; pBuffer : pPortLongFIFO);
                 stdcall; external 'TVicHW32.dll' name 'WritePortLFIFO';

procedure   GetHDDInfo(HW32      : THANDLE;
		       IdeNumber : Word;
                       Master    : Word;
                       Info      : pHDDInfo);
                 stdcall; external 'TVicHW32.dll' name 'GetHDDInfo';

function    GetLastPciBus(HW32      : THANDLE) : Word;
                 stdcall; external 'TVicHW32.dll' name 'GetLastPciBus';
function    GetHardwareMechanism(HW32      : THANDLE) : Word;
                 stdcall; external 'TVicHW32.dll' name 'GetHardwareMechanism';
function    GetPciDeviceInfo( HW32      : THANDLE;
                              Bus,Device,Func : Word;
                              CfgInfo         : pPciCfg) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetPciDeviceInfo';

function    GetSysDmaBuffer      ( HW32   : THANDLE;
	 		           BufReq : pDmaBufferRequest) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetSysDmaBuffer';

function    GetBusmasterDmaBuffer( HW32   : THANDLE;
	 		           BufReq : pDmaBufferRequest) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'GetBusmasterDmaBuffer';

function    FreeDmaBuffer        ( HW32   : THANDLE;
	 		           BufReq : pDmaBufferRequest) : BOOL;
                 stdcall; external 'TVicHW32.dll' name 'FreeDmaBuffer';

function  AcquireLPT  ( HW32 : THANDLE; LPTNumber : Word) : Word; 
                 stdcall; external 'TVicHW32.dll' name 'AcquireLPT';
procedure ReleaseLPT  ( HW32 : THANDLE; LPTNumber : Word);
                 stdcall; external 'TVicHW32.dll' name 'ReleaseLPT';
function IsLPTAcquired( HW32 : THANDLE; LPTNumber : Word) : Word;
                 stdcall; external 'TVicHW32.dll' name 'IsLPTAcquired';

function  RunRing0Function        (HW32                : THANDLE;
                                   Ring0FunctionAddress: TRing0Function;
                                   pParm               : Pointer) : Longint;
                 stdcall; external 'TVicHW32.dll' name 'RunRing0Function';

function DebugCode(HW32 : THANDLE): Longint; stdcall; external 'TVicHW32.dll' name 'DebugCode';

end.