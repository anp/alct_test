program ALCT_Test;

uses
  QForms,
  alct_tests in 'alct_tests.pas' {Form1},
  JTAGLib in 'JTAGLib.pas',
  ALCT in 'ALCT.pas',
  DlgNumPasses in 'DlgNumPasses.pas' {NumPasses},
  LPTJTAGLib in 'LPTJTAGLib.pas',
  FirmwareLoader in 'FirmwareLoader.pas',
  JTAGPortLib in 'JTAGPortLib.pas',
  SlowControl in 'SlowControl.pas',
  JTAGUtils in 'JTAGUtils.pas',
  OperProgress in 'OperProgress.pas' {frOperProgress},
  WriteToFileUnit in 'WriteToFileUnit.pas' {WriteToFileForm};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'ALCT Tests';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TNumPasses, NumPasses);
  Application.CreateForm(TfrOperProgress, frOperProgress);
  Application.CreateForm(TWriteToFileForm, WriteToFileForm);
  Application.Run;
end.
