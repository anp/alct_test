{========================================================================}
{=================  TVicHW32  Shareware Version 5.0     =================}
{======         Copyright (c) 1997-2000 Victor I.Ishikeev           =====}
{========================================================================}
{==========             mailto:ivi.ufanet.ru                      =======}
{========================================================================}

unit HW_32;

{$T-}

{--------}  interface    {---------}

uses SysUtils,Classes,Windows,TVicLib, HW_Types;

type TInterruptHandler = procedure (IrqNumber : Word) of object;
     TKeystrokeHandler = procedure (scan_code : Byte) of object;

type

  TVicHw32 = class(TComponent)

  private

    fHW32            : THANDLE;

    fOpenDrive       : Boolean;
    fHardAccess      : Boolean;

    fOnHwInterrupt   : TInterruptHandler;
    fOnKeystroke     : TKeystrokeHandler;

    fMasked          : array[0..15] of Boolean;
    fHooked          : Boolean;

    fLPTIRQEnabled   : Boolean;

    fLPTReadMode     : Boolean;

    procedure        fSetLPTReadMode(readmode : Boolean);

    procedure        fSetHardAccess(parm : Boolean);

    function         fGetIRQMasked(IrqNumber : Word) : Boolean;
    procedure	     fSetIRQMasked(IrqNumber : Word; bNewValue  : Boolean);

    procedure	     fSetLPTNumber(nNewValue  : Byte);
    function         fGetLPTNumber : Byte;
    function         fLPTs : Byte;
    function         fLPTBasePort : DWORD;

    function	     fGetLPTAckwl : Boolean;
    function	     fGetLPTBusy : Boolean;
    function	     fGetLPTPaperEnd : Boolean;
    function	     fGetLPTSlct : Boolean;
    function	     fGetLPTError : Boolean;
    procedure	     fSetLPTIRQ(nNewValue  : Boolean);

    function         fGetPin(nPin : Byte) : Boolean;
    procedure        fSetPin(nPin : Byte; bNewValue : Boolean);

    function         fGetPortB(PortAddr : DWORD) : Byte;
    procedure        fSetPortB(PortAddr : DWORD; nNewValue : Byte);
    function         fGetPortW(PortAddr : DWORD) : Word;
    procedure        fSetPortW(PortAddr : DWORD; nNewValue : Word);
    function         fGetPortL(PortAddr : DWORD) : DWORD;
    procedure        fSetPortL(PortAddr : DWORD; nNewValue : DWORD);

    function         fGetMemB(MappedAddr : Pointer; Offset : DWORD) : Byte;
    procedure        fSetMemB(MappedAddr : Pointer; Offset : DWORD; nNewValue : Byte);
    function         fGetMemW(MappedAddr : Pointer; Offset : DWORD) : Word;
    procedure        fSetMemW(MappedAddr : Pointer; Offset : DWORD; nNewValue : Word);
    function         fGetMemL(MappedAddr : Pointer; Offset : DWORD) : DWORD;
    procedure        fSetMemL(MappedAddr : Pointer; Offset : DWORD; nNewValue : DWORD);

  public

    function   OpenDriver (DriverEntryPoint : String) : Boolean;
    procedure  CloseDriver;

    function   MapPhysToLinear(PhAddr : DWORD; PhSize : DWORD) : Pointer;
    procedure  UnmapMemory(PhAddr : DWORD; PhSize : DWORD);

    procedure  fLPTStrobe;
    procedure  fLPTAutofd(Flag : Boolean);
    procedure  fLPTInit;
    procedure  fLPTSlctIn;
    procedure   PutScanCode(b:Byte);
    procedure   GetHDDInfo(IdeNumber:Word; Master:Word; var Info : THDDInfo);

    procedure   ReadPortFIFO ( PortAddr : DWORD; NumPorts:Word; var Buffer);
    procedure   WritePortFIFO( PortAddr : DWORD; NumPorts:Word; var Buffer);
    procedure   ReadPortWFIFO ( PortAddr : DWORD; NumPorts:Word; var Buffer);
    procedure   WritePortWFIFO( PortAddr : DWORD; NumPorts:Word; var Buffer);
    procedure   ReadPortLFIFO ( PortAddr : DWORD; NumPorts:Word; var Buffer);
    procedure   WritePortLFIFO( PortAddr : DWORD; NumPorts:Word; var Buffer);

    procedure	UnmaskInterrupt(IrqNumber : Word);
    procedure   UnmaskInterruptEx(IrqNumber    : Word;
                                  IrqShared    : Word;
                                  ClearRec     : pIrqClearRec);
    procedure	MaskInterrupt(IrqNumber : Word);

    procedure	HookKeyboard;
    procedure	UnhookKeyboard;

    function	LastPciBus : Byte;
    function	HardwareMechanism : Byte;

    function    PciDeviceInfo(bus,dev,func : Word; Info : pPciCfg) : Boolean;

    function    GetIRQCounter(IrqNumber:Word):Longint;

    procedure   AllocateSysDmaBuffer(DmaBufferRequest : pDmaBufferRequest);
    procedure   AllocateBusmasterDmaBuffer(DmaBufferRequest : pDmaBufferRequest);
    procedure   FreeDmaBuffer(DmaBufferRequest : pDmaBufferRequest);

    function    AcquireLPT : Word;
    procedure   ReleaseLPT;
    function    LPTAcquired : Word;

    function    RunRing0Function(R0proc : TRing0Function; pParm: Pointer):Longint;

    property    Port[Index:DWORD]  : Byte  read fGetPortB  write fSetPortB;
    property    PortW[Index:DWORD] : Word  read fGetPortW write fSetPortW;
    property    PortL[Index:DWORD] : DWORD read fGetPortL write fSetPortL;

    property    Mem[Base:Pointer; Offset:DWORD]  : Byte  read fGetMemB write fSetMemB;
    property    MemW[Base:Pointer; Offset:DWORD] : Word  read fGetMemW write fSetMemW;
    property    MemL[Base:Pointer; Offset:DWORD] : DWORD read fGetMemL write fSetMemL;


    property    IRQMasked[Index:Word]    : Boolean read fGetIRQMasked     write fSetIRQMasked;

    property    LPTAckwl     : Boolean read fGetLPTAckwl;
    property    LPTBusy      : Boolean read fGetLPTBusy;
    property    LPTPaperEnd  : Boolean read fGetLPTPaperEnd;
    property    LPTSlct      : Boolean read fGetLPTSlct;
    property    LPTError     : Boolean read fGetLPTError;
    property    LPTNumPorts  : Byte    read fLPTs;
    property    LPTBasePort  : DWORD   read fLPTBasePort;

    property    LPTIRQEnabled: Boolean read fLPTIRQEnabled write fSetLPTIRQ;

    property    LPTReadMode : Boolean read fLPTReadMode write fSetLPTReadMode;

    property    Pin[Index:Byte] : Boolean read fGetPin write fSetPin;

  published

    property    ActiveHW      : Boolean read fOpenDrive;
    property    OnHwInterrupt : TInterruptHandler read fOnHwInterrupt write fOnHwInterrupt;
    property    OnKeystroke   : TKeystrokeHandler read fOnKeystroke write fOnKeystroke;
    property    LPTNumber     : Byte    read fGetLPTNumber  write fSetLPTNumber default 1;
    property    HardAccess    : Boolean read fHardAccess write fSetHardAccess default TRUE;


  end;

procedure Register;

implementation

procedure  LocInterruptHandler( Owner : THANDLE; IrqNumber : Word); stdcall;
begin
  if (Owner<>0)and (Assigned (TVicHW32(Owner).fOnHwInterrupt) ) then
    TVicHW32(Owner).OnHwInterrupt(IrqNumber);
end;

procedure  LocHookHandler( Owner : THANDLE; scan_code : Byte ); stdcall;
begin
  if (Owner<>0)and (Assigned (TVicHW32(Owner).fOnKeystroke) ) then
    TVicHW32(Owner).OnKeystroke(scan_code);
end;


procedure  TVicHW32.fSetIRQMasked(IrqNumber : Word; bNewValue  : Boolean);
begin
  if (fOpenDrive) then
  begin
    if (bNewValue) then
    begin
      if (not fMasked[IrqNumber]) then
      begin
        fMasked[IrqNumber] := TRUE;
        TVicLib.MaskIRQ(fHW32,IrqNumber);
      end;
    end
    else begin
	   if fMasked[IrqNumber] then
           begin
             TVicLib.UnmaskDelphiIRQ(fHW32,THANDLE(Self),IrqNumber,@LocInterruptHandler);
             fMasked[IrqNumber] := FALSE;
           end;
         end;

  end;
end;

procedure  TVicHW32.UnmaskInterrupt(IrqNumber : Word);
begin
  fSetIRQMasked(IrqNumber, FALSE);
end;

procedure  TVicHW32.UnmaskInterruptEx(IrqNumber    : Word;
                                      IrqShared    : Word;
                                      ClearRec     : pIrqClearRec);
begin

  if (not fOpenDrive) then Exit;

  if fMasked[IrqNumber] then
  begin
    TVicLib.UnmaskDelphiIRQEx(fHW32,
                              THANDLE (Self),
                              IrqNumber,
                              IrqShared,
                              @LocInterruptHandler,
                              ClearRec);
    fMasked[IrqNumber] := FALSE;
  end;

end;


procedure  TVicHW32.MaskInterrupt(IrqNumber : Word);
begin
  fSetIRQMasked(IrqNumber, TRUE);
end;


procedure  TVicHW32.HookKeyboard;
begin
  if (fOpenDrive) then
  begin
    if not fHooked then
    begin
      TVicLib.HookDelphiKeyboard(fHW32,THANDLE(Self),@LocHookHandler);
      fHooked := TRUE;
    end;
  end;
end;

procedure  TVicHW32.UnhookKeyboard;
begin
  if (fOpenDrive) then
  begin
    if fHooked then
    begin
      TVicLib.UnhookKeyboard(fHW32);
      fHooked := FALSE;
    end;
  end;
end;


procedure TVicHW32.CloseDriver;
var i : Byte;
begin
  if fOpenDrive then CloseTVicHW32(fHW32);
  fOpenDrive  := FALSE;
  fHardAccess := TRUE;
  for i:= 0 to 15 do
  begin
    fMasked[i] := TRUE;
  end;
end;

function TVicHW32.OpenDriver(DriverEntryPoint : String) : Boolean;
var buf : array[0..255] of Char;
    i   : Byte;
begin
  if not fOpenDrive then
  begin
    fLPTReadMode := FALSE;
    fHardAccess := TRUE;
    for i:= 0 to 15 do
    begin
      fMasked[i] := TRUE;
    end;
    fHW32 := OpenTVicHW32(fHW32,'TVICHW32',StrPCopy(buf,DriverEntryPoint));
    fOpenDrive := (fHW32<>0) and GetActiveHW(fHW32);
  end;
  Result := fOpenDrive;
end;

function  TVicHW32.fGetPortB(PortAddr:DWORD):Byte;
begin
  if not fOpenDrive then begin Result:=$ff; Exit; end;
  Result := GetPortByte(fHW32,PortAddr);
end;

function  TVicHW32.fGetPortW(PortAddr : DWORD):Word;
begin
  if not fOpenDrive then begin Result:=$ffff; Exit; end;
  Result := GetPortWord(fHW32,PortAddr);
end;

function  TVicHW32.fGetPortL(PortAddr : DWORD):DWORD;
begin
  if not fOpenDrive then begin Result:=$ff; Exit; end;
  Result := GetPortLong(fHW32,PortAddr);
end;

procedure   TVicHW32.fSetPortB(PortAddr : DWORD; nNewValue: Byte);
begin
  if not fOpenDrive then Exit;
  SetPortByte(fHW32,PortAddr,nNewValue);
end;

procedure   TVicHW32.fSetPortW(PortAddr : DWORD; nNewValue: Word);
begin
  if not fOpenDrive then Exit;
  SetPortWord(fHW32,PortAddr,nNewValue);
end;

procedure   TVicHW32.fSetPortL(PortAddr : DWORD; nNewValue: DWORD);
begin
  if not fOpenDrive then Exit;
  SetPortLong(fHW32,PortAddr,nNewValue);
end;

procedure   TVicHW32.ReadPortFIFO ( PortAddr:DWORD; NumPorts:Word; var Buffer);
var  PortRec  : pPortByteFIFO;
begin

  if not fOpenDrive then Exit;

  GetMem(PortRec,8+NumPorts);
  PortRec^.PortAddr := PortAddr;
  PortRec^.NumPorts := NumPorts;

  TVicLib.ReadPortFIFO(fHW32,PortRec);

  Move(PortRec^.Buffer,Buffer,NumPorts);
  FreeMem(PortRec);

end;

procedure   TVicHW32.WritePortFIFO( PortAddr : DWORD; NumPorts:Word; var Buffer);
var PortRec  : pPortByteFifo;
begin
  if not ActiveHW then Exit;
  GetMem(PortRec,8+NumPorts);
  PortRec^.PortAddr:=PortAddr;
  PortRec^.NumPorts:=NumPorts;
  Move(Buffer,PortRec^.Buffer,NumPorts);
  TVicLib.WritePortFIFO(fHW32,PortRec);
  FreeMem(PortRec);
end;

procedure   TVicHW32.ReadPortWFIFO ( PortAddr : DWORD; NumPorts:Word; var Buffer);
var PortRec  :pPortWordFIFO;
begin
  if not fOpenDrive then Exit;
  GetMem(PortRec,8+2*NumPorts);
  PortRec^.PortAddr := PortAddr;
  PortRec^.NumPorts := NumPorts;

  TVicLib.ReadPortWFIFO(fHW32,PortRec);

  Move(PortRec^.Buffer,Buffer,2*NumPorts);
  FreeMem(PortRec);
end;

procedure   TVicHW32.WritePortWFIFO( PortAddr : DWORD; NumPorts:Word; var Buffer);
var PortRec  :pPortWordFIFO;
begin
  if not fOpenDrive then Exit;
  GetMem(PortRec,8+2*NumPorts);
  PortRec^.PortAddr := PortAddr;
  PortRec^.NumPorts := NumPorts;
  Move(Buffer,PortRec^.Buffer,2*NumPorts);

  TVicLib.WritePortWFIFO(fHW32,PortRec);

  FreeMem(PortRec);
end;

procedure   TVicHW32.ReadPortLFIFO ( PortAddr : DWORD; NumPorts:Word; var Buffer);
var PortRec  :pPortLongFIFO;
begin
  if not fOpenDrive then Exit;
  GetMem(PortRec,8+4*NumPorts);
  PortRec^.PortAddr := PortAddr;
  PortRec^.NumPorts := NumPorts;

  TVicLib.ReadPortLFIFO(fHW32,PortRec);

  Move(PortRec^.Buffer,Buffer,4*NumPorts);
  FreeMem(PortRec);
end;

procedure   TVicHW32.WritePortLFIFO( PortAddr : DWORD; NumPorts:Word; var Buffer);
var PortRec  :pPortLongFIFO;
begin
  if not fOpenDrive then Exit;
  GetMem(PortRec,8+4*NumPorts);
  PortRec^.PortAddr := PortAddr;
  PortRec^.NumPorts := NumPorts;
  Move(Buffer,PortRec^.Buffer,4*NumPorts);

  TVicLib.WritePortLFIFO(fHW32,PortRec);

  FreeMem(PortRec);

end;



function  TVicHW32.MapPhysToLinear(PhAddr:DWORD; PhSize:DWORD):Pointer;
begin
  Result:=TVicLib.MapPhysToLinear(fHW32,PhAddr,PhSize);
end;

procedure TVicHW32.UnmapMemory(PhAddr:DWORD; PhSize:DWORD);
begin
  TVicLib.UnmapMemory(fHW32,PhAddr,PhSize);
end;

procedure   TVicHW32.fSetHardAccess(Parm : Boolean);
begin
  fHardAccess:=PARM;
  TVicLib.SetHardAccess(fHW32,Parm);
end;

procedure TVicHW32.fSetLPTNumber(nNewValue  : Byte);
begin
  TVicLib.SetLPTNumber(fHW32,nNewValue);
end;

procedure TVicHW32.fLPTStrobe;
begin
  TVicLib.LPTStrobe(fHW32);
end;

function TVicHW32.fGetLPTAckwl : Boolean;
begin
  Result:= not TVicLib.GetPin(fHW32,10);
end;

function TVicHW32.fGetLPTBusy : Boolean;
begin
  Result:= TVicLib.GetPin(fHW32,11);
end;

function TVicHW32.fGetLPTPaperEnd : Boolean;
begin
  Result:= TVicLib.GetPin(fHW32,12);
end;

function TVicHW32.fGetLPTSlct: Boolean;
begin
  Result:=  not TVicLib.GetPin(fHW32,13);
end;

procedure TVicHW32.fLPTAutofd(Flag : Boolean);
begin
  TVicLib.SetPin(fHW32,14, not Flag);
end;

function TVicHW32.fGetLPTError : Boolean;
begin
  Result:=  not TVicLib.GetPin(fHW32,15);
end;

procedure TVicHW32.fLPTInit;
begin
  TVicLib.LptInit(fHW32);
end;

procedure TVicHW32.fLPTSlctIn;
begin
  TVicLib.LPTSlctIn(fHW32);
end;

procedure TVicHW32.fSetLPTIRQ(nNewValue  : Boolean);
begin
  TVicLib.ForceIrqLpt(fHW32,nNewValue);
end;

function TVicHW32.fGetPin(nPin : Byte) : Boolean;
begin
  Result := TVicLib.GetPin(fHW32,nPin);
end;

procedure TVicHW32.fSetPin(nPin : Byte; bNewValue : Boolean);
begin
  TVicLib.SetPin(fHW32,nPin,bNewValue);
end;

function TVicHW32.fGetMemB(MappedAddr : Pointer; Offset : DWORD) : Byte;
begin
  Result := TVicLib.GetMemByte(fHW32,DWORD(MappedAddr),Offset);
end;

procedure TVicHW32.fSetMemB(MappedAddr : Pointer; Offset : DWORD; nNewValue : Byte);
begin
  TVicLib.SetMemByte(fHW32,DWORD(MappedAddr),Offset, nNewValue);
end;

function TVicHW32.fGetMemW(MappedAddr : Pointer; Offset : DWORD) : Word;
begin
  Result := TVicLib.GetMemWord(fHW32,DWORD(MappedAddr),Offset);
end;

procedure TVicHW32.fSetMemW(MappedAddr : Pointer; Offset : DWORD; nNewValue : Word);
begin
  TVicLib.SetMemWord(fHW32,DWORD(MappedAddr),Offset, nNewValue);
end;

function TVicHW32.fGetMemL(MappedAddr : Pointer; Offset : DWORD) : DWORD;
begin
  Result := TVicLib.GetMemLong(fHW32,DWORD(MappedAddr),Offset);
end;

procedure TVicHW32.fSetMemL(MappedAddr : Pointer; Offset : DWORD; nNewValue : DWORD);
begin
  TVicLib.SetMemLong(fHW32,DWORD(MappedAddr),Offset, nNewValue);
end;

procedure TVicHW32.PutScanCode(b:Byte);
begin
  TVicLib.PutScanCode(fHW32,b);
end;


procedure TVicHW32.GetHDDInfo(IdeNumber:Word; Master:Word; var Info : THDDInfo);
begin
  TVicLib.GetHddInfo(fHW32,IdeNumber,Master,@Info);
end;


function    TVicHW32.GetIRQCounter(IrqNumber:Word):Longint;
begin
  Result := TVicLib.GetIrqCounter(fHW32,IrqNumber);
end;

function    TVicHW32.fLPTBasePort : DWORD;
begin
  Result := TVicLib.GetLPTBasePort(fHW32);
end;

function    TVicHW32.fLPTs : Byte;
begin
  Result := TVicLib.GetLPTNumPorts(fHW32);
end;

function    TVicHW32.fGetLPTNumber : Byte;
begin
  Result := TVicLib.GetLPTNumber(fHW32);
end;

function    TVicHW32.fGetIRQMasked(IrqNumber:Word) : Boolean;
begin
  Result := TVicLib.IsIrqMasked(fHW32,IrqNumber);
end;

function TVicHW32.LastPciBus : Byte;
begin
  Result := TVicLib.GetLastPciBus(fHW32);
end;

function TVicHW32.HardwareMechanism : Byte;
begin
  Result := TVicLib.GetHardwareMechanism(fHW32);
end;

function TVicHW32.PciDeviceInfo(bus,dev,func : Word; Info : pPciCfg) : Boolean;
begin
  Result := TVicLib.GetPciDeviceInfo(fHW32,bus,dev,func,Info);
end;

procedure TVicHW32.AllocateSysDmaBuffer(DmaBufferRequest : pDmaBufferRequest);
begin
  TVicLib.GetSysDmaBuffer(fHW32,DmaBufferRequest);
end;

procedure TVicHW32.AllocateBusmasterDmaBuffer(DmaBufferRequest : pDmaBufferRequest);
begin
  TVicLib.GetBusmasterDmaBuffer(fHW32,DmaBufferRequest);
end;

procedure TVicHW32.FreeDmaBuffer(DmaBufferRequest : pDmaBufferRequest);
begin
  TVicLib.FreeDmaBuffer(fHW32,DmaBufferRequest);
end;

function   TVicHW32.AcquireLPT : Word;
begin
  Result := TVicLib.AcquireLpt(fHW32,fGetLPTNumber);
end;
procedure   TVicHW32.ReleaseLPT;
begin
  TVicLib.ReleaseLpt(fHW32,fGetLPTNumber);
end;

function    TVicHW32.LPTAcquired : Word;
begin
  Result := TVicLib.IsLptAcquired(fHW32,fGetLPTNumber);
end;

procedure   TVicHW32.fSetLPTReadMode(readmode : Boolean);
begin
  if readmode then
    TVicLib.SetLPTReadMode(fHW32)
  else
    TVicLib.SetLPTWriteMode(fHW32);
  fLPTReadMode := readmode;
end;

function TVicHW32.RunRing0Function(R0proc : TRing0Function; pParm:Pointer):Longint;
begin
  Result := TVicLib.RunRing0Function(fHW32,R0proc,pParm);
end;

procedure Register;
begin
  RegisterComponents('Drivers', [TVicHw32]);
end;


end.
