program demo;

uses
  QForms,
  demo1 in 'demo1.pas' {Main},
  demo2 in 'demo2.pas' {PaintForm},
  ifpslib in '../libraries/ifpslib/ifpslib.pas',
  ifsdfrm in '../libraries/delphiforms/ifsdfrm.pas',
  ifsctrlstd in '../libraries/delphiforms/ifsctrlstd.pas',
  ifpstrans in '../libraries/translib/ifpstrans.pas',
  ifpsdll in '../libraries/call/ifpsdll.pas',
  ifpscall in '../libraries/call/ifpscall.pas',
  ifpsdelphi in '../libraries/call/ifpsdelphi.pas',
  ifpsdll2 in '../libraries/call/ifpsdll2.pas',
  ifpsdate in '../libraries/ifpslib/ifpsdate.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMain, Main);
  Application.CreateForm(TPaintForm, PaintForm);
  Application.Run;
end.

