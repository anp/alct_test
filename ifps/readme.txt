Innerfuse Pascal Script 2.81


Files:
License.txt          - License agreement.
History.txt          - Version history.
ifs_pas.pas          - Innerfuse Pascal Script engine.
ifs_var.pas          - Innerfuse Pascal Script variable management.
ifs_utl.pas          - Innerfuse Pascal Script parser.
ifs_obj.pas	     - Innerfuse Pascal Script external objects support.
Demo_Del             - Directory with a demo for Delphi.
Demo_Fpc             - Directory with a demo for FreePascal.
Libraries            - Directory with some usefull libraries.

Documentation can be found in a seperate archive on my website.


The syntax of Innerfuse Pascal Script is almost the same as Delphi,
except that pointers are not supported.

Carlo Kok (ck@carlo-kok.com)
http://carlo-kok.com
