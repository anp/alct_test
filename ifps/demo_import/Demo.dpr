program demo;

uses
  Forms,
  demo1 in 'demo1.pas' {Main},
  ifpslib in '..\libraries\ifpslib\ifpslib.pas',
  ifpstrans in '..\libraries\translib\ifpstrans.pas',
  ifpsdll in '..\libraries\call\ifpsdll.pas',
  ifpscall in '..\libraries\call\ifpscall.pas',
  ifpsdelphi in '..\libraries\call\ifpsdelphi.pas',
  ifpsdll2 in '..\libraries\call\ifpsdll2.pas',
  ifpsdate in '..\libraries\ifpslib\ifpsdate.pas',
  ifpsclass in '..\libraries\call\ifpsclass.pas',
  stdimport in '..\libraries\imports\stdimport.pas',
  stdctrlsimport in '..\libraries\imports\stdctrlsimport.pas',
  formsimport in '..\libraries\imports\formsimport.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMain, Main);
  Application.Run;
end.

