unit demo2;
interface

uses
 Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
ExtCtrls;
type
 TPaintForm = class(TForm)PaintBox1: TPaintBox;
  procedure FormCreate(Sender: TObject);
  procedure FormDestroy(Sender: TObject);
  procedure PaintBox1Paint(Sender: TObject);
  private
 { Private declarations }  public
 Bitmap: TBitmap;
  procedure DoUpdate;
end;
var
 PaintForm: TPaintForm;
implementation



{$R *.dfm}procedure TPaintForm.DoUpdate;
begin
 PaintBox1.Repaint;
end;

procedure TPaintForm.FormCreate(Sender: TObject);
begin
 Bitmap := TBitmap.Create;
end;

procedure TPaintForm.FormDestroy(Sender: TObject);
begin
 Bitmap.Free;
end;

procedure TPaintForm.PaintBox1Paint(Sender: TObject);
begin
 Canvas.Draw(0, 0, Bitmap);
end;
end.
