This directory contains some usefull (small) libraries:
ifpslib/ifpslib.pas
 Library containing TIfStringList
translib/ifpstrans.pas
 Library to transform types to other types
delphiforms/
 Directory containing a forms library.
com/
 Directory containing a not working external class, that will (in the future) once properly working serve com objects.
call/
 Library to call Dynamic Linked Libraries (dlls) or normal Delphi functions from within the script engine, also includes class calling library.  
3rdparty/
 Directory containing Third Party libraries.

