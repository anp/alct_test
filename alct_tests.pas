                                                         
unit alct_tests;

interface


uses
  Types, Math, Classes, Variants, QGraphics, QControls, QForms, QDialogs,
  QMenus, QTypes, QComCtrls, QExtCtrls, QStdCtrls, SysUtils, IniFiles,
  {$IFNDEF LINUX}
  Windows, ifspas, ifs_var, ifs_utl, ifs_obj, ifpstrans,
  {$ELSE}
  Qt, ifspas, ifs_var, ifs_utl, ifs_obj,
  {$ENDIF}
  JTAGLib,JTAGUtils, ALCT, FirmwareLoader, SlowControl, QButtons, QImgList, QGrids,
  QActnList, TeeComma, TeeProcs, TeEngine, Series, TeCanvas, Chart;


type
  TForm1 = class(TForm)
	sbBar: TStatusBar;
	MainMenu1: TMainMenu;
	File1: TMenuItem;
	New1: TMenuItem;
	Open1: TMenuItem;
	Save1: TMenuItem;
	SaveAs1: TMenuItem;
	N1: TMenuItem;
	Print1: TMenuItem;
	PrintSetup1: TMenuItem;
	N2: TMenuItem;
	Exit1: TMenuItem;
	Help1: TMenuItem;
	About1: TMenuItem;
    Load1: TMenuItem;
	SlowControlEprom1: TMenuItem;
    SlowControlFPGA1: TMenuItem;
	MezanineEPROMs1: TMenuItem;
    Virtex1: TMenuItem;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Log: TMemo;
    Splitter2: TSplitter;
    TestLog: TListView;
	TestPages: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Shape5: TShape;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Shape4: TShape;
	Chains: TComboBox;
    SetJTAGChain: TButton;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Button3: TButton;
	Button4: TButton;
	Edit1: TEdit;
	Edit2: TEdit;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    Label4: TLabel;
	Label5: TLabel;
	Label6: TLabel;
	Label7: TLabel;
    Button5: TButton;
    StrSize: TLabel;
    GroupBox3: TGroupBox;
    Driver: TRadioGroup;
    Label8: TLabel;
    Button6: TButton;
    LPTNumber: TSpinEdit;
    GroupBox4: TGroupBox;
	Button1: TButton;
    GroupBox14: TGroupBox;
    btDetectType: TButton;
    lbMezType: TLabel;
    TabSheet3: TTabSheet;
    DelayGroups: TGroupBox;
    DG0: TCheckBox;
    DG1: TCheckBox;
    DG2: TCheckBox;
    DG3: TCheckBox;
    DelayValues: TGroupBox;
	TrackBar1: TTrackBar;
	TrackBar2: TTrackBar;
	TrackBar3: TTrackBar;
    TrackBar4: TTrackBar;
    TrackBar5: TTrackBar;
    TrackBar6: TTrackBar;
    Label1: TLabel;
	Label9: TLabel;
    Label10: TLabel;
	Label11: TLabel;
    Label12: TLabel;
	Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Edit3: TEdit;
    Edit4: TEdit;
	Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    flagRdbk: TCheckBox;
    flagShowPat: TCheckBox;
    whichConnector: TCheckBox;
	Label20: TLabel;
	Label21: TLabel;
	Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    GroupBox5: TGroupBox;
	PrSt: TProgressBar;
    btPause: TSpeedButton;
    btStop: TSpeedButton;
    btOneStep: TSpeedButton;
    TestSets: TGroupBox;
    Button7: TButton;
	Button8: TButton;
	Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    StdCmnds: TGroupBox;
    GroupBox6: TGroupBox;
    chStopErr: TCheckBox;
    chRepeat: TCheckBox;
    chKeyPress: TCheckBox;
	TabSheet2: TTabSheet;
    btSCReadUserID: TButton;
    btTemperature: TButton;
	btReadAllVoltages: TButton;
    btReadAllCurrents: TButton;
	btReadAllThreshs: TButton;
	GroupBox7: TGroupBox;
    btSetThreshs: TButton;
    seAFEBn: TSpinEdit;
    seAFEBval: TSpinEdit;
    cbAllThreshs: TCheckBox;
    lbAFEBn: TLabel;
	lbAFEBval: TLabel;
    btReadThresh: TButton;
    GroupBox8: TGroupBox;
    cbTPPower: TCheckBox;
    btSetTPPower: TButton;
    GroupBox9: TGroupBox;
    btSetTPPowerAmp: TButton;
	Label26: TLabel;
    seTPPowerAmp: TSpinEdit;
	GroupBox10: TGroupBox;
    btSetTPWireGroup: TButton;
    Label27: TLabel;
    edTPWireGroupMask: TEdit;
    GroupBox11: TGroupBox;
    Label28: TLabel;
    edTPStripLayerMask: TEdit;
    btSetTPStripLayer: TButton;
    GroupBox12: TGroupBox;
    Label33: TLabel;
	edStndby1: TEdit;
	edStndby2: TEdit;
	edStndby3: TEdit;
	edStndby4: TEdit;
    btSetStandbyReg: TButton;
    edStndby5: TEdit;
    edStndby6: TEdit;
    edStndby7: TEdit;
    TabSheet4: TTabSheet;
	GroupBox13: TGroupBox;
    Label29: TLabel;
    seChanNum: TSpinEdit;
    Label30: TLabel;
    seNumWords: TSpinEdit;
    fFIFOall: TCheckBox;
    cbSuppressErrs: TCheckBox;
    seErrCnt: TSpinEdit;
    Label31: TLabel;
    seTestPassesFIFO: TSpinEdit;
	setupFIFOdata: TRadioGroup;
    btWriteFIFO: TButton;
	btReadFIFO: TButton;
    btCheckFIFO: TButton;
    Button18: TButton;
    btFIFOTest: TButton;
    ImageList1: TImageList;
    TabSheet5: TTabSheet;
    Info: TStringGrid;
	GroupBox15: TGroupBox;
	btSCLoadFPGA: TButton;
	btSCLoadEPROM: TButton;
	btSCEraseEPROM: TButton;
    btSCReadEPROMID: TButton;
    Label32: TLabel;
    Label34: TLabel;
	lbscfpga: TLabel;
    lbsceprom: TLabel;
	btSCReadFPGAID: TButton;
    btSCBlankCheckEPROM: TButton;
    GroupBox16: TGroupBox;
    btVLoadEPROM: TButton;
    btVLoadFPGA: TButton;
    btVEraseEPROM: TButton;
    btVBlankCheckEPROM: TButton;
    btVReadEPROMID: TButton;
    btVReadFPGAID: TButton;
    Label35: TLabel;
    lbvfpga: TLabel;
    Label37: TLabel;
    lbveprom1: TLabel;
	lbveprom2: TLabel;
    Label40: TLabel;
	btReadTPPower: TButton;
    btReadStandbyReg: TButton;
    btReadTPWireGroup: TButton;
    btReadTPStripLayer: TButton;
	Button30: TButton;
    PopupMenu1: TPopupMenu;
	Clear1: TMenuItem;
	ActionList1: TActionList;
    Clear: TAction;
    GroupBox17: TGroupBox;
	cbALCTType: TComboBox;
	cbChamberType: TComboBox;
    Label36: TLabel;
	Label38: TLabel;
    DG4: TCheckBox;
    DG5: TCheckBox;
    DG6: TCheckBox;
    btSetDelaysChips: TButton;
    ReadPtrns: TButton;
    CheckPtrns: TButton;
    TabSheet6: TTabSheet;
    VATChart: TChart;
    TeeCommander1: TTeeCommander;
    Timer1: TTimer;
    Panel2: TPanel;
    btTimer: TButton;
    seRI: TSpinEdit;
    Label39: TLabel;
    Timer2: TTimer;
    GroupBox18: TGroupBox;
	Button13: TButton;
    Label44: TLabel;
	Label45: TLabel;
	Label46: TLabel;
    seTCh: TSpinEdit;
    cbLoop: TCheckBox;
    cbStandby: TCheckBox;
	Button15: TButton;
    TabSheet7: TTabSheet;
	Memo1: TMemo;
    Memo2: TMemo;
    Splitter3: TSplitter;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ImageList2: TImageList;
    ToolButton4: TToolButton;
    btScriptLoad: TToolButton;
    btNewScript: TToolButton;
    btScriptSaveAs: TToolButton;
    btScriptSave: TToolButton;
    ToolButton7: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    tbTimeScale: TTrackBar;
    Label42: TLabel;
    TabSheet8: TTabSheet;
    cbConfigReg: TComboBox;
    sgConfigReg: TStringGrid;
	cbFlipDelay: TCheckBox;
    Button14: TButton;
    Button16: TButton;
    Button17: TButton;
    Button19: TButton;
	Button20: TButton;
    Button21: TButton;
    TabSheet9: TTabSheet;
    DACChart: TChart;
    Series1: TLineSeries;
    Series2: TLineSeries;
    Panel3: TPanel;
    Button22: TButton;
    Timer3: TTimer;
    seChartCh1: TSpinEdit;
    seChartCh2: TSpinEdit;
    Label41: TLabel;
    Label47: TLabel;
    Timer4: TTimer;
    GroupBox19: TGroupBox;
    SpinEdit6: TSpinEdit;
    cbWriteLoop: TCheckBox;
    cbReadLoop: TCheckBox;
    cbCheckPtrnsLoop: TCheckBox;
    btDelaysLoop: TButton;
    Timer5: TTimer;
    GroupBox20: TGroupBox;
    GroupBox21: TGroupBox;
    Label43: TLabel;
    SpinEdit4: TSpinEdit;
    Label48: TLabel;
    cbDlyLoop: TCheckBox;
    cbDlyOneStep: TCheckBox;
    Button23: TButton;
    lbDelay: TLabel;
    lbPrSt: TLabel;
    cbAllThreshChans: TCheckBox;
    TabSheet10: TTabSheet;
    CurrChart: TChart;
    Panel4: TPanel;
    Timer6: TTimer;
    Button2: TButton;
    cbTestPulseOnEffect: TCheckBox;
    cbStripMaskEffect: TCheckBox;
    cbGroupMaskEffect: TCheckBox;
    cbSingleAFEB: TCheckBox;
    cbALCTCurrOffset: TCheckBox;
    Config: TStringGrid;
    cbSoftMapping: TCheckBox;
    TabSheet11: TTabSheet;
    GroupBox22: TGroupBox;
    seSingleCableChan: TSpinEdit;
    Label49: TLabel;
    Label50: TLabel;
    seSingleTestPasses: TSpinEdit;
    Button24: TButton;
    lstSingleTests: TListBox;
    cbSingleAllTests: TCheckBox;
    edCustomData: TEdit;
    Label51: TLabel;
    cbTestLoop: TCheckBox;
    Timer7: TTimer;
    cbSuppressErrsSingleCable: TCheckBox;
    seErrCntSingleTest: TSpinEdit;
    cbStopOnErrorSingleCable: TCheckBox;
    Label52: TLabel;
    Label53: TLabel;
    lblSendData: TLabel;
    lblReadData: TLabel;
    cbPinMapOnRead: TCheckBox;
    TabSheet12: TTabSheet;
    Label54: TLabel;
    edBoardNumber: TEdit;
    GroupBox23: TGroupBox;
    Label55: TLabel;
    edFIFOValue: TEdit;
    Label56: TLabel;
    seFIFONumWords: TSpinEdit;
    Label57: TLabel;
    seFIFOCh: TSpinEdit;
    cbRWFIFOMode: TCheckBox;
    cbShowFIFOValue: TCheckBox;
    Button25: TButton;
    Button26: TButton;
    Label58: TLabel;
    seStartDelay: TSpinEdit;
    seALCTDelay: TSpinEdit;
    seTestBoardDelay: TSpinEdit;
    Label59: TLabel;
    Label60: TLabel;
    Button27: TButton;
    Button28: TButton;
    Button29: TButton;
    Button31: TButton;
    Button32: TButton;
    Button33: TButton;
    Button34: TButton;
    cbShowFIFOCounter: TCheckBox;
    Splitter4: TSplitter;
    DelaysPanel: TPanel;
    DelaysChart: TChart;
    Button35: TButton;
    GroupBox24: TGroupBox;
    lstIDs: TListBox;
    Button36: TButton;
    Button37: TButton;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    lbslowctl: TLabel;
    lbfastctl: TLabel;
    lbboardsn: TLabel;
    Label64: TLabel;
    lbmezansn: TLabel;
    Button38: TButton;
    Button39: TButton;
    cbWriteToFile: TCheckBox;
	procedure Exit1Click(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure Button6Click(Sender: TObject);
	procedure SetChainClick(Sender: TObject);
	procedure Button5Click(Sender: TObject);
	procedure Button3Click(Sender: TObject);
	procedure Button4Click(Sender: TObject);
	procedure Edit1KeyPress(Sender: TObject; var Key: Char);
	procedure Edit2KeyPress(Sender: TObject; var Key: Char);
	procedure SpinEdit2Changed(Sender: TObject; NewValue: Integer);
	procedure SpinEdit3Changed(Sender: TObject; NewValue: Integer);
	procedure Button1Click(Sender: TObject);
	procedure ReadPtrnsClick(Sender: TObject);
	procedure btSetDelaysChipsClick(Sender: TObject);
	procedure LockDelaysClick(Sender: TObject);
	procedure TrackBar1Change(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure flagRdbkClick(Sender: TObject);
	procedure flagShowPatClick(Sender: TObject);
	procedure Edit2Change(Sender: TObject);
	{$IFNDEF LINUX}
	procedure GetAppVersion;
    {$ENDIF}
	procedure whichConnectorClick(Sender: TObject);
	procedure CheckPtrnsClick(Sender: TObject);
	procedure SetDelayLines(cs: integer; delays: ALCTDelays);
	function ReadPatterns(out patterns: ALCTDelays): Longint;
	function CheckPatterns(RefPtrns: ALCTDelays; ReadPtrns: ALCTDelays): Longint;
	procedure Button7Click(Sender: TObject);
	procedure Button8Click(Sender: TObject);
	procedure Button9Click(Sender: TObject);
	procedure Button10Click(Sender: TObject);
	procedure Button11Click(Sender: TObject);
	procedure Button12Click(Sender: TObject);
	procedure bThreadedClick(Sender: TObject);
	procedure btPauseClick(Sender: TObject);
	procedure btStopClick(Sender: TObject);
	procedure btOneStepClick(Sender: TObject);
    procedure chStopErrClick(Sender: TObject);
    procedure chRepeatClick(Sender: TObject);
    procedure chKeyPressClick(Sender: TObject);
    procedure btSCReadUserIDClick(Sender: TObject);
    procedure btTemperatureClick(Sender: TObject);
	procedure btReadAllVoltagesClick(Sender: TObject);
    procedure btReadAllCurrentsClick(Sender: TObject);
    procedure btReadAllThreshsClick(Sender: TObject);
    procedure cbAllThreshsClick(Sender: TObject);
	procedure btSetThreshsClick(Sender: TObject);
	procedure btReadThreshClick(Sender: TObject);
	procedure btSetTPPowerClick(Sender: TObject);
	procedure btSetTPPowerAmpClick(Sender: TObject);
	procedure btSetTPWireGroupClick(Sender: TObject);
	procedure btSetTPStripLayerClick(Sender: TObject);
	procedure SlowControlEprom1Click(Sender: TObject);
	procedure btSetStandbyRegClick(Sender: TObject);
	procedure btWriteFIFOClick(Sender: TObject);
	procedure btReadFIFOClick(Sender: TObject);
	procedure btCheckFIFOClick(Sender: TObject);
	procedure Button18Click(Sender: TObject);
	procedure fFIFOallClick(Sender: TObject);
	procedure cbSuppressErrsClick(Sender: TObject);
	procedure btFIFOTestClick(Sender: TObject);
	procedure btDetectTypeClick(Sender: TObject);
	procedure ShowIDCode(idstr: string; alct_ctl: integer);
  procedure	ShowBoardSN(snstr: string; board_type: integer);
	procedure btSCLoadFPGAClick(Sender: TObject);
	procedure btSCLoadEPROMClick(Sender: TObject);
	procedure btSCReadEPROMIDClick(Sender: TObject);
	procedure btSCEraseEPROMClick(Sender: TObject);
	procedure btSCReadFPGAIDClick(Sender: TObject);
	procedure btSCBlankCheckEPROMClick(Sender: TObject);
	procedure btVReadFPGAIDClick(Sender: TObject);
	procedure btVReadEPROMIDClick(Sender: TObject);
    procedure btReadTPPowerClick(Sender: TObject);
    procedure btReadTPWireGroupClick(Sender: TObject);
    procedure btReadTPStripLayerClick(Sender: TObject);
	procedure btReadStandbyRegClick(Sender: TObject);
    procedure Button30Click(Sender: TObject);
    procedure ClearExecute(Sender: TObject);
    procedure cbALCTTypeChange(Sender: TObject);
    procedure cbChamberTypeChange(Sender: TObject);
    procedure btTimerClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure seRIClick(Sender: TObject);
    procedure Button13Click(Sender: TObject);
	procedure Timer2Timer(Sender: TObject);
    procedure seTChChanged(Sender: TObject; NewValue: Integer);
    procedure Button14Click(Sender: TObject);
	procedure Button15Click(Sender: TObject);
	procedure ToolButton1Click(Sender: TObject);
	procedure FormDestroy(Sender: TObject);
	procedure btNewScriptClick(Sender: TObject);
	procedure btScriptLoadClick(Sender: TObject);
	procedure btScriptSaveAsClick(Sender: TObject);
	procedure btScriptSaveClick(Sender: TObject);
	procedure Memo1Change(Sender: TObject);
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	procedure ToolButton3Click(Sender: TObject);
	procedure ToolButton6Click(Sender: TObject);
	procedure ToolButton5Click(Sender: TObject);
	procedure tbTimeScaleChange(Sender: TObject);
	procedure btVEraseEPROMClick(Sender: TObject);
	procedure btVBlankCheckEPROMClick(Sender: TObject);
	procedure btVLoadEPROMClick(Sender: TObject);
	procedure btVLoadFPGAClick(Sender: TObject);
	procedure Button19Click(Sender: TObject);
    procedure Button20Click(Sender: TObject);
    procedure Button21Click(Sender: TObject);
    procedure Button22Click(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure TestPagesPageChanging(Sender: TObject; NewPage: TTabSheet;
      var AllowChange: Boolean);
    procedure Button23Click(Sender: TObject);
    procedure Timer4Timer(Sender: TObject);
    procedure edTPWireGroupMaskKeyPress(Sender: TObject; var Key: Char);
    procedure SpinEdit6Changed(Sender: TObject; NewValue: Integer);
    procedure SpinEdit4Changed(Sender: TObject; NewValue: Integer);
    procedure btDelaysLoopClick(Sender: TObject);
    procedure Timer5Timer(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Timer6Timer(Sender: TObject);
    procedure cbSingleAllTestsClick(Sender: TObject);
    procedure Timer7Timer(Sender: TObject);
    procedure Button24Click(Sender: TObject);
    procedure Button28Click(Sender: TObject);
    procedure Button27Click(Sender: TObject);
    procedure Button29Click(Sender: TObject);
    procedure Button31Click(Sender: TObject);
    procedure Button32Click(Sender: TObject);
    procedure Button34Click(Sender: TObject);
    procedure Button33Click(Sender: TObject);
    procedure Button25Click(Sender: TObject);
    procedure Button26Click(Sender: TObject);
    procedure Button35Click(Sender: TObject);
    procedure Button36Click(Sender: TObject);
    procedure Button38Click(Sender: TObject);
    procedure Button39Click(Sender: TObject);
//	function  DoOper(const opertype: integer; filename: TFileName;title: string; fclose: boolean; fcancel :boolean): integer;
  protected
//    function WidgetFlags: Integer; override;
  private
	{ Private declarations }
	ThreadsRunning: Integer;
	procedure ThreadDone(Sender: TObject);
  procedure ChangeALCTType;
  public
	{ Public declarations }
	ps: TIfPasScript;
	fn: string;
	changed: Boolean;
	procedure AddLine(s: string);
	function SaveTest: Boolean;
  end;

TTestThread = class(TThread)
	private
		FTestForm:		TForm1;
		FNumOfPasses:	Integer;
		FTestType: 		Integer;
//		procedure DoVisualSwap;
	protected
		procedure Execute; override;
//		procedure VisualSwap(status: string; log: string);
		procedure Test;
	public
		constructor Create(Form: TForm1; TestType: Integer; NumPasses: Integer);
end;

type
	TestStatus = (testOK, testFail, testStop, testStart);

type
	TIStatus = (iStopped, iRunning, iStepOver, iStepOverWaiting);

type TDelay = (tMicroSec, tMSec, tSec);

const
	VRTX_CH = 3;
	passDefault = 50;

const TestNames:  array[0..5] of string =  ('Running ''1'' Test','Running ''0'' Test', 'Filling by ''1''s Test', 'Filling by ''0''s Test', 'Shifting ''5'' and ''A'' Test', 'Random Data Test');

var
  IniFile : TMemIniFile;
  fCalibrated : Boolean = false;
  delayCalibrated : longint;
  Form1: TForm1;
  arDelays : ALCTDelays;
  ReadDelays : ALCTDelays;
  fLockDelays : Boolean;
  fReadback   : Boolean;
  fShowPattern: Boolean;
  fOddConnector: Boolean;
  TestThread: TTestThread;
  ThreadHandle: THandle;
  fStopOnError: Boolean;
  fRepeatLast : Boolean;
  fKeyPress   : Boolean;
  fAllThreshs : Boolean;
  fFIFOOneWord: Boolean;
  dataFIFOwrite: array of array of WORD;
  dataFIFOread: array of array of WORD;
  MezChipType: Integer;
  ALCTBoard		: TALCTType;
  Chamber		: TChamberType;
  Wires : Integer;
  PassCnt 		: Integer;
  PassCnt2    : Integer;
  iStatus: TIStatus;
  LastLine: Longint;
  CurrOffset55_1: single;
  CurrOffset55_2: single;
//  parlen: Integer;

procedure Delay(const value: longint; dtype: TDelay); overload;
procedure CalibrateDelay;
procedure Delay(const value: longint); overload;

implementation

uses
  {$IFDEF WIN32}
//	  stdimport, formsimport, stdctrlsimport, ifpsclass, ifpsdll, ifpsdelphi, ifpsdll2, ifpslib, ifpstrans,
//	ifpsdate,
  {$ENDIF}
	ifpsdate, DlgNumPasses, OperProgress, WriteToFileUnit;


{$R *.xfm}

procedure GetConfigFromIniFile(filename: string);
const
  INISectionName = 'Limits';
var
  ALCTIniFile: TIniFile;
  ConfigParams : TStringList;
begin
  ALCTIniFile := TIniFile.Create(filename);
  if ALCTIniFile.SectionExists(INISectionName) then
  begin
    ConfigParams := TStringList.Create;
    ALCTIniFile.ReadSectionValues(INISectionName, ConfigParams);
    Form1.Log.Lines.AddStrings(ConfigParams);
    ConfigParams.Free;

    if ALCTIniFile.ValueExists(INISectionName, 'ThreshToler') then
      ThreshToler := ALCTIniFile.ReadInteger(INISectionName,'ThreshToler', ThreshToler);

    if ALCTIniFile.ValueExists(INISectionName, 'Volt18Ref') then
      arVoltages[0].RefVal := ALCTIniFile.ReadFloat(INISectionName,'Volt18Ref', arVoltages[0].RefVal);
    if ALCTIniFile.ValueExists(INISectionName, 'Volt18Toler') then
      arVoltages[0].Toler := ALCTIniFile.ReadFloat(INISectionName,'Volt18Toler', arVoltages[0].Toler);

    if ALCTIniFile.ValueExists(INISectionName, 'Volt33Ref') then
      arVoltages[1].RefVal := ALCTIniFile.ReadFloat(INISectionName,'Volt33Ref', arVoltages[1].RefVal);
    if ALCTIniFile.ValueExists(INISectionName, 'Volt33Toler') then
      arVoltages[1].Toler := ALCTIniFile.ReadFloat(INISectionName,'Volt33Toler', arVoltages[1].Toler);

    if ALCTIniFile.ValueExists(INISectionName, 'Volt551Ref') then
      arVoltages[2].RefVal := ALCTIniFile.ReadFloat(INISectionName,'Volt551Ref', arVoltages[2].RefVal);
    if ALCTIniFile.ValueExists(INISectionName, 'Volt551Toler') then
      arVoltages[2].Toler := ALCTIniFile.ReadFloat(INISectionName,'Volt551Toler', arVoltages[2].Toler);

    if ALCTIniFile.ValueExists(INISectionName, 'Volt552Ref') then
      arVoltages[3].RefVal := ALCTIniFile.ReadFloat(INISectionName,'Volt552Ref', arVoltages[3].RefVal);
    if ALCTIniFile.ValueExists(INISectionName, 'Volt552Toler') then
      arVoltages[3].Toler := ALCTIniFile.ReadFloat(INISectionName,'Volt552Toler', arVoltages[3].Toler);

    if ALCTIniFile.ValueExists(INISectionName, 'Curr18Ref') then
      arCurrents[0].RefVal := ALCTIniFile.ReadFloat(INISectionName,'Curr18Ref', arCurrents[0].RefVal);
    if ALCTIniFile.ValueExists(INISectionName, 'Curr18Toler') then
      arCurrents[0].Toler := ALCTIniFile.ReadFloat(INISectionName,'Curr18Toler', arCurrents[0].Toler);

    if ALCTIniFile.ValueExists(INISectionName, 'Curr33Ref') then
      arCurrents[1].RefVal := ALCTIniFile.ReadFloat(INISectionName,'Curr33Ref', arCurrents[1].RefVal);
    if ALCTIniFile.ValueExists(INISectionName, 'Curr33Toler') then
      arCurrents[1].Toler := ALCTIniFile.ReadFloat(INISectionName,'Curr33Toler', arCurrents[1].Toler);

    if ALCTIniFile.ValueExists(INISectionName, 'Curr551Ref') then
      arCurrents[2].RefVal := ALCTIniFile.ReadFloat(INISectionName,'Curr551Ref', arCurrents[2].RefVal);
    if ALCTIniFile.ValueExists(INISectionName, 'Curr551Toler') then
      arCurrents[2].Toler := ALCTIniFile.ReadFloat(INISectionName,'Curr551Toler', arCurrents[2].Toler);

    if ALCTIniFile.ValueExists(INISectionName, 'Curr552Ref') then
      arCurrents[3].RefVal := ALCTIniFile.ReadFloat(INISectionName,'Curr552Ref', arCurrents[3].RefVal);
    if ALCTIniFile.ValueExists(INISectionName, 'Curr552Toler') then
      arCurrents[3].Toler := ALCTIniFile.ReadFloat(INISectionName,'Curr552Toler', arCurrents[3].Toler);


    if ALCTIniFile.ValueExists(INISectionName, 'TemperRef') then
      arTemperature.RefVal := ALCTIniFile.ReadFloat(INISectionName,'TemperRef', arTemperature.RefVal);
    if ALCTIniFile.ValueExists(INISectionName, 'TemperToler') then
      arTemperature.Toler := ALCTIniFile.ReadFloat(INISectionName,'TemperToler', arTemperature.Toler);
  end;
  ALCTIniFile.WriteString(INISectionName, 'ThreshToler', IntToStr(ThreshToler));
  ALCTIniFile.WriteString(INISectionName, 'Volt18Ref', FloatToStrF(arVoltages[0].RefVal,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Volt18Toler', FloatToStrF(arVoltages[0].Toler,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Volt33Ref', FloatToStrF(arVoltages[1].RefVal,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Volt33Toler', FloatToStrF(arVoltages[1].Toler,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Volt551Ref', FloatToStrF(arVoltages[2].RefVal,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Volt551Toler', FloatToStrF(arVoltages[2].Toler,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Volt552Ref', FloatToStrF(arVoltages[3].RefVal,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Volt552Toler', FloatToStrF(arVoltages[3].Toler,ffFixed,5,3));

  ALCTIniFile.WriteString(INISectionName, 'Curr18Ref', FloatToStrF(arCurrents[0].RefVal,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Curr18Toler', FloatToStrF(arCurrents[0].Toler,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Curr33Ref', FloatToStrF(arCurrents[1].RefVal,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Curr33Toler', FloatToStrF(arCurrents[1].Toler,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Curr551Ref', FloatToStrF(arCurrents[2].RefVal,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Curr551Toler', FloatToStrF(arCurrents[2].Toler,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Curr552Ref', FloatToStrF(arCurrents[3].RefVal,ffFixed,5,3));
  ALCTIniFile.WriteString(INISectionName, 'Curr552Toler', FloatToStrF(arCurrents[3].Toler,ffFixed,5,3));

  ALCTIniFile.WriteString(INISectionName, 'TemperRef', FloatToStrF(arTemperature.RefVal,ffFixed,5,2));
  ALCTIniFile.WriteString(INISectionName, 'TemperToler', FloatToStrF(arTemperature.Toler,ffFixed,5,2));
  ALCTIniFile.Free;

end;

procedure TForm1.ChangeALCTType;
var
  i,j : Integer;
begin
   NUM_AFEB := ALCTBoard.chips * ALCTBoard.groups;
   Wires := ALCTBoard.channels;
 	 lbAFEBn.Caption := 'AFEB # (0..'+IntToStr(NUM_AFEB-1)+')';
	 seAFEBn.Max := NUM_AFEB-1;
   seTCH.Max := NUM_AFEB-1;
   seChanNum.Max := NUM_AFEB-1;
   seChartCh1.Max := NUM_AFEB-1;
   seChartCh2.Max := NUM_AFEB-1;
   seFIFOCh.Max := NUM_AFEB-1;
   seSingleCableChan.Max := NUM_AFEB-1;
	 SetLength(dataFIFOwrite, NUM_AFEB);
	 SetLength(dataFIFOread, NUM_AFEB);
   parlen := ALCTBoard.groups + 2;

   for i:= 0 to ALCTBoard.groups - 1 do
   begin
    for j:=0 to ALCTBoard.chips -1 do
    begin
      arDelays[i][j].Value := 0;
      arDelays[i][j].Pattern := 0;
    end;
   end;
   for i:=0 to MAX_DELAY_GROUPS - 1 do
   begin
       (FindComponent('DG'+IntToStr(i)) as TCheckBox).Enabled := false;
       (FindComponent('edStndby'+IntToStr(i+1)) as TEdit).Enabled := false;
       (FindComponent('edStndby'+IntToStr(i+1)) as TEdit).Text := '00';
   end;
   for i:=0 to ALCTBoard.groups - 1 do
   begin
       (FindComponent('DG'+IntToStr(i)) as TCheckBox).Enabled := true;
       (FindComponent('edStndby'+IntToStr(i+1)) as TEdit).Enabled := true;
   end;
   CurrChart.BottomAxis.Minimum := 0;
   CurrChart.BottomAxis.Maximum := NUM_AFEB;
end;

procedure TouchFIFO(ch: integer);
const
	len = 16;
begin
	if ActiveHW then
	begin

	if Form1.Chains.ItemIndex <> 3 then
	begin
		Form1.Chains.ItemIndex := 3;
		SetChain(3);
	end;

	// Write to FIFO
	WriteIR('1a', V_IR);
	WriteDR(IntToHex((1 and $1FFF) or (( ch and $3F) shl 13), 5), 19);
	WriteIR('18', V_IR);
	WriteDR($5555, len);
	WriteIR('18', V_IR);
	WriteDR($AAAA, len);

	// Read from FIFO
	WriteIR('1a', V_IR);
	WriteDR(IntToHex((1 and $1FFF) or (( ch and $3F) shl 13), 5), 19);
	WriteIR('19', V_IR);
	ReadDR('0', len);
	WriteIR('19', V_IR);
	ReadDR('0', len);
	end;
end;


procedure CalibrateDelay;
var
	Hour, Min, Sec, MSec: Word;
	i:	longint;
	j: double;
	t1, t2: TDateTime;
begin
	fCalibrated := true;
	t1:= Time;
	for i:=0 to 100000000 do;
	t2:= Time;
	DecodeTime(t2-t1, hour, min, sec, msec);
	j := 100000000 / (1000*sec+msec);
	delayCalibrated := Trunc(j);
end;

procedure Delay(const value: longint; dtype: TDelay); overload;
var
	i,j,k: longint;
begin
	if not fCalibrated then
		CalibrateDelay;
	case TDelay(dtype) of
		tMicroSec: j:= DelayCalibrated div 1000;
		tMSec	: j:= DelayCalibrated;
		tSec	 : j:= DelayCalibrated*1000;
  else
    j:= DelayCalibrated;
	end;
	for i:=1 to value do
		for k:=1 to j do;
end;

procedure Delay(const value: longint); overload;
begin
	Delay(value, tMSec);
end;

function RegProc(Sender: TIfPasScript; ScriptID: Pointer; proc: PProcedure; Params: PVariableManager; res: PIfVariant): TIfPasScriptError;
var
	value : string;
begin
	if proc^.Name = 'SETCHAIN' then
	begin
		set_chain(arJTAGChains[GetInteger(Vm_Get(Params,0))]);
		Form1.AddLine('Set '+Form1.Chains.Items.Strings[GetInteger(Vm_Get(Params,0))]);
	end else if proc^.Name = 'WRITEIR' then
	begin
		Form1.AddLine('IR->: ' + IntToStr(GetInteger(Vm_Get(Params,1))) + ' bits; Value 0x' + UpperCase(GetString(Vm_Get(Params,0))));
		value := WriteIR(GetString(Vm_Get(Params,0)),GetInteger(Vm_Get(Params,1)));
		Form1.AddLine('IR<-: ' + IntToStr(GetInteger(Vm_Get(Params,1))) + ' bits; Value 0x' + UpperCase(value));
		SetString(res, value);
	end else if proc^.Name = 'WRITEDR' then
	begin
		Form1.AddLine('DR->: ' + IntToStr(GetInteger(Vm_Get(Params,1))) + ' bits; Value 0x' + UpperCase(GetString(Vm_Get(Params,0))));
		value := WriteDR(GetString(Vm_Get(Params,0)),GetInteger(Vm_Get(Params,1)));
		Form1.AddLine('DR<-: ' + IntToStr(GetInteger(Vm_Get(Params,1))) + ' bits; Value 0x' + UpperCase(value));
		SetString(res, value);
	end else if proc^.Name = 'READDR' then
	begin
		Form1.AddLine('DR->: ' + IntToStr(GetInteger(Vm_Get(Params,1))) + ' bits; Value 0x' + UpperCase(GetString(Vm_Get(Params,0))));
		value := ReadDR(GetString(Vm_Get(Params,0)),GetInteger(Vm_Get(Params,1)));
		Form1.AddLine('DR<-: ' + IntToStr(GetInteger(Vm_Get(Params,1))) + ' bits; Value 0x' + UpperCase(value));
		SetString(res, value);
	end else if proc^.Name = 'WRITELN' then begin
		Form1.AddLine(GetString(Vm_Get(Params, 0)));
	end else if proc^.Name = 'LOG' then begin
		Form1.AddLine('Log->: ' + GetString(Vm_Get(Params, 0)));
		Form1.Log.Lines.Add(GetString(Vm_Get(Params, 0)));
	end else if proc^.Name = 'DELAY' then begin
		Delay(GetInteger(Vm_Get(Params,0)), tMSec);
  end else if proc^.Name = 'INTTOHEX' then begin
    value := IntToHex(GetInteger(Vm_Get(Params,0)), GetInteger(Vm_Get(Params,1)));
    SetString(res, value);
  end else if proc^.Name = 'FLIPBYTE' then begin
    value := IntToHex(FlipByte(GetInteger(Vm_Get(Params,0))), 2);
    SetString(res, value);
  end else if proc^.Name = 'FLIPHALFBYTE' then begin
    value := IntToHex(FlipHalfByte(GetInteger(Vm_Get(Params,0))), 1);
    SetString(res, value);
  end else if proc^.Name = 'FLIPWORD' then begin
    value := IntToHex(FlipWord(GetInteger(Vm_Get(Params,0))), 4);
    SetString(res, value);
	end;

{
  if proc^.Name = 'WRITELN' then begin
	Main.AddLine(GetString(Vm_Get(Params, 0)));
  end else if proc^.Name = 'READLN' then begin
	GetVarLink(Vm_Get(Params, 0))^.Cv_Str := InputBox('Demo', 'Readln:', '');
  end else if proc^.Name = 'RANDOM' then begin
	SetInteger(res, random(GetInteger(Vm_Get(Params, 0))));
  end;
}
  Result := ENoError;
end;

function OnUses(id: Pointer; Sender: TIfPasScript; Name: string): TCs2Error;
begin
  if Name = 'SYSTEM' then begin
	RegisterStdLib(Sender, False);
    RegisterTransLibrary(Sender);
{	RegisterTIfStringList(Sender);
	RegisterTransLibrary(Sender);
	RegisterEClasses(Sender, Cl);
	AddClassToScriptEngine(Sender, Main.Memo1, 'TMemo', 'Memo1');
	AddClassToScriptEngine(Sender, Main.Memo2, 'TMemo', 'Memo2');
	AddClassToScriptEngine(Sender, Main, 'TForm', 'Self');
	AddClassToScriptEngine(Sender, Application, 'TApplication', 'Application');

	RegisterDllCallLibrary(Sender);
	RegisterExceptionLib(Sender);
	RegisterDll2library(Sender);
}
	RegisterDateTimeLib(Sender);


	Sender.AddFunction(@RegProc, 'procedure SetChain(ch: integer)', nil);
	Sender.AddFunction(@RegProc, 'function WriteIR(IR: string; IRSize: Integer): string', nil);
	Sender.AddFunction(@RegProc, 'function WriteDR(DR: string; DRSize: Integer): string', nil);
	Sender.AddFunction(@RegProc, 'function ReadDR(DR: string; DRSize: Integer): string', nil);
	Sender.AddFunction(@RegProc, 'procedure Writeln(s: string)', nil);
	Sender.AddFunction(@RegProc, 'procedure Log(s: string)', nil);
	Sender.AddFunction(@RegProc, 'procedure Delay(delay: longint)', nil);
  Sender.AddFunction(@RegProc, 'function IntToHex(value: longint; length: longint): string', nil);
  Sender.AddFunction(@RegProc, 'function FlipByte(value: byte): string', nil);
  Sender.AddFunction(@RegProc, 'function FlipHalfByte(value: byte): string', nil);
  Sender.AddFunction(@RegProc, 'function FlipWord(value: word): string', nil);



{	Sender.AddFunction(@RegProc, 'procedure Readln(var s: string)', nil);
	Sender.AddFunction(@RegProc, 'function Random(I: Longint): Longint', nil);
 }
	Result := ENoError;
  end else Result := EUnknownIdentifier;
end;

function GetLine(const Text: string; Pos: Longint): Longint;
var
  I: Integer;
begin
  Result := 1;
  for I := 1 to Pos do
	if Text[I] = #10 then Inc(Result); // should work under linux too
end;


function OnRunLine(id: Pointer; Sender: TIfPasScript; Position: Longint): TCs2Error;
var
  I: Longint;
begin
  Application.ProcessMessages;
  Result := ENoError;

  case iStatus of
	iStopped: Result := EExitCommand;
	iStepOver: begin
		I := GetLine(Form1.Memo1.Text, Position);
		if I <> LastLine then begin
		  iStatus := iStepOverWaiting;
		  LastLine := I;
		  Form1.Memo1.SelStart := Position;
		  Form1.Memo1.SelLength := Length( Form1.Memo1.Lines[Form1.Memo1.CaretPos.Line])-1;
//		  Form1.sbBar.SimpleText := 'Line: ' + IntToStr(Form1.Memo1.CaretPos.Line);
		  while iStatus = iStepOverWaiting do
			Application.ProcessMessages;
		  Result := Sender.ErrorCode;
		end;
	  end;
  end;
end;


procedure TForm1.AddLine(s: string);
begin
  Memo2.Lines.Add(s);
end;

constructor TTestThread.Create(Form: TForm1; TestType: Integer; NumPasses: Integer);
begin
	FTestForm 	:= Form;
	FNumOfPasses:= NumPasses;
	FTestType 	:= TestType;
	FreeOnTerminate := True;
	inherited Create(False);
end;


procedure TTestThread.Test;
var
	old:	byte;
	i,j,p,gr,ch, pass:	Integer;
	Errs, ErrOnePass : Integer;
	SendPtrns, ReadPtrns: ALCTDelays;
	fStop:	Boolean;
	status, readstr, sentstr: string;
begin
	if ActiveHW then
	begin
		case FTestType of
		  0..3:	pass := Wires;
		  4:   	pass := FNumOfPasses*2;
		  5:      pass := FNumOfPasses;
		else
			pass := FNumOFPasses;
		end;
		Errs := 0;
//		ErrOnePass := 0;
		FTestForm.PrSt.Max := pass;
		FTestForm.PrSt.Position := 0;
		fStop := false;
		FTestForm.Log.Lines.Add(TimeToStr(Now())+ '> === ' + TestNames[FTestType] + '===');
    FTestForm.sbBar.SimpleText := TestNames[FTestType];

		old := FTestForm.Chains.ItemIndex;
		if old <> VRTX_CH then
		begin
			FTestForm.Chains.ItemIndex := VRTX_CH;
			FTestForm.SetChainClick(self);
		end;

		for i:= 0 to ALCTBoard.groups - 1 do
		begin
			for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
			begin
				SendPtrns[i][j].Value 		:= 0;
				if FTestType = 3 then
					SendPtrns[i][j].Pattern 	:= $FFFF
				else
					SendPtrns[i][j].Pattern 	:= 0;
				ReadPtrns[i][j].Value 		:= 0;
				ReadPtrns[i][j].Pattern 	:= 0;
			end;
		end;

		FTestForm.SetDelayLines($7f, SendPtrns);
    for i:= 0 to ALCTBoard.groups - 1 do
    begin
      if not (FTestForm.FindComponent('DG'+IntToStr(i)) as TCheckBox).Checked then
         FTestForm.Log.Lines.Add('Delay Group #' + IntToStr(i+1) + ' will be skipped from testing');
    end;

    FTestForm.Log.Lines.BeginUpdate;

		for p := 0 to pass-1  do
		begin
 			FTestForm.PrSt.Position := p+1;
			if FTestType = 4 then
				FTestForm.lbPrSt.Caption := 'Pass #' + IntToStr((p div 2) + 1) + ' of ' +IntToStr(pass div 2)
			else
				FTestForm.lbPrSt.Caption := 'Pass #' + IntToStr(p+1) + ' of ' +IntToStr(pass);

			case FTestType of
        0..3:  if not (FTestForm.FindComponent('DG'+IntToStr(p div (ALCTBoard.chips * ALCTBoard.delaylines))) as TCheckBox).Checked then
                continue;
      end;
			WriteIR('11', V_IR);
			WRiteDR('4000', 16);

			for i:= 0 to ALCTBoard.groups - 1 do
			begin
				for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
				begin
					SendPtrns[i][j].Value := 0;
					case FTestType of
					  0: SendPtrns[i][j].Pattern 	:= 0 or (Integer ((p div 16) = ((i*6)+j)) shl (p mod 16));
					  1: SendPtrns[i][j].Pattern 	:= $FFFF and (not(Integer ((p div 16) = ((i*6)+j)) shl (p mod 16)));
					  2: SendPtrns[i][j].Pattern 	:= SendPtrns[i][j].Pattern or ((Integer ((p div 16) = ((i*6)+j)) shl (p mod 16)));
					  3: SendPtrns[i][j].Pattern 	:= SendPtrns[i][j].Pattern and (not(Integer ((p div 16) = ((i*6)+j)) shl (p mod 16)));
					  4: begin
                if Boolean ((p+1) mod 2) then
								  SendPtrns[i][j].Pattern := $5555
							  else
								  SendPtrns[i][j].Pattern := $AAAA;
               end;
					  5: SendPtrns[i][j].Pattern := Random($FFFF);
					else
						SendPtrns[i][j].Pattern := 0;
					end;
					ReadPtrns[i][j].Value 		:= 0;
					ReadPtrns[i][j].Pattern 	:= 0;
				end;
			end;

			FTestForm.SetDelayLines($7f, SendPtrns);

			if FTestForm.ReadPatterns(ReadPtrns) = Wires then
			begin
				ErrOnePass := FTestForm.CheckPatterns(SendPtrns, ReadPtrns);
				Errs := Errs + ErrOnePass;
        sentstr := '';
        readstr := '';
				for gr:= 0 to ALCTBoard.groups - 1 do
					for ch:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
          begin
            sentstr := IntToHex(SendPtrns[gr][ch].Pattern, 4) + ' ' +sentstr;
						readstr := IntToHex(ReadPtrns[gr][ch].Pattern, 4) + ' ' +readstr;
          end;
        status := readstr;
        FTestForm.sbBar.SimpleText := status;

				if ErrOnePass > 0 then
				begin
					FTestForm.Log.Lines.Add('Pass: #' + IntToStr(p+1) + ' with ' + IntToStr(ErrOnePass) + ' Errors ');
//          FTestForm.Log.Lines.Add('Possibly problem with Chip #' + IntToStr(p div 16 + 1) + ' Channel #' + IntToStr(p mod 16 + 1));
          FTestForm.Log.Lines.Add('->Sent: '+sentstr);
          FTestForm.Log.Lines.Add('<-Read: '+readstr);
          if fStopOnError then
          begin
            //FTestForm.Log.Lines.Add('->Sent: '+sentstr);
            //FTestForm.Log.Lines.Add('<-Read: '+readstr);
            if not fRepeatLast then
              begin
                fStop := True;
                for gr:= 0 to ALCTBoard.groups - 1 do
                begin
                  for ch:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
                  begin
{                    if ReadPtrns[gr][ch].Pattern <> SendPtrns[gr][ch].Pattern then
                    begin
                      FTestForm.Log.Lines.Add('    Group #'+ IntToStr(gr+1) + ':-> Chip #' + IntToStr(ch+1) + ' Read 0x'+IntToHex(ReadPtrns[gr][ch].Pattern, 4) + ' Expected 0x' + IntToHex(SendPtrns[gr][ch].Pattern, 4));
                      WriteIR('0A', V_IR);
                      WriteDR(IntToHex((gr*6+ch) div 2,4),16);

                      WriteIR('11', V_IR);
                      WriteDR('4000',16);

                      WriteIR('11', V_IR);
                      if Boolean (ch mod 2) then
                        WriteDR('4000',16)
                      else
                        WriteDR('2000',16);

                      for bit := 0 to 15 do
                      begin
                        if ((ReadPtrns[gr][ch].Pattern shr bit) and $1) <> ((SendPtrns[gr][ch].Pattern shr bit) and $1) then
                        begin
                          FTestForm.Log.Lines.Add('       Bit[' + IntToStr(bit) + '] is '+IntToStr((ReadPtrns[gr][ch].Pattern shr bit) and $1) + ' should be ' + IntToStr((SendPtrns[gr][ch].Pattern shr bit) and $1));
                        end;
                      end;
                    end;
}
                  end;
                end;
                FTestForm.Log.Lines.Add(TimeToStr(Now())+ '> === ' + 'Stopped due to errors on pass #' + IntToStr(p+1));
                FTestForm.sbBar.SimpleText := 'Stopped due to errors on pass #' + IntToStr(p+1);
              end
              else
              begin
            end;
        end;
      end;
			end
			else
			begin
				fStop := true;
                FTestForm.Log.Lines.Add('Pass #' + IntToStr((p div 2) +1 ) + ': Reading Failure ');
			end;
			if Terminated or fStop then Exit;
		end;
     FTestForm.Log.Lines.EndUpdate;

		if old <> VRTX_CH then
		begin
			FTestForm.Chains.ItemIndex := old;
			FTestForm.SetChainClick(self);
		end;
  		FTestForm.Log.Lines.Add(TimeToStr(Now())+ '> === ' + TestNames[FTestType] + ' Test Finished with ' + IntToStr(Errs) + ' Errors' + '===');
        FTestForm.sbBar.SimpleText := TestNames[FTestType] + ' Test Finished with ' + IntToStr(Errs) + ' Errors';
        FTestForm.Update;
	end;
end;

procedure TTestThread.Execute;
begin
    Synchronize(Test);
end;

{$IFNDEF LINUX}
procedure TForm1.GetAppVersion;
const
  InfoNum = 10;
  InfoStr: array[1..InfoNum] of string = ('CompanyName', 'FileDescription', 'FileVersion', 'InternalName', 'LegalCopyright', 'LegalTradeMarks', 'OriginalFileName', 'ProductName', 'ProductVersion', 'Comments');
var
  S: string;
  n, Len : DWORD;
  Buf: PChar;
  Value: PChar;
begin
  S := Application.ExeName;
  n := GetFileVersionInfoSize(PChar(S), n);
  if n > 0 then
  begin

    Buf := AllocMem(n);
    GetFileVersionInfo(PChar(S), 0, n, Buf);
      if VerQueryValue(Buf, PChar('StringFileInfo\040904E4\' + InfoStr[3]), Pointer(Value), Len) then
        Form1.Caption := Form1.Caption + ' (Ver. '+ Value + ')';
    FreeMem(Buf, n);
  end
  else
  Form1.Caption := Form1.Caption + ' <No Version Info>';
end;
{$ENDIF}

procedure TForm1.Exit1Click(Sender: TObject);
begin
    Close();
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	close_jtag;
	if iStatus <> iStopped then iStatus := iStopped;
end;

procedure TForm1.Button6Click(Sender: TObject);
var
	portnum: byte;
begin
	if not ActiveHW then
	begin
	portnum := LPTNumber.Value-1;
	case Driver.ItemIndex of
	0: begin
//            portnum := LPTNumber.Value-1;
	{$IFNDEF LINUX}
			open_jtag(self);
	{$ELSE}
			open_jtag(portnum, false);
	{$ENDIF}
			if nPort = -1 then
							begin
								sbBar.SimpleText := 'Error #' + IntToStr(nPort) + ' accessing LPT port';
	{$IFDEF LINUX}
								Log.Lines.Add(TimeToStr(Now())+ '> Error #' + IntToStr(nPort) + ' accessing LPT port. Check Is LPT Device Driver ' + modulename +' loaded?');
	{$ENDIF}
							end
			else
				begin
					ActiveHW := True;
					sbBar.SimpleText := 'JTAG Opened via LPT Port #' + IntToStr(portnum);
					Log.Lines.Add(TimeToStr(Now())+ '> JTAG Opened via LPT' + IntToStr(LPTNumber.Value));
					SetChainClick(self);
					Button6.Caption := 'Close';
				end;
		end;
	1: begin

	{$IFDEF LINUX}
			open_jtag(portnum, true);
			if nPort = -1 then
							begin
								sbBar.SimpleText := 'Error #' + IntToStr(nPort) + ' accessing JTAG port';
								Log.Lines.Add(TimeToStr(Now())+ '> Error #' + IntToStr(nPort) + ' accessing JTAG port. Check Is JTAG Device Driver "jtag" is loaded?');
							end
			else
				begin
					ActiveHW := True;
					sbBar.SimpleText := 'JTAG Opened via Port #' + IntToStr(portnum);
					Log.Lines.Add(TimeToStr(Now())+ '> JTAG Opened via Port #' + IntToStr(LPTNumber.Value));
					SetChainClick(self);
					Button6.Caption := 'Close';
				end;
	{$ELSE}
		   sbBar.SimpleText := 'JTAG Driver Access is Not Available for Windows. Use LPT Driver.'
	{$ENDIF}
		end;
	end;
	end
	else
	begin

		 close_jtag;
		 sbBar.SimpleText := 'JTAG Closed ';
		 Log.Lines.Add(TimeToStr(Now())+ '> JTAG Closed');

		 Button6.Caption := 'Open';
		 Shape1.Brush.Color := clGray;
		 Shape2.Brush.Color := clGray;
		 Shape3.Brush.Color := clGray;
		 Shape4.Brush.Color := clGray;
	end;
end;

procedure TForm1.SetChainClick(Sender: TObject);
var
	ch      : byte;
	color   : TColor;
begin
	if ActiveHW then
	begin
		ch := arJTAGChains[Chains.ItemIndex];
		set_chain(ch);
    sbBar.SimpleText := 'Set '+ Chains.Text +' Chain';
		Log.Lines.Add(TimeToStr(Now())+ '> Set '+ Chains.Text +' Chain');

    Shape4.Brush.Color := clLime;
    Shape3.Brush.Color := clLime;
    if boolean (ch and 4) then  color := clWhite else color:= clLime;
    Shape2.Brush.Color := color;
    if boolean (ch and 1) then color := clWhite else color:= clLime;
    Shape1.Brush.Color := color;
  end
  else
    sbBar.SimpleText := 'Error: Driver is not Active'
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
    if ActiveHW then reset_jtag();
end;

procedure TForm1.Button3Click(Sender: TObject);
var
    IRstr: string;
    len: Integer;
    i:  Integer;
begin
	if ActiveHW then
  begin
    IRstr := Edit1.Text;
    len := SpinEdit2.Value;
		for i:= 1 to (((len - 1) div 4 + 1) - Length(Edit1.Text)) do
      IRstr := '0' + IRStr;
		Log.Lines.Add(TimeToStr(Now())+ '> IR->: ' + IntToStr(len) + ' bits; Value 0x' + UpperCase(IRstr));
		Log.Lines.Add(TimeToStr(Now())+ '> IR<-: ' + IntToStr(len) + ' bits; Value 0x' + UpperCase(WriteIR(IRstr, len)));
   end;
end;

procedure TForm1.Button4Click(Sender: TObject);
var
    DRstr: string;
    len: Integer;
    i:  Integer;
begin
    if ActiveHW then
    begin
        DRstr := Edit2.Text;
        len := SpinEdit3.Value;
        for i:= 1 to ((len - 1) div 4 + 1) - Length(Edit2.Text) do
        DRstr := '0' + DRstr;

        Log.Lines.Add(TimeToStr(Now())+ '> DR->: ' + IntToStr(len) + ' bits; Value 0x' + UpperCase(DRstr));
        Log.Lines.Add(TimeToStr(Now())+ '> DR<-: ' + IntToStr(len) + ' bits; Value 0x' + UpperCase(WriteDR(DRstr, len)));
    end;
end;

procedure TForm1.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
	 if not(Key in ['0'..'9','a'..'f','A'..'F',#08]) then Key := #0;
	 if Key = Chr(13) then  Button3Click(self);
end;

procedure TForm1.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
	 if not(Key in ['0'..'9','a'..'f','A'..'F',#08]) then Key := #0;
	 if Key = Chr(13) then  Button4Click(self);
end;

procedure TForm1.SpinEdit2Changed(Sender: TObject; NewValue: Integer);
begin
    Edit1.MaxLength :=  ((SpinEdit2.Value - 1) div 4 + 1);
end;

procedure TForm1.SpinEdit3Changed(Sender: TObject; NewValue: Integer);
begin
   Edit2.MaxLength :=  ((SpinEdit3.Value - 1) div 4 + 1);
end;

procedure TForm1.Button1Click(Sender: TObject);
var
    old:    Integer;
    cmd:    string;
    data:   string;
begin
	if ActiveHW then
	begin
	old := Chains.ItemIndex;

	Chains.ItemIndex := 0;
	SetChainClick(self);
	Log.Lines.Add(TimeToStr(Now())+ '> === Slow Control Programming Chain Read Chips ID codes');
	cmd := '07ff';
	WriteIR(cmd, PROG_SC_IR_SIZE);
	Log.Lines.Add(TimeToStr(Now())+ '> = Reading EPROM ID code...');
	cmd := '07fe';
	WriteIR(cmd, PROG_SC_IR_SIZE);
	data := '0000000000';
	data := '$'+ReadDR(data, PROG_SC_ID_DR_SIZE);
//	data := IntToStr(StrToInt64(data) and (Int64(PROG_SC_EPROM_ID_MASK)));
	if StrToInt64(data) = PROG_SC_EPROM_ID then
	begin
		Log.Lines.Add(TimeToStr(Now())+ '> ID = '+IntToHex(StrToInt64(data), (PROG_SC_ID_DR_SIZE-1) div 4 +1) + ' - OK');
	end
	else
	begin
		Log.Lines.Add(TimeToStr(Now())+ '> Error: ID = '+IntToHex(StrToInt64(data), (PROG_SC_ID_DR_SIZE-1) div 4 +1) + ' Expected - ' + IntToHex(PROG_SC_EPROM_ID, (PROG_SC_ID_DR_SIZE-1) div 4 +1) + ' Check Frequency ON/OFF Jumper (Should be OFF)');
	end;

      cmd := '07ff';
      WriteIR(cmd, PROG_SC_IR_SIZE);
    Log.Lines.Add(TimeToStr(Now())+ '> = Reading Spartan FPGA ID code...');
    cmd := '06ff';
    WriteIR(cmd, PROG_SC_IR_SIZE);
    data := '0000000000';
	data := '$'+ReadDR(data, PROG_SC_ID_DR_SIZE);
    data := IntToStr(StrToInt64(data) and PROG_SC_FPGA_ID_MASK);
	if StrToInt64(data) = PROG_SC_FPGA_ID  then
	begin
		Log.Lines.Add(TimeToStr(Now())+ '> ID = '+IntToHex(StrToInt64(data), (PROG_SC_ID_DR_SIZE-1) div 4 +1) + ' - OK');
	end
	else
	begin
		Log.Lines.Add(TimeToStr(Now())+ '> Error: ID = '+IntToHex(StrToInt64(data), (PROG_SC_ID_DR_SIZE-1) div 4 +1) + ' Expected - ' + IntToHex(PROG_SC_FPGA_ID, (PROG_SC_ID_DR_SIZE-1) div 4 +1)+ ' Check Frequency ON/OFF Jumper (Should be OFF)');
	end;


	Chains.ItemIndex := 1;
	SetChainClick(self);
	Log.Lines.Add(TimeToStr(Now())+ '> === Slow Control Control Chain Read Firmware ID codes');

	Log.Lines.Add(TimeToStr(Now())+ '> = Reading Firmware ID code...');
	cmd := '00';
	WriteIR(cmd, SC_IR);
	data := '0000000000';

	data := '$'+ReadDR(data, CTRL_SC_ID_DR_SIZE);
	if StrToInt64(data) = CTRL_SC_FPGA_ID  then
	begin
		Log.Lines.Add(TimeToStr(Now())+ '> ID = '+IntToHex(StrToInt64(data), (CTRL_SC_ID_DR_SIZE-1) div 4 +1) + ' - OK');
	end
	else
	begin
		Log.Lines.Add(TimeToStr(Now())+ '> Error: ID = '+IntToHex(StrToInt64(data), (CTRL_SC_ID_DR_SIZE-1) div 4 +1) + ' Expected - ' + IntToHex(CTRL_SC_FPGA_ID, (CTRL_SC_ID_DR_SIZE-1) div 4 +1) + ' Check Frequency ON/OFF Jumper (Should be ON) or Re-Program Slow Control Firmware');
	end;


	Chains.ItemIndex := 2;
	SetChainClick(self);

	btDetectTypeClick(Self);
	case MezChipType of
	VIRTEX1000: begin
					Log.Lines.Add(TimeToStr(Now())+ '> === Virtex Programming Chain Read Chips ID codes');
					cmd := '1fffff';
					WriteIR(cmd, PROG_V_IR_SIZE);
					Log.Lines.Add(TimeToStr(Now())+ '> = Reading EPROM #1 ID code...');
					cmd := '1ffeff';
					WriteIR(cmd, PROG_V_IR_SIZE);
					data := '0000000000';
					data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
					data := IntToStr(StrToInt64(data) and PROG_V_EPROM1_ID_MASK);
					if (StrToInt64(data) = PROG_V_EPROM1_ID) or
                                           (StrToInt64(data) = PROG_V_EPROM1_ID2) then
					begin
						Log.Lines.Add(TimeToStr(Now())+ '> ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' - OK');
					end
					else
					begin
						Log.Lines.Add(TimeToStr(Now())+ '> Error: ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' Expected - ' + IntToHex(PROG_V_EPROM1_ID, (PROG_V_ID_DR_SIZE-1) div 4 +1)+ ' Check Frequency ON/OFF Jumper (Should be OFF)');
                    end;

                    cmd := '1fffff';
                    WriteIR(cmd, PROG_V_IR_SIZE);
					Log.Lines.Add(TimeToStr(Now())+ '> = Reading EPROM #2 ID code...');
                    cmd := '1ffffe';
                    WriteIR(cmd, PROG_V_IR_SIZE);
                    data := '0000000000';
                    data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
                    data := IntToStr(StrToInt64(data) and PROG_V_EPROM2_ID_MASK);
                    if (StrToInt64(data) = PROG_V_EPROM2_ID) or
                        (StrToInt64(data) = PROG_V_EPROM2_ID2)  then
                    begin
                		Log.Lines.Add(TimeToStr(Now())+ '> ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' - OK');
                    end
                    else
                    begin
                        Log.Lines.Add(TimeToStr(Now())+ '> Error: ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' Expected - ' + IntToHex(PROG_V_EPROM2_ID, (PROG_V_ID_DR_SIZE-1) div 4 +1)+ ' Check Frequency ON/OFF Jumper (Should be OFF)');
                    end;

                    cmd := '1fffff';
                    WriteIR(cmd, PROG_V_IR_SIZE);

                    Log.Lines.Add(TimeToStr(Now())+ '> = Reading Virtex 1000 FPGA ID code...');
                    cmd := '09ffff';

                    WriteIR(cmd, PROG_V_IR_SIZE);
                    data := '0000000000';
                    data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
                    data := IntToStr(StrToInt64(data) and PROG_V_FPGA_ID_MASK);
                    if StrToInt64(data) = PROG_V_FPGA_ID  then
                    begin
                    Log.Lines.Add(TimeToStr(Now())+ '> ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' - OK');
					end
                    else
                    begin
                        Log.Lines.Add(TimeToStr(Now())+ '> Error: ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' Expected - ' + IntToHex(PROG_V_FPGA_ID, (PROG_V_ID_DR_SIZE-1) div 4 +1)+ ' Check Frequency ON/OFF Jumper (Should be OFF)');
                    end;
                end;
    VIRTEX600: begin
                    Log.Lines.Add(TimeToStr(Now())+ '> === Virtex Programming Chain Read ID codes');
                    cmd := '1fff';
                    WriteIR(cmd, PROG_V_IR_SIZE-8);
                    Log.Lines.Add(TimeToStr(Now())+ '> = Reading EPROM #1 ID code...');
                    cmd := '1ffe';
                    WriteIR(cmd, PROG_V_IR_SIZE-8);
                    data := '0000000000';
                    data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
                    data := IntToStr(StrToInt64(data) and PROG_V_EPROM2_ID_MASK);
                    if (StrToInt64(data) = PROG_V_EPROM2_ID) or
                        (StrToInt64(data) = PROG_V_EPROM2_ID2)  then
                    begin
                        Log.Lines.Add(TimeToStr(Now())+ '> ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' - OK');
                    end
                    else
                    begin
                        Log.Lines.Add(TimeToStr(Now())+ '> Error: ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' Expected - ' + IntToHex(PROG_V_EPROM2_ID, (PROG_V_ID_DR_SIZE-1) div 4 +1)+ ' Check Frequency ON/OFF Jumper (Should be OFF)');
                    end;

                    cmd := '1fff';
                    WriteIR(cmd, PROG_V_IR_SIZE-8);

					          Log.Lines.Add(TimeToStr(Now())+ '> = Reading Virtex 600 FPGA ID code...');
                    cmd := '09ff';

                    WriteIR(cmd, PROG_V_IR_SIZE-8);
                    data := '0000000000';
                    data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
                    data := IntToStr(StrToInt64(data) and PROG_V_FPGA600_ID_MASK);
                    if StrToInt64(data) = PROG_V_FPGA600_ID  then
                    begin
                    Log.Lines.Add(TimeToStr(Now())+ '> ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' - OK');
                    end
                    else
                    begin
                        Log.Lines.Add(TimeToStr(Now())+ '> Error: ID = '+IntToHex(StrToInt64(data), (PROG_V_ID_DR_SIZE-1) div 4 +1) + ' Expected - ' + IntToHex(PROG_V_FPGA600_ID, (PROG_V_ID_DR_SIZE-1) div 4 +1)+ ' Check Frequency ON/OFF Jumper (Should be OFF)');
                    end;
                end;
            else
               Log.Lines.Add(TimeToStr(Now())+ '> = Unknown Mezanine Board Type or Hardware Failure...');
            end;

    Chains.ItemIndex := 3;
    SetChainClick(self);
    Log.Lines.Add(TimeToStr(Now())+ '> === Virtex Control Chain Read Firmware ID codes');
   	cmd := '00';
	  WriteIR(cmd, V_IR);
	  data := '0000000000';

	  data := '$'+ReadDR(data, USER_V_ID_DR_SIZE);
	  if StrToInt64(data) = USER_V_FPGA_ID  then
	  begin
		  Log.Lines.Add(TimeToStr(Now())+ '> ID = '+IntToHex(StrToInt64(data), (USER_V_ID_DR_SIZE-1) div 4 +1) + ' - OK');
	  end
	    else
	  begin
		  Log.Lines.Add(TimeToStr(Now())+ '> Error: ID = '+IntToHex(StrToInt64(data), (USER_V_ID_DR_SIZE-1) div 4 +1) + ' Expected - ' + IntToHex(USER_V_FPGA_ID, (USER_V_ID_DR_SIZE-1) div 4 +1) + ' Check Frequency ON/OFF Jumper (Should be ON) or Re-Program Virtex Fast Control Firmware');
	  end;

    Log.Lines.Add(TimeToStr(Now())+ '> === Read ALCT Board    Serial Number');
    Log.Lines.Add(TimeToStr(Now())+ '> SN = '+ ReadBoardSNStr(BOARD_SN));
    Log.Lines.Add(TimeToStr(Now())+ '> === Read Mezanine Card Serial Number');
    Log.Lines.Add(TimeToStr(Now())+ '> SN = '+ ReadBoardSNStr(MEZAN_SN));

    //	Log.Lines.Add(TimeToStr(Now())+ '> Error: Not Implemented inside FPGA');

    Chains.ItemIndex := old;
    SetChainClick(self);
    end;
end;

procedure TForm1.ReadPtrnsClick(Sender: TObject);
var
    old : byte;
    DR : array of byte;
    DRR: array of byte;
//    DRR : array of Int64;
    tmpstr: string;
    I, J, k:  Integer;
    DlyPtrn, DlyPtrnTmp : WORD;
begin
    if ActiveHW then
    begin
        Log.Lines.EndUpdate;
        Log.Lines.Add(TimeToStr(Now())+ '> === Reading Patterns from ALCT ===');
        SetLength(DR, ((Wires - 1) div 8)+1);
        SetLength(DRR, ((Wires - 1) div 8)+1);
		old := Chains.ItemIndex;
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := VRTX_CH;
			SetChainClick(self);
		end;

		WriteIR('10', V_IR);
		if IOExchange(DR, DRR, Wires, DR_REG)<> Wires then
		Log.Lines.Add('Reading Failure:');

		tmpstr := '';
{		for I := Low(DRR) to High(DRR) do
		begin
			tmpstr := IntToHex(DRR[I],2 ) + tmpstr;
			if Boolean (i mod 2) then tmpstr := ' ' + tmpstr;
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> Patterns <-: (' + IntToStr(Wires) + ')bits; 0x' + tmpstr);
}
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := old;
			SetChainClick(self);
		end;
		for i:= 0 to ALCTBoard.groups - 1 do
		begin
			for j:=0 to 5 do
			begin
				ReadDelays[i][j].Value := arDelays[i][j].Value;
//                ReadDelays[i][j].Pattern := (DR[((i*6)+j) div 4] shr (((i*6+j)mod 4)*16)) and $ffff;
				ReadDelays[i][j].Pattern := ((DRR[(i*12)+2*j+1] shl 8) or DRR[(i*12)+2*j]) and $ffff;

        if cbPinMapOnRead.Checked then
        begin
          DlyPtrn := ReadDelays[i][j].Pattern;
			    DlyPtrnTmp := 0;
			    if boolean (j mod 2) then
			    begin
				    for k:= 0 to 7 do
					    DlyPtrnTmp := DlyPtrnTmp or (((DlyPtrn shr k) and $01) shl (7 -k)) ;
				    DlyPtrnTmp := DlyPtrnTmp or (DlyPtrn and $ff00);
			    end
			    else
			    begin
				    for k:= 0 to 7 do
					    DlyPtrnTmp := DlyPtrnTmp or (((DlyPtrn shr (k+8)) and $01) shl (15 -k));
				    DlyPtrnTmp := DlyPtrnTmp or (DlyPtrn and $00ff);
			    end;
          ReadDelays[i][j].Pattern := DlyPtrnTmp;
        end;
        tmpstr := IntToHex(ReadDelays[i][j].Pattern,4 )+' ' + tmpstr;
//                Log.Lines.Add('Group #'+ IntToStr(i+1) + ':-> Chip #' + IntToStr(j+1) + ' Ptrn=0x' +IntToHex(ReadDelays[i][j].Pattern,4));
			end;
		end;
    Log.Lines.Add(TimeToStr(Now())+ '> Patterns <-: (' + IntToStr(Wires) + ')bits; 0x' + tmpstr);
		CheckPtrns.Enabled := true;
		Log.Lines.Add(TimeToStr(Now())+ '> === Reading Finished ===')
	end;

end;


procedure TForm1.btSetDelaysChipsClick(Sender: TObject);
var
   old    : byte;
   cs     : Integer;
   r      : Int64;
   i,j    : byte;
   DlyVal, DlyValTmp : Int64;
   DlyPtrn, DlyPtrnTmp : Int64;
begin
	 if ActiveHW then
	 begin
    Log.Lines.EndUpdate;
		Log.Lines.Add(TimeToStr(Now())+ '> === Setting Delays and Patterns ===');

		old := Chains.ItemIndex;
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := VRTX_CH;
			SetChainClick(self);
		end;

    parlen := ALCTBoard.groups + 2;
    cs := 0;

		for i:= 0 to ALCTBoard.groups - 1 do
		begin
			if (TCheckBox(DelayGroups.Controls[i]).Checked) then
      begin
			  for j:=0 to 5 do
			  begin
				  arDelays[i][j].Value := (TTrackBar(DelayValues.Controls[j])).Position;
				  arDelays[i][j].Pattern := StrToInt('$'+(TEdit(DelayValues.Controls[18+j])).Text );
			  end;
        cs := cs or (1 shl i);
   			ReadPtrns.Enabled := true;
      end;
		end;

//    cs := 0 or byte(DG0.Checked) or byte(DG1.Checked)*2 or byte(DG2.Checked)*4 or byte(DG3.Checked)*8 ;// or byte(DG4.Checked)*16 or byte(DG5.Checked)*32 or byte(DG6.Checked)*64;

		r  := $1ff and (not (cs shl 2));

		if fOddConnector then
		begin
			WriteIR('11', V_IR);
			WRiteDR('a000', 16);
		end
		else
		begin
			WriteIR('11', V_IR);
			WRiteDR('4000', 16);
		end;


//		Log.Lines.Add('Parameter Reg - 0x' + IntToHex(r,2));
//		WriteRegister(ParamRegWrite, IntToHex(r,3));
    WriteIR(IntToHex(ParamRegWrite,2), V_IR);
    WriteDR(IntToHex(r,3), parlen);
//		Log.Lines.Add('Read Back ParamReg - 0x' + ReadRegister(ParamRegRead));
//		ReadRegister(ParamRegRead);
    WriteIR(IntToHex(ParamRegRead,2), V_IR);
//    Log.Lines.Add('Read Back ParamReg - 0x' + ReadDR('0', parlen));
    ReadDR('0', parlen);

		WriteIR(IntToHex(Wdly,2), V_IR);

		Log.Lines.Add('Writing Values to ALCT');
		StartDRShift;
		for i := 0 to 5 do
		begin
			DlyVal :=  Longword((TTrackBar(DelayValues.Controls[i])).Position);
			DlyPtrn:=  Longword(StrToInt('$'+(TEdit(DelayValues.Controls[18+i])).Text));
      DlyPtrnTmp := DlyPtrn;
			//      === Pin Remapping ===
      if not cbPinMapOnRead.Checked then
      begin
			  DlyPtrnTmp := 0;
			  if boolean (i mod 2) then
			  begin
				  for j:= 0 to 7 do
					  DlyPtrnTmp := DlyPtrnTmp or (((DlyPtrn shr j) and $01) shl (7 -j)) ;
				  DlyPtrnTmp := DlyPtrnTmp or (DlyPtrn and $ff00);
			  end
			  else
			  begin
				  for j:= 0 to 7 do
					  DlyPtrnTmp := DlyPtrnTmp or (((DlyPtrn shr (j+8)) and $01) shl (15 -j));
				  DlyPtrnTmp := DlyPtrnTmp or (DlyPtrn and $00ff);
			  end;
      end;

			if cbFlipDelay.Checked then
				DlyValTmp := ShiftData(Longword(FlipHalfByte(DlyVal)), 4, false)
			else
				DlyValTmp := ShiftData(Longword(DlyVal), 4, false);
			DlyPtrnTmp := ShiftData(Longword(DlyPtrnTmp), 16, i=5);

			if fReadback then
			begin
				DlyVal := DlyValTmp;
				DlyPtrn := DlyPtrnTmp;
				Log.Lines.Add('<- Chip #' + IntToStr(i+1) + ': Delay=0x'+IntToHex(DlyVal,2) + ' Pattern=0x' +IntToHex(DlyPtrn,4));
			end;
		end;
		ExitDRShift;
{
		r  := $3f; //and (not (cs shl 2))
		Log.Lines.Add('Parameter Reg - 0x' + IntToHex(r,3));
		WriteRegister(ParamRegWrite, r);

		r  := $3d; //and (not (cs shl 2))
		Log.Lines.Add('Parameter Reg - 0x' + IntToHex(r,3));
		WriteRegister(ParamRegWrite, r);
}
		r  := $1ff; //and (not (cs shl 2))

//		Log.Lines.Add('Parameter Reg - 0x' + IntToHex(r,3));
//		WriteRegister(ParamRegWrite, IntToHex(r,3));
    WriteIR(IntToHex(ParamRegWrite,2), V_IR);
    WriteDR(IntToHex(r,3), parlen);
//		Log.Lines.Add('Read Back ParamReg - 0x' + ReadRegister(ParamRegRead));
//		ReadRegister(ParamRegRead);
    WriteIR(IntToHex(ParamRegRead,2), V_IR);
    ReadDR('0', parlen);

//     	else

{     	if fShowPattern then
		begin
			r  := $3d;
		Log.Lines.Add('Parameter Reg - 0x' + IntToHex(r,2));

		WriteIR(IntToHex(ParamRegWrite,2), V_IR);
		WRiteDR(IntToHex(r,2), RegSz[ParamRegWrite]);
		Log.Lines.Add('Read Back ParamReg - 0x' + IntToHex(r,2));
		end;
}


		WriteIR('11', V_IR);
		WRiteDR('0001', 16);
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := old;
			SetChainClick(self);
		end;
//		if (DG0.Checked  or DG1.Checked or DG2.Checked or DG3.Checked) then
//			ReadPtrns.Enabled := true;
		Log.Lines.Add(TimeToStr(Now())+ '> === Delay Chip Patterns are set  ===');
	 end;
end;


procedure TForm1.LockDelaysClick(Sender: TObject);
begin
//	 fLockDelays := LockDelays.Checked;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
//      if Sender is  TTrackBar then
//      with DelayValues.Controls[25+(TTrackBar(Sender)).Tag] as  TLabel do
      (TLabel(DelayValues.Controls[12+(TTrackBar(Sender)).Tag])).Caption := IntToStr((TTrackBar(Sender)).Position*2) + ' ns [0x'+IntToHex((TTrackBar(Sender)).Position,1)+']';
//      (TTrackBar(DelayValues.Controls[4])).Position := 8;
//      (TEdit(DelayValues.Controls[18+(TTrackBar(Sender)).Tag])).Text := 'ff';
end;

procedure TForm1.FormCreate(Sender: TObject);
const
	titles : array[0..8] of string = ('Volt 1.8V',
									  'Volt 3.3V',
									  'Volt 5.5V(1)',
									  'Volt 5.5V(2)',
									  'Curr 1.8V',
									  'Curr 3.3V',
									  'Curr 5.5V(1)',
									  'Curr 5.5V(2)',
									  'Temp');
var
	i: Integer;
	tmpSeries : TFastLineSeries;
begin
//        GetConfigFromIniFile('alct_tests.ini');
//	CalibrateDelay;
	ps := TIfPasScript.Create(nil);
	ps.OnRunLine := OnRunLine;
	ps.OnUses := OnUses;
	ps.MaxBeginNesting := 1000;
//	ps.OnExternal := DllExternalProc;
	fn := '';
	Memo1.Lines.Text := 'Program TestScript;'#13#10'Begin'#13#10'End.';
	Memo2.Lines.Clear;
	changed := False;
	iStatus := iStopped;
	LastLine := 0;

	 TestPages.ActivePageIndex := 0;
	 fReadBack :=  FlagRdbk.Checked;
	 fShowPattern := flagShowPat.Checked;
	 fOddCOnnector := WhichConnector.Checked;
//	 fOneStep := False;
	 for i:=Low(alct_table) to High(alct_table) do
	 begin
		cbALCTType.Items.Add(alct_table[i].name);
	 end;
	 for i:=Low(chamb_table) to High(chamb_table) do
	 begin
		cbChamberType.Items.Add(chamb_table[i].name + ' -> ' + alct_table[chamb_table[i].alct].name);
	 end;
   cbChamberType.DropDownCount := High(chamb_table)+1;
 	 cbChamberType.ItemIndex := ME1_2;
   Chamber := chamb_table[cbChamberType.ItemIndex];
   ALCTBoard := alct_table[cbALCTType.ItemIndex];

   cbChamberType.Style := csDropDownList;
   ChangeALCTType;

	 SpinEdit2.Value := 5;
	 SpinEdit3.Value := 16;
	 Edit1.MaxLength :=  ((SpinEdit2.Value - 1) div 8 + 1)*2;
	 Edit2.MaxLength :=  ((SpinEdit3.Value - 1) div 8 + 1)*2;
	 MezChipType := UNKNOWN;
{$IFDEF LINUX}
	 btOneStep.Visible := false;
	 btPause.Visible := false;
	 btStop.Visible := false;
{$ENDIF}

{$IFNDEF LINUX}
	 GetAppVersion;
{$ENDIF}
	SpinEdit4.Value := 500;
	Timer2.Interval := SpinEdit4.Value;
	Timer4.Interval := SpinEdit4.Value;
	for i:= 0 to 8 do
	begin
		tmpSeries := TFastLineSeries.Create(self);
		tmpSeries.Title := titles[i];
		tmpSeries.XValues.DateTime := true;
		VATChart.AddSeries(tmpSeries);
	end;

	with sgConfigReg do
	begin
		ColCount := 3;
		FixedCols := 2;
		RowCount := High(CRfld)+2;
		Cells[0, 0] := 'Parameter';
		Cells[2, 0] := 'Value in Hex';
		Cells[1, 0] := 'Mask';
	end;

	cbConfigReg.Clear;
	for i:=Low(CRfld) to High(CRfld) do
	begin
		cbConfigReg.Items.Add(CRfld[i].name);
		sgConfigReg.Cells[0,i+1] := CRfld[i].name;
		sgConfigReg.Cells[2,i+1] := IntToHex(CRfld[i].default,2);
		sgConfigReg.Cells[1,i+1] := IntToHex(CRfld[i].mask,2);
	end;
	cbConfigReg.ItemIndex:=Low(CRfld);

  Log.Lines.Add('Loading Tests Configuration from '+ConfigFile);
  GetConfigFromIniFile(ConfigFile);
  Log.Lines.Add('Done');

//  Config.Cells[0,0] := 'Parameter Name';
//  Config.Cells[1,0] := 'Value';

end;

procedure TForm1.flagRdbkClick(Sender: TObject);
begin
	 fReadBack :=  FlagRdbk.Checked;
end;

procedure TForm1.flagShowPatClick(Sender: TObject);
begin
	 fShowPattern := flagShowPat.Checked;
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
	StrSize.Caption := 'Size ' + IntToStr(Length(TEdit(Sender).Text)*4) + ' bits';
end;

procedure TForm1.whichConnectorClick(Sender: TObject);
begin
    fOddConnector := WhichConnector.Checked;
end;

procedure TForm1.CheckPtrnsClick(Sender: TObject);
var
Errs: Integer;
i,j, bit:    Integer;
begin
  Log.Lines.EndUpdate;
	Log.Lines.Add(TimeToStr(Now())+ '> === Checking ReadBack Patterns ===');
	Errs := 0;
   for i:= 0 to ALCTBoard.groups - 1 do
   begin
    if (FindComponent('DG'+IntToStr(i)) as TCheckBox).Checked then
    begin
	  for j:=0 to 5 do
	  begin
//         ReadDelays[i][j].Value := arDelays[i][j].Value;
		 if ReadDelays[i][j].Pattern <> arDelays[i][j].Pattern then
		 begin
			Log.Lines.Add('Group #'+ IntToStr(i+1) + ':-> Chip #' + IntToStr(j+1) + ' Read 0x'+IntToHex(ReadDelays[i][j].Pattern, 4) + ' Expected 0x' + IntToHex(arDelays[i][j].Pattern, 4));
			for bit := 0 to 15 do
			begin
				if ((ReadDelays[i][j].Pattern shr bit) and $1) <> ((arDelays[i][j].Pattern shr bit) and $1) then
				begin
				Inc(Errs);
				Log.Lines.Add('       Bit[' + IntToStr(bit) + '] is '+IntToStr((ReadDelays[i][j].Pattern shr bit) and $1) + ' should be ' + IntToStr((arDelays[i][j].Pattern shr bit) and $1));
				end;
//                Log.Lines.Add('Group #'+ IntToStr(i) + ':-> Chip #' + IntToStr(j) + ' Bit[' + IntToStr(bit) + '] is '+IntToStr(ReadDelays[i][j].Pattern) + ' should be ' + IntToStr(arDelays[i][j].Pattern));

			end;
		 end;
	  end;
    end
    else
      Log.Lines.Add('Group #'+ IntToStr(i+1) + ' Skipped');
   end;
   Log.Lines.Add(TimeToStr(Now())+ '> === Checking Finished: ' + IntToStr(Errs) + ' Errors found ===');
end;

procedure TForm1.SetDelayLines(cs: integer; delays: ALCTDelays);
var
   r		: integer;
   gr,i,j   : byte;
   DlyVal, DlyValTmp 	: Longword;
   DlyPtrn, DlyPtrnTmp 	: Longword;
//   parlen : Integer;
begin
   parlen := ALCTBoard.groups + 2;
	 for gr:= 0 to ALCTBoard.groups - 1 do
	 begin
		if Boolean( (cs and $7F) and ($1 shl gr)) then
		begin
			r  := $1ff and (not ( (cs and ($1 shl gr)) shl 2) );
			WriteIR(IntToHex(ParamRegWrite,2), V_IR);
			WRiteDR(IntToHex(r,3), parlen);

			WriteIR(IntToHex(Wdly, 2), V_IR);

			StartDRShift;

			for i := 0 to 5 do
			begin
				DlyVal :=  Longword(delays[gr][i].Value);
				DlyPtrn:=  Longword(delays[gr][i].Pattern);
        DlyPtrnTmp := DlyPtrn;

        if not cbPinMapOnRead.Checked then
        begin
				//      === Pin Remapping ===

  				DlyPtrnTmp := 0;
	  			if boolean (i mod 2) then
		  		begin
			  		for j:= 0 to 7 do
				  		DlyPtrnTmp := DlyPtrnTmp or (((DlyPtrn shr j) and $01) shl (7 -j)) ;
					  DlyPtrnTmp := DlyPtrnTmp or (DlyPtrn and $ff00);
  				end
	  			else
		  		begin
			  		for j:= 0 to 7 do
				  		DlyPtrnTmp := DlyPtrnTmp or (((DlyPtrn shr (j+8)) and $01) shl (15 -j));
					  DlyPtrnTmp := DlyPtrnTmp or (DlyPtrn and $00ff);
				  end;
        end;
				DlyValTmp := ShiftData(Longword(DlyVal), 4, false);
				DlyPtrnTmp := ShiftData(Longword(DlyPtrnTmp), 16, i=5);

			end;

			ExitDRShift;

{			r := $1ff;
			WriteIR(IntToHex(ParamRegWrite,2), V_IR);
			WRiteDR(IntToHex(r,3), parlen);

			r := $1fd;
			WriteIR(IntToHex(ParamRegWrite,2), V_IR);
			WRiteDR(IntToHex(r,3), parlen);
}
			r := $1ff;
			WriteIR(IntToHex(ParamRegWrite,2), V_IR);
			WriteDR(IntToHex(r,3), parlen);
		end;
	 end;
end;

function TForm1.ReadPatterns(out Patterns: ALCTDelays): longint;
var
	DR: array of byte;
	i,j,k: byte;
	tmpstr: string;
  DlyPtrn, DlyPtrnTMp : WORD;
begin
	SetLength(DR, ((Wires - 1) div 8)+1);
	WriteIR('11', V_IR);
	WRiteDR('4001', 16);      // Enable Patterns from DelayCHips into 384 bits ALCT Register
	Delay(5);
//	for i:= 0 to 255 do j:=i; // Short Delay
	WriteIR('10', V_IR);      // Send 384 bits ALCT Register to PC via JTAG
	Result := IOExchange(DR, DR, Wires, DR_REG);
	if Boolean(Result = Wires) then
  begin
    tmpstr := '';
		for i:= 0 to ALCTBoard.groups - 1  do
    begin
			for j:=0 to 5 do
      begin
        Patterns[i][j].Pattern := ((DR[(i*12)+2*j+1] shl 8) or DR[(i*12)+2*j]) and $ffff;
        if cbPinMapOnRead.Checked then
        begin
          DlyPtrn := Patterns[i][j].Pattern;
          DlyPtrnTmp := 0;
          if boolean (j mod 2) then
          begin
            for k:= 0 to 7 do
              DlyPtrnTmp := DlyPtrnTmp or (((DlyPtrn shr k) and $01) shl (7 -k)) ;
            DlyPtrnTmp := DlyPtrnTmp or (DlyPtrn and $ff00);
          end
          else
          begin
            for k:= 0 to 7 do
              DlyPtrnTmp := DlyPtrnTmp or (((DlyPtrn shr (k+8)) and $01) shl (15 -k));
            DlyPtrnTmp := DlyPtrnTmp or (DlyPtrn and $00ff);
          end;
          Patterns[i][j].Pattern := DlyPtrnTmp;
        end;
        tmpstr := IntToHex(Patterns[i][j].Pattern,4 )+' ' + tmpstr;
      end;
    end;
{
		tmpstr := '';
		for I := Low(DR) to High(DR) do
		begin
		tmpstr := IntToHex(DR[I],2 ) + tmpstr;
		if Boolean (i mod 2) then tmpstr := ' ' + tmpstr;
		end;
}
//        Log.Lines.Add(TimeToStr(Now())+ '> Patterns <-: (' + IntToStr(Wires) + ')bits; 0x' + tmpstr);
		sbBar.SimpleText := tmpstr;
		Update;
		end;
end;

function TForm1.CheckPatterns(RefPtrns: ALCTDelays; ReadPtrns: ALCTDelays): Longint;
var
	Errs: Longint;
	i,j, bit:    Integer;
begin
	Errs := 0;
	for i:= 0 to ALCTBoard.groups - 1 do
	begin
    if (FindComponent('DG'+IntToStr(i)) as TCheckBox).Checked then
    begin
		for j:=0 to 5 do
		begin
			if ReadPtrns[i][j].Pattern <> RefPtrns[i][j].Pattern then
			begin
//				Log.Lines.Add('    Group #'+ IntToStr(i+1) + ':-> Chip #' + IntToStr(j+1) + ' Read 0x'+IntToHex(ReadPtrns[i][j].Pattern, 4) + ' Expected 0x' + IntToHex(RefPtrns[i][j].Pattern, 4));
				for bit := 0 to 15 do
				begin
					if ((ReadPtrns[i][j].Pattern shr bit) and $1) <> ((RefPtrns[i][j].Pattern shr bit) and $1) then
					begin
						Inc(Errs);
//						Log.Lines.Add('       Bit[' + IntToStr(bit) + '] is '+IntToStr((ReadPtrns[i][j].Pattern shr bit) and $1) + ' should be ' + IntToStr((RefPtrns[i][j].Pattern shr bit) and $1));
					end;
				end;
			end;
		end;
    end;
    //else
    //  Log.Lines.Add('Group #'+ IntToStr(i+1) + ' Skipped');
   end;
   Result := Errs;
end;


procedure TForm1.Button7Click(Sender: TObject);
var
	old:	byte;
	i,j, bit:	Integer;
	Errs, ErrOnePass :  Longint;
	SendPtrns, ReadPtrns: ALCTDelays;
	fStop:	Boolean;
  sentstr, readstr: String;
begin
	if ActiveHW then
	begin
    Log.Lines.EndUpdate;
        Errs := 0;
        ErrOnePass := 0;
        PrSt.Max := Wires;
//        PrSt.Position := 0;
        fStop := false;
		Log.Lines.Add(TimeToStr(Now())+ '> === Running ''1'' Test ===');
    	sbBar.SimpleText := 'Running ''1'' Test';
		old := Chains.ItemIndex;
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := VRTX_CH;
			SetChainClick(self);
		end;

   for i:= 0 to ALCTBoard.groups - 1 do
    begin
      if not (FindComponent('DG'+IntToStr(i)) as TCheckBox).Checked then
         Log.Lines.Add('Delay Group #' + IntToStr(i+1) + ' will be skipped from testing');
    end;

  Log.Lines.BeginUpdate;
	for bit := 0 to Wires - 1 do
	begin
//    	Log.Lines.Add(TimeToStr(Now())+ '> ' +IntToStr(bit) + ': Group #' + IntToStr(i) + ' Chip #' + IntTostr(j) + ' Checking bit #' + IntToStr(bit mod 16));
//    	Log.Lines.Add(TimeToStr(Now())+ '> Checking bit #' + IntToStr(bit));
//      St.Caption :='Checking bit #' + IntToStr(bit+1);
		PrSt.Position := bit+1;
  	lbPrSt.Caption := 'Checking bit #' + IntToStr(bit+1);
		Update;
    if not (FindComponent('DG'+IntToStr(bit div (ALCTBoard.chips*ALCTBoard.delaylines))) as TCheckBox).Checked then
      continue;
		for i:=0 to 5000 do j:=i;
		WriteIR('11', V_IR);
		WRiteDR('4000', 16);

		// === Initial Reset to 0
		for i:= 0 to ALCTBoard.groups - 1 do
		begin
			for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
			begin
				SendPtrns[i][j].Value 		:= 0;
				SendPtrns[i][j].Pattern 	:= 0 or (Integer ((bit div 16) = ((i*6)+j)) shl (bit mod 16));
				ReadPtrns[i][j].Value 		:= 0;
				ReadPtrns[i][j].Pattern 	:= 0;
			end;
		end;

		SetDelayLines($7f, SendPtrns);

		Delay(5);
		if ReadPatterns(ReadPtrns) = Wires then
		begin
            ErrOnePass := CheckPatterns(SendPtrns, ReadPtrns);
            Errs := Errs + ErrOnePass;
            if ErrOnePass > 0 then
            begin
              Log.Lines.Add(TimeToStr(Now())+ '> Pass: #' + IntToStr(bit+1) + ' with ' + IntToStr(ErrOnePass) + ' Errors ');
              Log.Lines.Add(TimeToStr(Now())+ '> Possibly problem with Chip #' + IntToStr(bit div 16 + 1) + ' Channel #' + IntToStr(bit mod 16 + 1));
              if fStopOnError then
              begin
                sentstr := '';
                readstr := '';
                for i:= 0 to ALCTBoard.groups - 1 do
                begin
			            for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
		              begin
                    sentstr := IntToHex(SendPtrns[i][j].Pattern,4) + ' ' + sentstr;
                    readstr := IntToHex(ReadPtrns[i][j].Pattern,4) + ' ' + readstr;
                  end;
                end;
                Log.Lines.Add('->Sent: '+sentstr);
                Log.Lines.Add('<-Read: '+readstr);
                break;
              end;
            end;
			// Log.Lines.Add(TimeToStr(Now())+ '> Checking: ' + IntToStr(ErrOnePass) + ' Errors Found');
//            St.Caption :=  St.Caption + ' ... ' + IntToStr(ErrOnePass) + ' Errors Found';
		end
		else
		begin
			fStop := true;
			Log.Lines.Add(TimeToStr(Now())+ '> Pass #' + IntToStr(bit+1) + ': Reading Failure ');
		end;
		// === End Of Reset
	end;
  Log.Lines.EndUpdate;

		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := old;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Running ''1'' Test Finished with ' + IntToStr(Errs) + ' Errors ===');
        sbBar.SimpleText := 'Running ''1'' Test... Finished with ' + IntToStr(Errs) + ' Errors';
	end;
end;

procedure TForm1.Button8Click(Sender: TObject);
var
	old:	byte;
	i,j, bit:	Integer;
    Errs, ErrOnePass : Integer;
	SendPtrns, ReadPtrns: ALCTDelays;
	fStop:	Boolean;
begin
	if ActiveHW then
	begin
        Errs := 0;
        ErrOnePass := 0;
        PrSt.Max := 384;
        PrSt.Position := 0;
		fStop := false;
		Log.Lines.Add(TimeToStr(Now())+ '> === Running ''0'' Test ===');
    	sbBar.SimpleText := 'Running ''0'' Test';

		old := Chains.ItemIndex;
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := VRTX_CH;
			SetChainClick(self);
		end;

	for bit := 0 to 383 do
	begin
//    	Log.Lines.Add(TimeToStr(Now())+ '> ' +IntToStr(bit) + ': Group #' + IntToStr(i) + ' Chip #' + IntTostr(j) + ' Checking bit #' + IntToStr(bit mod 16));
//    	Log.Lines.Add(TimeToStr(Now())+ '> Checking bit #' + IntToStr(bit));
		PrSt.Position := bit+1;
        lbPrSt.Caption := 'Checking bit #' + IntToStr(bit+1);
        Update;
//        St.Caption := 'Checking bit #' + IntToStr(bit+1);
		WriteIR('11', V_IR);
		WRiteDR('4000', 16);

		// === Initial Reset to 0
		for i:= 0 to NUM_OF_DELAY_GROUPS - 1 do
		begin
			for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
			begin
				SendPtrns[i][j].Value 		:= 0;
				SendPtrns[i][j].Pattern 	:= $FFFF and (not(Integer ((bit div 16) = ((i*6)+j)) shl (bit mod 16)));
                // (not ( ($1 shl (bit mod 16)) and (Integer(  Boolean((bit div 16) = ((i*6)+j)) )) ));
				ReadPtrns[i][j].Value 		:= 0;
				ReadPtrns[i][j].Pattern 	:= 0;
			end;
		end;

		SetDelayLines($f, SendPtrns);

		if ReadPatterns(ReadPtrns) = Wires then
		begin
            ErrOnePass := CheckPatterns(SendPtrns, ReadPtrns);
            Errs := Errs + ErrOnePass;
            if ErrOnePass > 0 then
            Log.Lines.Add(TimeToStr(Now())+ '> Pass: #' + IntToStr(bit+1) + ' with ' + IntToStr(ErrOnePass) + ' Errors ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Checking: ' + IntToStr(ErrOnePass) + ' Errors Found');
            // sbBar.SimpleText :=  ' ... ' + IntToStr(ErrOnePass) + ' Errors Found';
		end
		else
		begin
			fStop := true;
			Log.Lines.Add(TimeToStr(Now())+ '> Pass #' + IntToStr(bit+1) + ': Reading Failure ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Reading Failure ');
		end;
		// === End Of Reset
	end;

		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := old;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Running ''0'' Test Finished with ' + IntToStr(Errs) + ' Errors ===');
        sbBar.SimpleText := 'Running ''0'' Test Finished with ' + IntToStr(Errs) + ' Errors';
	end;
end;

procedure TForm1.Button9Click(Sender: TObject);
var
	old:	byte;
	i,j,p, pass:	Integer;
    Errs, ErrOnePass : Integer;
	SendPtrns, ReadPtrns: ALCTDelays;
	fStop:	Boolean;
begin
	if ActiveHW then
	begin
		if NumPasses.ShowModal() = mrOk then
			pass := NumPasses.Value
        else
            pass := passDefault;

        Errs := 0;
        ErrOnePass := 0;

        PrSt.Max := pass*2;
		PrSt.Position := 0;
		fStop := false;
		Log.Lines.Add(TimeToStr(Now())+ '> === Shifting ''5'' and ''A'' Test ===');
    	sbBar.SimpleText := 'Shifting ''5'' and ''A'' Test';

		old := Chains.ItemIndex;
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := VRTX_CH;
			SetChainClick(self);
		end;

	for p := 0 to 2*pass-1 do
	begin
		PrSt.Position := p+1;
        lbPrSt.Caption := 'Pass #' + IntToStr((p div 2) + 1) + ' of ' +IntToStr(pass);;
        Update;
		WriteIR('11', V_IR);
		WRiteDR('4000', 16);

		// === Initial Reset to 0
		for i:= 0 to NUM_OF_DELAY_GROUPS - 1 do
		begin
			for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
			begin
				SendPtrns[i][j].Value 		:= 0;
				if Boolean ((p+1) mod 2) then
					SendPtrns[i][j].Pattern := $5555
				else
					SendPtrns[i][j].Pattern := $AAAA;
				//$FFFF and (not(Integer ((bit div 16) = ((i*6)+j)) shl (bit mod 16)));
                // (not ( ($1 shl (bit mod 16)) and (Integer(  Boolean((bit div 16) = ((i*6)+j)) )) ));
				ReadPtrns[i][j].Value 		:= 0;
				ReadPtrns[i][j].Pattern 	:= 0;
			end;
		end;

		SetDelayLines($f, SendPtrns);

		if ReadPatterns(ReadPtrns) = Wires then
		begin
            ErrOnePass := CheckPatterns(SendPtrns, ReadPtrns);
            Errs := Errs + ErrOnePass;
            if ErrOnePass > 0 then
            Log.Lines.Add(TimeToStr(Now())+ '> Pass: #' + IntToStr((p div 2) + 1) + ' with ' + IntToStr(ErrOnePass) + ' Errors ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Checking: ' + IntToStr(ErrOnePass) + ' Errors Found');
            // sbBar.SimpleText :=  ' ... ' + IntToStr(ErrOnePass) + ' Errors Found';
		end
		else
		begin
			fStop := true;
			Log.Lines.Add(TimeToStr(Now())+ '> Pass #' + IntToStr((p div 2) +1 ) + ': Reading Failure ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Reading Failure ');
		end;
		// === End Of Reset
	end;

		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := old;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Shifting ''5'' and ''A'' Test Finished with ' + IntToStr(Errs) + ' Errors ===');
        sbBar.SimpleText := 'Shifting ''5'' and ''A'' Test Finished with ' + IntToStr(Errs) + ' Errors';
	end;
end;


procedure TForm1.Button10Click(Sender: TObject);
var
	old:	byte;
	i,j, bit:	Integer;
    Errs, ErrOnePass : Integer;
	SendPtrns, ReadPtrns: ALCTDelays;
	fStop:	Boolean;
begin
	if ActiveHW then
	begin
        Errs := 0;
        ErrOnePass := 0;
        PrSt.Max := 384;
        PrSt.Position := 0;
		fStop := false;
		Log.Lines.Add(TimeToStr(Now())+ '> === Filling by ''1''s Test ===');
		sbBar.SimpleText := 'Filling ''1''s Test';

		old := Chains.ItemIndex;
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := VRTX_CH;
			SetChainClick(self);
		end;


		for i:= 0 to NUM_OF_DELAY_GROUPS - 1 do
		begin
			for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
			begin
				SendPtrns[i][j].Value 		:= 0;
				SendPtrns[i][j].Pattern 	:= 0;
				// (not ( ($1 shl (bit mod 16)) and (Integer(  Boolean((bit div 16) = ((i*6)+j)) )) ));
				ReadPtrns[i][j].Value 		:= 0;
				ReadPtrns[i][j].Pattern 	:= 0;
			end;
		end;

		SetDelayLines($f, SendPtrns);


	for bit := 0 to 383 do
	begin
		PrSt.Position := bit+1;
		lbPrSt.Caption := 'Moving ''1'' to position #' + IntToStr(bit+1);
        Update;

		WriteIR('11', V_IR);
		WRiteDR('4000', 16);

		// === Initial Reset to 0
		for i:= 0 to NUM_OF_DELAY_GROUPS - 1 do
		begin
			for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
			begin
				SendPtrns[i][j].Value 		:= 0;
				SendPtrns[i][j].Pattern 	:= SendPtrns[i][j].Pattern or ((Integer ((bit div 16) = ((i*6)+j)) shl (bit mod 16)));
                // (not ( ($1 shl (bit mod 16)) and (Integer(  Boolean((bit div 16) = ((i*6)+j)) )) ));
				ReadPtrns[i][j].Value 		:= 0;
				ReadPtrns[i][j].Pattern 	:= 0;
			end;
		end;

		SetDelayLines($f, SendPtrns);

		if ReadPatterns(ReadPtrns) = Wires then
		begin
            ErrOnePass := CheckPatterns(SendPtrns, ReadPtrns);
            Errs := Errs + ErrOnePass;
            if ErrOnePass > 0 then
            Log.Lines.Add(TimeToStr(Now())+ '> Setting Position: #' + IntToStr(bit+1) + ' with ' + IntToStr(ErrOnePass) + ' Errors ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Checking: ' + IntToStr(ErrOnePass) + ' Errors Found');
			// sbBar.SimpleText :=  ' ... ' + IntToStr(ErrOnePass) + ' Errors Found';
		end
		else
		begin
			fStop := true;
			Log.Lines.Add(TimeToStr(Now())+ '> Setting Position #' + IntToStr(bit+1) + ': Reading Failure ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Reading Failure ');
		end;
		// === End Of Reset
	end;

		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := old;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Filling by ''1''s Test Finished with ' + IntToStr(Errs) + ' Errors ===');
		sbBar.SimpleText := 'Filling by ''1''s Test Finished with ' + IntToStr(Errs) + ' Errors';
	end;
end;

procedure TForm1.Button11Click(Sender: TObject);
var
	old:	byte;
	i,j, bit:	Integer;
    Errs, ErrOnePass : Integer;
	SendPtrns, ReadPtrns: ALCTDelays;
	fStop:	Boolean;
begin
	if ActiveHW then
	begin
        Errs := 0;
        ErrOnePass := 0;
        PrSt.Max := 384;
        PrSt.Position := 0;
		fStop := false;
		Log.Lines.Add(TimeToStr(Now())+ '> === Filling by ''0''s Test ===');
		sbBar.SimpleText := 'Filling ''0''s Test';

		old := Chains.ItemIndex;
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := VRTX_CH;
			SetChainClick(self);
		end;


		for i:= 0 to NUM_OF_DELAY_GROUPS - 1 do
		begin
			for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
			begin
				SendPtrns[i][j].Value 		:= 0;
				SendPtrns[i][j].Pattern 	:= $FFFF;
				// (not ( ($1 shl (bit mod 16)) and (Integer(  Boolean((bit div 16) = ((i*6)+j)) )) ));
				ReadPtrns[i][j].Value 		:= 0;
				ReadPtrns[i][j].Pattern 	:= 0;
			end;
		end;

		SetDelayLines($f, SendPtrns);


	for bit := 0 to 383 do
	begin
		PrSt.Position := bit+1;
        lbPrSt.Caption := 'Moving ''0'' to position #' + IntToStr(bit+1);
        Update;

		WriteIR('11', V_IR);
		WRiteDR('4000', 16);

		// === Initial Reset to 0
		for i:= 0 to NUM_OF_DELAY_GROUPS - 1 do
		begin
			for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
			begin
				SendPtrns[i][j].Value 		:= 0;
				SendPtrns[i][j].Pattern 	:= SendPtrns[i][j].Pattern and (not(Integer ((bit div 16) = ((i*6)+j)) shl (bit mod 16)));
                // (not ( ($1 shl (bit mod 16)) and (Integer(  Boolean((bit div 16) = ((i*6)+j)) )) ));
				ReadPtrns[i][j].Value 		:= 0;
				ReadPtrns[i][j].Pattern 	:= 0;
			end;
		end;

		SetDelayLines($f, SendPtrns);

		if ReadPatterns(ReadPtrns) = Wires then
		begin
            ErrOnePass := CheckPatterns(SendPtrns, ReadPtrns);
            Errs := Errs + ErrOnePass;
            if ErrOnePass > 0 then
            Log.Lines.Add(TimeToStr(Now())+ '> Setting Position: #' + IntToStr(bit+1) + ' with ' + IntToStr(ErrOnePass) + ' Errors ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Checking: ' + IntToStr(ErrOnePass) + ' Errors Found');
            // sbBar.SimpleText :=  ' ... ' + IntToStr(ErrOnePass) + ' Errors Found';
		end
		else
		begin
			fStop := true;
			Log.Lines.Add(TimeToStr(Now())+ '> Setting Position #' + IntToStr(bit+1) + ': Reading Failure ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Reading Failure ');
		end;
		// === End Of Reset
	end;

		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := old;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Filling by ''0''s Test Finished with ' + IntToStr(Errs) + ' Errors ===');
        sbBar.SimpleText := 'Filling by ''0''s Test Finished with ' + IntToStr(Errs) + ' Errors';
	end;
end;

procedure TForm1.Button12Click(Sender: TObject);
var
	old:	byte;
	i,j,p, pass:	Integer;
    Errs, ErrOnePass : Integer;
	SendPtrns, ReadPtrns: ALCTDelays;
	fStop:	Boolean;
begin
	if ActiveHW then
	begin
        if NumPasses.ShowModal() = mrOk then
            pass := NumPasses.Value
        else
            pass := passDefault;
        Errs := 0;
		ErrOnePass := 0;
		PrSt.Max := pass;
		PrSt.Position := 0;
		fStop := false;
		Log.Lines.Add(TimeToStr(Now())+ '> === Random Data Test ===');
		sbBar.SimpleText := 'Random Data Test';

		old := Chains.ItemIndex;
		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := VRTX_CH;
			SetChainClick(self);
		end;

	for p := 0 to pass do
	begin
		PrSt.Position := p+1;
		lbPrSt.Caption := 'Pass #' + IntToStr(p) + ' of ' +IntToStr(pass);;
		Update;
		WriteIR('11', V_IR);
		WRiteDR('4000', 16);

		// === Initial Reset to 0
		for i:= 0 to NUM_OF_DELAY_GROUPS - 1 do
		begin
			for j:=0 to NUM_OF_DELAY_CHIPS_IN_GROUP - 1 do
			begin
				SendPtrns[i][j].Value 		:= 0;
				SendPtrns[i][j].Pattern := Random($FFFF);
				//$FFFF and (not(Integer ((bit div 16) = ((i*6)+j)) shl (bit mod 16)));
				// (not ( ($1 shl (bit mod 16)) and (Integer(  Boolean((bit div 16) = ((i*6)+j)) )) ));
				ReadPtrns[i][j].Value 		:= 0;
				ReadPtrns[i][j].Pattern 	:= 0;
			end;
		end;

		SetDelayLines($f, SendPtrns);

		if ReadPatterns(ReadPtrns) = Wires then
		begin
            ErrOnePass := CheckPatterns(SendPtrns, ReadPtrns);
			Errs := Errs + ErrOnePass;
            if ErrOnePass > 0 then
            Log.Lines.Add(TimeToStr(Now())+ '> Pass: #' + IntToStr(p) + ' with ' + IntToStr(ErrOnePass) + ' Errors ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Checking: ' + IntToStr(ErrOnePass) + ' Errors Found');
            // sbBar.SimpleText :=  ' ... ' + IntToStr(ErrOnePass) + ' Errors Found';
		end
		else
		begin
			fStop := true;
			Log.Lines.Add(TimeToStr(Now())+ '> Pass #' + IntToStr((p div 2) +1 ) + ': Reading Failure ');
//			Log.Lines.Add(TimeToStr(Now())+ '> Reading Failure ');
		end;
		// === End Of Reset
	end;

		if old <> VRTX_CH then
		begin
			Chains.ItemIndex := old;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Random Data Test Finished with ' + IntToStr(Errs) + ' Errors ===');
		sbBar.SimpleText := 'Random Data Test Finished with ' + IntToStr(Errs) + ' Errors';
	end;
end;

procedure TForm1.bThreadedClick(Sender: TObject);
var
	pass: Integer;
	test: Integer;

begin
	if ActiveHW then begin
    Log.Lines.EndUpdate;
		if Sender is TButton then
			test := TButton(Sender).Tag;

		case test of
		4..5:	if NumPasses.ShowModal() = mrOk then
					pass := NumPasses.Value
				else
					pass := passDefault;
		else;
				pass := Wires;
		end;

		ThreadsRunning := 1;
//		TestThread := TestThread.Create(Form1, test, pass);
//		TestThread := TTestThread.Create(Form1, test, pass);
//		TestThread.OnTerminate := ThreadDone;
//        TestThread.Priority := 10;
		with (TTestThread.Create(Form1, test, pass)) do
			OnTerminate := ThreadDone;
//		TestPages.Enabled := False;
		StdCmnds.Enabled := False;
		TabSheet1.Enabled := False;
//		bThreaded.Enabled := False;
//		TabSheet3.Enabled := False;
		TestSets.Enabled := False;
		btPause.Down := False;
		btPause.Enabled := True;
		btStop.Enabled :=True;
//		PrSt.Enabled := True;
	end;
end;

procedure TForm1.ThreadDone(Sender: TObject);
begin
  Dec(ThreadsRunning);
  if ThreadsRunning = 0 then
  begin
//	TestPages.Enabled := True;
	StdCmnds.Enabled := True;
	TabSheet1.Enabled := True;
	TestSets.Enabled := True;
	btPause.Down := False;
	btPause.Enabled := False;
	btStop.Enabled := False;
//    TestThread.Free;
//	bThreaded.Enabled := True;
//	TabSheet3.Enabled := True;
//	TestSets.Enabled := True;
  end;
end;

procedure TForm1.btPauseClick(Sender: TObject);
begin
//	if btPause.Down then TestThread.Resume
//	else	TestThread.Suspend;
end;

procedure TForm1.btStopClick(Sender: TObject);
begin
//	TestThread.Resume;
	TestThread.Terminate;
end;

procedure TForm1.btOneStepClick(Sender: TObject);
begin
//	fOneStep := btOneStep.Down;
end;

procedure TForm1.chStopErrClick(Sender: TObject);
begin
    chRepeat.Enabled := chStopErr.Checked;
    chKeyPress.Enabled := chRepeat.Enabled and chRepeat.Checked;
    fStopOnError := chStopErr.Checked;
end;

procedure TForm1.chRepeatClick(Sender: TObject);
begin
    fRepeatLast := chRepeat.Checked;
	chKeyPress.Enabled := chRepeat.Checked;
end;

procedure TForm1.chKeyPressClick(Sender: TObject);
begin
    fKeyPress := chKeyPress.Checked;
end;


procedure TForm1.btSCReadUserIDClick(Sender: TObject);
var
	data:   string;
	TestLogEntry: TListItem;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
        TestLog.Items.Clear;
		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Slow Control Firmware ID');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);
		data := ReadIDCodeStr(SLOW_CTL);
		ShowIDCode(data, SLOW_CTL);
		if StrToInt64('$'+data) = CTRL_SC_FPGA_ID  then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

	end;
end;

procedure TForm1.btTemperatureClick(Sender: TObject);
var
	value	: integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;

		Log.Lines.Add(TimeToStr(Now())+ '> === Read OnBoard Temperature');
		value := ReadTemperatureADC;
		Log.Lines.Add(TimeToStr(Now())+ '> OnBoard Temperature = '+ FloatToStrF(value*arTemperature.Coef-50, ffFixed,5,2) +'C (ADC=0x'+IntToHex(value, 3)+ ')');
	end;
end;

procedure TForm1.btReadAllVoltagesClick(Sender: TObject);
var
	ch, value	: integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;

		Log.Lines.Add(TimeToStr(Now())+ '> === Read Power Supply Voltages');
		for ch := 0 to 3 do
		begin
			value := ReadVoltageADC(ch);
			Log.Lines.Add(TimeToStr(Now())+ '> Voltage (Ref ' + arVoltages[ch].Ref + ') = ' + FloatToStrF(value*arVoltages[ch].Coef, ffFixed,7,3) +'V (ADC=0x'+IntToHex(value, 3)+ ')');
		end;
	end;
end;

procedure TForm1.btReadAllCurrentsClick(Sender: TObject);
var
	ch, value:   integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;

		Log.Lines.Add(TimeToStr(Now())+ '> === Read Power Supply Currents');
		for ch := 0 to 3 do
		begin
			value := ReadCurrentADC(ch);
			Log.Lines.Add(TimeToStr(Now())+ '> Current (for Ref ' + arCurrents[ch].Ref + ') = ' + FloatToStrF(value*arCurrents[ch].Coef, ffFixed,7,4) +'A (ADC=0x'+IntToHex(value, 3)+ ')');
		end;
	end;
end;

procedure TForm1.btReadAllThreshsClick(Sender: TObject);
const len = 11;
var
	data:   string;
	i,j, k, temp, tmp:   integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;

		Log.Lines.Add(TimeToStr(Now())+ '> === Read All Thresholds (Ref '+FloatToStrF(ADC_REF,ffFixed,3,1)+'V)');
		for j := 0 to NUM_AFEB-1 do
		begin
			temp := ReadThreshold(j);
			Log.Lines.Add('    AFEB #' +IntToStr(j)+ ' Threshold = ' + FloatToStrF((ADC_REF/1023)*temp, ffFixed,7,4)+ 'V (ADC='+ IntToStr(temp)+')');
		end;
	end;
end;

procedure TForm1.cbAllThreshsClick(Sender: TObject);
begin
	fAllThreshs := cbAllThreshs.Checked;
	seAFEBn.Enabled := not cbAllThreshs.Checked;
end;

procedure TForm1.btSetThreshsClick(Sender: TObject);
const len = 12;
var
	data:   string;
	i,j, k, temp, tmp:   integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;

		if fAllThreshs then
		begin
			Log.Lines.Add(TimeToStr(Now())+ '> === Set All Thresholds to '+ IntToStr(seAFEBval.Value));
			for j := 0 to NUM_AFEB-1 do
			begin
				SetThreshold(j, seAFEBval.Value);
			end;
		end
		else
			begin
			Log.Lines.Add(TimeToStr(Now())+ '> === Set Threshold for AFEB #' + IntToStr(seAFEBn.Value) + ' to ' + IntToStr(seAFEBval.Value));
			SetThreshold(seAFEBn.Value, seAFEBval.Value);
			end;
	end;
end;

procedure TForm1.btReadThreshClick(Sender: TObject);
const len = 11;
var
	data:   string;
	i,j, k, temp, tmp:   integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;

		if fAllThreshs then
		begin
			Log.Lines.Add(TimeToStr(Now())+ '> === Read All Thresholds (Ref '+FloatToStrF(ADC_REF,ffFixed,3,1)+'V)');
			for j := 0 to NUM_AFEB-1 do
			begin
				temp := ReadThreshold(j);
				Log.Lines.Add('    AFEB #' +IntToStr(j)+ ' Threshold = ' + FloatToStrF((ADC_REF/1023)*temp, ffFixed,7,4)+ 'V (ADC='+ IntToStr(temp)+')');
			end
		end
		else
		begin
			Log.Lines.Add(TimeToStr(Now())+ '> === Read Threshold for AFEB #' + IntToStr(seAFEBn.Value));
			temp := ReadThreshold(seAFEBn.Value);
			Log.Lines.Add('    AFEB #' +IntToStr(seAFEBn.Value)+ ' Threshold = ' + FloatToStrF((ADC_REF/1023)*temp, ffFixed,7,4)+ 'V (ADC='+ IntToStr(temp)+')');
		end;
	end;
end;

procedure TForm1.btSetTPPowerClick(Sender: TObject);
const len = 1;
var
	data:   string;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;

		SetTestPulsePower(Integer(cbTPPower.Checked));
		if cbTPPower.Checked then
			Log.Lines.Add(TimeToStr(Now())+ '> === Set Test Pulse Power On')
		else
			Log.Lines.Add(TimeToStr(Now())+ '> === Set Test Pulse Power Off');
	end;
end;

procedure TForm1.btSetTPPowerAmpClick(Sender: TObject);
const
	len = 9;
var
	i, temp	: integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;

		Log.Lines.Add(TimeToStr(Now())+ '> === Set Test Pulse Amplitude to '+IntToStr(seTPPowerAmp.Value));
        SetTestPulsePowerAmp(seTPPowerAmp.Value);
//		temp := 0;
//		for i:=0 to len-2 do temp := temp or (((seTPPowerAmp.Value shr i)  and $1) shl (7-i));

//        Log.Lines.Add(IntTOHex(temp, 2));
	end;
end;

procedure TForm1.btSetTPWireGroupClick(Sender: TObject);
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Set Test Pulse Wire Group Mask to 0x'+edTPWireGroupMask.Text );
		SetTestPulseWireGroupMask(StrToInt('$'+edTPWireGroupMask.Text));
	end;
end;

procedure TForm1.btSetTPStripLayerClick(Sender: TObject);
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Set Test Pulse Strip Layer Mask to 0x'+edTPStripLayerMask.Text );
		SetTestPulseStripLayerMask(StrToInt('$'+edTPStripLayerMask.Text) and $3f);
	end;
end;

procedure TForm1.SlowControlEprom1Click(Sender: TObject);
var
	FWFileOpen 	: TOpenDialog;
	StrList 	: TStringList;
	TmpStrList  : TStringList;
	InstrList	: TStringList;
	i,j, cnt	: Integer;
	tmpstr, cmd	: string;
	SVFFile 	: TFileStream;
	Data		: TStringStream;
	sz 			: Int64;
begin
	FWFileOpen := TOpenDialog.Create(self);
	StrList := TStringList.Create;
	TmpStrList := TStringList.Create;
	InstrList := TStringList.Create;
	with FWFileOpen do
	begin
	DefaultExt := 'svf';
	Filter := 'SVF Files (*.svf)|All Files (*.*)';
	FilterIndex := 1;
	Options := [ofPathMustExist, ofFileMustExist, ofViewDetail];
	if Execute then
	begin
		Log.Lines.Add(TimeToStr(Now())+ '> === Loading Firmware to Slow Control EPROM from File ' + Filename);
{		StrList.LoadFromFile(Filename);
//		Log.Lines.LoadFromFile(Filename);
		cnt := 0;
		tmpstr := '';
		cmd := '';
		for i:= 0 to StrList.Count-1 do
			if Pos( '//', Trim(StrList.Strings[i])) = 1  then
				begin
					tmpstr := Trim(StrList.Strings[i]);
					Delete(tmpstr, Pos( '//', tmpstr), 2);
					TmpStrList.Add(Trim(tmpstr));
				end
			else
			begin
				cmd := cmd + Trim(StrList.Strings[i]);
				if Pos(';', Trim(cmd)) <> 0 then
				begin
					for j:= Low(SVFInstrs) to High(SVFInstrs) do
						if Pos(SVFInstrs[j], Trim(cmd)) = 1 then
						begin
							InstrList.Add(Trim(cmd));
//							Log.Lines.Add(Trim(cmd));
							Inc(cnt);
						end;
					cmd := '';
				end
			end;
}
//		Log.Lines := InstrList;
//		Log.Lines := TmpStrList;
//		Log.Lines.Add('File size is ' + IntToStr(SVFLoad(Filename)) + ' bytes ' + IntToStr(StrList.Count) + ' lines ' + IntToStr(cnt)+ ' JTAG Instructions');
//		Log.Lines.Add('File size is ' + IntToStr(SVFLoad(Filename)) + ' bytes ');
		SVFFile := TFileStream.Create(string (FWFileOpen.Filename), fmOpenRead);
		sz := SVFFile.Size;
		Data := TStringStream.Create('');
		Data.CopyFrom(SVFFile, sz);
		Data.Seek(0, soFromBeginning);
		Log.Text := Log.Text + #$0d#$0a+ Data.ReadString(sz);
		Log.Lines.Add('File size is ' + IntToStr(sz) + ' bytes ');
		Data.Free;
		SVFFile.Free;

	end;
	Free;
	StrList.Free;
	TmpStrList.Free;
	InstrList.Free;
	end;
end;

procedure TForm1.btSetStandbyRegClick(Sender: TObject);
const len = 6;
var
	data:   string;
	i,j, k, temp, tmp:   integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
		WriteIR('24', SC_IR);
		StartDRShift;
		for i:=1 to 7 do
		begin
			data := IntToHex(StrToInt('$'+TEdit(FindComponent('edStndby'+IntToStr(i))).Text) and $3f, 2);
			Log.Lines.Add(TimeToStr(Now())+ '> === Set Standby Register for Group #'+ IntToStr(i)+ ' to 0x' +data );
			ShiftData(StrToInt('$'+data), len, i = 7);
		end;
        ExitDRSHift;
	end;
end;

procedure TForm1.btWriteFIFOClick(Sender: TObject);
const
    len = 16;
var
    i,j,k, cnt :  Integer;
    data: string;
    fifodata: Integer;
    numwords: Integer;
	  chan: Integer;
    start, finish: TDateTime;
    hour, min, sec, msec: word;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
		numwords := seNumWords.Value;
		chan := seChanNum.Value;
		cnt := 0;
		if numwords = 1 then fFIFOOneWord := true
		else fFIFOOneWord := false;

		if fFIFOall.Checked then
			cnt := NUM_AFEB-1;
		start := Now();
        Log.Lines.Add(TimeToStr(Now)+ '> FIFO -> Write '+ IntToStr(numwords)+ ' 16bits words');
		for j:=0 to cnt do
		begin
			if fFIFOall.Checked then
				chan := j;
			SetLength(dataFIFOwrite[chan], numwords);
			Random($FFFF);
			for i:=0 to numwords-1 do
			begin
				if Boolean ((i+1) mod 2) then
				begin
					case setupFIFOdata.ItemIndex of
					0: dataFIFOwrite[chan][i] := Random($FFFF);
					1: dataFIFOwrite[chan][i] := $5555;
					2: dataFIFOwrite[chan][i] := $AAAA;
					3: dataFIFOwrite[chan][i] := $FFFF;
					6: dataFIFOwrite[chan][i] := $FFFF and (not(Integer (1 shl (i mod 16))));
					end;
				end
				else dataFIFOwrite[chan][i] := $0000;

				case setupFIFOdata.ItemIndex of
				4: begin if Boolean ((i+1) mod 2) then
						dataFIFOwrite[chan][i] := $5555
					else
						dataFIFOwrite[chan][i] := $AAAA;
			        end;
                5: dataFIFOwrite[chan][i] := 0 or (Integer (1 shl (i mod 16)));
                end;
            end;

            data := IntToHex( ((numwords-1) and $1FFF) or (( chan and $3F) shl 13), 5);
//            Log.Lines.Add(' address 0x' + data);
		    Log.Lines.Add(TimeToStr(Now)+ '> FIFO -> Write '+ IntToStr(numwords)+ ' 16bits words for channel #' + IntToStr(chan));
		    // sbBar.SimpleText := ' address 0x' + data +'> FIFO -> Write '+ IntToStr(numwords)+ ' 16bits words for channel #' + IntToStr(chan);
		    WriteIR('1a', V_IR);
        WriteDR(data, 19);


		    for i := 0 to numwords-1 do
		    begin
          fifodata := dataFIFOwrite[chan][i];
          if cbSoftMapping.Checked then
            begin
              fifodata := 0;
              if boolean (chan mod 2) then
				      begin
					      for k:= 0 to 7 do
						      fifodata := fifodata or (((dataFIFOwrite[chan][i] shr k) and $01) shl (7 -k)) ;
					      fifodata := fifodata or (dataFIFOwrite[chan][i] and $ff00);
				      end
				      else
				      begin
					      for k:= 0 to 7 do
						      fifodata := fifodata or (((dataFIFOwrite[chan][i] shr (k+8)) and $01) shl (15 -k));
					      fifodata := fifodata or (dataFIFOwrite[chan][i] and $00ff);
				      end;
          end;
          WriteIR('18', V_IR);
          WriteDR(IntToHex(fifodata,4), len);
//                WriteIR('18', V_IR);
//                WriteDR(IntToHex(dataFIFOwrite[chan][i],4), len);
		    end;
        end;
        finish := Now();
        DecodeTime(finish-start, hour, min, sec, msec);
  		Log.Lines.Add(TimeToStr(finish)+ '> Finished in ' + FloatToStrF( (60*min+sec) + msec/1000 ,ffFixed, 7, 3)+ ' seconds');
	end;
end;

procedure TForm1.btReadFIFOClick(Sender: TObject);
const
    len = 16;
var
    i, j, cnt:  Integer;
    DR : array of byte;
    DRR: array of byte;
    tmpstr: string;
    data: string;
    numwords: Integer;

    chan: Integer;
    start, finish: TDateTime;
    hour, min, sec, msec: word;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 3 then
		begin
		Chains.ItemIndex := 3;
		SetChainClick(self);
		end;
        SetLength(DR, ((Wires - 1) div 8)+1);
        SetLength(DRR, ((Wires - 1) div 8)+1);
		numwords := seNumWords.Value;
		chan := seChanNum.Value;
		cnt := 0;
        start := Now();
        if fFIFOall.Checked then
        begin
            for j := 0 to NUM_AFEB-1 do
                SetLength(dataFIFOread[j], numwords);
            for i := 0 to numwords-1 do
            begin
        		WriteIR('11', V_IR);
		        WRiteDR('0001', 16);
                WriteIR('19', V_IR);
                Delay(5);
        		WriteIR('10', V_IR);
	        	if IOExchange(DR, DRR, Wires, DR_REG)<> Wires then
		        Log.Lines.Add('Reading Failure:');

		        tmpstr := '';
		        for j := Low(DRR) to High(DRR) do
		        begin
			        tmpstr := IntToHex(DRR[j],2 ) + tmpstr;
			        if Boolean (j mod 2) then tmpstr := ' ' + tmpstr;
		        end;
		        Log.Lines.Add(TimeToStr(Now())+ '> FIFO <- Read : word #' +IntToStr(i) + '; 0x' + tmpstr);
                for j := 0 to NUM_AFEB-1 do
    				dataFIFOread[j][i] := ((DRR[2*j+1] shl 8) or DRR[2*j]) and $ffff;
            end;
        end
        else
        begin
            SetLength(dataFIFOread[chan], numwords);
            data := IntToHex( ((numwords-1) and $1FFF) or (( chan and $3F) shl 13), 5);
            Log.Lines.Add(' address 0x' + data);
            start := Now();
            Log.Lines.Add(TimeToStr(start)+ '> FIFO <- Read '+ IntToStr(numwords)+ ' 16bits words from channel #' + IntToStr(chan));
            if fFIFOOneWord then
            begin
                WriteIR('1a', V_IR);
                WriteDR(data, 19);
                WriteIR('19', V_IR);
                WriteDR('0', len);
            end;

            WriteIR('1a', V_IR);
            WriteDR(data, 19);

            for i := 0 to numwords-1 do
            begin
                dataFIFOread[chan][i] := 0;
                WriteIR('19', V_IR);
                dataFIFOread[chan][i] := StrToInt('$' + ReadDR('0', len));
            end;
        end;
		finish := Now();
		DecodeTime(finish-start, hour, min, sec, msec);
		Log.Lines.Add(TimeToStr(finish)+ '> Finished in ' + FloatToStrF( (60*min+sec) + msec/1000 ,ffFixed, 7, 3)+ ' seconds');
	end;
end;

procedure TForm1.btCheckFIFOClick(Sender: TObject);
var
	i,j,cnt:  Integer;
	numwords: Integer;
    chan: Integer;
    errs: Integer;
    start, finish : TDateTime;
    len : Integer;
begin
    numwords := seNumWords.Value;
    chan := seChanNum.Value;
    errs := 0;
    start := Now();
	cnt := 0;
    if fFIFOall.Checked then
        cnt := NUM_AFEB-1;
//    start := Now();
    for j:=0 to cnt do
    begin
       if fFIFOall.Checked then
        chan := j;

    Log.Lines.Add(TimeToStr(start)+ '> FIFO Checking readback of '+ IntToStr(numwords)+ ' 16bits words from chip #' + IntToStr(chan));

    if Length(dataFIFOwrite[chan]) = 0 then
        Log.Lines.Add('  Error: Write data to FIFO chip #' + IntToStr(chan) + ' first' );
    if Length(dataFIFOread[chan]) = 0 then
        Log.Lines.Add('  Error: Read data from FIFO chip #' + IntToStr(chan) + ' first' );
    len := Max(Length(dataFIFOwrite[chan]),numwords);
    SetLength(dataFIFOwrite[chan], len);
    len := Max(Length(dataFIFOread[chan]),numwords);
    SetLength(dataFIFOread[chan], len);

    for i:= 0 to numwords-1 do
    begin
        if dataFIFOread[chan][i] <> dataFIFOwrite[chan][i] then
		begin
            Inc(errs);
            if ((not cbSuppressErrs.Checked) or (cbSuppressErrs.Checked and (errs <= seErrCnt.Value))) then
               Log.Lines.Add('   Error #' + IntToStr(errs) + ' at FIFO address ' + IntToStr(i) + ': Expected 0x' + IntToHex(dataFIFOwrite[chan][i],4) + ' Read 0x' + IntToHex(dataFIFOread[chan][i],4));
        end;
	end;
    end;
    if ((errs > seErrCnt.Value) and (cbSuppressErrs.Checked)) then
        Log.Lines.Add('   Error Output was suspended');

    finish := Now();
    Log.Lines.Add(TimeToStr(finish)+ '> Finished with ' + IntToStr(errs) + ' errors');
end;

procedure TForm1.Button18Click(Sender: TObject);
begin
    if ActiveHW then
	begin
		if Chains.ItemIndex <> 3 then
		begin
		Chains.ItemIndex := 3;
		SetChainClick(self);
		end;
 		WriteIR('11', V_IR);
        WriteDR('4000', 16);
	end;
end;

procedure TForm1.fFIFOallClick(Sender: TObject);
begin
    seChanNum.Enabled := not fFIFOall.Checked;
end;

procedure TForm1.cbSuppressErrsClick(Sender: TObject);
begin
    seErrCnt.Enabled := cbSuppressErrs.Checked;
end;

procedure TForm1.btFIFOTestClick(Sender: TObject);
var
    i: Integer;
    start, finish : TDateTime;
    hour, min, sec, msec: word;
begin
	if ActiveHW then
    begin
        start := Now();
        for i:=1 to seTestPassesFIFO.Value do
		begin
			Log.Lines.Add(TimeToStr(start)+ '> [+++] FIFO Test Pass #'+IntToStr(i));
			btWriteFIFOClick(self);
			btReadFIFOClick(self);
			btCheckFIFOClick(self);
        end;
        finish := Now();
        DecodeTime(finish-start, hour, min, sec, msec);

        Log.Lines.Add(TimeToStr(Now())+ '> [++++++] ' +IntToStr(seTestPassesFIFO.Value)+ ' Passes FIFO Test Finished in '+FloatToStrF( (60*min+sec) + msec/1000 ,ffFixed, 7, 3)+ ' seconds');
    end;
end;

procedure TForm1.ShowIDCode(idstr: string; alct_ctl: integer);
var
	id:	alct_idreg;
begin
 	StrToIDCode(idstr, id);
  case alct_ctl of
    SLOW_CTL:	Log.Lines.Add('Slow Control Firmware ID  - [' + idstr + ']');
    FAST_CTL: Log.Lines.Add('Fast Control Firmware ID  - [' + idstr + ']');
  end;
	Log.Lines.Add('Chip: ' + IntToStr(id.chip) + ' Version: ' + IntToStr(id.version) + ' DateTest: ' + IntToHex(id.month,2)+'/'+IntToHex(id.day,2)+'/'+IntToHex(id.year,4));
end;

procedure TForm1.ShowBoardSN(snstr: string; board_type: integer);
var
	sn:	string;
begin
  case (board_type) of
    BOARD_SN:	Log.Lines.Add('ALCT Board    Serial Number - ['+Copy(snstr,1,Length(snstr)-2)+']');
	  MEZAN_SN: Log.Lines.Add('Mezanine Card Serial Number - ['+Copy(snstr,1,Length(snstr)-2)+']');
  end;
end;

procedure TForm1.btDetectTypeClick(Sender: TObject);
var
	Err:    Integer;
	cmd, data: string;
begin
	if ActiveHW then
	begin
		Err := 0;
		if Chains.ItemIndex <> 2 then
		begin
			Chains.ItemIndex := 2;
			SetChainClick(self);
		end;
		cmd := '1fffff';
		WriteIR(cmd, PROG_V_IR_SIZE);
		cmd := '1ffeff';
		WriteIR(cmd, PROG_V_IR_SIZE);
		data := '0000000000';
		data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
		data := IntToStr(StrToInt64(data) and PROG_V_EPROM1_ID_MASK);
		if (StrToInt64(data) <> PROG_V_EPROM1_ID) and
                   (StrToInt64(data) <> PROG_V_EPROM1_ID2) then Inc(Err);
		Log.Lines.Add('data= '+data);

		cmd := '1fffff';
		WriteIR(cmd, PROG_V_IR_SIZE);
		cmd := '1ffffe';
		WriteIR(cmd, PROG_V_IR_SIZE);
		data := '0000000000';
		data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
		data := IntToStr(StrToInt64(data) and PROG_V_EPROM2_ID_MASK);
		if (StrToInt64(data) <> PROG_V_EPROM2_ID) and
                        (StrToInt64(data) <> PROG_V_EPROM2_ID2)  then Inc(Err);

		cmd := '1fffff';
		WriteIR(cmd, PROG_V_IR_SIZE);

		cmd := '09ffff';

		WriteIR(cmd, PROG_V_IR_SIZE);
		data := '0000000000';
		data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
		data := IntToStr(StrToInt64(data) and PROG_V_FPGA_ID_MASK);
		if StrToInt64(data) <> PROG_V_FPGA_ID  then Inc(Err);

		if not Boolean (Err) then
		begin
			Log.Lines.Add(TimeToStr(Now())+ '> Mezanine Board with Xilinx Virtex 1000 chip is detected');
			lbMezType.Caption := 'Virtex 1000';
			MezChipType := VIRTEX1000;
		end
		else
		begin
			Err := 0;
			cmd := '1fff';
			WriteIR(cmd, PROG_V_IR_SIZE-8);

			cmd := '1fff';
			WriteIR(cmd, PROG_V_IR_SIZE-8);
			cmd := '1ffe';
			WriteIR(cmd, PROG_V_IR_SIZE-8);
			data := '0000000000';
			data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
			data := IntToStr(StrToInt64(data) and PROG_V_EPROM2_ID_MASK);
			if (StrToInt64(data) <> PROG_V_EPROM2_ID) and
                                (StrToInt64(data) <> PROG_V_EPROM2_ID2)  then
				Inc(Err);


			cmd := '1fff';
			WriteIR(cmd, PROG_V_IR_SIZE-8);

			cmd := '09ff';

			WriteIR(cmd, PROG_V_IR_SIZE-8);
			data := '0000000000';
			data := '$'+ReadDR(data, PROG_V_ID_DR_SIZE);
			data := IntToStr(StrToInt64(data) and PROG_V_FPGA600_ID_MASK);
//			if (StrToInt64(data) <> PROG_V_FPGA600_ID) and (StrToInt64(data) <> PROG_V_FPGA600_7_ID) then
			if StrToInt64(data) <> PROG_V_FPGA600_ID then
				Inc(Err);
			if not Boolean (Err) then
			begin
				Log.Lines.Add(TimeToStr(Now())+ '> Mezanine Board with Xilinx Virtex 600 chip is detected');
				lbMezType.Caption := 'Virtex 600';
				MezChipType := VIRTEX600;
			end
			else
				begin
					Log.Lines.Add(TimeToStr(Now())+ '> Could not detect Mezanine Board');
					lbMezType.Caption := 'Unknown';
					MezChipType := UNKNOWN;
				end;
		end;
	end;
end;



procedure TForm1.btSCLoadFPGAClick(Sender: TObject);
const BitInfo: array[1..8] of string = ('BIT File',
									'Configuration Info',
									'# chips to program',
									'Filename',
									'Device type',
									'File Date',
									'File Time',
									'Bitstream size' );
type BInfo = (BITFILE, CONFIG, NUMCHIPS, FILENAM, DEVICE, FILEDATE, FILETIME, BITSIZE);
var
	bfiledlg	: TOpenDialog;
//	bfile		: TFileStream;
	bfile		: TMemoryStream;
	offs		: WORD;
	v, data		: BYTE;
	i,p, len	: longint;
	value		: string;
	Res, dlgRes : integer;
begin
	if ActiveHW then
	begin
	bfiledlg := TOpenDialog.Create(self);
	with bfiledlg do
	begin
		DefaultExt := 'bit';
		Filter := 'BIT Files (*.bit)';
		Options := [ofPathMustExist, ofFileMustExist, ofViewDetail];
		if Execute then
		begin
			Log.Lines.Add(TimeToStr(Now())+ '> Loading Slow Control FPGA');
			Info.RowCount := High(BitInfo);
			for i:=1 to Info.RowCount do
			begin
				Info.Cells[0,i-1] := BitInfo[i];
			end;
//			bfile := TFileStream.Create(string(Filename),fmOpenRead);
			bfile := TMemoryStream.Create;
			bfile.LoadFromFile(FileName);
			Info.Cells[1, Integer(BITFILE)] := Filename;
			Info.Cells[2, Integer(BITFILE)] := IntToStr(bfile.Size) +' bytes';
			bfile.Seek(0, soFromBeginning);

			bfile.Read(Offs,2);
			Offs := swap(Offs);
			SetLength(value, Offs);

			value := '';
			for p:= 1 to Offs do
			begin
				bfile.Read(v, 1);
				value := value + ' '+ IntToHex(v, 2);
			end;
			Info.Cells[1, Integer(CONFIG)] := value;
			Info.Cells[2, Integer(CONFIG)] := IntToStr(Offs) +' bytes';

			bfile.Read(Offs,2);
			Offs := swap(Offs);
			Info.Cells[1, Integer(NUMCHIPS)] := IntToStr(Offs);
			Info.Cells[2, Integer(NUMCHIPS)] := '1 byte';


			bfile.Read(v,1);
			assert( v = $61);

			bfile.Read(Offs, 2);
			Offs := swap(Offs);

			SetLength(value, Offs-1);
			value := '';
			for p:= 1 to Offs-1  do
			begin
				bfile.Read(v, 1);
				value := value + Chr(v);
			end;
			bfile.Read(v, 1);

			Info.Cells[1, Integer(FILENAM)] := string(value);
			Info.Cells[2, Integer(FILENAM)] := IntToStr(Offs) +' bytes';

			bfile.Read(v,1);
			assert( v = $62);

			bfile.Read(Offs, 2);
			Offs := swap(Offs);

			SetLength(value, Offs-1);
			value := '';
			for p:= 1 to Offs-1  do
			begin
				bfile.Read(v, 1);
				value := value + Chr(v);
			end;
			bfile.Read(v, 1);

			Info.Cells[1, Integer(DEVICE)] := string(value);
			Info.Cells[2, Integer(DEVICE)] := IntToStr(Offs) +' bytes';

			bfile.Read(v,1);
			assert( v = $63);

			bfile.Read(Offs, 2);
			Offs := swap(Offs);

			SetLength(value, Offs-1);
			value := '';
			for p:= 1 to Offs-1  do
			begin
				bfile.Read(v, 1);
				value := value + Chr(v);
			end;
			bfile.Read(v, 1);

			Info.Cells[1, Integer(FILEDATE)] := string(value);
			Info.Cells[2, Integer(FILEDATE)] := IntToStr(Offs) +' bytes';

			bfile.Read(v,1);
			assert( v = $64);

			bfile.Read(Offs, 2);
			Offs := swap(Offs);

			SetLength(value, Offs-1);
			value := '';
			for p:= 1 to Offs-1  do
			begin
				bfile.Read(v, 1);
				value := value + Chr(v);
			end;
			bfile.Read(v, 1);

			Info.Cells[1, Integer(FILETIME)] := string(value);
			Info.Cells[2, Integer(FILETIME)] := IntToStr(Offs) +' bytes';

			bfile.Read(v,1);
			assert( v = $65);
			bfile.Read(Offs, 2);
			Offs := swap(Offs);

			value := IntToHex(Offs, 4);
			bfile.Read(Offs, 2);
			Offs := swap(Offs);
			value := value + IntToHex(Offs, 4);
			len := StrToInt('$'+value);
			Info.Cells[2, Integer(BITSIZE)] := IntToStr(len) +' bytes';
//			Info.Cells[2, Integer(BITSIZE)] := '4 bytes';

			if Chains.ItemIndex <> 0 then
			begin
				Chains.ItemIndex := 0;
				SetChainClick(self);
			end;
			bfile.Free;
			bfile := nil;

			// Start Programming FPGA
			Res := 0;
			dlgRes := frOperProgress.doOper(5, FileName, true, false, Res);
			if dlgRes = mrOk then
				case Res of
				-1: Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
				0: 	Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Programmed');
				end;
		end;
		Free;
	end;
	end;
end;

procedure TForm1.btSCLoadEPROMClick(Sender: TObject);
var
	bfiledlg	: TOpenDialog;
	Res, dlgRes : integer;
begin
	if ActiveHW then
	begin
		bfiledlg := TOpenDialog.Create(self);
		with bfiledlg do
		begin
			DefaultExt := 'mcs';
			Filter := 'MCS Hex Files (*.mcs)';
			Options := [ofPathMustExist, ofFileMustExist, ofViewDetail];
			if Execute then
			begin
				if Chains.ItemIndex <> 0 then
				begin
					Chains.ItemIndex := 0;
					SetChainClick(self);
				end;
				Log.Lines.Add(TimeToStr(Now())+ '> === Loading Slow Control EPROM');
				Res := 0;
				dlgRes := frOperProgress.doOper(4, FileName, true, false, Res);
				if dlgRes = mrOk then
					case Res of
					-1: Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
					0: 	Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Programmed');
					end;
			end;
			Free;
		end;
	end;
end;

procedure TForm1.btSCReadEPROMIDClick(Sender: TObject);
var
	id: string;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 0 then
		begin
			Chains.ItemIndex := 0;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> Reading Slow Control EPROM ID Code');
		lbsceprom.Caption := '0x'+ SCReadEPROMID;
	end;
end;



procedure TForm1.btSCEraseEPROMClick(Sender: TObject);
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 0 then
		begin
			Chains.ItemIndex := 0;
			SetChainClick(self);
		end;
//		btSCReadIDClick(self);
		Log.Lines.Add(TimeToStr(Now())+ '> Erasing Slow Control EPROM');
		if SCEraseEPROM then
			Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Erased')
		else
			Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
	end;
end;

procedure TForm1.btSCReadFPGAIDClick(Sender: TObject);
var
	id: string;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 0 then
		begin
			Chains.ItemIndex := 0;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> Reading Slow Control FPGA ID Code');
		lbscfpga.Caption := '0x'+ SCReadFPGAID;
	end;
end;


procedure TForm1.btSCBlankCheckEPROMClick(Sender: TObject);
var
	dlgRes, Res	: Integer;
	errs: integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 0 then
		begin
			Chains.ItemIndex := 0;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Blank Check of Slow Control EPROM');
		Res := 0;
		dlgRes := frOperProgress.doOper(0, '', true, true, Res);
		if dlgRes = mrOk then
			case Res of
			-1: Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
			0: 	Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Blank');
			else
				Log.Lines.Add(TimeToStr(Now())+ '> EPROM is NOT Blank');
			end
		else
			if dlgRes = mrCancel then
			begin
				if Res = 0 then Log.Lines.Add(TimeToStr(Now())+ '> EPROM seems to be Blank');
				Log.Lines.Add(TimeToStr(Now())+ '> Operation Incomplete: Cancelled by User');
			end;
	end;
end;

procedure TForm1.btVReadFPGAIDClick(Sender: TObject);
var
	id: string;
begin
	if ActiveHW then
	begin
		btDetectTypeClick(self);
		if Chains.ItemIndex <> 2 then
		begin
			Chains.ItemIndex := 2;
			SetChainClick(self);
		end;

		if MezChipType = UNKNOWN then
		begin
			Log.Lines.Add(TimeToStr(Now())+ '> Error Reading Virtex FPGA ID Code: Unknown Mezanine Board');
			lbvfpga.Caption := '---';
		end
		else
		begin
			Log.Lines.Add(TimeToStr(Now())+ '> Reading Virtex FPGA ID Code');
			lbvfpga.Caption := '0x'+ IntToHex(StrToInt64('$'+VReadFPGAID) and PROG_V_FPGA_ID_MASK,9);
		end;
	end;
end;

procedure TForm1.btVReadEPROMIDClick(Sender: TObject);
var
	id: string;
begin
	if ActiveHW then
	begin
		btDetectTypeClick(self);
		if Chains.ItemIndex <> 2 then
		begin
			Chains.ItemIndex := 2;
			SetChainClick(self);
		end;

		if MezChipType = UNKNOWN then
		begin
			Log.Lines.Add(TimeToStr(Now())+ '> Error Reading Virtex EPROM ID Codes: Unknown Mezanine Board');
			lbveprom1.Caption := '---';
			lbveprom2.Caption := '---';
		end
        else
        begin
		    Log.Lines.Add(TimeToStr(Now())+ '> Reading Virtex EPROM #1 ID Code');
		    lbveprom1.Caption := '0x'+ IntToHex(StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM1_ID_MASK, 9);
            if MezChipType = VIRTEX1000 then
            begin
    		    Log.Lines.Add(TimeToStr(Now())+ '> Reading Virtex EPROM #2 ID Code');
	    	    lbveprom2.Caption := '0x'+ IntToHex(StrToInt64('$'+VReadEPROMID2)
                        and PROG_V_EPROM2_ID_MASK, 9);;
            end;
        end;
	end;
end;

procedure TForm1.btReadTPPowerClick(Sender: TObject);
const len = 1;
var
	value:   integer;
	status:	 string;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
		value := ReadTestPulsePower;
		if value < 0 then
			Log.Lines.Add(TimeToStr(Now())+ '> === Read Test Pulse Power Register Failure')
		else
		begin
			if value = 0 then status := 'Off'
			else status := 'On';
			Log.Lines.Add(TimeToStr(Now())+ '> === Read Test Pulse Power Register is '+ status);
		end;
	end;
end;
procedure TForm1.btReadTPWireGroupClick(Sender: TObject);
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Read Test Pulse Wire Group Mask is 0x'+ IntToHex(ReadTestPulseWireGroupMask, 2));
	end;
end;

procedure TForm1.btReadTPStripLayerClick(Sender: TObject);
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Read Test Pulse Strip Layer Mask is 0x'+ IntToHex(ReadTestPulseStripLayerMask,2));
	end;
end;

procedure TForm1.btReadStandbyRegClick(Sender: TObject);
const len = 42;
var
	data	: string;
	i		: Integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
		WriteIR('25', SC_IR);
		data := ReadDR('0',len);
//		Log.Lines.Add(TimeToStr(Now())+ '> === Read Test Pulse Standby Register is 0x' +data );
		for i:= 0 to 6 do
		Log.Lines.Add(TimeToStr(Now())+ '> === Read Test Pulse Standby Register for Group #'+ IntToStr(i+1)+ ' to 0x' +IntToHex((StrToInt64('$'+data)shr (i*6)) and $3F,2));

	end;
end;
procedure TForm1.Button30Click(Sender: TObject);
var
	data			: string;
	TestLogEntry	: TListItem;
	CurErrs, Errs 	: Integer;
	i, k, sendval,readval: Integer;
	diff 			: single;
begin
	if ActiveHW then
	begin
		CurErrs := 0;
		Errs   := 0;
		if Chains.ItemIndex <> 1 then
		begin
			Chains.ItemIndex := 1;
			SetChainClick(self);
		end;
		Log.Lines.Add(TimeToStr(Now())+ '> === Start Slow Control Self Test' );
		btSCReadUserIDClick(Self);
		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Thresholds');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);
		CurErrs := 0;
		for i:= 0 to NUM_AFEB-1 do
			SetThreshold(i, 0);
		for k:=0 to 5 do
		begin
			sendval := k*50;
			for i:=0 to NUM_AFEB-1 do
			begin
				SetThreshold(i, sendval);
        Delay(10);
				readval := ReadThreshold(i);
				if	abs(sendval*4 - readval) > ThreshToler then
				begin
					Inc(CurErrs);
					Log.Lines.Add('Error: Threshold for ch #' +IntToStr(i) + ' send=' +IntToStr(sendval*4)+ ' read=' + IntToStr(readval) + ' Difference more than ' + FloatToStrF(ThreshToler,ffFixed,5,0));
				end;
			end;
		end;
		if not Boolean(CurErrs) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Finished Thresholds Test with ' + IntToStr(CurErrs)+ ' errors');
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Standby Register');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);
		CurErrs := 0;
		for i:=0 to 6 do
		begin
			sendval := $30 or (i and $0f);
			SetGroupStandbyReg(i, sendval);
			readval := ReadGroupStandbyReg(i);
			if readval <> sendval then
			begin
				Inc(CurErrs);
				Log.Lines.Add('Error: Standby Register for Wire Group #' +IntToStr(i+1) + ' send=' +IntToHex(sendval,2)+ ' read=' + IntToHex(readval,2));
			end;

		end;

		if not Boolean(CurErrs) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Finished Standby Register Test with ' + IntToStr(CurErrs)+ ' errors');
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Test Pulse Power Down');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		CurErrs := 0;
		sendval := 0;
		SetTestPulsePower(sendval);
		readval := ReadTestPulsePower;
		if readval <> sendval then
		begin
			Inc(CurErrs);
			Log.Lines.Add('Error: Test Pulse Power Down send=' +IntToHex(sendval,2)+ ' read=' + IntToHex(readval,2));
		end;

		if not Boolean(CurErrs) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Finished Test Pulse Power Down Test with ' + IntToStr(CurErrs)+ ' errors');
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Test Pulse Power Up');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		CurErrs := 0;
		sendval := 0;
		SetTestPulsePower(sendval);
		readval := ReadTestPulsePower;
		if readval <> sendval then
		begin
			Inc(CurErrs);
			Log.Lines.Add('Error: Test Pulse Power Up send=' +IntToHex(sendval,2)+ ' read=' + IntToHex(readval,2));
		end;

		if not Boolean(CurErrs) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Finished Test Pulse Power Up Test with ' + IntToStr(CurErrs)+ ' errors');
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Test Pulse Wire Group Mask');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		CurErrs := 0;
		for sendval:=0 to $7F do
		begin
			SetTestPulseWireGroupMask(sendval);
			readval := ReadTestPulseWireGroupMask;
			if readval <> sendval then
			begin
				Inc(CurErrs);
				Log.Lines.Add('Error: Test Pulse Wire Group Mask send=' +IntToHex(sendval,2)+ ' read=' + IntToHex(readval,2));
			end;
		end;

		if not Boolean(CurErrs) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Finished Test Pulse Wire Group Mask Test with ' + IntToStr(CurErrs)+ ' errors');
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Test Pulse Strip Layer Mask');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		CurErrs := 0;
		for sendval:=0 to $3F do
		begin
			SetTestPulseWireGroupMask(sendval);
			readval := ReadTestPulseWireGroupMask;
			if readval <> sendval then
			begin
				Inc(CurErrs);
				Log.Lines.Add('Error: Test Pulse Strip Layer Mask send=' +IntToHex(sendval,2)+ ' read=' + IntToHex(readval,2));
			end;
		end;

		if not Boolean(CurErrs) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Finished Test Pulse Strip Layer Mask Test with ' + IntToStr(CurErrs)+ ' errors');
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

    SetStandbyReg('0'); // Turn Off All AFEBs


                if (ALCTBoard.alct = ALCT672 ) then
                begin
                arVoltages[0].RefVal := 1.70 ;
                arCurrents[0].RefVal := 0.94 ;
                arCurrents[1].RefVal := 2.81 ;
                end;
                if (ALCTBoard.alct = ALCT288 ) then
                begin
                arCurrents[0].RefVal := 0.58 ;
                arCurrents[1].RefVal := 1.28 ;
                end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Power Supply 1.8V Voltage');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		readval := ReadVoltageADC(0);
		if not (abs(readval*arVoltages[0].Coef-arVoltages[0].RefVal) > arVoltages[0].Toler) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Error: Voltage for 1.8V  read='+ FloatToStrF(readval*arVoltages[0].Coef, ffFixed,7,3) + 'V (' +  IntToStr(readval) +  ') Difference more than ' + FloatToStrF(arVoltages[0].Toler,ffFixed,5,2) + ' from reference of ' + FloatToStrF(arVoltages[0].RefVal,ffFixed,5,2));
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Power Supply 3.3V Voltage');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		readval := ReadVoltageADC(1);
		if not (abs(readval*arVoltages[1].Coef-arVoltages[1].RefVal) > arVoltages[1].Toler) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Error: Voltage for 3.3V  read='+ FloatToStrF(readval*arVoltages[1].Coef, ffFixed,7,3) + 'V (' +  IntToStr(readval) +  ') Difference more than ' + FloatToStrF(arVoltages[1].Toler,ffFixed,5,2) + ' from reference of ' + FloatToStrF(arVoltages[1].RefVal,ffFixed,5,2));
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Power Supply 5.5V (1) Voltage');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		readval := ReadVoltageADC(2);
		if not (abs(readval*arVoltages[2].Coef-arVoltages[2].RefVal) > arVoltages[2].Toler) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Error: Voltage for 5.5V (1)  read='+ FloatToStrF(readval*arVoltages[2].Coef, ffFixed,7,3) + 'V (' +  IntToStr(readval) +  ') Difference more than ' + FloatToStrF(arVoltages[2].Toler,ffFixed,5,2) + ' from reference of ' + FloatToStrF(arVoltages[2].RefVal,ffFixed,5,2));
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

                if not ( ALCTBoard.alct  = ALCT288 ) then
                begin
		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Power Supply 5.5V (2) Voltage');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		readval := ReadVoltageADC(3);
		if not (abs(readval*arVoltages[3].Coef-arVoltages[3].RefVal) > arVoltages[3].Toler) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Error: Voltage for 5.5V (2)  read='+ FloatToStrF(readval*arVoltages[3].Coef, ffFixed,7,3) + 'V (' +  IntToStr(readval) +  ') Difference more than ' + FloatToStrF(arVoltages[3].Toler,ffFixed,5,2) + ' from reference of ' + FloatToStrF(arVoltages[3].RefVal,ffFixed,5,2));
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;
                end;



		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Power Supply 1.8V Current');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		readval := ReadCurrentADC(0);
		if not (abs(readval*arCurrents[0].Coef-arCurrents[0].RefVal) > arCurrents[0].Toler) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Error: Current for 1.8V  read='+ FloatToStrF(readval*arCurrents[0].Coef, ffFixed,7,3) + 'A (' +  IntToStr(readval) +  ') Difference more than ' + FloatToStrF(arCurrents[0].Toler,ffFixed,5,2) + ' from reference of ' + FloatToStrF(arCurrents[0].RefVal,ffFixed,5,2));
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Power Supply 3.3V Current');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		readval := ReadCurrentADC(1);
                
		if not (abs(readval*arCurrents[1].Coef-arCurrents[1].RefVal) > arCurrents[1].Toler) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Error: Current for 3.3V  read='+ FloatToStrF(readval*arCurrents[1].Coef, ffFixed,7,3) + 'A (' +  IntToStr(readval) +  ') Difference more than ' + FloatToStrF(arCurrents[1].Toler,ffFixed,5,2) + ' from reference of ' + FloatToStrF(arCurrents[1].RefVal,ffFixed,5,2));
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Power Supply 5.5V (1) Current');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		readval := ReadCurrentADC(2);
		if not (abs(readval*arCurrents[2].Coef-arCurrents[2].RefVal) > arCurrents[2].Toler) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Error: Current for 5.5V (1)  read='+ FloatToStrF(readval*arCurrents[2].Coef, ffFixed,7,3) + 'A (' +  IntToStr(readval) +  ') Difference more than ' + FloatToStrF(arCurrents[2].Toler,ffFixed,5,2) + ' from reference of ' + FloatToStrF(arCurrents[2].RefVal,ffFixed,5,2));
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

                if not ( ALCTBoard.alct  = ALCT288 ) then
                begin
		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking Power Supply 5.5V (2) Current');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		readval := ReadCurrentADC(3);
		if not (abs(readval*arCurrents[3].Coef-arCurrents[3].RefVal) > arCurrents[3].Toler) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Error: Current for 5.5V (2)  read='+ FloatToStrF(readval*arCurrents[3].Coef, ffFixed,7,3) + 'A (' +  IntToStr(readval) +  ') Difference more than ' + FloatToStrF(arCurrents[3].Toler,ffFixed,5,2) + ' from reference of ' + FloatToStrF(arCurrents[2].RefVal,ffFixed,5,2));
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;
                end;

		TestLogEntry := TestLog.Items.Add;
		TestLog.Items[TestLog.Items.Count-1].Caption := IntToStr(TestLog.Items.Count);
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Checking On Board Temperature');
		TestLog.Items[TestLog.Items.Count-1].SubItems.Add('Start');
		TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testStart);

		readval := ReadTemperatureADC;
		if not (abs((readval*arTemperature.Coef-50)- arTemperature.RefVal) > arTemperature.Toler) then
		begin
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Passed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testOK);
		end
		else
		begin
			Log.Lines.Add('Error: On Board Temperature read='+ FloatToStrF(readval*arTemperature.Coef-50, ffFixed,7,3) + ' C (' +  IntToStr(readval) +  ') Difference more than ' + FloatToStrF(arTemperature.Toler,ffFixed,5,2) + ' from reference of ' + FloatToStrF(arTemperature.RefVal,ffFixed,5,2));
			Inc(Errs);
			TestLog.Items[TestLog.Items.Count-1].SubItems[1] := 'Failed';
			TestLog.Items[TestLog.Items.Count-1].SubItemImages[1] := Integer(testFail);
		end;

		if Errs>0 then
			Log.Lines.Add(TimeToStr(Now())+ '> === Slow Control Self Test Failed with ' + IntToStr(Errs) + ' Errors' )
		else
			Log.Lines.Add(TimeToStr(Now())+ '> === Slow Control Self Test Finished Without Errors' );
	end;
end;

procedure TForm1.ClearExecute(Sender: TObject);
begin
	TestLog.Items.Clear;
end;



procedure TForm1.cbALCTTypeChange(Sender: TObject);
begin
	if cbALCTType.ItemIndex <> chamb_table[cbChamberType.ItemIndex].alct then
//		sbBar.SimpleText := 'Warning: Incorrect ALCT Type for this Chamber'
    Log.Lines.Add('Warning: Incorrect ALCT Type for this Chamber')
	else
    begin
      ALCTBoard := alct_table[cbALCTType.ItemIndex];
      ChangeALCTType;
//		sbBar.SimpleText := 'ALCT Type is Changed'
      Log.Lines.Add('ALCT Type is changed to ' + ALCTBoard.name);
    end;
end;

procedure TForm1.cbChamberTypeChange(Sender: TObject);
begin
    Chamber := chamb_table[cbChamberType.ItemIndex];
    Log.Lines.Add('Chamber Type is changed to ' + Chamber.name);
    ALCTBoard := alct_table[Chamber.alct];
    ChangeALCTType;
	if cbALCTType.ItemIndex <> Chamber.alct then
	begin
//    cbALCTType.Style := csDropDown;
		cbALCTType.ItemIndex := Chamber.alct;
//    cbALCTType.Style := csDropDownList;
		// sbBar.SimpleText := 'ALCT Type is Changed due Changed Chamber Type';
	end;
end;

procedure TForm1.btTimerClick(Sender: TObject);
begin
	if ActiveHW then
	begin
		Timer1.Enabled := not Timer1.Enabled;
		if Timer1.Enabled then
		begin
			btTimer.Caption := 'Stop';
			sbBar.SimpleText := 'Timer Started';
			VATChart.BottomAxis.Automatic := false;
			VATChart.BottomAxis.Maximum := Now + EncodeTime(tbTimeScale.Position div 60,tbTimeScale.Position mod 60,0,0);
			VATChart.BottomAxis.Minimum := Now;

		end
		else
		begin
			btTimer.Caption := 'Start';
			sbBar.SimpleText := 'Timer Stopped';
		end;
	end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
	i: Integer;
	t: TDateTime;
begin
	with VATChart do
	begin
		t:= Now;
		if Chains.ItemIndex <> 1 then
		begin
			Chains.ItemIndex := 1;
			SetChainClick(self);
		end;
{	   Series[0].Add(ReadVoltageADC(V18),'',clTeeColor);
	   Series[1].Add(ReadVoltageADC(V33),'',clTeeColor);
	   Series[2].Add(ReadVoltageADC(V55_1),'',clTeeColor);
	   Series[3].Add(ReadVoltageADC(V55_2),'',clTeeColor);
	   Series[4].Add(ReadCurrentADC(V18),'',clTeeColor);
	   Series[5].Add(ReadCurrentADC(V33),'',clTeeColor);
	   Series[6].Add(ReadCurrentADC(V55_1),'',clTeeColor);
	   Series[7].Add(ReadCurrentADC(V55_2),'',clTeeColor);
	   Series[8].Add(ReadTemperatureADC,'',clTeeColor);
}
	   Series[0].AddXY(t,ReadVoltageADC(V18),'',clTeeColor);
	   Series[1].AddXY(t,ReadVoltageADC(V33),'',clTeeColor);
	   Series[2].AddXY(t,ReadVoltageADC(V55_1),'',clTeeColor);
	   Series[3].AddXY(t,ReadVoltageADC(V55_2),'',clTeeColor);
	   Series[4].AddXY(t,ReadCurrentADC(V18),'',clTeeColor);
	   Series[5].AddXY(t,ReadCurrentADC(V33),'',clTeeColor);
	   Series[6].AddXY(t,ReadCurrentADC(V55_1),'',clTeeColor);
	   Series[7].AddXY(t,ReadCurrentADC(V55_2),'',clTeeColor);
	   Series[8].AddXY(t,ReadTemperatureADC,'',clTeeColor);
	   if (Now+EncodeTime(seRI.Value div 3600, seRI.Value div 60, seRI.Value mod 60,0) >= BottomAxis.Maximum) then
		   BottomAxis.Maximum := Now+EncodeTime(seRI.Value div 3600, seRI.Value div 60, seRI.Value mod 60,0)
	   else
		   BottomAxis.Maximum := BottomAxis.Minimum + EncodeTime(tbTimeScale.Position div 60,tbTimeScale.Position mod 60,0,0);
	   BottomAxis.Minimum := BottomAxis.Maximum - EncodeTime(tbTimeScale.Position div 60,tbTimeScale.Position mod 60,0,0);
	end;

end;

procedure TForm1.seRIClick(Sender: TObject);
begin
	Timer1.Interval := 1000*seRI.Value;
end;

procedure TForm1.Button13Click(Sender: TObject);
var
	j	: Integer;
begin
	if ActiveHW then
	begin
		PassCnt := 0;
		Timer2.Enabled := not Timer2.Enabled;
		if Timer2.Enabled then
		begin
			Button13.Caption := 'Stop';
			Log.Lines.Add(TimeToStr(Now())+ '> Thresholds Test Started (Check Visually with Oscilloscope)');

			TouchFIFO(seTCH.Value);
			if Chains.ItemIndex <> 1 then
			begin
				Chains.ItemIndex := 1;
				SetChain(1);
			end;
			// Setting Thresholds to 200
			for j := 0 to NUM_AFEB-1 do
			begin
				SetThreshold(j, 200);
			end;
			SetTestPulseWireGroupMask($7F);
			SetTestPulseStripLayerMask($3f);
			SetStandbyReg('0');
			SetTestPulsePower(0);
			SetTestPulsePowerAmp(255);
			SetTestPulsePower(1);
			Log.Lines.Add('Input Channel #'+IntToStr(seTCh.Value)+' is selected');
		end
		else
		begin
			Button13.Caption := 'Go';
			Log.Lines.Add(TimeToStr(Now())+ '> Thresholds Test Stopped');
		end;
	end;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
var
	i	: longint;
  grpMask : Integer;
begin

	Inc(PassCnt);

  if Chains.ItemIndex <> 1 then
	begin
		Chains.ItemIndex := 1;
		SetChain(1);
	end;

  if (PassCnt > 4) then
  begin
    if seTCH.Value < (NUM_AFEB div 2) then
      grpmask := seTCH.Value div 3
    else
      grpmask := (seTCH.Value - NUM_AFEB div 2) div 3;
//    SetTestPulseWireGroupMask((not (1 shl (seTCH.Value div 6))) and $7f);
    SetTestPulseWireGroupMask((not (1 shl grpmask)) and $7f);
    if (ReadTestPulseWireGroupMask <> ((not (1 shl grpmask)) and $7f)) then
    begin
      Log.Lines.Add('ERROR: Test Stopped -> Couldn''t set Test Pulse Wire Group Mask');
      Button13Click(self);
    end;
  end
  else
  begin
    SetTestPulseWireGroupMask($7f);
  end;

	if PassCnt > 8 then
	begin
		if cbLoop.Checked then
		begin
			if Chains.ItemIndex <> 1 then
			begin
				Chains.ItemIndex := 1;
				SetChain(1);
			end;

			SetStandbyForChan(seTCh.Value, false);
			if seTCh.Value = seTCh.Max then
				begin
					seTCh.Value := seTCh.Min;
					Button13Click(self);
				end
			else
			begin
				seTCh.Value := seTCh.Value + 1;
				Log.Lines.Add('Input Channel #'+IntToStr(seTCh.Value)+' is selected');
			end;
		end;
		PassCnt := 0;
//		seChanNum.Value := seTCh.Value;
//		btFIFOTestClick(self);
	end;
	if Chains.ItemIndex <> 1 then
	begin
		Chains.ItemIndex := 1;
		SetChain(1);
	end;
	SetStandbyForChan(seTCh.Value, false);
	SetStandbyForChan(seTCh.Value, true);
	SetThreshold(seTCh.Value, (PassCnt mod 2)*255);
    Delay(10);
	Label46.Caption := IntToStr(ReadThreshold(seTCh.Value));
	TouchFIFO(seTCh.Value);
//	btFIFOTestClick(self);
end;

procedure TForm1.seTChChanged(Sender: TObject; NewValue: Integer);
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
			Chains.ItemIndex := 1;
			SetChain(1);
		end;
		SetStandbyReg('0');
//		seChanNum.Value := seTCh.Value;
		SetStandbyForChan(seTCh.Value, true);
		TouchFIFO(seTCh.Value);
//		btFIFOTestClick(self);
	end;
end;


procedure TForm1.Button14Click(Sender: TObject);
var
	Hour, Min, Sec, MSec: Word;
	i,k :	longint;
	t1, t2: TDateTime;
begin
//	DecodeTime(Time);
	k:=1;
	t1:= Time;
	Delay(1, tSec);
	t2:= Time;
	Log.Lines.Add(TimeToStr(t2));
	DecodeTime(t2-t1, hour, min, sec, msec);
	Log.Lines.Add(IntToStr(sec)+ ':' + IntToStr(msec));
//	Log.Lines.Add(Chains.Items.Strings[1]);
//	Log.Lines.Add(AdjustStrSize(Edit9.Text, 32));
//	Log.Lines.Add(ReverseStream(Edit9.Text,31));
end;

procedure TForm1.Button15Click(Sender: TObject);
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
		SetStandbyForChan(seTCh.Value,cbStandby.Checked);
	end;
end;

procedure TForm1.ToolButton1Click(Sender: TObject);
begin
	if ActiveHW then
	begin
		if (iStatus = iStepOverWaiting) then
		begin
			iStatus := iRunning;
		end;
		if (iStatus <> iStopped) then exit;
  		iStatus := iRunning;

		try
			Memo2.Clear;
			ps.SetText(Memo1.Text);
			if ps.ErrorCode = ENoError then begin
				AddLine('=== Script is running.');
				ps.RunScript;
			end;
			if ps.ErrorCode = ENoError then begin
				AddLine('=== Script finished without errors.');
			end else begin
				AddLine('=== Error in ' + ps.ErrorModule + '(' + inttostr(ps.ErrorPos) + ') ' + ErrorToString(ps.ErrorCode, ps.ErrorString));
			Memo1.SelStart := ps.ErrorPos;
		end;
		finally
			iStatus := iStopped;
		end;
  		ps.Cleanup;
	end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  try
	ps.Free;
  except
	ShowMessage('Error ???');
  end;

end;

procedure TForm1.btNewScriptClick(Sender: TObject);
begin
	if not SaveTest then exit;
	Memo1.Clear;
	Memo1.Lines.Text := 'Program Script;'#13#10'Begin'#13#10'End.';
	fn := '';
end;

procedure TForm1.btScriptLoadClick(Sender: TObject);
var
	ScriptOpenDlg : TOpenDialog;
begin
	if not SaveTest then exit;
	ScriptOpenDlg := TOpenDialog.Create(self);
	with ScriptOpenDlg do
	begin
		DefaultExt := 'ifs';
		Filter := 'Script Files (*.ifs)| Text Files (*.txt)| All Files (*.*)';
		FilterIndex := 1;
		Options := [ofPathMustExist, ofFileMustExist, ofViewDetail];
		if Execute then
		begin
			Memo1.Lines.LoadFromFile(FileName);
			changed := False;
			Memo2.Lines.Clear;
			Log.Lines.Add(TimeToStr(Now())+ '> Script Loaded from: '+FileName);
			fn := FileName;
		end;
		Free;
	end;
end;

procedure TForm1.btScriptSaveAsClick(Sender: TObject);
var
	ScriptSaveDlg	: TSaveDialog;
begin
	ScriptSaveDlg := TSaveDialog.Create(self);
	with ScriptSaveDlg do
	begin
		DefaultExt := 'ifs';
		Filter := 'Script Files (*.ifs)| Text Files (*.txt)| All Files (*.*)';
		FilterIndex := 1;
		Options := [ofPathMustExist];
		FileName := '';
		if Execute then
		begin
			fn := FileName;
			Memo1.Lines.SaveToFile(fn);
			Log.Lines.Add(TimeToStr(Now())+ '> Script Saved to: '+FileName);
			changed := False;
		end;
	end;
end;

procedure TForm1.btScriptSaveClick(Sender: TObject);
begin
  if fn = '' then begin
	btScriptSaveAsClick(nil);
  end else begin
	Memo1.Lines.SaveToFile(fn);
	changed := False;
  end;
end;

function TForm1.SaveTest: Boolean;
begin
  if changed then begin
	case MessageDlg(fn + ' Script File is not saved, save now?', mtWarning, mbYesNoCancel, 0) of
	  mrYes: begin
		  btScriptSaveClick(nil);
		  Result := not changed;
		end;
	  mrNo: Result := True;
	else
	  Result := False;
	end;
  end else
	Result := True;
end;


procedure TForm1.Memo1Change(Sender: TObject);
begin
	changed := true;
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := SaveTest;
end;

procedure TForm1.ToolButton3Click(Sender: TObject);
begin
	Memo2.Clear;
	AddLine('Pre-Defined Functions:');
	AddLine('--------------------------');
	AddLine('SetChain(chan) - Set JTAG X-Blaster Channel (SetChain(0))');
	AddLine('WriteIR(data, size) - Write to JTAG IR Register Send Instruction (WriteIR(''FF'',16))');
	AddLine('WriteDR(data, size) - Write to JTAG DR Register Send Data (WriteDR(''FF'',16))');
	AddLine('ReadDR(data, size) - Read from JTAG DR Register Read Data (ReadDR(''FF'',16))');
	AddLine('Log(string) - Write string to Log window (Log(''Log Entry''))');
	AddLine('WriteLn(string) - Write string to Output window (WriteLn(''Some Text''))');
	AddLine('Delay(msec) - Insert Delay in Milliseconds (Delay(1000))');
	AddLine('--------------------------');
	AddLine('Delphi Pascal syntax');
	AddLine('All functions are not case-sensitive');

{
	'WriteDR(data, size)'#13#10
	'ReadDR(data, size)'#13#10
	'WriteLn(string)'#13#10
	'Log(string)'#13#10;
}
end;

procedure TForm1.ToolButton6Click(Sender: TObject);
begin
	if iStatus <> iStopped then iStatus := iStopped;
end;

procedure TForm1.ToolButton5Click(Sender: TObject);
begin
	if ActiveHW then
	begin
		if (iStatus = iStepOverWaiting) then begin
			iStatus := iStepOver;
		end;
		if (iStatus <> iStopped) then exit;
		iStatus := iStepOver;
		try
			Memo2.Clear;
			ps.SetText(Memo1.Text);
			if ps.ErrorCode = ENoError then begin
			  AddLine('=== Script is running.');
			  ps.RunScript;
			end;
			if ps.ErrorCode = ENoError then begin
			  AddLine('=== Script finished without errors.');
			end else begin
			  AddLine('=== Error in ' + ps.ErrorModule + '(' + inttostr(ps.ErrorPos) + ') ' + ErrorToString(ps.ErrorCode, ps.ErrorString));
			  Memo1.SelStart := ps.ErrorPos;
			end;
		finally
			iStatus := iStopped;
		end;
		ps.Cleanup;
	end;
end;


procedure TForm1.tbTimeScaleChange(Sender: TObject);
begin
	Label42.Caption := 'Time Scale: '+IntToStr(tbTimeScale.Position div 60)+'h '+IntToStr(tbTimeScale.Position mod 60) + 'min';
end;



procedure TForm1.btVEraseEPROMClick(Sender: TObject);
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 2 then
		begin
			Chains.ItemIndex := 2;
			SetChainClick(self);
		end;
		btDetectTypeClick(self);
		Log.Lines.Add(TimeToStr(Now())+ '> Erasing Virtex EPROMs');
		case MezChipType of
		VIRTEX1000:
					begin
						if VEraseEPROM1 then
							Log.Lines.Add(TimeToStr(Now())+ '> Virtex EPROM #1 is Erased')
						else
							Log.Lines.Add(TimeToStr(Now())+ '> Virtex EPROM #1 ID Check Error');
                        if VEraseEPROM2 then
							Log.Lines.Add(TimeToStr(Now())+ '> Virtex EPROM #2 is Erased')
						else
							Log.Lines.Add(TimeToStr(Now())+ '> Virtex EPROM #2 ID Check Error');
					end;
		VIRTEX600:
					begin
						if V600EraseEPROM then
							Log.Lines.Add(TimeToStr(Now())+ '> Virtex EPROM is Erased')
						else
							Log.Lines.Add(TimeToStr(Now())+ '> Virtex EPROM ID Check Error');
					end;
		else
			Log.Lines.Add(TimeToStr(Now())+ '> Error Erasing Virtex EPROMs: Unknown Mezanine Board');
		end;
	end;
end;

procedure TForm1.btVBlankCheckEPROMClick(Sender: TObject);
var
	dlgRes, Res	: Integer;
	errs: integer;
begin
	if ActiveHW then
	begin
		btDetectTypeClick(self);
		if Chains.ItemIndex <> 2 then
		begin
			Chains.ItemIndex := 2;
			SetChainClick(self);
		end;

		case MezChipType of
		VIRTEX1000: begin
						Log.Lines.Add(TimeToStr(Now())+ '> === Blank Check of Virtex 1000 EPROM #1');
						Res := 0;
						dlgRes := frOperProgress.doOper(1, '', true, true, Res);
						if dlgRes = mrOk then
						begin
							case Res of
							-1: Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
							0: 	Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Blank');
							else
								Log.Lines.Add(TimeToStr(Now())+ '> EPROM is NOT Blank');
							end;
						end
						else
						begin
						if dlgRes = mrCancel then
						begin
							if Res = 0 then Log.Lines.Add(TimeToStr(Now())+ '> EPROM seems to be Blank');
							Log.Lines.Add(TimeToStr(Now())+ '> Operation Incomplete: Cancelled by User');
						end;
						end;

						Log.Lines.Add(TimeToStr(Now())+ '> === Blank Check of Virtex 1000 EPROM #2');
						Res := 0;
						dlgRes := frOperProgress.doOper(2, '', true, true, Res);
						if dlgRes = mrOk then
						begin
							case Res of
							-1: Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
							0: 	Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Blank');
							else
								Log.Lines.Add(TimeToStr(Now())+ '> EPROM is NOT Blank');
							end;
						end
						else
						begin
							if dlgRes = mrCancel then
							begin
								if Res = 0 then Log.Lines.Add(TimeToStr(Now())+ '> EPROM seems to be Blank');
								Log.Lines.Add(TimeToStr(Now())+ '> Operation Incomplete: Cancelled by User');
							end;
						end;
					end;
		VIRTEX600:  begin
						Log.Lines.Add(TimeToStr(Now())+ '> === Blank Check of Virtex 600 EPROM');
						Res := 0;
						dlgRes := frOperProgress.doOper(3, '', true, true, Res);
						if dlgRes = mrOk then
						begin
							case Res of
							-1: Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
							0: 	Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Blank');
							else
								Log.Lines.Add(TimeToStr(Now())+ '> EPROM is NOT Blank');
							end;
						end
						else
						if dlgRes = mrCancel then
						begin
							if Res = 0 then Log.Lines.Add(TimeToStr(Now())+ '> EPROM seems to be Blank');
							Log.Lines.Add(TimeToStr(Now())+ '> Operation Incomplete: Cancelled by User');
						end;
					end;
		else
					Log.Lines.Add(TimeToStr(Now())+ '> Error Blank Check of Virtex EPROMs: Unknown Mezanine Board');
		end;
	end;
end;




procedure TForm1.btVLoadEPROMClick(Sender: TObject);
var
	bfiledlg	: TOpenDialog;
	Res, dlgRes : integer;
begin
	if ActiveHW then
	begin
		bfiledlg := TOpenDialog.Create(self);
		with bfiledlg do
		begin
			DefaultExt := 'mcs';
			Filter := 'MCS Hex Files (*.mcs)';
			Options := [ofPathMustExist, ofFileMustExist, ofViewDetail];
			btDetectTypeClick(self);
			if Chains.ItemIndex <> 2 then
			begin
				Chains.ItemIndex := 2;
				SetChainClick(self);
			end;
			case MezChipType of
			VIRTEX1000: begin
							Title := 'Open Virtex 1000 EPROM #1 Firmware file';
							if Execute then
							begin
								Log.Lines.Add(TimeToStr(Now())+ '> Loading Virtex 1000 EPROM #1');
								Res := 0;
								dlgRes := frOperProgress.doOper(8, FileName, true, false, Res);
								if dlgRes = mrOk then
									case Res of
									-1: Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
									0: 	Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Programmed');
								end;
							end;

							Title := 'Open Virtex 1000 EPROM #2 Firmware file';
							if Execute then
							begin
								Log.Lines.Add(TimeToStr(Now())+ '> Loading Virtex 1000 EPROM #2');
								Res := 0;
								dlgRes := frOperProgress.doOper(9, FileName, true, false, Res);
								if dlgRes = mrOk then
									case Res of
									-1: Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
									0: 	Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Programmed');
								end;
							end;

						end;
			VIRTEX600: begin
							Title := 'Open Virtex 600 EPROM Firmware file';
							if Execute then
							begin
								Log.Lines.Add(TimeToStr(Now())+ '> Loading Virtex 600 EPROM');
								Res := 0;
								dlgRes := frOperProgress.doOper(6, FileName, true, false, Res);
								if dlgRes = mrOk then
									case Res of
									-1: Log.Lines.Add(TimeToStr(Now())+ '> EPROM ID Check Error');
									0: 	Log.Lines.Add(TimeToStr(Now())+ '> EPROM is Programmed');
								end;
							end;
					   end;
			else
					Log.Lines.Add(TimeToStr(Now())+ '> Error: Could not detect Mezanine Board')
			end;
			Free;
		end;
	end;
end;

procedure TForm1.btVLoadFPGAClick(Sender: TObject);
const BitInfo: array[1..8] of string = ('BIT File',
									'Configuration Info',
									'# chips to program',
									'Filename',
									'Device type',
									'File Date',
									'File Time',
									'Bitstream size' );
type BInfo = (BITFILE, CONFIG, NUMCHIPS, FILENAM, DEVICE, FILEDATE, FILETIME, BITSIZE);
var
	bfiledlg	: TOpenDialog;
//	bfile		: TFileStream;
	bfile		: TMemoryStream;
	offs		: WORD;
	v, data		: BYTE;
	i,p, len	: longint;
	value		: string;
	fStart		: boolean;
	FPGAdevice		: string;
	Res, dlgRes : integer;
begin
	if ActiveHW then
	begin
		fStart := false;
		if Chains.ItemIndex <> 2 then
		begin
			Chains.ItemIndex := 2;
			SetChainClick(self);
		end;
		btDetectTypeClick(self);
		bfiledlg := TOpenDialog.Create(self);
		with bfiledlg do
		begin
			DefaultExt := 'bit';
			Filter := 'BIT Files (*.bit)';
			Options := [ofPathMustExist, ofFileMustExist, ofViewDetail];
			if Execute then
			begin
				Log.Lines.Add(TimeToStr(Now())+ '> Loading Virtex FPGA');
				Info.RowCount := High(BitInfo);
				for i:=1 to Info.RowCount do
				begin
					Info.Cells[0,i-1] := BitInfo[i];
				end;
//				bfile := TFileStream.Create(string(Filename),fmOpenRead);
				bfile := TMemoryStream.Create;
				bfile.LoadFromFile(Filename);
				Info.Cells[1, Integer(BITFILE)] := Filename;
				Info.Cells[2, Integer(BITFILE)] := IntToStr(bfile.Size) +' bytes';
				bfile.Seek(0, soFromBeginning);
				bfile.Read(Offs,2);
				Offs := swap(Offs);
				SetLength(value, Offs);
				value := '';
				for p:= 1 to Offs do
				begin
					bfile.Read(v, 1);
					value := value + ' '+ IntToHex(v, 2);
				end;
				Info.Cells[1, Integer(CONFIG)] := value;
				Info.Cells[2, Integer(CONFIG)] := IntToStr(Offs) +' bytes';

				bfile.Read(Offs,2);
				Offs := swap(Offs);
				Info.Cells[1, Integer(NUMCHIPS)] := IntToStr(Offs);
				Info.Cells[2, Integer(NUMCHIPS)] := '1 byte';

				bfile.Read(v,1);
				assert( v = $61);
				bfile.Read(Offs, 2);
				Offs := swap(Offs);
				SetLength(value, Offs-1);
				value := '';
				for p:= 1 to Offs-1  do
				begin
					bfile.Read(v, 1);
					value := value + Chr(v);
				end;
				bfile.Read(v, 1);

				Info.Cells[1, Integer(FILENAM)] := string(value);
				Info.Cells[2, Integer(FILENAM)] := IntToStr(Offs) +' bytes';

				bfile.Read(v,1);
				assert( v = $62);

				bfile.Read(Offs, 2);
				Offs := swap(Offs);

				SetLength(value, Offs-1);
				value := '';
				for p:= 1 to Offs-1  do
				begin
					bfile.Read(v, 1);
					value := value + Chr(v);
				end;
				bfile.Read(v, 1);

				Info.Cells[1, Integer(DEVICE)] := string(value);
				FPGAdevice := LowerCase(string(value));
				Info.Cells[2, Integer(DEVICE)] := IntToStr(Offs) +' bytes';

				bfile.Read(v,1);
				assert( v = $63);

				bfile.Read(Offs, 2);
				Offs := swap(Offs);

				SetLength(value, Offs-1);
				value := '';
				for p:= 1 to Offs-1  do
				begin
					bfile.Read(v, 1);
					value := value + Chr(v);
				end;
				bfile.Read(v, 1);

				Info.Cells[1, Integer(FILEDATE)] := string(value);
				Info.Cells[2, Integer(FILEDATE)] := IntToStr(Offs) +' bytes';

				bfile.Read(v,1);
				assert( v = $64);

				bfile.Read(Offs, 2);
				Offs := swap(Offs);

				SetLength(value, Offs-1);
				value := '';

				for p:= 1 to Offs-1  do
				begin
					bfile.Read(v, 1);
					value := value + Chr(v);
				end;
				bfile.Read(v, 1);

				Info.Cells[1, Integer(FILETIME)] := string(value);
				Info.Cells[2, Integer(FILETIME)] := IntToStr(Offs) +' bytes';

				bfile.Read(v,1);
				assert( v = $65);
				bfile.Read(Offs, 2);
				Offs := swap(Offs);

				value := IntToHex(Offs, 4);
				bfile.Read(Offs, 2);
				Offs := swap(Offs);
				value := value + IntToHex(Offs, 4);
				len := StrToInt('$'+value);
				Info.Cells[2, Integer(BITSIZE)] := IntToStr(len) +' bytes';
//				Info.Cells[2, Integer(BITSIZE)] := '4 bytes';

				bfile.Free;
				// Start Programming FPGA
				case MezChipType of
				VIRTEX1000: begin
								if Pos('v1000efg680', FPGAdevice) > 0 then
								begin
									Res := 0;
									dlgRes := frOperProgress.doOper(10, FileName, true, false, Res);
									if dlgRes = mrOk then
										case Res of
										-1: Log.Lines.Add(TimeToStr(Now())+ '> Virtex 1000 FPGA ID Check Error');
										0: 	Log.Lines.Add(TimeToStr(Now())+ '> Virtex 1000 FPGA is Programmed');
										end;
								end
								else
									Log.Lines.Add(TimeToStr(Now())+ '> Error: Firmware file is NOT for Virtex 1000E FPGA Chip');
							end;
				VIRTEX600: 	begin
								if Pos('v600efg680', FPGAdevice) > 0 then
								begin
									Res := 0;
									dlgRes := frOperProgress.doOper(7, FileName, true, false, Res);
									if dlgRes = mrOk then
										case Res of
										-1: Log.Lines.Add(TimeToStr(Now())+ '> Virtex 600 FPGA ID Check Error');
										0: 	Log.Lines.Add(TimeToStr(Now())+ '> Virtex 600 FPGA is Programmed');
										end;
								end
								else
									Log.Lines.Add(TimeToStr(Now())+ '> Error: Firmware file is NOT for Virtex 600E FPGA Chip');
							end;
				else
					Log.Lines.Add(TimeToStr(Now())+ '> Error: Could not detect Mezanine Board')
				end;
				// Finish
				end;
			Free;
		end;
	end;
end;

procedure TForm1.Button19Click(Sender: TObject);
var
	CReg:	string;
	OpenCfgFile	: TOpenDialog;
	CfgParams	: TStringList;
	i,j	: integer;
	fStartScan: boolean;
	Param:	string;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 2 then
		begin
			Chains.ItemIndex := 2;
			SetChainClick(self);
		end;
		fStartScan := false;
		CReg := ReadRegister(RdCfg);
		Log.Lines.Add(TimeToStr(Now())+ '> Read Virtex Configuration Register - 0x'+CReg);
		OpenCfgFile := TOpenDialog.Create(self);
		CfgParams := TStringList.Create;
		with OpenCfgFile do
		begin
			DefaultExt := 'file';
			Filter := 'All Files (*.*)';
			Options := [ofPathMustExist, ofFileMustExist, ofViewDetail];
			if Execute then
			begin
				CfgParams.LoadFromFile(Filename);
				for i:=0 to CfgParams.Count-1 do
				begin
					if Pos('</alct_fast>', LowerCase(Trim(CfgParams.Strings[i]))) = 1 then
					begin
						fStartScan := false;
						Log.Lines.Add('End of Configuration Section');
					end;
					if fStartScan then
					begin
						Param := Trim(LowerCase( Copy(Trim(CfgParams.Strings[i]),1, FirstDelimiter(' =',Trim(CfgParams.Strings[i]) ) ) ));
						for j:=0 to 24 do
							if SameText(Param, CRfld[j].name) then
								begin
									Log.Lines.Add(Param);
								end;
					end;
					if Pos('<alct_fast>', LowerCase(Trim(CfgParams.Strings[i]))) = 1 then
					begin
						fStartScan := true;
						Log.Lines.Add('Start of Configuration Section');
					end;
				end;
			end;
			Free;
		end;
		CfgParams.Free;
		CfgParams := nil;
	end;
end;

procedure TForm1.Button20Click(Sender: TObject);
var
	cr: array of longword;
	os: array of longword;
	i,j,k : integer;
	crstr: string;
	osstr: string;
	dlys: ALCTDelays;
	image : TPtrnImage;
    hfa, h, hn, hpatb, lfa, l, ln, lpatb, daq : longword;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
		crstr := ReadRegister(RdCfg);
		Log.Lines.Add('Configuration Register: 0x'+crstr);
		SetLength(cr,3);
		for i:=1 to sizeof(longword)*Length(cr)*2-Length(crstr) do crstr := '0'+crstr;
		for i:=0 to 2 do
		begin
			cr[i] := StrToInt('$'+Copy(crstr, 1+(2-i)*8,8));
		end;
		cr[0] := (cr[0] and (not($1f shl 26))) or ($10 shl 26);
		crstr := '';
		for i:=0 to 2 do
		 crstr := IntToHex(cr[i],8) + crstr;
		WriteRegister(WrCfg, crstr);
		crstr := ReadRegister(RdCfg);
		Log.Lines.Add('Configuration Register: 0x'+crstr);

		for i:=0 to 3 do
		begin
			for j:=0 to 5 do
			begin
				dlys[i][j].Value := 5;
				dlys[i][j].Pattern := 0;
			end;
		end;

		WriteIR(InputDisable,V_IR);
		Write6DelayLines(dlys[0], $f);
		WriteIR(InputEnable,V_IR);
		WriteIR(InputDisable,V_IR);


		image[0][0] := $1;
		image[1][0] := $1;
		image[2][0] := $2;
		image[3][0] := $4;
		image[4][0] := $4;
		image[5][0] := $8;

		image[0][7] := $1;
		image[1][7] := $1;
		image[2][7] := $2;
		image[3][7] := $4;
		image[4][7] := $4;
		image[5][7] := $8;

		PrepareDelayLinePatterns(dlys, image);
		Write6DelayLines(dlys[0], $1);
		Write6DelayLines(dlys[1], $2);
		Write6DelayLines(dlys[2], $4);
		Write6DelayLines(dlys[3], $8);

		WriteRegister(YRWrite, longword(1));
		WriteIR(InputEnable,V_IR);
		WriteIR(InputDisable, V_IR);
		WriteRegister(YRWrite, longword(0));

		SetLength(os,2);
		for i:= 0 to 10 do
		begin
			OSstr := ReadRegister(OSRead);

			for k:=1 to sizeof(longword)*Length(os)*2-Length(osstr) do osstr := '0'+osstr;
			os[0] := StrToInt('$'+Copy(osstr, 9,8));
			os[1] := StrToInt('$'+Copy(osstr, 1,8));
			Log.Lines.Add(IntToHex(os[0], 8) + ' ' + IntToHex(os[1], 8));

			hfa := (os[1] shr 15) and $1;
			h   := (os[1] shr 13) and $3;
			hn  := (os[1] shr 7) and $3f;
			hpatb:= (os[1] shr 6) and $1;

			lfa := (os[1] shr 5) and $1;
			l   := (os[1] shr 3) and $3;
			ln  := ((os[1] shl 3) and $38) or ((os[0] shr 29) and $7);
			lpatb:= (os[0] shr 28) and $1;

			daq := (os[0] shr 9) and $7ffff;
			Log.Lines.Add('i = '+IntToStr(i)+' hfa:'+IntToStr(hfa)+' h:'+IntToStr(h)+' hn:'+IntToStr(hn)+' hpatb:'+IntToStr(hpatb)+' lfa:'+IntToStr(lfa)+' l:'+IntToStr(l)+' ln:'+IntToStr(ln)+' lpatb:'+IntToStr(lpatb));
			if Boolean((os[1] shr 16) and $1) then break;

			Delay(100, tMSec);
		end;

	end;
end;

procedure TForm1.Button21Click(Sender: TObject);
var
	ch: DelayGroup;
	i,j : integer;
	r : longword;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;

			for j:=0 to 5 do
			begin
				ch[j].Value := $5;
				ch[j].Pattern := $FFFF * (j and $1);
			end;

		Log.Lines.Add('Read Back ParamReg - 0x' + ReadRegister(ParamRegRead));
		Write6DelayLines(ch, $f);
{		r:=$3f and (not($f shl 2));
		WriteRegister(ParamRegWrite, r);
		Log.Lines.Add('Read Back ParamReg - 0x' + ReadRegister(ParamRegRead));
		WriteIR(Wdly, V_IR);
		StartDRShift;
		for i:=0 to 5 do
		begin
			Log.Lines.Add(IntToStr(i)+ ': Dly - '+IntToStr(ShiftData(Longword(FlipHalfByte(ch[0][i].Value)), 4, false)));
			Log.Lines.Add(IntToStr(i)+ ': Ptn - '+IntToHex(ShiftData(Longword(ch[0][i].Pattern), 16, i=5),4));
		end;
		ExitDRShift;
		r:=$3f;
		WriteRegister(ParamRegWrite, r);
}
		Log.Lines.Add('Read Back ParamReg - 0x' + ReadRegister(ParamRegRead));
	end;
end;

procedure TForm1.Button22Click(Sender: TObject);
var
  i: integer;
  ser: TLineSeries;
  NumSeries: Integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
//    ser := TLineSeries.Create();
		PassCnt2 :=0;
    DACChart.RemoveAllSeries;
//		DACChart.Series[0].Clear;
//		DACChart.Series[1].Clear;
    NumSeries := 2;
    if cbAllThreshChans.Checked then
      NumSeries := NUM_AFEB; //ALCTBoard.chips*ALCTBoard.groups;

    for i:=0 to ALCTBoard.groups*ALCTBoard.chips-1 do      //Iav
      SetThreshold(i,0);                                //Iav

    for i:=0 to NumSeries-1 do
    begin
        DACChart.AddSeries(TLineSeries.Create(self));
        DACChart.Series[i].SeriesColor := Random(16777216);
        DACChart.Series[i].Title := 'Thresh #'+IntToStr(i);
    end;
		Timer3.Enabled := true;

	end;
end;

procedure TForm1.Timer3Timer(Sender: TObject);
var
	i: integer;
	ch1, ch2: integer;
begin
	seChartCh1.Enabled := false;
	seChartCh2.Enabled := false;
  cbAllThreshChans.Enabled := false;

  if not cbAllThreshChans.Checked then
  begin
	ch1 := seChartCh1.Value;
	ch2 := seChartCh2.Value;

	with DACChart do
	begin
		Series[0].Title := 'Thresh Ch#'+IntToStr(ch1);
		Series[1].Title := 'Thresh Ch#'+IntToStr(ch2);
		   SetThreshold(ch1,PassCnt2);
		   SetThreshold(ch2,PassCnt2);
		   Series[0].AddXY(PassCnt2,ReadThreshold(ch1),'',clTeeColor);
		   Series[1].AddXY(PassCnt2,ReadThreshold(ch2),'',clTeeColor);
		   sbBar.SimpleText := 'DAC: ' + IntToStr(PassCnt2);
	end;
  end
  else
  begin
    with DACChart do
    begin
      for i:=0 to ALCTBoard.groups*ALCTBoard.chips-1 do
      begin
         SetThreshold(i,PassCnt2);
         Series[i].AddXY(PassCnt2,ReadThreshold(i),'',clTeeColor);
         sbBar.SimpleText := 'DAC: ' + IntToStr(PassCnt2);
      end;
    end;
  end;
	Inc(PassCnt2);
	if PassCnt2 > 255 then
	begin
	Timer3.Enabled := false;
	seChartCh1.Enabled := true;
	seChartCh2.Enabled := true;
  cbAllThreshChans.Enabled := true;
	sbBar.SimpleText := '';
    end;
end;

procedure TForm1.TestPagesPageChanging(Sender: TObject; NewPage: TTabSheet;
  var AllowChange: Boolean);
begin
	if (NewPage = TabSheet9) then
	begin
		TeeCommander1.Parent := NewPage;
		TeeCommander1.Panel := DACChart;
	end;
	if (NewPage = TabSheet6) then
	begin
		TeeCommander1.Parent := NewPage;
		TeeCommander1.Panel := VATChart;
	end;
	if (NewPage = TabSheet10) then
	begin
		TeeCommander1.Parent := NewPage;
		TeeCommander1.Panel := CurrChart;
	end;
	if (NewPage = TabSheet12) then
	begin
		TeeCommander1.Parent := DelaysPanel;
		TeeCommander1.Panel := DelaysChart;
	end;

end;    


procedure TForm1.Button23Click(Sender: TObject);
var
	j : integer;
begin
	if ActiveHW then
	begin

		PassCnt := 0;
		Timer4.Enabled := not Timer4.Enabled;
		if Timer4.Enabled then
		begin
			if Chains.ItemIndex <> 1 then
			begin
				Chains.ItemIndex := 1;
				SetChainClick(self);
			end;
			for j := 0 to NUM_AFEB-1 do
			begin
				SetThreshold(j, 200);
			end;
			SetTestPulseWireGroupMask($7F);
			SetTestPulseStripLayerMask($3f);
			SetStandbyReg('3ffffffffff');
			SetTestPulsePower(0);
			SetTestPulsePowerAmp(255);
			SetTestPulsePower(1);

			if Chains.ItemIndex <> 3 then
			begin
				Chains.ItemIndex := 3;
				SetChainClick(self);
			end;

			WriteIR('11', V_IR);
			WRiteDR('4002', 16);
			Log.Lines.Add(TimeToStr(Now())+ '> Delays Test Started (Check Visually with oscilloscope)');
			Button23.Caption := 'Stop';
		end
		else
		begin
			WriteIR('11', V_IR);
			WRiteDR('4000', 16);
			Log.Lines.Add(TimeToStr(Now())+ '> Delays Test Stopped');
			Button23.Caption := 'Delays';
		end
	end;
end;

procedure TForm1.Timer4Timer(Sender: TObject);
var
	i	: longint;
	dlys: DelayGroup;
begin
	if PassCnt > 15 then PassCnt := 0;
	for i:=0 to 5 do
	begin
		if cbDlyOneStep.Checked then
			dlys[i].Value := PassCnt
		else
			dlys[i].Value := 15*(PassCnt mod 2);
		dlys[i].Pattern := 0;
	end;
	lbDelay.Caption := IntToStr(dlys[0].Value);
  Write6DelayLines(dlys, $7f);

  WriteIR(IntToHex(ParamRegWrite,2), V_IR);
  WriteDR('1fd', parlen);
//	WriteRegister(ParamRegWrite, $3d);
	if cbDlyLoop.Checked then
		Inc(PassCnt);
end;

procedure TForm1.edTPWireGroupMaskKeyPress(Sender: TObject; var Key: Char);
begin
	 if not(Key in ['0'..'9','a'..'f','A'..'F',#08]) then Key := #0;
end;

procedure TForm1.SpinEdit6Changed(Sender: TObject; NewValue: Integer);
begin
	Timer5.Interval := SpinEdit6.Value;
end;

procedure TForm1.SpinEdit4Changed(Sender: TObject; NewValue: Integer);
begin
	Timer2.Interval := SpinEdit4.Value;
	Timer4.Interval := SpinEdit4.Value;
end;

procedure TForm1.btDelaysLoopClick(Sender: TObject);
begin
	if ActiveHW then
	begin
		Timer5.Enabled := not Timer5.Enabled;
		if Timer5.Enabled then
		begin
			btDelaysLoop.Caption := 'Stop';
			Log.Lines.Add(TimeToStr(Now())+ '> Delay Chips Loop Test Started');
		end
		else
		begin
			btDelaysLoop.Caption := 'Start';
			Log.Lines.Add(TimeToStr(Now())+ '> Delay Chips Loop Test Stopped');
		end;
	end;
end;

procedure TForm1.Timer5Timer(Sender: TObject);
begin
	if ActiveHW then
	begin
		if cbWriteLoop.Checked then
			btSetDelaysChipsClick(self);
		if cbReadLoop.Checked then
			ReadPtrnsClick(self);
		if cbCheckPtrnsLoop.Checked then
			CheckPtrnsClick(self);
	end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  i: integer;
  ser: TLineSeries;
  NumSeries: Integer;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 1 then
		begin
		Chains.ItemIndex := 1;
		SetChainClick(self);
		end;
//    ser := TLineSeries.Create();
		PassCnt :=0;
  	for i := 0 to NUM_AFEB-1 do
    begin
	 		SetThreshold(i, 0);
		end;

    if cbGroupMaskEffect.Checked then
		  SetTestPulseWireGroupMask($7F)
    else
      SetTestPulseWireGroupMask($0);

    if cbStripMaskEffect.Checked then
  		SetTestPulseStripLayerMask($3f)
    else
      SetTestPulseStripLayerMask($0);

    if cbTestPulseOnEffect.Checked then
  		SetTestPulsePower(1)
    else
      SetTestPulsePower(0);

		SetTestPulsePowerAmp(0);

    SetStandbyReg('0');
    Delay(1000);

    with CurrChart do
    begin
      RemoveAllSeries;
//      BottomAxis.Minimum := 0;
//      BottomAxis.Maximum := NUM_AFEB-1;
//		DACChart.Series[0].Clear;
//		DACChart.Series[1].Clear;
      NumSeries := 2;
      for i:=0 to NumSeries-1 do
      begin
        AddSeries(TLineSeries.Create(self));
//        Series[i].SeriesColor := Random(16777216);
        Series[i].Title := 'Current for 5.5V ('+IntToStr(i+1)+')';
      end;
      AddSeries(TBarSeries.Create(self));
//      Series[2].SeriesColor := Random(16777216);
      Series[2].Title := 'AFEB Current #1';
      AddSeries(TBarSeries.Create(self));
//      Series[3].SeriesColor := Random(16777216);
      Series[3].Title := 'AFEB Current #2';

      CurrOffset55_1 := ReadCurrent(V55_1);;
      Delay(20);
      while (abs(CurrOffset55_1-ReadCurrent(V55_1))>0.001) do
      begin
        CurrOffset55_1 := ReadCurrent(V55_1);
        Delay(20);
      end;

      CurrOffset55_2 := ReadCurrent(V55_2);;
      Delay(50);
      while (abs(CurrOffset55_2-ReadCurrent(V55_2))>0.001) do
      begin
        CurrOffset55_2 := ReadCurrent(V55_2);
        Delay(50);
      end;

      BottomAxis.Automatic := false;
      Series[0].AddXY(PassCnt,CurrOffset55_1,'',clTeeColor);
      Series[1].AddXY(PassCnt,CurrOffset55_2,'',clTeeColor);
      Series[2].Add(CurrOffset55_1,'',clTeeColor);
      Series[3].Add(CurrOffset55_2,'',clTeeColor);

    end;
    if cbSingleAFEB.Checked then
      SetStandbyReg('0');
    SetStandbyForChan(PassCnt, true);
    Delay(500);
		Timer6.Enabled := true;
	end;
end;

procedure TForm1.Timer6Timer(Sender: TObject);
var
  i: integer;
  value: single;
begin
//  if cbSingleAFEB.Checked then
//    SetStandbyReg('0');
//  SetStandbyForChan(PassCnt, true);
//  Delay(300);
  with CurrChart do
  begin
    value := ReadCurrent(V55_1);
    Delay(50);
    while (abs(value-ReadCurrent(V55_1))>0.001) do
    begin
       value := ReadCurrent(V55_1);
       Delay(50);
    end;
    if cbALCTCurrOffset.Checked then
      value := value - CurrOffset55_1;
    Series[0].AddXY(PassCnt+1,value,IntToStr(PassCnt+1),clTeeColor);
    Series[2].Add(value,'',clTeeColor);

    value := ReadCurrent(V55_2);
    Delay(50);
    while (abs(value-ReadCurrent(V55_2))>0.001) do
    begin
       value := ReadCurrent(V55_2);
       Delay(50);
    end;
    if cbALCTCurrOffset.Checked then
      value := value - CurrOffset55_2;
    Series[1].AddXY(PassCnt+1,value,'',clTeeColor);
    Series[3].Add(value,'',clTeeColor);
  end;
  sbBar.SimpleText := 'AFEB : ' + IntToStr(PassCnt);
  Inc(PassCnt);

//  Delay(300);
  if PassCnt >= NUM_AFEB then
  begin
    CurrChart.BottomAxis.Automatic := true;
    Timer6.Enabled := false;
  end
  else
  begin
  if cbSingleAFEB.Checked then
    SetStandbyReg('0');
  SetStandbyForChan(PassCnt, true);
  end
end;

procedure TForm1.cbSingleAllTestsClick(Sender: TObject);
begin
  lstSingleTests.Enabled := not cbSingleAllTests.Checked;
end;

procedure TForm1.Timer7Timer(Sender: TObject);
var
  i : Integer;
begin
  if ActiveHW then
  begin
      Timer7.Enabled := false;
  end;
end;

procedure TForm1.Button24Click(Sender: TObject);
var
  i : Integer;
  cnt, maxcnt : Integer;
  errcnt : Integer;
  senddata, readdata : word;

begin
	if ActiveHW then
	begin
    errcnt := 0;
		if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    with lstSingleTests do
    begin
      if not cbSingleAllTests.Checked then
      begin
        for i:=0 to ItemIndex do
        begin
          if Selected[i] then
          begin
            Log.Lines.Add('Running ' + Items[i] + ' test on channel ' + IntToStr(seSingleCableChan.Value));
            case i of
            0: maxcnt := seSingleTestPasses.Value;
            1..3: maxcnt := seSingleTestPasses.Value*16;
            4: begin senddata := $ffff; maxcnt := seSingleTestPasses.Value*16; end;
            5: maxcnt := seSingleTestPasses.Value*2;
            6: maxcnt := seSingleTestPasses.Value;
            end;
            Log.Lines.BeginUpdate;
            for cnt:=0 to maxcnt-1 do
            begin
              case i of
              0: senddata := StrToInt('$'+edCustomData.Text);
              1: senddata := 0 or (word (1 shl (cnt mod 16)));
              2: senddata := $FFFF and (not(word (1 shl (cnt mod 16))));
              3: senddata := senddata or (word (1 shl (cnt mod 16)));
              4: senddata := senddata and (not(word (1 shl (cnt mod 16))));
              5:  if Boolean ((cnt+1) mod 2) then
						        senddata := $5555
					        else
						        senddata := $AAAA;
              6: senddata := Random($FFFF);
            end;

              WriteIR('16',V_IR);
              WriteDR(IntToHex(seSingleCableChan.Value,3),9);
              WriteIR('15',V_IR);
              ReadDR('0',9);
              WriteIR('18',V_IR);
              WriteDR(IntToHex(senddata,4),16);
              lblSendData.Caption := '0x'+IntToHex(senddata,4);
              WriteIR('17',V_IR);
              readdata := StrToInt('$' + ReadDR('0',16));
              if readdata <> senddata then
              begin
                Inc(errcnt);
                if ((not cbSuppressErrsSingleCable.Checked) or (cbSuppressErrsSingleCable.Checked and (errcnt <= seErrCntSingleTest.Value))) then
                  Log.Lines.Add('ERROR: Pass #' + IntToStr(cnt) +' Set Mask Register to 0x' + IntToHex(senddata,4) + ' ReadBack 0x' + IntToHex(readdata, 4));
                if cbStopOnErrorSingleCable.Checked then
                  break;
              end;

              WriteIR('16',V_IR);
              WriteDR(IntToHex(seSingleCableChan.Value or $40,3),9);
              WriteIR('16',V_IR);
              WriteDR(IntToHex(seSingleCableChan.Value,3),9);

              WriteIR('19',V_IR);
              readdata := StrToInt('$' + ReadDR('0',16));
              lblReadData.Font.Color := clGreen;
              lblReadData.Caption := '0x'+IntToHex(readdata,4);

              sbBar.SimpleText := 'Pass #' + IntToStr(cnt) + ' SendData 0x' + IntToHex(senddata,4) + ' ReadData 0x' + IntToHex(readdata,4);
              if readdata <> senddata then
              begin
                lblReadData.Font.Color := clRed;
                Inc(errcnt);
                if ((not cbSuppressErrsSingleCable.Checked) or (cbSuppressErrsSingleCable.Checked and (errcnt <= seErrCntSingleTest.Value))) then
                  Log.Lines.Add('ERROR: Pass #' + IntToStr(cnt) + ' ReadBack Data 0x' + IntToHex(readdata, 4) + ' Expected 0x' + IntToHex(senddata,4));
                if cbStopOnErrorSingleCable.Checked then
                  break;
              end;
            end;
            Log.Lines.Add('Test finished with ' + IntToStr(errcnt) + ' errors');
            Log.Lines.EndUpdate;
            sbBar.SimpleText := 'Test finished with ' + IntToStr(errcnt) + ' errors';
          end;
        end;
      end;
    end;
  end;
end;


procedure TForm1.Button28Click(Sender: TObject);
begin
  if ActiveHW then
  begin
	  if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    SetFIFOReset;
  end;
end;

procedure TForm1.Button27Click(Sender: TObject);
begin
  if ActiveHW then
  begin
    if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    Log.Lines.Add('FIFO Counters: 0x' + ReadFIFOCounters());

  end;
end;

procedure TForm1.Button29Click(Sender: TObject);
begin
  if ActiveHW then
  begin
    if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    Log.Lines.Add('Set FIFO Value to: 0x'+edFIFOValue.Text);
    SetFIFOValue(StrToInt('$'+edFIFOValue.Text));
  end;
end;

procedure TForm1.Button31Click(Sender: TObject);
begin
  if ActiveHW then
  begin
    if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    Log.Lines.Add('Set FIFO Channel to: '+IntToStr(seFIFOCh.Value));
    SetFIFOChannel(seFIFOCh.Value, seStartDelay.Value );
  end;
end;

procedure TForm1.Button32Click(Sender: TObject);
begin
  if ActiveHW then
  begin
    if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    Log.Lines.Add('Set Start Delay to: '+IntToStr(seStartDelay.Value) + ' Time Bins');
    SetFIFOChannel(seFIFOCh.Value, seStartDelay.Value );
  end;
end;

procedure TForm1.Button34Click(Sender: TObject);
begin
 if ActiveHW then
  begin
    if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    Log.Lines.Add('Set ALCT Board Delay to: '+IntToStr(seTestBoardDelay.Value) + ' ~' + FloatToStrF(seTestBoardDelay.Value*0.25,ffFixed,5,2)+ 'ns');
    SetTestBoardDelay(seTestBoardDelay.Value);
  end;
end;

procedure TForm1.Button33Click(Sender: TObject);
begin
   if ActiveHW then
  begin
    if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    Log.Lines.Add('Set ALCT Board Delay to: '+IntToStr(seALCTDelay.Value) + ' ~' + FloatToStrF(seALCTDelay.Value*2,ffFixed, 5, 2)+ 'ns');
    SetALCTBoardDelay(seFIFOCh.Value, seALCTDelay.Value);
  end;

end;

procedure TForm1.Button25Click(Sender: TObject);
var
  num : integer;
begin
  if ActiveHW then
  begin
    if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    num := seFIFONumWords.Value;
    if cbRWFIFOMode.Checked then
      num := 1;
    SetTestBoardFIFO(StrToInt('$'+edFIFOValue.Text),seFIFOCh.Value,num, seStartDelay.Value, seALCTDelay.Value, seTestBoardDelay.Value)
  end;
end;

procedure TForm1.Button26Click(Sender: TObject);
var
  i,num : integer;
  value : integer;
  counters : string;
  arCntr : array[1..16] of byte;
begin
  if ActiveHW then
  begin
    if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    num := seFIFONumWords.Value;

    ALCTEnableInput;
    SetFIFORead;
    if cbRWFIFOMode.Checked then
      SetFIFOReadWrite;
    for i := 1 to num do
    begin
      FIFOClock;
      value := ReadFIFOValue();
      if cbShowFIFOValue.Checked then
        Log.Lines.Add(IntToStr(i) + ': 0x'+ IntToHex(value, 4));
      if cbShowFIFOCounter.Checked then
      begin
        counters := ReadFIFOCounters();
        Log.Lines.Add(IntToStr(i) + ': 0x'+ counters);
      end;
    end;

    with DelaysChart do
    begin
      RemoveAllSeries;
      BottomAxis.Minimum := 0;
      BottomAxis.Maximum := ALCTBoard.delaylines;
      LeftAxis.Minimum := 0;
      LeftAxis.Maximum := 110;

      AddSeries(TBarSeries.Create(self));
      Series[0].Title := ' Chan #' + IntToStr(seFIFOCh.Value) + ' Stats';
      BottomAxis.Automatic := false;
      LeftAxis.Automatic := false;
    end;
  end;
end;

procedure TForm1.Button35Click(Sender: TObject);
var
  ch, num, value, start_dly, tb_dly, alct_dly : integer;
  i, j : integer;
  vals : array of word;
  cntrs : array of byte;
  fWrong, fGoodStart : boolean;
  base_sum_cntrs, sum_cntrs : longint;
  num_dly_ch : integer;
  cntrs_str : string;
begin
  if ActiveHW then
  begin
    if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;

    num_dly_ch := 0;
    SetLength(cntrs,16);
//    value := $FFFF;
    value := StrToInt('$'+edFIFOValue.Text);
    alct_dly := 0;
    ch := seFIFOCh.Value;
//    for ch:= 0 to NUM_AFEB -1 do
    with DelaysChart do
    begin
      Title.Text.Clear;
      Title.Text.Add('Delays for Chip #'+IntToStr(ch));
      RemoveAllSeries;
      BottomAxis.Minimum := 0;
      BottomAxis.Maximum := 255;
      BottomAxis.Title.Caption := 'Test Board DelayChip Values (0..255)';
      LeftAxis.Minimum := 0;
      LeftAxis.Maximum := 102;
      LeftAxis.Title.Caption := 'FIFO Reading Rate (%)';
      BottomAxis.Automatic := false;
      LeftAxis.Automatic := false;
    end;

//    for ch:= 0 to seFIFOCh.Value do
    ch := seFIFOCh.Value;
    begin
      Log.Lines.Add('Chan #' + IntToStr(ch));
      for start_dly := 2 to 15 do
      begin
//        Log.Lines.Add('ALCT Start Delay :' + IntToStr(start_dly));
        fWrong := false;
        fGoodStart := false;
        num := 10;
        SetLength(vals, num);
        tb_dly := 0;
        SetTestBoardFIFO(value, ch, 1, start_dly, alct_dly, tb_dly);
        SetFIFOReadWrite;
        ReadFIFO(vals,num,cntrs);
//        Log.Lines.Add('startdly :'+ IntToStr(start_dly)+ ' testdelay :'+ IntToStr(tb_dly)+' cnts :'+ReadFIFO(vals,num,cntrs));

        for i:=0 to 15 do
          if (cntrs[i] <> 0) and ((value and (1 shl i))>0) then
          begin
            fWrong := true; break;
          end;
        if fWrong then continue;

        tb_dly := 255;
        SetTestBoardFIFO(value, ch, 1, start_dly, alct_dly, tb_dly);
        SetFIFOReadWrite;
        ReadFIFO(vals,num,cntrs);
//        Log.Lines.Add('startdly :'+ IntToStr(start_dly)+ ' testdelay :'+ IntToStr(tb_dly)+' cnts :'+ReadFIFO(vals,num,cntrs));
        for i:=0 to 15 do
          if (cntrs[i] <> 0) and ((value and (1 shl i))>0) then
          begin
            fWrong := true; break;
          end;
        if fWrong then continue;

        tb_dly := 255 div 2;
        SetTestBoardFIFO(value, ch, 1, start_dly, alct_dly, tb_dly);
        SetFIFOReadWrite;
        ReadFIFO(vals,num,cntrs);
//        Log.Lines.Add('startdly :'+ IntToStr(start_dly)+ ' testdelay :'+ IntToStr(tb_dly)+' cnts :'+ReadFIFO(vals,num,cntrs));
        for i:=0 to 15 do
          if (cntrs[i] <> 0) and ((value and (1 shl i))>0) then
          begin
            fGoodStart := true; break;
          end;


        tb_dly := 255 div 4;
        SetTestBoardFIFO(value, ch, 1, start_dly, alct_dly, tb_dly);
        SetFIFOReadWrite;
        ReadFIFO(vals,num,cntrs);
//        Log.Lines.Add('startdly :'+ IntToStr(start_dly)+ ' testdelay :'+ IntToStr(tb_dly)+' cnts :'+ReadFIFO(vals,num,cntrs));
        for i:=0 to 15 do
          if (cntrs[i] <> 0) and ((value and (1 shl i))>0) then
          begin
            fGoodStart := true; break;
          end;

        tb_dly := (255 div 4)*3;
        SetTestBoardFIFO(value, ch, 1, start_dly, alct_dly, tb_dly);
        SetFIFOReadWrite;
        ReadFIFO(vals,num,cntrs);
//        Log.Lines.Add('startdly :'+ IntToStr(start_dly)+ ' testdelay :'+ IntToStr(tb_dly)+' cnts :'+ReadFIFO(vals,num,cntrs));

        for i:=0 to 15 do
          if (cntrs[i] <> 0) and ((value and (1 shl i))>0) then
          begin
            fGoodStart := true; break;
          end;

        if fGoodStart then
        begin
          Log.Lines.Add('Found Good ALCT Start Delay :' + IntToStr(start_dly));
          Log.Lines.Add('Scanning by Test Board Delay...');
          with DelaysChart do
          begin
            for i:=0 to 15 do
            begin
              AddSeries(TLineSeries.Create(self));
              Series[i].Title := ' Channel #' + IntToStr(i) + ' Stats';
            end
          end;

          num := 100;
          num_dly_ch := 0;
//          value := $FFFF;
          for i:=0 to 15 do
            if ((value and (1 shl i))>0) then
              Inc(num_dly_ch);

          base_sum_cntrs := num*num_dly_ch;
          SetLength(vals, num);
          for tb_dly:=0 to 255 do
          begin
            Delay(20);
//            sbBar.SimpleText := 'Test Board Delay :' + IntToStr(tb_dly) + ' ~' + FloatToStrF(tb_dly*0.25, ffFixed, 5,2)+'ns';
            sum_cntrs := 0;
            SetTestBoardFIFO(value, ch, num, start_dly, alct_dly, tb_dly);
//            SetFIFOReadWrite;
            SetFIFORead;
            Delay(20);
            cntrs_str := ReadFIFO(vals,num,cntrs);

            for i:=0 to 15 do
            begin
              sum_cntrs := sum_cntrs + Min(num, cntrs[i]);
              DelaysChart.Series[i].AddXY(tb_dly, (Min(num,cntrs[i])/num)*100, '',clTeeColor);
            end;
//            Log.Lines.Add('TB Dly: ' + IntToStr(tb_dly) + ' ' + cntrs_str + ' sum:' + IntToStr(sum_cntrs) + ' base:' + IntToStr(base_sum_cntrs));
            if ((sum_cntrs/base_sum_cntrs) > 0.0) then
            begin
              Log.Lines.Add('TB Dly: ' + IntToStr(tb_dly) + ' In ' + FloatToStrF((sum_cntrs/base_sum_cntrs)*100, ffFixed, 6,2)+'%');
            end;
            // DelaysChart.Series[ch].AddXY(tb_dly, (sum_cntrs/base_sum_cntrs)*100, '',clTeeColor);

         end;
          fWrong := false;
          break;
        end
        else
          fWrong := true;
      end;
        if fWrong then
          Log.Lines.Add('ERROR: Could not find any good Start Delay Value');
    end;
  end;
end;

procedure TForm1.Button36Click(Sender: TObject);
var
  i: integer;
  data: string;
begin
	if ActiveHW then
	begin
		if Chains.ItemIndex <> 3 then
		begin
			Chains.ItemIndex := 3;
			SetChainClick(self);
		end;
    with lstIDs do
    begin
      for i:=0 to ItemIndex do
      begin
          if Selected[i] then
          begin
            case i of
            SLOW_CTL: begin
                if Chains.ItemIndex <> 1 then
                  begin
                		Chains.ItemIndex := 1;
                		SetChainClick(self);
              		end;
            		data := ReadIDCodeStr(SLOW_CTL);
            		ShowIDCode(data, SLOW_CTL);
          			lbslowctl.Caption := '0x '+data;
              end;
            FAST_CTL: begin
                if Chains.ItemIndex <> 3 then
                  begin
                		Chains.ItemIndex := 3;
                		SetChainClick(self);
              		end;
            		data := ReadIDCodeStr(FAST_CTL);
            		ShowIDCode(data, FAST_CTL);
           			lbfastctl.Caption := '0x '+data;
              end;
            BOARD_SN: begin
                if Chains.ItemIndex <> 3 then
                  begin
                		Chains.ItemIndex := 3;
                		SetChainClick(self);
              		end;
                data := ReadBoardSNStr(BOARD_SN);
            		ShowBoardSN(data, BOARD_SN);
           			lbboardsn.Caption := '0x '+Copy(data,1,Length(data)-2);
              end;
            MEZAN_SN: begin
                if Chains.ItemIndex <> 3 then
                  begin
                		Chains.ItemIndex := 3;
                		SetChainClick(self);
              		end;
                data := ReadBoardSNStr(MEZAN_SN);
            		ShowBoardSN(data, MEZAN_SN);
           			lbmezansn.Caption := '0x '+Copy(data,1,Length(data)-2);
              end;
            end;
          end;
      end;
    end;
  end;
end;

procedure TForm1.Button38Click(Sender: TObject);
var
  ch, i, j, k, l, ErrMeasDly, count: integer;
  MinDelay, MaxDelay, MaxDeltaDelay, MaxDeltaBegin: single;
  DeltaDelay, PulseWidth: array[0..15] of Currency;
  BeginTime_Min, AverageDelay_Time, DeltaDelay_Max, ErrorDeltaDelay: MeasDly;
  DeltaBegin_Max: MeasDly;
  DeltaBeginTime, Delay_Time: MeasDlyChan;
  ErrDelayTest: boolean;
  BoardNum: integer;
  PathString, PinErrors: string;//Zach
  RegMaskDone: array[0..41] of word;
begin
  //This function will NOT execute unless hardware is active
  if ActiveHW then
  begin
//    if Chains.ItemIndex <> 3 then
//    begin
      Chains.ItemIndex := 3;
      SetChainClick(self);
//    end;

    //    value := StrToInt('$'+edFIFOValue.Text);
//    ch := seFIFOCh.Value;
    BoardNum := strtoint(edBoardNumber.Text);
    ErrDelayTest:= false;
    MaxDeltaBegin:= 2;
    MaxDeltaDelay:= 2;
    MinDelay:= 25; MaxDelay:= 35;

    for ch:= 0 to NUM_AFEB-1 do
    begin
      BeginTime_Min[ch] := 0;
      AverageDelay_Time[ch] := 0;
      RegMaskDone[ch] :=1;
      for i:=0 to 15 do
      begin
        DeltaBeginTime[ch][i] := 0;
        Delay_Time[ch][i] := 0;
      end;
    end;

    for i:=0 to 15 do
    begin
        PulseWidth[i] := 0;
    end;

    Log.Lines.Add(' ');
    Log.Lines.Add('Scan Board: ' + IntToStr(BoardNum));

    for ch:= 0 to NUM_AFEB-1 do
    begin
      count:=0;
      seFIFOCH.Value := ch;
//      Log.Lines.Add('Accessing channel: ' + IntToStr(ch));
      MeasureDelay(ch, PulseWidth, BeginTime_Min, DeltaBeginTime, Delay_Time, AverageDelay_Time, ErrMeasDly, RegMaskDone);
      DeltaBegin_Max[ch]:= 0;
      DeltaDelay_Max[ch]:= 0;

      //RegMaskDoneF[ch] :=ch;

      for i:= 0 to 15 do
      begin
        if (DeltaBeginTime[ch][i] > DeltaBegin_Max[ch]) then
          DeltaBegin_Max[ch]:= DeltaBeginTime[ch][i];
        DeltaDelay[i]:= abs(Delay_Time[ch][i] - AverageDelay_Time[ch]);
        if (DeltaDelay[i] > DeltaDelay_Max[ch]) then
          DeltaDelay_Max[ch]:= DeltaDelay[i];

      end;
      if (DeltaBegin_Max[ch] > MaxDeltaBegin) then
        ErrMeasDly:= ErrMeasDly or $10;
      if (DeltaDelay_Max[ch] > MaxDeltaDelay) then
        ErrMeasDly:= ErrMeasDly or $20;

      if (ErrMeasDly <> 0) then
      begin
//      Log.Lines.Add('ErrMeasDly on channel: ' + IntToStr(ch) + ' = ' + IntToStr(ErrMeasDly));
        ErrDelayTest:= true;
        for i:=0 to 5 do
          if ((ErrMeasDly and (1 shl i))>0) then
          begin
            k:= i;
            break;
          end;
        case k of
          0:
          begin
             Log.Lines.Add('ERROR: Cannot find StartDly_0 on channel: ' + IntToStr(ch));
             if (RegMaskDone[ch]=0) then Log.Lines.Add('   All pins failed')
             else
             begin
               PinErrors :='   Pin(s): ';
               for j:=0 to 15 do
               begin
                 if ((RegMaskDone[ch] and (1 shl j))=0) then
                 begin
                   if (count=0) then PinErrors := PinErrors + IntToStr(j)
                   else
                   begin
                     PinErrors := PinErrors + ', ' + IntToStr(j)
                   end;
                   count := count+1;
                 end;
               end;
             PinErrors := PinErrors + ' bad';
             Log.Lines.Add(PinErrors);
             end;
          end;
          1: Log.Lines.Add('ERROR: Cannot find StartDly_15 on channel: ' + IntToStr(ch));
          2:
          begin
          for l:=0 to 15 do
          begin
                if (PulseWidth[l] < 30)    then Log.Lines.Add('ERROR: Width of Pulse on channeld: ' + IntToStr(ch) + ' Pin: ' + IntToStr(l) + ' less than 30 nS  = ' + CurrToStr(PulseWidth[l]));
          end;
          end;
          3:
          begin
          for l:=0 to 15 do
          begin
                if (PulseWidth[l] > 45)    then Log.Lines.Add('ERROR: Width of Pulse on channel: ' + IntToStr(ch) + ' Pin: ' + IntToStr(l) + ' greater than 45 nS  = ' + CurrToStr(PulseWidth[l]));
          end;
          end;
          4: Log.Lines.Add('ERROR: DeltaBeginTime on channel: ' + IntToStr(ch) + ' more ' + CurrToStr(MaxDeltaBegin) + ' nS  = ' + CurrToStr(DeltaBegin_Max[ch]));
          5: Log.Lines.Add('ERROR: DeltaDelay on channel: ' + IntToStr(ch) + ' more  ' + CurrToStr(MaxDeltaDelay) + ' nS  = ' + CurrToStr(DeltaDelay_Max[ch]));
        end;
      end;
      if ((AverageDelay_Time[ch] < MinDelay) or (AverageDelay_Time[ch] > MaxDelay)) then
        begin
        ErrorDeltaDelay[ch]:= AverageDelay_Time[ch];
        if (AverageDelay_Time[ch] < MinDelay) then
        if (AverageDelay_Time[ch] > 0) then Log.Lines.Add('ERROR: Average Delay on channel: ' + IntToStr(ch) + ' of ' + CurrToStr(AverageDelay_Time[ch]) + 'ns is less than ' + FloatToStrF(MinDelay, ffFixed,7,1) + 'nS');
        if (AverageDelay_Time[ch] > MaxDelay) then Log.Lines.Add('ERROR: Average Delay on channel: ' + IntToStr(ch) + ' of ' + CurrToStr(AverageDelay_Time[ch]) + 'ns is more than ' + FloatToStrF(MaxDelay, ffFixed,7,1) + 'nS');
        end;
    end;

    if (not ErrDelayTest) then
      Log.Lines.Add('Test of Delays for ALCT # ' + IntToStr(BoardNum) + ' Done');

   with DelaysChart do
    begin
      Title.Text.Clear;
      Title.Text.Add('Test of Delays for ALCT # '+IntToStr(ch));
      RemoveAllSeries;
      BottomAxis.Minimum := 0;
      BottomAxis.Maximum := 20;
      LeftAxis.Minimum := 0;
      LeftAxis.Maximum := 50*(0.000000001);

      AddSeries(TBarSeries.Create(self));
      AddSeries(TLineSeries.Create(self));
      AddSeries(TBarSeries.Create(self));
      AddSeries(TLineSeries.Create(self));

      Series[0].Title := 'Error Delay';
      Series[1].Title := 'Min Delay';
      Series[2].Title := 'DelayAverage';
      Series[3].Title := 'Max Delay ';

      for i:=0 to NUM_AFEB-1 do
      begin
//        BottomAxis.Automatic := false;
        Series[0].Add(ErrorDeltaDelay[i],'',clTeeColor);
        Series[1].AddXY(i, MinDelay,'',clTeeColor);
        Series[2].Add(AverageDelay_Time[i],'',clTeeColor);
        Series[3].AddXY(i, MaxDelay,'',clTeeColor);
      end;
    end;

    PathString := '';
    PathString := 'D:\alct\DelayTest_Files\' + 'DelayTest_Results_' + edBoardNumber.Text + '.txt';

    if cbWriteToFile.Checked then
    begin
        if FileExists(PathString) then
        begin
          WriteToFileForm.ShowModal;
          if WriteToFileForm.ModalResult = mrYes then
            WriteToFile(BeginTime_Min, DeltaBeginTime, Delay_Time, AverageDelay_Time, edBoardNumber.Text, PathString);
        end

        else
          WriteToFile(BeginTime_Min, DeltaBeginTime, Delay_Time, AverageDelay_Time, edBoardNumber.Text, PathString);
    end;//end of write to file if statement

  end;//end of if active HW
end;//end of function

procedure TForm1.Button39Click(Sender: TObject);
var
  ch, num, value, start_dly, tb_dly, alct_dly : integer;
  i, j : integer;
//  cntrs : array[0..15] of byte;
  num_dly_ch : integer;
//  cntrs_str : string;

  StartDly_R, StartDly_F: integer;

  //arrays for storing test board delay for each channel//
  TimeR_0, TimeF_0, TimeR_15, TimeF_15: array[0..15] of integer;
  TimeR_50, DelayTimeR_50: array[0..15] of Currency;
  //arrays for storing test board delay for each channel//
  DelayTimeR_0, DelayTimeF_0, DelayTimeR_15, DelayTimeF_15: array[0..15] of Currency;

  //arrays for storing pulse widths on each channel
  PulseWidth_0, PulseWidth_15: array[0..15] of Currency;

  //Array for storing pulse shifts
  Delay_Time, ErrorDeltaDelay, DeltaBeginTime: array[0..15] of Currency;

  MinDelay, MaxDelay, MinDelayTimeR_0: Currency;

  //Register mask
  RegisterMaskDone: word;

  //actual delay time and pulse width
//  Delay_Time: Currency;
//  Pulse_Width: Currency;

begin
  //This function will NOT execute unless hardware is active
  if ActiveHW then
  begin

//    if Chains.ItemIndex <> 3 then
//    begin
      Chains.ItemIndex := 3;
      SetChainClick(self);
//    end;

//    DelaysChart.CleanupInstance;
    //Initialize variables and arrays
//    ChannelsCntr := 0;
    MinDelay:= 29; MaxDelay:= 35;
    num_dly_ch := 0;
    value := StrToInt('$'+edFIFOValue.Text);
    ch := seFIFOCh.Value;
    num := seFIFONumWords.Value;
    alct_dly := 0;

    with DelaysChart do
      begin
        Title.Text.Clear;
        Title.Text.Add('Delays for Chip #'+IntToStr(ch));
        PassCnt :=4;
        RemoveAllSeries;
      end;
//      DACChart.RemoveAllSeries;
{    with DelaysChart do
    begin
      Title.Text.Clear;
      Title.Text.Add('Delays for Chip #'+IntToStr(ch));
      RemoveAllSeries;
      BottomAxis.Minimum := 0;
      BottomAxis.Maximum := 255;
      BottomAxis.Title.Caption := 'Test Board DelayChip Values (0..255)';
      LeftAxis.Minimum := 0;
      LeftAxis.Maximum := 102;
      LeftAxis.Title.Caption := 'FIFO Reading Rate (%)';
      BottomAxis.Automatic := false;
      LeftAxis.Automatic := false;
    end;
}
    for i:=0 to 15 do
    begin
      TimeR_0[i] := 0;
      TimeF_0[i] := 0;
      TimeR_15[i] := 0;
      TimeF_15[i] := 0;
      DelayTimeR_0[i] := 0;
      DelayTimeF_0[i] := 0;
      DelayTimeR_15[i] := 0;
      DelayTimeF_15[i] := 0;
      PulseWidth_0[i] := 0;
      PulseWidth_15[i] := 0;
      Delay_Time[i] := 0;
      ErrorDeltaDelay[i] := 0;
    end;


    //Calculating number of requested channels
    for i:=0 to 15 do
      if ((value and (1 shl i))>0) then
        Inc(num_dly_ch);

    Log.Lines.Add(IntToStr(num_dly_ch) + 'channels Requested on Chan #' + IntToStr(ch));
//------------------------------------------------------------------------
    //Pinpoint rise/fall times for ALCT delay of 0
    alct_dly:= 0;
    if not FindStartDly(StartDly_R, StartDly_F, value, ch, alct_dly, num) then
      Log.Lines.Add('Cannot find StartDly_0 on channel: ' + IntToStr(ch))
    else
      begin
        Log.Lines.Add('Found Delay pulse on channel #' + IntToStr(ch) + 'with alct delay of 0');
        Log.Lines.Add('StartDly_R =' + IntToStr(StartDly_R) + '  StartDly_F =' + IntToStr(StartDly_F));

        PinPointRiseTime(TimeR_0, value, ch, StartDly_R, alct_dly, num);
        PinPointFallTime(TimeF_0, value, ch, StartDly_F-1, alct_dly, num);
      end;


    //Print delay times to log
//    Log.Lines.Add('Rise Times with ALCT delay 0 on channel #' + inttostr(ch));
    MinDelayTimeR_0:= StartDly_R*25 + 255*0.25;
    for i:=0 to 15 do
    begin
      if ((value and (1 shl i))>0) then
      begin
        DelayTimeR_0[i] := StartDly_R*25 + TimeR_0[i]*0.25;
//        DelayTimeR_50[i] := StartDly_R*25 + TimeR_50[i]*0.25;
        if (MinDelayTimeR_0 > DelayTimeR_0[i]) then
          MinDelayTimeR_0:= DelayTimeR_0[i];
      end;
    end;
    Log.Lines.Add('Rise Times with ALCT delay 0 on channel # ' + IntToStr(ch) + ' = ' + CurrToStr(MinDelayTimeR_0) + ' nS');
    for i:=0 to 15 do
    begin
      DeltaBeginTime[i]:= DelayTimeR_0[i] - MinDelayTimeR_0;
      Log.Lines.Add('ch #' + inttostr(i) + ' Delta= ' + CurrToStr(DeltaBeginTime[i]) + '  ' + CurrToStr(DelayTimeR_0[i]) + '  ( ' + CurrToStr(DelayTimeR_50[i]) + ' nS )');
    end;

    //Print delay times to log
    Log.Lines.Add('Fall Times with ALCT delay 0 on channel #' + inttostr(ch));
    for i:=0 to 15 do
    begin
      if ((value and (1 shl i))>0) then
      begin
        DelayTimeF_0[i] := (StartDly_F-1)*25 + TimeF_0[i]*0.25;
        Log.Lines.Add('ch #' + inttostr(i) + ' = ' + CurrToStr(DelayTimeF_0[i]) + 'ns');
      end;
    end;
//-------------------------------------------------------------------------------------
    //Pinpoint rise/fall times for ALCT delay of 15
    alct_dly:= 15;
    if not FindStartDly(StartDly_R, StartDly_F, value, ch, alct_dly, num) then
      Log.Lines.Add('Cannot find StartDly_15 on channel: ' + IntToStr(ch))
    else
      begin
        Log.Lines.Add('Found Delay pulse on channel #' + IntToStr(ch) + 'with alct delay of 15');
        Log.Lines.Add('StartDly_R =' + IntToStr(StartDly_R) + '  StartDly_F =' + IntToStr(StartDly_F));

        PinPointRiseTime(TimeR_15, value, ch, StartDly_R, alct_dly, num);
        PinPointFallTime(TimeF_15, value, ch, StartDly_F, alct_dly, num);
      end;
//      exit;

     //Print delay times to log
    Log.Lines.Add('Rise Times with ALCT delay 15 on channel #' + inttostr(ch));
    for i:=0 to 15 do
    begin
      if ((value and (1 shl i))>0) then
      begin
        DelayTimeR_15[i] := StartDly_R*25 + TimeR_15[i]*0.25;
        Log.Lines.Add('ch #' + inttostr(i) + ' = ' + CurrToStr(DelayTimeR_15[i]) + 'ns');
      end;
    end;

    //Print delay times to log
    Log.Lines.Add('Fall Times with ALCT delay 15 on channel #' + inttostr(ch));
    for i:=0 to 15 do
    begin
      if ((value and (1 shl i))>0) then
      begin
        DelayTimeF_15[i] := StartDly_F*25 + TimeF_15[i]*0.25;
        Log.Lines.Add('ch #' + inttostr(i) + ' = ' + CurrToStr(DelayTimeF_15[i]) + 'ns');
      end;
    end;

    Log.Lines.Add('Pulse widths at alct delay of 0 on channel #' + inttostr(ch));
    for i:=0 to 15 do
    begin
      PulseWidth_0[i] := DelayTimeF_0[i] - DelayTimeR_0[i];
      Log.Lines.Add('ch #' + inttostr(i) + ' = ' + CurrToStr(PulseWidth_0[i]) + 'ns');
    end;

{    Log.Lines.Add('Pulse widths at alct delay of 15 on channel #' + inttostr(ch));
    for i:=0 to 15 do
    begin
      PulseWidth_15[i] := DelayTimeF_15[i] - DelayTimeR_15[i];
      Log.Lines.Add('ch #' + inttostr(i) + ' = ' + CurrToStr(PulseWidth_15[i]) + 'ns');
    end;
}
    Log.Lines.Add('Pulse shifts on channel #' + inttostr(ch));
    for i:=0 to 15 do
    begin
      Delay_Time[i] := DelayTimeR_15[i] - DelayTimeR_0[i];
      Log.Lines.Add('ch #' + inttostr(i) + ' = ' + CurrToStr(Delay_Time[i]) + 'ns');
      if ((Delay_Time[i] < MinDelay) or (Delay_Time[i] > MaxDelay))then
        begin
          ErrorDeltaDelay[i]:= Delay_Time[i];
//          Delay_Time[i]:= 0;
        end;
    end;

    with DelaysChart do
    begin
//      Title.Text.Clear;
//      Title.Text.Add('Delays for Chip #'+IntToStr(ch));
//      RemoveAllSeries;
      BottomAxis.Minimum := 0;
      BottomAxis.Maximum := 20;
      LeftAxis.Minimum := 0;
      LeftAxis.Maximum := 50*(0.000000001);

      PassCnt :=4;
//      GetDefaultColor(t);
      AddSeries(TBarSeries.Create(self));
      AddSeries(TLineSeries.Create(self));
      AddSeries(TBarSeries.Create(self));
      AddSeries(TLineSeries.Create(self));

      Series[0].Title := 'Error shifts';
      Series[1].Title := 'Min shifts';
      Series[2].Title := 'Max shifts';
      Series[3].Title := 'Pulse shifts';

      for i:=0 to 15 do
      begin
//        BottomAxis.Automatic := false;
        Series[0].Add(ErrorDeltaDelay[i],'',clTeeColor);
        Series[1].AddXY(i, MinDelay,'',clTeeColor);
        Series[2].Add(Delay_Time[i],'',clTeeColor);
        Series[3].AddXY(i, MaxDelay,'',clTeeColor);
      end;
    end;

  end;//end of if active HW
end;//end of function

end.

