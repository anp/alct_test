{-------------------------------------------------------------------------}
{									  }
{   Copyright (C) 1992-2000  Michel Catudal <bbcat@netonecom.net>         }
{   libprtio.pas- Kylix Runtime Library for the printer port support      }
{									  }
{   This program is free software; you can redistribute it and/or modify  }
{   it under the terms of the GNU General Public License as published by  }
{   the Free Software Foundation; either version 2 of the License, or     }
{   (at your option) any later version.					  }
{									  }
{   This program is distributed in the hope that it will be useful,       }
{   but WITHOUT ANY WARRANTY; without even the implied warranty of	  }
{   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	  }
{   GNU General Public License for more details.			  }
{									  }
{   You should have received a copy of the GNU General Public License     }
{   along with this program; if not, write to the Free Software		  }
{   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		  }
{									  }
{      Last edited 8-23-01 by Michel Catudal dit St-Jean		  }
{									  }
{-------------------------------------------------------------------------}

unit libprtio;

interface

{ 0x3bc  The port included on some monochrome video cards                 }
{ 0x378  The most common one, LPT1 in DOS if you don't have a mono card   }
{ 0x278  Usually the 2'nd parallel port, LPT2 in PC DOS                   }
{ 0xa800 The address of my LAVA PCI parallel port                         }
{									  }
{ Bit Definitions: (x = don't care, a = active bit)                       }
{									  }
{ 7 6 5 4  3 2 1 0                                                        }
{ a a a a  a a a a  BASEPORT (output bits to AVR)                         }
{ | | | |  | | | +- STK-dongle-detect0, connects to STATUS(5)             }
{ | | | |  | | +--- STK-dongle-detect1, connects to STATUS(7)             }
{ | | | |  | +----- GB    0 to enable B port on 74HC244 of STK-dongle     }
{ | | | |  +------- GA    0 to enable A port on 74HC244 of STK-dongle     }
{ | | | +---------- SCK:  0=low, 1=high                                   }
{ | | +------------ MOSI: 0=low, 1=high                                   }
{ | +-------------- AIR:  Turn an air valve on or off                     }
{ +---------------- RST:  0=AVR in RESET mode, 1=AVR in RUN (normal) mode }
{									  }
{ 7 6 5 4  3 2 1 0                                                        }
{ a a a x  x x x x  STATUS   (input bit from AVR)                         }
{ | | +------------ Input from STK-dongle-detect0   BASEPORT(0)           }
{ | +-------------- MISO: 0=low, 1=high                                   }
{ +---------------- Input from STK-dongle-detect1   BASEPORT(1)           }
{									  }

const
   SET_HIGHLOW_BIT_HIGH = 8;
   T_WRITE_WAIT  = 4000; { Time to wait after writing a location (us) }
   SCK_HIGH      = $10;  { logical OR mask to set SCK high }
   SCK_LOW       = $ef;  { logical AND mask to set SCK low }
   G_OFF         = $0c;  { logical OR mask to turn off 74HC244 }
   G_ON          = $f3;  { logical AND mask to turn on 74HC244 }
   AVR_RUN       = $80;  { logical OR mask to put AVR in RUN mode }
   AVR_RESET     = $7f;  { logical AND mask to put AVR into RESET }
   MOSI_HIGH     = $20;  { logical OR mask to set MOSI high }
   MOSI_LOW      = $df;  { logical AND mask to set MOSI low }
   MISO_HIGH     = $40;  { logical AND mask to check if MISO is high (nonzero = high) }
   T_SCK_HIGH    = 1;    { Time that SCK is HIGH (uS) }
   T_SCK_LOW     = 1;    { Time that SCK is LOW (uS) }
   dongleOffOut  = $fc;
   dongleOnOut   = 3;
   dongleInMask  = $a0;
   dongleOffIn   = $80;
   dongleOnIn    = $20;

type
  MArray = array[0..3] of Byte;


{ A Delay in uSecs.  }
procedure Delay_uSecs(usecs : LongWord); cdecl;
function openParPort(portN : PChar) : Integer; cdecl;
procedure closeParPort(prtPort : Integer); cdecl;
procedure wrDataPort(prtPort : Integer; c : Byte); cdecl;
procedure wrControlPort(prtPort : Integer; c : Byte); cdecl;
procedure wrDir(prtPort : Integer; c : Byte); cdecl;
function rdStatusPort(prtPort : Integer) : Byte; cdecl;
function rdDataPort(prtPort : Integer) : Byte; cdecl;
function rdControlPort(prtPort : Integer) : Byte; cdecl;
function wr2SPI(prtPort : Integer; avrCtrl : PChar; c : Byte) : Byte; cdecl;
procedure pavrOn(prtPort : Integer; avrCtrl : PChar); cdecl;
procedure pavrOff(prtPort : Integer; avrCtrl : PChar); cdecl;
procedure SendSPIcmd(prtPort : Integer; avrCtrl : PChar; command : array of Byte); cdecl;
function getIDcode(prtPort : Integer; avrCtrl : PChar) : LongWord; cdecl;
function rdByteFromFlash(prtPort : Integer; avrCtrl : PChar; address : Word; highlow : Byte) : Byte; cdecl;
function rdByteFromEE(prtPort : Integer; avrCtrl : PChar; address : Word) : Byte; cdecl;
procedure wrByte2Flash(prtPort : Integer; avrCtrl : PChar; address : Word; highlow : Byte; c : Byte); cdecl;
procedure wrByte2EE(prtPort : Integer; avrCtrl : PChar; address : Word; c : Byte); cdecl;
procedure eraseAvr(prtPort : Integer; avrCtrl : PChar); cdecl;


implementation

{ A Delay in uSecs.  }
procedure Delay_uSecs(usecs : LongWord); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM Delay_uSecs}

function openParPort(portN : PChar) : Integer; cdecl; external 'libprtio.so.1';
{$EXTERNALSYM openParPort}

procedure closeParPort(prtPort : Integer); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM closeParPort}

procedure wrDataPort(prtPort : Integer; c : Byte); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM wrDataPort}

procedure wrControlPort(prtPort : Integer; c : Byte); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM wrControlPort}

procedure wrDir(prtPort : Integer; c : Byte); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM wrDir}

function rdStatusPort(prtPort : Integer) : Byte; cdecl; external 'libprtio.so.1';
{$EXTERNALSYM rdStatusPort}

function rdDataPort(prtPort : Integer) : Byte; cdecl; external 'libprtio.so.1';
{$EXTERNALSYM rdDataPort}

function rdControlPort(prtPort : Integer) : Byte; cdecl; external 'libprtio.so.1';
{$EXTERNALSYM rdControlPort}

function wr2SPI(prtPort : Integer; avrCtrl : PChar; c : Byte) : Byte; cdecl; external 'libprtio.so.1';
{$EXTERNALSYM wr2SPI}

procedure pavrOn(prtPort : Integer; avrCtrl : PChar); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM pavrOn}

procedure pavrOff(prtPort : Integer; avrCtrl : PChar); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM pavrOff}

procedure SendSPIcmd(prtPort : Integer; avrCtrl : PChar; command : array of Byte); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM SendSPIcmd}

function getIDcode(prtPort : Integer; avrCtrl : PChar) : LongWord; cdecl; external 'libprtio.so.1';
{$EXTERNALSYM getIDcode}

function rdByteFromFlash(prtPort : Integer; avrCtrl : PChar; address : Word; highlow : Byte) : Byte; cdecl; external 'libprtio.so.1';
{$EXTERNALSYM rdByteFromFlash}

function rdByteFromEE(prtPort : Integer; avrCtrl : PChar; address : Word) : Byte; cdecl; external 'libprtio.so.1';
{$EXTERNALSYM rdByteFromFlash}

procedure wrByte2Flash(prtPort : Integer; avrCtrl : PChar; address : Word; highlow : Byte; c : Byte); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM wrByte2Flash}

procedure wrByte2EE(prtPort : Integer; avrCtrl : PChar; address : Word; c : Byte); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM wrByte2EE}

procedure eraseAvr(prtPort : Integer; avrCtrl : PChar); cdecl; external 'libprtio.so.1';
{$EXTERNALSYM eraseAvr}

end.

