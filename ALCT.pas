unit ALCT;

interface

const ConfigFile = 'alct_tests.ini';

const VIRTEX600 = 0;
const VIRTEX1000 = 1;
const UNKNOWN = 255;
const
{		HCMaskRead    = '01';
		HCMaskWrite   = '02';
		RdTrig        = '03';
		WrTrig        = '04';
		RdCfg         = '06'; // read control register
		WrCfg         = '07'; // write control register
		Wdly          = '0d'; // write delay lines. cs_dly bits in Par
		Rdly          = '0e'; // read  delay lines. cs_dly bits in Par
		CollMaskRead  = '13';
		CollMaskWrite = '14';
		ParamRegRead  = '15';
		ParamRegWrite = '16';
		InputEnable   = '17';
		InputDisable  = '18';
		YRwrite       = '19';
		OSread        = '1a';
		SNread        = '1b';
    SNwrite0      = '1c';
    SNwrite1      = '1d';
    SNreset       = '1e';
		Bypass        = '1f';
}
    IDRead        = $00;
		HCMaskRead    = $01;
		HCMaskWrite   = $02;
		RdTrig        = $03;
		WrTrig        = $04;
		RdCfg         = $06; // read control register
		WrCfg         = $07; // write control register
		Wdly          = $0d; // write delay lines. cs_dly bits in Par
		Rdly          = $0e; // read  delay lines. cs_dly bits in Par
		CollMaskRead  = $13;
		CollMaskWrite = $14;
		ParamRegRead  = $15;
		ParamRegWrite = $16;
		InputEnable   = $17;
		InputDisable  = $18;
		YRwrite       = $19;
		OSread        = $1a;
		SNRead		    = $1b;
    SNwrite0      = $1c;
    SNwrite1      = $1d;
    SNreset       = $1e;
		Bypass        = $1f;



const
		PROG_SC_EPROM_ID   = $0005024093;
		PROG_SC_EPROM_ID_MASK= $1ffffffff;

		PROG_SC_FPGA_ID    = $0000838126;
		PROG_SC_FPGA_ID_MASK : Int64 = $1ffffffff;

		PROG_V_EPROM1_ID   = $000A04C126;
        PROG_V_EPROM1_ID_MASK= $03fffffff;
		PROG_V_EPROM1_ID2   = $000A06C126;

        PROG_V_EPROM2_ID   = $0005026093;
        PROG_V_EPROM2_ID_MASK= $03fffffff;

        PROG_V_EPROM2_ID2   = $0005036093;

        PROG_V_FPGA_ID     = $000290024c;
        PROG_V_FPGA_ID_MASK= $003fffffff;

		PROG_V_FPGA600_7_ID     = $0021460126;
		PROG_V_FPGA600_7_ID_MASK= $003fffffff;

		PROG_V_FPGA600_ID     = $0001460126;
		PROG_V_FPGA600_ID_MASK= $000fffffff;


        PROG_SC_IR_SIZE    = 11;
        PROG_SC_ID_DR_SIZE = 33;
        PROG_V_IR_SIZE     = 21;
        PROG_V_ID_DR_SIZE  = 34;

		CTRL_SC_FPGA_ID    = $09072001B8;
    CTRL_SC_ID_DR_SIZE = 40;

    USER_V_FPGA_ID    = $0925200207;
    USER_V_ID_DR_SIZE = 40;


		V_IR = 5;
		SC_IR = 6;

    SLOW_CTL = 0;
    FAST_CTL = 1;
    BOARD_SN = 2;
    MEZAN_SN = 3;
const RegSz: array[0..31] of  Integer =  (
		    40,     // IDRead = 0x0,
        384,    // HCMaskRead    = 0x1,
        384,    // HCMaskWrite   = 0x2,
		    2,      // RdTrig        = 0x3,
        2,      // WrTrig        = 0x4,
        0,
        69,     // RdCfg         = 0x6, // read control register
        69,     // WrCfg         = 0x7, // write control register

        0,
        0,
        0,
        0,
        0,
        120,    // Wdly          = 0xd, // write delay lines. cs_dly bits in Par
        120,    // Rdly          = 0xe, // read  delay lines. cs_dly bits in Par
        0,

        0,
        0,
        0,
        224,    // CollMaskRead  = 0x13,
        224,    // CollMaskWrite = 0x14,
        6,      // ParamRegRead  = 0x15,
		    6,      // ParamRegWrite = 0x16,
		    0,      // InputEnable   = 0x17,

        0,      // InputDisable  = 0x18,
		    31,     // YRwrite       = 0x19,
		    49,     // OSread        = 0x1a,
		    1,     // SNread        = 0x1b,
		    0,
		    0,
		    0,
		    1       // Bypass        = 0x1f
		);



var NUM_OF_DELAY_GROUPS : integer = 4;
const NUM_OF_DELAY_CHIPS_IN_GROUP = 6;
// var	NUM_AFEB : integer = NUM_OF_DELAY_GROUPS*NUM_OF_DELAY_CHIPS_IN_GROUP;
const MAX_DELAY_GROUPS = 7;
const MAX_DELAY_CHIPS_IN_GROUP = 6;
const MAX_NUM_AFEB = 42;
var NUM_AFEB : integer = 42;

var parlen: Integer;

var  ADC_REF : single = 1.225;

var arJTAGChains: array[0..3] of shortint = (1, 0, 5, 4);

{
var arADCChannel: array[0..NUM_AFEB-1] of Integer =
              (1, 0,
							10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
							10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0);

var arADCChip:    array[0..NUM_AFEB-1] of Integer =
              (2, 2,
							1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

var arADCChannel: array[0..MAX_NUM_AFEB-1] of Integer =
             ( 1, 0,
							10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
							10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
              0, 1, 2, 3, 4, 5, 6, 7, 8,
              0, 1, 2, 3, 4, 5, 6, 7, 8);

var arADCChip:    array[0..MAX_NUM_AFEB-1] of Integer =
              (2, 2,
							1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              3, 3, 3, 3, 3, 3, 3, 3, 3,
              4, 4, 4, 4, 4, 4, 4, 4, 4);

var mapGroupMask: array[0..MAX_NUM_AFEB-1] of Integer =
              (0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3,
               0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3,
               4, 4, 4, 5, 5, 5, 6, 6, 6,
               4, 4, 4, 5, 5, 5, 6, 6, 6);

type
	ADCValues = record
		Ref: string;
		RefVal: single;
		Coef: single;
		Toler: single;
	end;
{
var arVoltages: array[0..3] of ADCValues = ((Ref:'1.8V';RefVal:1.8;Coef:0.002451;Toler:0.1),
											(Ref:'3.3V';RefVal:3.3;Coef:0.004902;Toler:0.1),
											(Ref:'1st 5.5V';RefVal:5.5;Coef:0.007353;Toler:0.1),
											(Ref:'2nd 5.5V';RefVal:5.5;Coef:0.007353;Toler:0.1));
}

var ThreshToler : Integer = 4;

var arVoltages: array[0..3] of ADCValues = ((Ref:'1.8V';RefVal:1.8;Coef:0.005878;Toler:0.1),
											(Ref:'3.3V';RefVal:3.3;Coef:0.005878;Toler:0.2),
											(Ref:'1st 5.5V';RefVal:5.65;Coef:0.005878;Toler:0.2),
											(Ref:'2nd 5.5V';RefVal:5.65;Coef:0.005878;Toler:0.2));

{
var arCurrents: array[0..3] of ADCValues = ((Ref:'1.8V';RefVal:0.667;Coef:0.004902;Toler:0.1),
											(Ref:'3.3V';RefVal:0.726;Coef:0.004902;Toler:0.1),
											(Ref:'1st 5.5V';RefVal:0.176;Coef:0.004902;Toler:0.1),
											(Ref:'2nd 5.5V';RefVal:0.176;Coef:0.004902;Toler:0.1));
}
var arCurrents: array[0..3] of ADCValues = ((Ref:'1.8V';RefVal:0.667;Coef:0.002987;Toler:0.1),
//											(Ref:'3.3V';RefVal:0.726;Coef:0.002987;Toler:0.1),
											(Ref:'3.3V';RefVal:1.1;Coef:0.002987;Toler:0.2),
 											(Ref:'1st 5.5V';RefVal:0.150;Coef:0.002987;Toler:0.1),
											(Ref:'2nd 5.5V';RefVal:0.100;Coef:0.002987;Toler:0.1));
{
											(Ref:'1st 5.5V';RefVal:0.176;Coef:0.002987;Toler:0.1),
											(Ref:'2nd 5.5V';RefVal:0.176;Coef:0.002987;Toler:0.1));
}
// var arTemperature : ADCValues = (Ref:'On Board'; RefVal:21.0; Coef:0.2451;Toler:5.0);
var arTemperature : ADCValues = (Ref:'On Board'; RefVal:25.0; Coef:0.1197;Toler:5.0);

type
	ALCTTYPE = integer;

const
	ALCT288 = 0;  // ALCT 288 Channels
	ALCT384 = 1;  // ALCT 384 Channels
	ALCT672 = 2;  // ALCT 672 Channels

type
	CHAMBER = integer;

const
	ME1_1	= 0; // ME1/1
	ME1_2	= 1; // ME1/2
	ME1_3	= 2; // ME1/3
	ME2_1	= 3; // ME2/1
	ME2_2	= 4; // ME2/2
	ME3_1	= 5; // ME3/1
	ME3_2	= 6; // ME3/2
	ME4_1	= 7; // ME4/1
	ME4_2	= 8; // ME4/2

type
	PWR_SPLY = integer;

const
	V18:PWR_SPLY = 0;	// Power Supply 1.8V
	V33:PWR_SPLY = 1;	// Power Supply 3.3V
	V55_1:PWR_SPLY = 2;	// Power Supply 5.5V (1)
	V55_2:PWR_SPLY = 3;	// Power Supply 5.5V (2)

type
	TALCTType = record
		name	: string;
		alct	: ALCTTYPE ;
		channels	: longint;   // Channels on ALCT
		groups		: longint;     // Delay chips groups
		chips		: longint;      // Number of delay chips in group
		delaylines	: longint;  // Number of lines in delay chip
    pwrchans : longint;
	end;

type
	TChamberType = record
		name	: string;
		chmbtype	: CHAMBER;  // Chamber type
		alct		: ALCTTYPE;    // Appropriate ALCT type
		wires	: longint;   // Wire groups on chamber
    afebs_on551 : longint;
    afebs_on552 : longint;
	end;

const alct_table : array[0..2] of TALCTType =
(
	( name:'ALCT288'; alct:ALCT288; channels:288; groups:3; chips:6; delaylines:16; pwrchans:3), // ALCT 288
	( name:'ALCT384'; alct:ALCT384; channels:384; groups:4; chips:6; delaylines:16; pwrchans:4), // ALCT 384
	( name:'ALCT672'; alct:ALCT672; channels:672; groups:7; chips:6; delaylines:16; pwrchans:4)  // ALCT 672
);

const chamb_table : array[0..8] of TChamberType =
(
	( name:'ME1/1'; chmbtype:ME1_1; alct:ALCT288; wires:288; afebs_on551:18; afebs_on552:0), // ME 1/1
	( name:'ME1/2'; chmbtype:ME1_2; alct:ALCT384; wires:384; afebs_on551:12; afebs_on552:12), // ME 1/2
	( name:'ME1/3'; chmbtype:ME1_3; alct:ALCT288; wires:192; afebs_on551:12; afebs_on552:0), // ME 1/3
	( name:'ME2/1'; chmbtype:ME2_1; alct:ALCT672; wires:672; afebs_on551:18; afebs_on552:24), // ME 2/1
	( name:'ME2/2'; chmbtype:ME2_2; alct:ALCT384; wires:384; afebs_on551:12; afebs_on552:12), // ME 2/2
	( name:'ME3/1'; chmbtype:ME3_1; alct:ALCT672; wires:576; afebs_on551:12; afebs_on552:24), // ME 3/1
	( name:'ME3/2'; chmbtype:ME3_2; alct:ALCT384; wires:384; afebs_on551:12; afebs_on552:12), // ME 3/2
	( name:'ME4/1'; chmbtype:ME4_1; alct:ALCT672; wires:576; afebs_on551:12; afebs_on552:24), // ME 4/1
	( name:'ME4/2'; chmbtype:ME4_2; alct:ALCT384; wires:384; afebs_on551:12; afebs_on552:12)  // ME 4/2
);


type
	DelayChip = record
		Value   : 0..15;
		Pattern : word;
	end;

type
	DelayGroup = array [0..MAX_DELAY_CHIPS_IN_GROUP-1] of DelayChip;

type
	ALCTDelays = array [0..MAX_DELAY_GROUPS-1] of DelayGroup;

type
	TPtrnImage = array[0..5] of array[0..7] of byte;

type
	ALCTSTATUS =  (	EALCT_SUCCESS,   	// Successful completion
					EALCT_FILEOPEN,     // Filename could not be opened
					EALCT_FILEDEFECT, 	// Configuration file inconsistency
					EALCT_PORT, 		// JTAG Device problem
					EALCT_CHAMBER, 		// Bad chamber_type number
					EALCT_ARG, 			// Argument Out Of Range
					EALCT_TESTFAIL);	 	// Test failure);


type
	alct_idreg = record
	chip:		  byte;
	version:  byte;
	day:		  byte;
	month:		byte;
	year:		  word;
	end;

type
	sc_idreg = record
	chip:		byte;
	version:    byte;
	day:		byte;
	month:		byte;
	year:		word;
	end;

type
  v_idreg = record
  chip: byte;
  version: byte;
  day: byte;
  month: byte;
  year: word;
  end;

type
	Rfield = record
	name:		string;
	mask:		longword;
	bit:		longword;
	default:	longword;
	end;

type
  ConfigParameter = record
  name: string;
  descr: string;
  value: string;
  default: string;
  value_type: integer;
  end;

const
  FIFO_RESET = $C;
  FIFO_READ  = $A;
  FIFO_WRITE = $9;
type
  JTAGReg = record
  val: longint;
  len: longint;
  end;

type
  MeasDlyChan = array[0..41] of array[0..15] of Currency;
  MeasDly = array[0..41] of Currency;

const
  IDReadReg : JTAGReg = (val: $00; len: 40);
  RdDataReg	: JTAGReg = (val: $01; len: 16);
  RdCntReg	: JTAGReg = (val: $02; len: 128);
  WrDatF 	  : JTAGReg = (val: $04; len: 16);
  WrAddF 	  : JTAGReg = (val: $06; len: 10);
  WrParamF	: JTAGReg = (val: $08; len: 4);
  WrFIFO	  : JTAGReg = (val: $0A; len: 1);
  WrDlyF	  : JTAGReg = (val: $0B; len: 8);
  ALCTWdly	: JTAGReg = (val: $0D; len: 120);
  ALCTRdly	:	JTAGReg = (val: $0E; len: 120);
  RdParamDly :	JTAGReg = (val: $15; len: 6); // !! Change for Different Boards
  WrParamDly :	JTAGReg = (val: $16; len: 6); // !! Change for Different Boards

// Fields of Configuration Register Parameters
const CRfld: array[0..25] of Rfield =
(
	(name: 'trig_mode';		mask: 3; 	bit: 0; 	default: 0),
	(name: 'ext_trig_en';	mask: 1;	bit: 2; 	default: 0),
	(name: 'pretrig_halt';	mask: 1; 	bit: 3; 	default: 0),
	(name: 'inject';		mask: 1; 	bit: 4; 	default: 0),
	(name: 'inject_mode';	mask: 1; 	bit: 5; 	default: 0),
	(name: 'inject_mask';	mask: $7f; 	bit: 6; 	default: $7f),
	(name: 'nph_thresh';	mask: 7; 	bit: 13; 	default: 2),
	(name: 'nph_pattern';	mask: 7; 	bit: 16; 	default: 4),
	(name: 'drift_delay';	mask: 3; 	bit: 19; 	default: 3),
	(name: 'fifo_tbins';	mask: $1f; 	bit: 21; 	default: 7),
	(name: 'fifo_pretrig';	mask: $1f; 	bit: 26; 	default: 1),
	(name: 'fifo_mode';		mask: 3; 	bit: 31; 	default: 1),
	(name: 'fifo_lastlct';	mask: 7; 	bit: 33; 	default: 3),
	(name: 'l1a_delay';		mask: $ff; 	bit: 36; 	default: $78),
	(name: 'l1a_window';   	mask: $f; 	bit: 44; 	default: 3),
	(name: 'l1a_offset';   	mask: $f; 	bit: 48; 	default: 0),
	(name: 'l1a_internal'; 	mask: 1; 	bit: 52; 	default: 0),
	(name: 'board_id'; 		mask: 7; 	bit: 53; 	default: 5),
	(name: 'bxn_offset'; 	mask: $f; 	bit: 56; 	default: 0),
	(name: 'ccb_enable'; 	mask: 1; 	bit: 60; 	default: 0),
	(name: 'alct_jtag_ds'; 	mask: 1; 	bit: 61; 	default: 1),
	(name: 'alct_tmode'; 	mask: 3; 	bit: 62; 	default: 0),
	(name: 'alct_amode'; 	mask: 3; 	bit: 64; 	default: 0),
	(name: 'alct_mask_all';	mask: 1; 	bit: 66; 	default: 0),
	(name: 'trig_info_en';	mask: 1; 	bit: 67; 	default: 0),
	(name: 'sn_select';	mask: 1; 	bit: 68; 	default: 0)
);

const CRdescr: array[0..24] of string =
(
	'Virtex Trigger Mode',
	'External Trigger Enable',
	'Pre-Trigger and Halt Mode',
	'Inject Test Pattern',
	'Injector Repeat Mode (requires Inject Test Pattern = 1)',
	'Injector LCT Chip Mask [6..0]. Bit n maps to LCT chip n',
	'Number of Planes Hit Threshold for Pre-Trigger',
	'Pattern hits required after drift delay to allow an LCT-trigger',
	'Drift delay after pre-trigger (25ns steps)',
	'Total number of FIFO time bins per wire group',
	'FIFO time bins before per-trigger (included in total)',
	'FIFO Mode',
	'FIFO: Last LCT chip to be read out',
	'Level 1 Accept delay after pre-trigger',
	'Level 1 Accept window width (25ns steps)',
	'Level 1 Accept counter Pre-Load value (arbitrary value)',
	'L1A generated internally during L1A window',
	'ALCT2001 circuit board ID (arbitrary value)',
	'Bunch Crossing Counter Offset (set to match Cathode LCT bxn)',
	'CCB Disable (Ignores CCb signals l1A, BXn, BxReset)',
	'ALCT-bus Enable (affects all LCT chips)',
	'ALCT Test Pattern Mode (affects all LCT chips if ALCT-bus Enable = 1)',
	'ALCT Accelerator Muon Mode (affects all LCT chips)',
	'Mask All Wire Group inputs to LCT chips (affects all LCT chips)',
	'ALCT-bus spare signals (not currently used, set to be 0)'
);

{
const Config: array[0..10] of ConfigParameter =
(

);
}

const OperDescr: array[0..10] of string =
(
	'Blank Check of Slow Control EPROM',
	'Blank Check of Virtex 1000 EPROM #1',
	'Blank Check of Virtex 1000 EPROM #2',
	'Blank Check of Virtex 600 EPROM',
	'Load Slow Control EPROM',
	'Load Slow Control FPGA',
	'Load Virtex 600 EPROM',
	'Load Virtex 600 FPGA',
	'Load Virtex 1000 EPROM #1',
	'Load Virtex 1000 EPROM #2',
	'Load Virtex 1000 FPGA'
);

procedure SetChain(ch: integer);

function WriteRegister(const reg: integer; value: string): string; overload;
function WriteRegister(const reg: integer; value: longword): string; overload;
function ReadRegister(const reg: integer): string;

procedure PrepareDelayLinePatterns(var dlys: ALCTDelays;  image: TPtrnImage);
procedure Write6DelayLines(const dlys: DelayGroup; mask: word); overload;
procedure Write6DelayLines(var dlys: DelayGroup; mask: word; freadback: boolean); overload;
procedure Write6DelayLines(const dlys: DelayGroup; mask: word; out readdlys: DelayGroup); overload;
{
procedure Write6DelayLines(const dlys: ALCTDelays; mask: word); overload;
procedure Write6DelayLines(var dlys: ALCTDelays; mask: word; freadback: boolean); overload;
procedure Write6DelayLines(const dlys: ALCTDelays; mask: word; out readdlys: ALCTDelays ); overload;
}
function SCReadEPROMID: string;
function SCReadFPGAID: string;
function SCEraseEPROM: boolean;
function SCBlankCheckEPROM(out errs: longint): boolean;

function VReadFPGAID: string;
function VReadEPROMID1: string;
function VReadEPROMID2: string;
function VEraseEPROM1: boolean;
function VEraseEPROM2: boolean;
function V600EraseEPROM: boolean;
function VBlankCheckEPROM1(out errs: longint): boolean;
function VBlankCheckEPROM2(out errs: longint): boolean;
function V600BlankCheckEPROM(out errs: longint): boolean;

procedure SetThreshold(const ch: integer; value: integer);
function  ReadThreshold(const ch: integer): integer;
procedure SetGroupStandbyReg(const group: integer; value: integer);
function  ReadGroupStandbyReg(const group: integer): integer;
procedure SetStandbyReg(const value: string);
function  ReadStandbyReg: string;
procedure SetStandbyForChan(const chan: integer; onoff: boolean);
procedure SetTestPulsePower(const sendval: integer);
function  ReadTestPulsePower: integer;
procedure SetTestPulsePowerAmp(const value: integer);
procedure SetTestPulseWireGroupMask(const value: integer);
function  ReadTestPulseWireGroupMask: integer;
procedure SetTestPulseStripLayerMask(const value: integer);
function  ReadTestPulseStripLayerMask: integer;
function  ReadVoltageADC(const chan: integer): integer;
function  ReadVoltage(const chan: integer): single;
function  ReadCurrentADC(const chan: integer): integer;
function  ReadCurrent(const chan: integer): single;
function  ReadTemperatureADC: integer;
function  ReadTemperature: single;

function ALCTEnableInput: integer;
function ALCTDisableInput: integer;

procedure SetFIFOMode(mode: integer);
procedure SetFIFOReset;
procedure SetFIFORead;
procedure SetFIFOWrite;
procedure SetFIFOReadWrite;
procedure FIFOClock;
function ReadFIFOCounters: AnsiString;
procedure SetFIFOValue(val: WORD);
procedure SetFIFOChannel(ch: integer; startdly: integer);
procedure SetTestBoardDelay(delay: integer);
procedure SetALCTBoardDelay(ch: integer; delay: integer);
procedure SetTestBoardFIFO(const fifoval: integer; const fifochan: integer; const numwords: integer = 1; const startdelay: integer = 2; const  alctdelay: integer = 0; const testboarddelay: integer = 0);
procedure SetDelayTest(const fifoval: integer; const fifochan: integer; const startdelay: integer = 2; const  alctdelay: integer = 0; const testboarddelay: integer = 0);
function ReadFIFOValue: word;
function ReadFIFO(var vals: array of word; numwords: integer; var cntrs: array of byte): string;
//Iav
procedure ReadFIFOfast(numwords: integer; var cntrs: array of byte);
procedure PinPointRiseTime(var TimeR: array of integer; value: integer; ch: integer; StartDly_R: integer; alct_dly: integer; num: integer);
procedure PinPointRiseTime50(var TimeR: array of integer; var TimeR_50: array of Currency; value: integer; ch: integer; StartDly_R: integer; alct_dly: integer; num: integer);
procedure PinPointFallTime(var TimeF: array of integer; value: integer; ch: integer; StartDly_F: integer; alct_dly: integer; num: integer);

function FindStartDly(var StartDly_R: integer; var StartDly_F: integer; value: integer; ch: integer; alct_dly: integer; num: integer): boolean;
function FindStartDlyPin(var StartDly_R: integer; var StartDly_F: integer; value: integer; ch: integer; alct_dly: integer; num: integer; var RegMaskDone: array of word): boolean;
procedure MeasureDelay(ch: integer; var PulseWidth: array of Currency; var BeginTime_Min: MeasDly; var DeltaBeginTime: MeasDlyChan;
                                    var Delay_Time: MeasDlyChan; var AverageDelay_Time: MeasDly; var ErrMeasDly: integer; var RegMaskDone: array of word);

//Write delay test results to this file D:\alct\DelayTest_Files\
procedure WriteToFile(BeginTime_Min: MeasDly;
                   DeltaBeginTime: MeasDlyChan;
                   DelayTime: MeasDlyChan;
                   Average: MeasDly;
                   BoardNum: string;
                   PathString: string);

{
procedure MeasureDelay(ch: integer;     var BeginTime_Min: MeasDly;     var DeltaBeginTime: MeasDlyChan; var Delay_Time: MeasDlyChan;
                                        var AverageDelay_Time: MeasDly; var ErrMeasDly: integer);

function FindStartDlyR(var StartDlyR_f: integer; StartDly: integer; value: integer; ch: integer; alct_dly: integer; num: integer): boolean;
function FindStartDlyF_tmp(var StartDlyF_f: integer; StartDly: integer; value: integer; ch: integer; alct_dly: integer; num: integer): boolean;
function FindStartDlyF(var StartDlyF_f: integer; value: integer; ch: integer; alct_dly: integer; num: integer): boolean;
}
implementation
uses JTAGLib, JTAGUtils,SysUtils ;

procedure SetChain(ch: integer);
begin
	set_chain(arJTAGChains[ch]);
end;

function WriteRegister(const reg: integer; value: string): string; overload;
begin
	Result := '';
	WriteIR(IntToHex(reg, 2), V_IR);
	Result := WriteDR(value, RegSz[reg]);
end;

function WriteRegister(const reg: integer; value: longword): string; overload;
begin
	Result := '';
	WriteIR(IntToHex(reg, 2), V_IR);
	Result := WriteDR(IntToHex(value, sizeof(longword)), RegSz[reg]);
end;


function ReadRegister(const reg: integer): string;
begin
	Result := '';
	WriteIR(IntToHex(reg, 2), V_IR);
	Result := ReadDR('0', RegSz[reg]);
end;

procedure PrepareDelayLinePatterns(var dlys: ALCTDelays;  image: TPtrnImage);
var
	i : integer;
begin
	for i:=0 to 3 do
	begin
	dlys[i][0].Pattern := FlipByte(image[1][i*2]) or (word(image[0][i*2]) shl 8);
	dlys[i][1].Pattern := FlipByte(image[3][i*2]) or (word(image[2][i*2]) shl 8);
	dlys[i][2].Pattern := FlipByte(image[5][i*2]) or (word(image[4][i*2]) shl 8);
	dlys[i][3].Pattern := FlipByte(image[1][i*2]) or (word(image[0][i*2+1]) shl 8);
	dlys[i][4].Pattern := FlipByte(image[3][i*2]) or (word(image[2][i*2+1]) shl 8);
	dlys[i][5].Pattern := FlipByte(image[5][i*2]) or (word(image[4][i*2+1]) shl 8);
    end;
end;



procedure Write6DelayLines(const dlys: DelayGroup; mask: word); overload;
var
	i	: integer;
begin
//		r:=$3f and (not(mask shl 2));
//		WriteRegister(ParamRegWrite, IntToHex($3f and (not(mask shl 2)),2));
//		ReadRegister(ParamRegRead);

    WriteIR(IntToHex(ParamRegWrite,2), V_IR);
    WriteDR(IntToHex($1ff and (not(mask shl 2)),3), parlen);

    WriteIR(IntToHex(ParamRegRead,2), V_IR);
    ReadDR('0', parlen);

		WriteIR(IntToHex(Wdly,2), V_IR);
		StartDRShift;
		for i:=0 to 5 do
		begin
			ShiftData(Longword(FlipHalfByte(dlys[i].Value)), 4, false);
			ShiftData(Longword(dlys[i].Pattern), 16, i=5);
		end;
		ExitDRShift;
    WriteIR(IntToHex(ParamRegWrite,2), V_IR);
    WriteDR('1ff', parlen);
//		WriteRegister(ParamRegWrite, IntToHex($3f,2));
//		Log.Lines.Add('Read Back ParamReg - 0x' + ReadRegister(ParamRegRead));
end;

procedure Write6DelayLines(var dlys: DelayGroup; mask: word; freadback: boolean); overload;
var
	i	: integer;
	value, pattern: longword;

begin
//		WriteRegister(ParamRegWrite, IntToHex($3f and (not(mask shl 2)),2));
//		ReadRegister(ParamRegRead);
    WriteIR(IntToHex(ParamRegWrite,2), V_IR);
    WriteDR(IntToHex($1ff and (not(mask shl 2)),3), parlen);

    WriteIR(IntToHex(ParamRegRead,2), V_IR);
    ReadDR('0', parlen);

		WriteIR(IntToHex(Wdly,2), V_IR);
		StartDRShift;
		for i:=0 to 5 do
		begin
			value := ShiftData(Longword(FlipHalfByte(dlys[i].Value)), 4, false);
			pattern := ShiftData(Longword(dlys[i].Pattern), 16, i=5);
			if freadback then
			begin
				dlys[i].Value := value;
				dlys[i].Pattern := pattern;
			end;
		end;
		ExitDRShift;
		//WriteRegister(ParamRegWrite, IntToHex($3f,2));
    WriteIR(IntToHex(ParamRegWrite,2), V_IR);
    WriteDR('1ff', parlen);

end;


procedure Write6DelayLines(const dlys: DelayGroup; mask: word; out readdlys: DelayGroup ); overload;
var
	i	: integer;
begin
//		r:=$3f and (not(mask shl 2));
//		WriteRegister(ParamRegWrite, IntToHex($3f and (not(mask shl 2)),2));
//		ReadRegister(ParamRegRead);
    WriteIR(IntToHex(ParamRegWrite,2), V_IR);
    WriteDR(IntToHex($1ff and (not(mask shl 2)),3), parlen);

    WriteIR(IntToHex(ParamRegRead,2), V_IR);
    ReadDR('0', parlen);

		WriteIR(IntToHex(Wdly,2), V_IR);
		StartDRShift;
		for i:=0 to 5 do
		begin
			readdlys[i].Value := ShiftData(Longword(FlipHalfByte(dlys[i].Value)), 4, false);
			readdlys[i].Pattern := ShiftData(Longword(dlys[i].Pattern), 16, i=5);
		end;
		ExitDRShift;
//		WriteRegister(ParamRegWrite, IntToHex($3f,2));
    WriteIR(IntToHex(ParamRegWrite,2), V_IR);
    WriteDR('1ff', parlen);

//		Log.Lines.Add('Read Back ParamReg - 0x' + ReadRegister(ParamRegRead));
end;


function SCReadEPROMID: string;
var
	p: longint;
begin
		WriteIR('7F0',11);
		for p:=1 to 1000000 do begin end;
		WriteIR('7FF',11);
		WriteIR('7FE',11);
		Result := ReadDR('ffffffff',33);
		WriteIR('7FF',11);
end;

function SCReadFPGAID: string;
var
	p: longint;
begin
		WriteIR('7F0',11);
		for p:=1 to 1000000 do begin end;
		WriteIR('7FF',11);
		WriteIR('6FF',11);
		Result := ReadDR('1fffffffe',33);
		WriteIR('7FF',11);
end;


function SCEraseEPROM: boolean;
var
	p: 	longint;
begin
		if	(StrToInt64('$'+SCReadEPROMID) and PROG_SC_EPROM_ID_MASK) = PROG_SC_EPROM_ID then
		begin
			WriteIR('7E8',11);
			WriteDR('4',7);
			WriteIR('7EB',11);
			WriteDR('1',17);
			WriteIR('7EC',11);
			for p:=1 to 1000000 do begin end;
			for p:=1 to 1000000 do begin end;
			WriteIR('7F0',11);
			for p:=1 to 1100000 do begin end;
			WriteIR('7FF',11);
			WriteDR('0',2);
			Result := true;
		end
		else
			Result := false;
end;

function  SCBlankCheckEPROM(out errs: longint): boolean;
const
	len = 16385;
	blocks = 64;
var
	i,p: 	longint;
	data: longword;
//	errs: longint;
begin
		if	(StrToInt64('$'+SCReadEPROMID) and PROG_SC_EPROM_ID_MASK) = PROG_SC_EPROM_ID then
		begin
			WriteIR('7E8',11);
			WriteDR('34',7);
			WriteIR('7E5',11);
			for p:=1 to 10000 do begin end;

			errs := 0;
			for i:=0 to blocks-1 do
			begin
				StartDRShift;
				for p:= 0 to (((len-1) div 32)) do
				begin
					data := 0;
					if (len - 32*p) > 32 then
					begin
						data := ShiftData($FFFFFFFF, 32, false);
						if data <> $FFFFFFFF then
							begin
								Inc(errs);
							end;
					end
					else
					begin
						data := ShiftData($FFFFFFFF, len - 32*p, true);
						if data <> ($FFFFFFFF shr (32-(len - 32*p))) then
							begin
								Inc(errs);
							end;
					end;
				end;
				ExitDRShift;
				if errs > 0 then break;
			end;
			WriteIR('7F0',11);
			for p:=1 to 1100000 do begin end;
			WriteIR('7FF',11);
			WriteIR('7F0',11);
			for p:=1 to 1100000 do begin end;
			WriteIR('7FF',11);
			WriteIR('7F0',11);
			for p:=1 to 1100000 do begin end;
			WriteIR('7FF',11);
			if errs = 0 then Result := true;
		end
		else
			Result := false;
end;

function  VReadFPGAID: string;
begin
		WriteIR('1F',5);
		WriteIR('1F',5);
		WriteIR('9',5);
		Result := ReadDR('ffffffff',34);
		WriteIR('1F',5);
end;

function  VReadEPROMID1: string;
var
	p: longint;
begin
		WriteIR('1FF0',13);
		for p:=1 to 1000000 do begin end;
		WriteIR('1FFF',13);
		WriteIR('1FFF',13);
		WriteIR('1FFE',13);
		Result := ReadDR('ffffffff',34);
		WriteIR('1FFF',13);
end;

function  VReadEPROMID2: string;
var
	p: longint;
begin
		WriteIR('1FFFF0',21);
		for p:=1 to 1000000 do begin end;
		WriteIR('1FFFFF',21);
		WriteIR('1FFFFE',21);
		Result := ReadDR('ffffffff',34);
		WriteIR('1FFFFFF',21);
end;

function  VEraseEPROM1: boolean;
var
	p: 	longint;
begin
		if	((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM1_ID_MASK) = PROG_V_EPROM1_ID) or
                        ((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM1_ID_MASK) = PROG_V_EPROM1_ID2) then
		begin
			WriteIR('1FE8',13);
			WriteDR('8',8);
			WriteIR('1FEB',13);
			WriteDR('2',18);
			WriteIR('1FEC',13);
			for p:=1 to 1000000 do begin end;
			for p:=1 to 1000000 do begin end;
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			Result := true;
		end
		else
			Result := false;
end;

function  VEraseEPROM2: boolean;
var
	p: 	longint;
begin
		if	((StrToInt64('$'+VReadEPROMID2) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID) or
                ((StrToInt64('$'+VReadEPROMID2) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID2) then
		begin
			WriteIR('1FFFE8',21);
			WriteDR('4',8);
			WriteIR('1FFFEB',21);
			WriteDR('1',18);
			WriteIR('1FFFEC',21);
			for p:=1 to 1000000 do begin end;
			for p:=1 to 1000000 do begin end;
			WriteIR('1FFFF0',21);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFFFF',21);
			Result := true;
		end
		else
			Result := false;
end;


function  V600EraseEPROM: boolean;
var
	p: 	longint;
begin
		if	((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID) or
                        ((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID2) then
		begin
			WriteIR('1FE8',13);
			WriteDR('8',8);
			WriteIR('1FEB',13);
			WriteDR('2',18);
			WriteIR('1FEC',13);
			for p:=1 to 1000000 do begin end;
			for p:=1 to 1000000 do begin end;
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			Result := true;
		end
		else
			Result := false;
end;

function VBlankCheckEPROM1(out errs: longint): boolean;
const
	len = 8192;
	blocks = 512;
var
	i,p: 	longint;
	data: longword;
//	errs: longint;
begin
		if	((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM1_ID_MASK) = PROG_V_EPROM1_ID) or
                        ((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM1_ID_MASK) = PROG_V_EPROM1_ID2) then
		begin
			WriteIR('1FE8',13);
			WriteDR('34',7);
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FE8',13);
			WriteDR('34',7);
			WriteIR('1FEB',13);
			WriteDR('0',17);
			errs := 0;

			for i:= 0 to blocks-1 do
			begin
				WriteIR('1FEF',13);
				for p:=1 to 5000 do begin end;
				StartDRShift;
				for p:= 0 to (((len-1) div 32)) do
				begin
					data := 0;
					if (len - 32*p) > 32 then
					begin
						data := ShiftData($FFFFFFFF, 32, false);
						if data <> $FFFFFFFF then
						begin
							Inc(errs);
//							ExitDRShift;
//							break;
						end;
					end
					else
					begin
						data := ShiftData($FFFFFFFF, len - 32*p, true);
						if data <> ($FFFFFFFF shr (32-(len - 32*p))) then
						begin
							Inc(errs);
//							ExitDRShift;
//							break;
						end;
					end;
				end;
				ExitDRShift;
				if errs > 0 then break;
			end;
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FFF',13);
			WriteDR('0',2);
			if errs = 0 then Result := true;

		end
		else
			Result := false;
end;

function  VBlankCheckEPROM2(out errs: longint): boolean;
const
	len = 8192;
	blocks = 512;
var
	i,p: 	longint;
	data: longword;
//	errs: longint;
begin
		if	((StrToInt64('$'+VReadEPROMID2) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID) or
                        ((StrToInt64('$'+VReadEPROMID2) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID2) then
		begin
			WriteIR('1FFFE8',21);
			WriteDR('34',7);
			WriteIR('1FFFF0',21);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFFE8',21);
			WriteDR('34',7);
			WriteIR('1FFFEB',21);
			WriteDR('0',17);
			errs := 0;

			for i:= 0 to blocks-1 do
			begin
				WriteIR('1FFFEF',21);
				for p:=1 to 5000 do begin end;
				StartDRShift;
				for p:= 0 to (((len-1) div 32)) do
				begin
					data := 0;
					if (len - 32*p) > 32 then
					begin
						data := ShiftData($FFFFFFFF, 32, false);
						if data <> $FFFFFFFF then
						begin
							Inc(errs);
//							ExitDRShift;
//							break;
						end;
					end
					else
					begin
						data := ShiftData($FFFFFFFF, len - 32*p, true);
						if data <> ($FFFFFFFF shr (32-(len - 32*p))) then
						begin
							Inc(errs);
//							ExitDRShift;
//							break;
						end;
					end;
				end;
				ExitDRShift;
				if errs > 0 then break;
			end;
			WriteIR('1FFFF0',21);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFFFF',21);
			WriteIR('1FFFFF',21);
			WriteDR('0',2);
			if errs = 0 then Result := true;

		end
		else
			Result := false;
end;


function  V600BlankCheckEPROM(out errs: longint): boolean;
const
	len = 16385;
	blocks = 256;
var
	i,p: 	longint;
	data: longword;
//	errs: longint;
begin
		if	((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID) or
                        ((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID2) then
		begin
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FFF',13);
			WriteIR('1FFE',13);
			WriteDR('00ffffffff',33);
			WriteIR('1FFF',13);
			WriteIR('1FE8',13);
			WriteDR('34',7);
{			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FE8',13);
			WriteDR('34',7);
			WriteIR('1FEB',13);
			WriteDR('0',17);
}
			WriteIR('1FE5',13);
//			for p:=1 to 50000 do begin end;
			errs := 0;

			for i:= 0 to blocks-1 do
			begin

//				for p:=1 to 5000 do begin end;
				StartDRShift;
				for p:= 0 to (((len-1) div 32)) do
				begin
					data := 0;
					if (len - 32*p) > 32 then
					begin
						data := ShiftData($FFFFFFFF, 32, false);
						if data <> $FFFFFFFF then
						begin
							Inc(errs);
//							ExitDRShift;
//							break;
						end;
					end
					else
					begin
						data := ShiftData($FFFFFFFF, len - 32*p, true);
						if data <> ($FFFFFFFF shr (32-(len - 32*p))) then
						begin
							Inc(errs);
//							ExitDRShift;
//							break;
						end;
					end;
				end;
				ExitDRShift;
				if errs > 0 then break;
			end;
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			if errs = 0 then Result := true;

		end
		else
			Result := false;
end;

procedure SetThreshold(const ch: integer; value: integer);
const len = 12;
var
	i, k, temp, tmp, realch:   integer;
begin
	if ActiveHW then
	begin
		temp := 0;
    realch := ch;
    if ch >= 33 then realch := ch+3;
//		for k:=0 to 2000000 do begin temp:= temp; end;
                for k:=0 to 2000 do begin temp:= temp; end;
		tmp := value and $FF;
		tmp := tmp or (( realch mod 12) shl 8);
    //tmp := tmp or (arADCChannel[ch] shl 8);
		for i:=0 to len-1 do
			temp := temp or (((tmp shr i)  and $1) shl (11-i));
		WriteIR(IntToHex($8+ (realch div 12),2), SC_IR);
//		WriteIR(IntToHex($8+ arADCChip[ch],2), SC_IR);
		WriteDR(IntToHex(temp,3), len);
	end;
end;

function  ReadThreshold(const ch: integer): integer;
const len = 11;
var
	i,k		: integer;
	tmp, temp: integer;
begin
	Result := 0;
	if ActiveHW then
	begin
//		for k:=0 to 2000000 do begin temp:= temp; end;
                for k:=0 to 2000 do begin temp:= temp; end;
		temp := 0;
		tmp := arADCChannel[ch];
		for i:=0 to 3 do
			temp := temp or (((tmp shr i)  and $1) shl (3-i));
		WriteIR(IntToHex($10+arADCChip[ch],2), SC_IR);
		WriteDR(IntToHex(temp,3),len);
//		for k:=0 to 2000000 do begin temp:= temp; end;
                for k:=0 to 2000 do begin temp:= temp; end;
		WriteIR(IntToHex($10+arADCChip[ch],2), SC_IR);
		tmp := StrToInt('$' + ReadDR(IntToHex(temp,3),len));
		temp := 0;
		for i:=1 to len-1  do
			temp := temp or (((tmp shr i) and $1) shl (10-i));
		Result := temp;
	end;
end;

procedure SetGroupStandbyReg(const group: integer; value: integer);
const
	wgroups = 7;
var
	data	: string;
	res		: int64;
begin
	data := ReadStandbyReg;
	if (data <> '') and (group >=0) and (group < wgroups) then
	begin
		res := StrToInt64('$'+data);
		res := (res xor ((res shr (group*6) and (Int64($3f))) shl (6*group))) or ((Int64(value and $3F)) shl (group*6));
		SetStandbyReg(IntToHex(res, 12));
	end;
end;

function  ReadGroupStandbyReg(const group: integer): integer;
const
	wgroups = 7;
var
	data	: string;
begin
	Result := 0;
	data := ReadStandbyReg;
	if (data <> '') and (group >= 0) and (group < (wgroups)) then
	Result := (StrToInt64('$'+data) shr (group*6)) and $3F;
end;

procedure SetStandbyReg(const value: string);
const
	len = 42;
var
	i	: integer;
begin
	if ActiveHW then
	begin
		WriteIR('24', SC_IR);
		WriteDR(value, len);
	end;
end;

function  ReadStandbyReg: string;
const len = 42;
var
	i	: integer;
begin
	Result := '';
	if ActiveHW then
	begin
		WriteIR('25', SC_IR);
		Result := ReadDR('0',len);
	end;
end;

procedure SetStandbyForChan(const chan: integer; onoff: boolean);
var
	value	: integer;
begin
	if (chan >= 0) and (chan < 42) then
	begin
		value := ReadGroupStandbyReg(chan div 6) and $3F;
		value := (value xor (((value shr (chan mod 6)) and $1 ) shl (chan mod 6) )) or (Integer(onoff) shl (chan mod 6));
		SetGroupStandbyReg(chan div 6, value);
	end;
end;

procedure SetTestPulsePower(const sendval: integer);
const
	len = 1;
begin
	if ActiveHW then
	begin
		WriteIR('26', SC_IR);
		WriteDR(IntToHex(sendval,1), len);
	end;
end;

function  ReadTestPulsePower: integer;
const
	len = 1;
begin
	Result := -1;
	if ActiveHW then
	begin
		WriteIR('27', SC_IR);
		Result := StrToInt('$'+ReadDR('0',len));
	end;
end;

procedure SetTestPulsePowerAmp(const value: integer);
const
	len = 9;
var
	i, temp	: integer;
begin
	if ActiveHW then
	begin
		temp := 0;
		for i:=0 to len-2 do temp := temp or (((value shr i)  and $1) shl (7-i));
		WriteIR('3', SC_IR);
		WriteDR(IntToHex(temp, 3), len);
	end;
end;

procedure SetTestPulseWireGroupMask(const value: integer);
const len = 7;
begin
	if ActiveHW then
	begin
		WriteIR('20', SC_IR);
		WriteDR(IntToHex(value and $7f, 2), len);
	end;
end;

function  ReadTestPulseWireGroupMask: integer;
const len = 7;
begin
	Result := -1;
	if ActiveHW then
	begin
		WriteIR('21', SC_IR);
		Result := StrToInt('$'+ReadDR('0', len));
	end;
end;

procedure SetTestPulseStripLayerMask(const value: integer);
const len = 6;
begin
	if ActiveHW then
	begin
		WriteIR('22', SC_IR);
		WriteDR( IntToHex(value and $3f, 2), len);
	end;
end;

function  ReadTestPulseStripLayerMask: integer;
const len = 6;
begin
	Result := -1;
	if ActiveHW then
	begin
		WriteIR('23', SC_IR);
		Result := StrToInt('$'+ReadDR('0', len));
	end;
end;

function  ReadVoltageADC(const chan: integer): integer;
const len = 11;
var
	i, temp: integer;
begin
	Result := 0;
	if ActiveHW then
	begin
		temp := 0;
		for i:=0 to 3 do
			temp := temp or ((((chan+6) shr i)  and $1) shl (3-i));

		for i:=0 to 2 do
		begin
			WriteIR('12', SC_IR);
			Result := StrToInt('$'+ReadDR(IntToHex(temp,3), len));
		end;
		temp := Result;
		Result := 0;
		for i:=1 to len-1  do
			Result := Result or (((temp shr i) and $1) shl (10-i));
	end;
end;

function  ReadVoltage(const chan: integer): single;
begin
  Result := ReadVoltageADC(chan)*arVoltages[chan].Coef;
end;

function  ReadCurrentADC(const chan: integer): integer;
const len = 11;
var
	i, temp: integer;
begin
	Result := 0;
	if ActiveHW then
	begin
		temp := 0;
		for i:=0 to 3 do
			temp := temp or ((((chan+2) shr i)  and $1) shl (3-i));
		for i:=0 to 2 do
		begin
			WriteIR('12', SC_IR);
			Result := StrToInt('$'+ReadDR(IntToHex(temp,3), len));
		end;
		temp := Result;
		Result := 0;
		for i:=1 to len-1  do
			Result := Result or (((temp shr i) and $1) shl (10-i));
	end;
end;

function  ReadCurrent(const chan: integer): single;
begin
  Result := ReadCurrentADC(chan)*arCurrents[chan].Coef;
end;

function  ReadTemperatureADC: integer;
const len = 11;
var
	i, temp: integer;
begin
	Result := 0;
	if ActiveHW then
	begin
		for i:=0 to 2 do
		begin
			WriteIR('12', SC_IR);
			temp := StrToInt('$'+ReadDR('5', len));
		end;

		for i:=1 to len-1  do
			Result := Result or (((temp shr i) and $1) shl (10-i));
	end;
end;

function  ReadTemperature: single;
begin
  Result := ReadTemperatureADC*arTemperature.Coef-50;
end;

// Test Board FIFO Functions

procedure SetFIFOMode(mode: integer);
begin
  if ActiveHW then
  begin
    WriteIR(WrParamF.val, V_IR);
    WriteDR(IntToHex(($8 or mode) and $F,1),WrParamF.len);
  end;

end;

procedure SetFIFOReset;
begin
  if ActiveHW then
  begin
    WriteIR(WrParamF.val, V_IR);
    WriteDR(8,WrParamF.len);
    WriteIR(WrParamF.val, V_IR);
    WriteDR(FIFO_RESET,WrParamF.len);
    WriteIR(WrParamF.val, V_IR);
    WriteDR(8,WrParamF.len);

  end;
end;


procedure SetFIFOWrite;
begin
  if ActiveHW then
  begin
    WriteIR(WrParamF.val, V_IR);
    WriteDR(FIFO_WRITE,WrParamF.len);
  end;
end;

procedure SetFIFORead;
begin
  if ActiveHW then
  begin
    WriteIR(WrParamF.val, V_IR);
    WriteDR(FIFO_READ,WrParamF.len);
  end;
end;

procedure SetFIFOReadWrite;
begin
  if ActiveHW then
  begin
    WriteIR(WrParamF.val, V_IR);
    WriteDR(FIFO_WRITE or FIFO_READ,WrParamF.len);
  end;
end;

procedure FIFOClock;
begin
    if ActiveHW then
  begin
    WriteIR(WrFIFO.val, V_IR);
    WriteDR('1',WrFIFO.len);
  end;
end;


function ReadFIFOCounters: string;
begin
  Result := '';
  if ActiveHW then
  begin
    WriteIR(RdCntReg.val, V_IR);
    Result := ReadDR('0',RdCntReg.len);
  end;
end;

procedure SetFIFOValue(val: WORD);
begin
  if ActiveHW then
  begin
    WriteIR(WrDatF.val, V_IR);
    WriteDR(IntToHex(val,4),WrDatF.len);
  end;
end;

procedure SetFIFOChannel(ch: integer; startdly: integer);
begin
  if ActiveHW then
  begin
    WriteIR(WrAddF.val, V_IR);
    WriteDR(IntToHex(ch or (startdly shl 6),4),WrAddF.len);
  end;
end;

procedure SetTestBoardDelay(delay: integer);
begin
  if ActiveHW then
  begin
    WriteIR(WrParamF.val, V_IR);
    WriteDR(FIFO_RESET,WrParamF.len);
    
    WriteIR(WrDlyF.val, V_IR);
    WriteDR(IntToHex(FlipByte((255-delay) and $FF),2) ,WrDlyF.len);

    WriteIR(WrParamF.val, V_IR);
    WriteDR(8,WrParamF.len);

  end;
end;

procedure SetALCTBoardDelay(ch: integer; delay: integer);
var
  dlys: DelayGroup;
  i : Integer;
begin
  if ActiveHW then
  begin
    for i:=0 to MAX_DELAY_CHIPS_IN_GROUP-1 do
    begin
      dlys[i].Value := 0;
//      dlys[i].Value := delay;
      dlys[i].Pattern := 0;
    end;
    dlys[ch mod MAX_DELAY_CHIPS_IN_GROUP].Value := delay;
    Write6DelayLines(dlys, 1 shl (ch div MAX_DELAY_CHIPS_IN_GROUP));
//ZACH MODIFICATION
    //dlys[i].Value := delay;
    //dlys[i].Pattern := 0;

//    Write6DelayLines(dlys, $7f);
//ZACH MODIFICATION

  end;
end;


procedure SetTestBoardFIFO(const fifoval: integer; const fifochan: integer; const numwords: integer = 1; const startdelay: integer = 2; const  alctdelay: integer = 0; const testboarddelay: integer = 0);
var
  i : integer;
begin
  if ActiveHW then
  begin
    SetFIFOReset;
    SetFIFOChannel(fifochan, startdelay );
    SetFIFOValue(fifoval);
    SetTestBoardDelay(testboarddelay);
    SetALCTBoardDelay(fifochan, alctdelay);
    SetFIFOWrite;
    for i:= 1 to numwords do
      FIFOClock;
  end;
end;

procedure SetDelayTest(const fifoval: integer; const fifochan: integer; const startdelay: integer = 2; const  alctdelay: integer = 0; const testboarddelay: integer = 0);
var
  i : integer;
begin
  if ActiveHW then
  begin
//    SetFIFOReset;
    SetFIFOChannel(fifochan, startdelay );
    SetFIFOValue(fifoval);
    SetTestBoardDelay(testboarddelay);
    SetALCTBoardDelay(fifochan, alctdelay);
//    SetFIFOWrite;
//    for i:= 1 to numwords do
//      FIFOClock;
  end;
end;

function ReadFIFOValue: word;
begin
  if ActiveHW then
  begin
    WriteIR(RdDataReg.val, V_IR);
    Result := ReadDR(0,RdDataReg.len);
  end;
end;

function ALCTEnableInput: integer;
begin
 if ActiveHW then
  begin
    WriteIR(WrParamDly.val, V_IR);
    WriteDR('1fd',parlen);
    WriteIR(RdParamDly.val, V_IR);
    Result:= ReadDR(0, parlen);
  end;
end;

function ALCTDisableInput: integer;
begin
 if ActiveHW then
  begin
    WriteIR(WrParamDly.val, V_IR);
    WriteDR('1ff',parlen);
    WriteIR(RdParamDly.val, V_IR);
    Result:= ReadDR(0, parlen);
  end;
end;

function ReadFIFO(var vals: array of word; numwords: integer; var cntrs: array of byte): AnsiString;
var
  i: integer;
  cntstr : string;
begin
//  SetLength(vals, numwords);
//  SetLength(cntrs, 16);
  Result := '';
  if ActiveHW then
  begin
    SetFIFOReset;
    SetFIFOWrite;
    FIFOClock;
    SetFIFOReadWrite;
    ALCTEnableInput;

    for i:=0 to numwords-1 do
    begin
      FIFOClock;
      vals[i] := ReadFIFOValue;
    end;
//    Sleep(5);
    cntstr := ReadFIFOCounters();
    Result := cntstr;

    for i:=0 to 15 do
      cntrs[15-i] := StrToInt('$'+ Copy(cntstr,i*2+1,1) + Copy(cntstr,i*2+2,1));
      //cntrs[i] := StrToInt('$'+Copy(cntstr,i*2,2));
  end;
end;

procedure ReadFIFOfast(numwords: integer; var cntrs: array of byte);
var
  i: integer;
  cntstr : string;
begin
//  SetLength(cntrs, 16);

  if ActiveHW then
  begin
    SetFIFOReset;
    SetFIFOWrite;
    FIFOClock;
    SetFIFOReadWrite;
    ALCTEnableInput;
    WriteIR(WrFIFO.val,V_IR);   // FIFOClock;
    for i:=0 to numwords-1 do
      if ActiveHW then WriteDR('1',WrFIFO.len); // FIFOClock;
//    Sleep(5);
    cntstr := ReadFIFOCounters();

    for i:=0 to 15 do
      cntrs[15-i] := StrToInt('$'+ Copy(cntstr,i*2+1,1) + Copy(cntstr,i*2+2,1));
      //cntrs[i] := StrToInt('$'+Copy(cntstr,i*2,2));
  end;
end;


procedure PinPointRiseTime(var TimeR: array of integer; value: integer; ch: integer; StartDly_R: integer; alct_dly: integer; num: integer);
var
  tb_dly, i, j: integer;
  RegisterMaskDone: word;
//  cntrs_str : string;
  cntrs : array of byte;
  FirstChn: boolean;

begin
  SetLength(cntrs, 16);
  RegisterMaskDone := 0;
  tb_dly:= 0;

  SetDelayTest(value, ch, StartDly_R, alct_dly, tb_dly);

  FirstChn:= false;
  for j:= 0 to 25 do
  begin
    tb_dly:= 10*j;
    SetTestBoardDelay(tb_dly);
    ReadFIFOfast(num,cntrs);

    for i:=0 to 15 do
      if(cntrs[i] > 0) and ((value and (1 shl i))>0) then
        FirstChn:= true;

      if(FirstChn) then
      begin
        tb_dly:= 10*(j-1);
        break;
      end;
  end;

  for tb_dly:= tb_dly to 255 do
  begin
    SetTestBoardDelay(tb_dly);
    ReadFIFOfast(num,cntrs);

    for i:=0 to 15 do
    begin
      if(cntrs[i]>(num/2)) and ((value and (1 shl i))>0) and ((RegisterMaskDone and (1 shl i))=0) then
      begin
        TimeR[i] := tb_dly;
        RegisterMaskDone := RegisterMaskDone or (1 shl i);
      end;
    end;
    if(RegisterMaskDone = Value) then
      break;
  end;//end of test board delay for loop
end;//End of procedure

procedure PinPointRiseTime50(var TimeR: array of integer; var TimeR_50: array of Currency; value: integer; ch: integer; StartDly_R: integer; alct_dly: integer; num: integer);
var
  tb_dly, i, j: integer;
  RegisterMaskDone: word;
//  cntrs_str : string;
  cntrs : array of byte;
  tmp1, tmp2, cnt1, cnt2: array[0..15] of integer;
  FirstChn: boolean;

begin
  SetLength(cntrs, 16);
  RegisterMaskDone := 0;
  tb_dly:= 0;

  SetDelayTest(value, ch, StartDly_R, alct_dly, tb_dly);

  FirstChn:= false;
  for j:= 0 to 25 do
  begin
    tb_dly:= 10*j;
    SetTestBoardDelay(tb_dly);
    ReadFIFOfast(num,cntrs);

    for i:=0 to 15 do
      if(cntrs[i] > 0) and ((value and (1 shl i))>0) then
        FirstChn:= true;

      if(FirstChn) then
      begin
        tb_dly:= 10*(j-1);
        break;
      end;
  end;

  for tb_dly:= tb_dly to 255 do
  begin
    SetTestBoardDelay(tb_dly);
    ReadFIFOfast(num,cntrs);

    for i:=0 to 15 do
    begin
      if(cntrs[i]>(num/2)) and ((value and (1 shl i))>0) and ((RegisterMaskDone and (1 shl i))=0) then
      begin
        TimeR[i] := tb_dly;
        tmp2[i]:= tb_dly;  cnt2[i]:= cntrs[i];
        TimeR_50[i]:= tmp1[i] + ((tmp2[i]-tmp1[i])*(num/2 - cnt1[i]) / (cnt2[i]-cnt1[i]));
        RegisterMaskDone := RegisterMaskDone or (1 shl i);
      end;
    tmp1[i]:= tb_dly;  cnt1[i]:= cntrs[i];
    end;
    if(RegisterMaskDone = Value) then
      break;
  end;//end of test board delay for loop
end;//End of procedure


procedure PinPointFallTime(var TimeF: array of integer; value: integer; ch: integer; StartDly_F: integer; alct_dly: integer; num: integer);
var
  tb_dly, i, j: integer;
  RegisterMaskDone: word;
//  cntrs_str : string;
  cntrs : array of byte;
  FirstChn: boolean;

begin
  SetLength(cntrs, 16);
  RegisterMaskDone := 0;
  tb_dly:= 0;

  SetDelayTest(value, ch, StartDly_F, alct_dly, tb_dly);

    FirstChn:= false;
  for j:= 0 to 25 do
  begin
    tb_dly:= 10*j;
    SetTestBoardDelay(tb_dly);
    ReadFIFOfast(num,cntrs);

    for i:=0 to 15 do
      if(cntrs[i] > 0) and ((value and (1 shl i))>0) then
        FirstChn:= true;

      if(FirstChn) then
      begin
        tb_dly:= 10*(j-1);
        break;
      end;
  end;

  for tb_dly:= 0 to 255 do
  begin
  SetTestBoardDelay(tb_dly);
  ReadFIFOfast(num,cntrs);

    for i:=0 to 15 do
    begin
      if(cntrs[i]<(num/2)) and ((value and (1 shl i))>0) and ((RegisterMaskDone and (1 shl i))=0) then
      begin
        TimeF[i] := tb_dly;
        RegisterMaskDone := RegisterMaskDone or (1 shl i);
      end;
    end;
    if(RegisterMaskDone = Value) then
      break;
  end;//end of test board delay for loop
end;//End of procedure


function FindStartDly(var StartDly_R: integer; var StartDly_F: integer; value: integer; ch: integer; alct_dly: integer; num: integer): boolean;
var
  i, {num_dly_ch,} tb_dly, StartDly: integer;
  ChannelsCntr, MaxChannelsCntr: integer;
  cntrs : array of byte;
  RegMaskDoneR, MaskDoneR, RegMaskDoneF, MaskDoneF: word;
  FoundTimeBin: boolean;
  FirstCnhR, AllChnR, FirstChnF, AllChnF: boolean;
  cntrs_str : string;
begin
  RegMaskDoneR:= 0;
  RegMaskDoneF:= 0;
  MaxChannelsCntr:= 0;
  FoundTimeBin := false;
  FirstCnhR:= false;    AllChnR:= false;
  FirstChnF:= false;    AllChnF:= false;
  StartDly_R:= 5;       StartDly_F:= 5;
  StartDly:= 5;
  SetLength(cntrs, 16);
  tb_dly := 0;
//  num:= 20;

//  for i:=0 to 15 do
 //   if ((value and (1 shl i))>0) then
 //     Inc(num_dly_ch);
  SetDelayTest(value, ch, StartDly, alct_dly, tb_dly);

  for StartDly := 5 to 15 do
  begin
    //Access board
    SetFIFOChannel(ch, StartDly );
    ReadFIFOfast(num,cntrs);

    //Check counters and increment
    ChannelsCntr := 0;  MaskDoneR:= 0;

    for i:=0 to 15 do
      if (cntrs[i] > (num/2)) and ((value and (1 shl i))>0) then
        begin
          Inc(ChannelsCntr);
          MaskDoneR:= MaskDoneR or (1 shl i);
        end;

    //Check if time bin is found
    if (ChannelsCntr > 0)  then FirstCnhR:= true;
    if (MaskDoneR = value) then AllChnR:= true;

    if (not FirstCnhR) then StartDly_R:= StartDly
//    if (not AllChnR) then StartDly_R:= StartDly
    else
      begin
        if ((ChannelsCntr > 0) and (ChannelsCntr >= MaxChannelsCntr)) then
          begin
            MaxChannelsCntr:= ChannelsCntr;
            StartDly_F:= StartDly;
            RegMaskDoneR:= MaskDoneR;
          end
        else FirstChnF:= true;
      end;

    if ( FirstChnF) then
      begin
        MaskDoneF:= 0;
        for i:=0 to 15 do
          if (cntrs[i] < (num/2)) and ((value and (1 shl i))>0) then
//            Inc(ChannelsCntr);
            MaskDoneF:= MaskDoneF or (1 shl i);
        RegMaskDoneF:= MaskDoneF;
        if (MaskDoneF = value) then AllChnF:= true;
      end;

    if (AllChnR and AllChnF ) then
      begin
        FoundTimeBin := true;
        break
      end;
  end;//End of time bin for loop
  if (not FirstCnhR) then StartDly_R:= 5;
  Result:= FoundTimeBin;
end;

function FindStartDlyPin(var StartDly_R: integer; var StartDly_F: integer; value: integer; ch: integer; alct_dly: integer; num: integer;var RegMaskDone: array of word): boolean;
var
  i, {num_dly_ch,} tb_dly, StartDly: integer;
  ChannelsCntr, MaxChannelsCntr: integer;
  cntrs : array of byte;
  MaskDoneR, RegMaskDoneR, MaskDoneF, RegMaskDoneF: word;
  FoundTimeBin: boolean;
  FirstCnhR, AllChnR, FirstChnF, AllChnF: boolean;
  cntrs_str : string;
begin
  RegMaskDoneR:= 0;
  RegMaskDoneF:= 0;
  MaxChannelsCntr:= 0;
  FoundTimeBin := false;
  FirstCnhR:= false;    AllChnR:= false;
  FirstChnF:= false;    AllChnF:= false;
  StartDly_R:= 5;       StartDly_F:= 5;
  StartDly:= 5;
  SetLength(cntrs, 16);
  tb_dly := 0;
//  num:= 20;

//  for i:=0 to 15 do
 //   if ((value and (1 shl i))>0) then
 //     Inc(num_dly_ch);
  SetDelayTest(value, ch, StartDly, alct_dly, tb_dly);

  for StartDly := 5 to 15 do
  begin
    //Access board
    SetFIFOChannel(ch, StartDly );
    ReadFIFOfast(num,cntrs);

    //Check counters and increment
    ChannelsCntr := 0;  MaskDoneR:= 0;

    for i:=0 to 15 do
      if (cntrs[i] > (num/2)) and ((value and (1 shl i))>0) then
        begin
          Inc(ChannelsCntr);
          MaskDoneR:= MaskDoneR or (1 shl i);
        end;

    //Check if time bin is found
    if (ChannelsCntr > 0)  then FirstCnhR:= true;
    if (MaskDoneR = value) then AllChnR:= true;

    if (not FirstCnhR) then StartDly_R:= StartDly
//    if (not AllChnR) then StartDly_R:= StartDly
    else
      begin
        if ((ChannelsCntr > 0) and (ChannelsCntr >= MaxChannelsCntr)) then
          begin
            MaxChannelsCntr:= ChannelsCntr;
            StartDly_F:= StartDly;
            RegMaskDoneR:= MaskDoneR;
          end
        else FirstChnF:= true;
      end;

    if ( FirstChnF) then
      begin
        MaskDoneF:= 0;
        for i:=0 to 15 do
          if (cntrs[i] < (num/2)) and ((value and (1 shl i))>0) then
//            Inc(ChannelsCntr);
            MaskDoneF:= MaskDoneF or (1 shl i);
        RegMaskDoneF:= MaskDoneF;
        if (MaskDoneF = value) then AllChnF:= true;
      end;

    if (AllChnR and AllChnF ) then
      begin
        FoundTimeBin := true;
        break
      end;
  end;//End of time bin for loop
  if (not FirstCnhR) then StartDly_R:= 5;
  RegMaskDone[ch]:=RegMaskDoneR and RegMaskDoneF;
  Result:= FoundTimeBin;
end;


procedure MeasureDelay(ch: integer; var PulseWidth: array of Currency; var BeginTime_Min: MeasDly; var DeltaBeginTime: MeasDlyChan;
                                    var Delay_Time: MeasDlyChan; var AverageDelay_Time: MeasDly; var ErrMeasDly: integer; var RegMaskDone: array of word);
var
  num, value, start_dly, tb_dly, alct_dly : integer;
  i, j : integer;
//  num_dly_ch : integer;

  StartDly_R, StartDly_F: integer;
  TimeR_0, TimeF_0, TimeR_15: array[0..15] of integer;
  DelayTimeR_0, DelayTimeF_0, DelayTimeR_15:array[0..15] of Currency;
   //arrays for storing pulse widths on each channel
  MinWidth, MaxWidth, PulseWidth_Min, PulseWidth_Max: Currency;
   //Array for storing pulse shifts
//  ErrorDeltaDelay: array[0..15] of Currency;
//  MaxDeltaBeginTime: Currency;
  SumDelay_Time: Currency;
  //Register mask

begin
  MinWidth:= 30; MaxWidth:= 45;
//  MaxDeltaBeginTime:= 1;
//  MinDelay:= 33; MaxDelay:= 35;


//    DelaysChart.CleanupInstance;
    //Initialize variables and arrays
//    ChannelsCntr := 0;
//  num_dly_ch := 0;
  value := $FFFF;
//    value := StrToInt('$'+edFIFOValue.Text);
//    ch := seFIFOCh.Value;
//  num := seFIFONumWords.Value;
  num:= 100;
  ErrMeasDly:= 0;

  for i:=0 to 15 do
  begin
    TimeR_0[i]:= 0;
    TimeF_0[i]:= 0;
    TimeR_15[i]:= 0;
    DelayTimeR_0[i]:= 0;
    DelayTimeF_0[i]:= 0;
    DelayTimeR_15[i]:= 0;
    PulseWidth[i]:= 0;
    DeltaBeginTime[ch][i]:= 0;
    Delay_Time[ch][i]:= 0;
//    ErrorDeltaDelay[i]:= 0;
  end;

    //Calculating number of requested channels
//    for i:=0 to 15 do
//      if ((value and (1 shl i))>0) then
//        Inc(num_dly_ch);

    alct_dly:= 0;
    if FindStartDlyPin(StartDly_R, StartDly_F, value, ch, alct_dly, num, RegMaskDone) then
    begin
      PinPointRiseTime(TimeR_0, value, ch, StartDly_R, alct_dly, num);
      PinPointFallTime(TimeF_0, value, ch, StartDly_F, alct_dly, num);

      BeginTime_Min[ch]:= StartDly_R*25 + 255*0.25;     
      for i:=0 to 15 do
      begin
        DelayTimeR_0[i] := StartDly_R*25 + TimeR_0[i]*0.25;
        DelayTimeF_0[i] := StartDly_F*25 + TimeF_0[i]*0.25;
        PulseWidth[i] := DelayTimeF_0[i] - DelayTimeR_0[i];
        if (i=0) then
        begin
          PulseWidth_Min:= PulseWidth[i];
          PulseWidth_Max:= PulseWidth[i];
        end;
        if (DelayTimeR_0[i] < BeginTime_Min[ch]) then
          BeginTime_Min[ch]:= DelayTimeR_0[i];
        if (PulseWidth[i] < PulseWidth_Min) then
        begin
          PulseWidth_Min:= PulseWidth[i];
        end;
        if (PulseWidth[i] > PulseWidth_Max) then
        begin
          PulseWidth_Max:= PulseWidth[i];
        end;
      end;
    end
    else
    begin
      ErrMeasDly:= ErrMeasDly or $1;
//      break;
    end;
//------------------------------------------------------------------------------
    alct_dly:= 15;
    AverageDelay_Time[ch]:= 0; SumDelay_Time:= 0;
    if FindStartDly(StartDly_R, StartDly_F, value, ch, alct_dly, num) then
    begin
      PinPointRiseTime(TimeR_15, value, ch, StartDly_R, alct_dly, num);
      for i:=0 to 15 do
      begin
        DelayTimeR_15[i] := StartDly_R*25 + TimeR_15[i]*0.25;
        Delay_Time[ch][i] := DelayTimeR_15[i] - DelayTimeR_0[i];
        SumDelay_Time:= SumDelay_Time + Delay_Time[ch][i];
      end;
      AverageDelay_Time[ch]:= SumDelay_Time / 16;
    end
    else
    begin
      ErrMeasDly:= ErrMeasDly or $2;
//      break;
    end;
//------------------------------------------------------------------------------

    if (PulseWidth_Min < MinWidth) then ErrMeasDly:= ErrMeasDly or $4;

    if (PulseWidth_Max > MaxWidth) then ErrMeasDly:= ErrMeasDly or $8;

    for i:=0 to 15 do
    begin
      DeltaBeginTime[ch][i]:= DelayTimeR_0[i] - BeginTime_Min[ch];
//      if (DeltaBeginTime[ch][i] > MaxDeltaBeginTime) then
//        ErrMeasDly:= ErrMeasDly or $10;

//      if ((Delay_Time[ch][i] < MinDelay) or (Delay_Time[ch][i] > MaxDelay))then
//      begin
//        ErrorDeltaDelay[i]:= Delay_Time[ch][i];
//        ErrMeasDly:= ErrMeasDly or $20;
    end;
end;//end of function

procedure WriteToFile(BeginTime_Min: MeasDly;
                   DeltaBeginTime: MeasDlyChan;
                   DelayTime: MeasDlyChan;
                   Average: MeasDly;
                   BoardNum: string;
                   PathString: string);
var
  TxtFile : TextFile;
  DataBuffer1, DataBuffer2: string;
  chip, channel, i: integer;
begin
  //Create a file with the given path and the ALCT Board Number
  AssignFile(TxtFile, PathString);
  try
    Rewrite(TxtFile);

    writeln(TxtFile, '**********************************');
    writeln(TxtFile, 'Delay Test Results' + #9 + 'Board# ' + BoardNum);
    writeln(TxtFile, '**********************************');
    writeln(TxtFile, '');
    DataBuffer1 := '';
    for i:=0 to 15 do
      DataBuffer1 := DataBuffer1 + inttostr(i) + #9;
    writeln(TxtFile, #9 +#9 +#9 + 'Channel #');
    writeln(TxtFile, #9 +#9 +#9 + DataBuffer1);
    
    for chip:= 0 to NUM_AFEB-1 do
    begin
      writeln(TxtFile, 'Chip #' + inttostr(chip));
      writeln(TxtFile, 'Begin time: ' + CurrToStr(BeginTime_Min[chip]));
      DataBuffer1 := '';
      DataBuffer2 := '';
      for channel := 0 to 15 do
      begin
        DataBuffer1 := DataBuffer1 + CurrToStr(DeltaBeginTime[chip][channel]) + #9;
        DataBuffer2 := DataBuffer2 + CurrToStr(DelayTime[chip][channel]) + #9;
      end;

      writeln(TxtFile, 'Delta Begin Times:' + #9 + DataBuffer1);
      writeln(TxtFile, 'Delay Times:' + #9 + #9 + DataBuffer2);
      writeln(TxtFile, 'Average: ' + CurrToStr(Average[chip]));
      writeln(TxtFile, '');
    end;
  finally
    CloseFile(TxtFile);
  end;
end;




{
function FindStartDlyR(var StartDlyR_f: integer; StartDly: integer; value: integer; ch: integer; alct_dly: integer; num: integer): boolean;
var
  size, i, num_dly_ch, ChannelsCntr, tb_dly, StartDlyR: integer;
  cntrs : array of byte;
  vals : array of word;
begin
  SetLength(vals, num);
  SetLength(cntrs, 16);
  tb_dly := 0;
  for i:=0 to 15 do
    if ((value and (1 shl i))>0) then
      Inc(num_dly_ch);

  for StartDlyR := StartDly-1 downto 2 do
  begin
    //Access board
    SetTestBoardFIFO(value, ch, 1, StartDlyR, alct_dly, tb_dly);
    SetFIFOReadWrite;
    ReadFIFO(vals,num,cntrs);

    //Check counters and increment
    ChannelsCntr := 0;
    for i:=0 to 15 do
      if (cntrs[i] = 0) and ((value and (1 shl i))>0) then
        Inc(ChannelsCntr);

    //Check if time bin is found
    if(ChannelsCntr = num_dly_ch) then
    begin
      StartDlyR_f := StartDlyR;
      break;
    end;
  end;//End of time bin for loop

  if(ChannelsCntr = num_dly_ch) then
    Result := true
  else
    Result := false;
end;


function FindStartDlyF_tmp(var StartDlyF_f: integer; StartDly: integer; value: integer; ch: integer; alct_dly: integer; num: integer): boolean;
var
  size, i, num_dly_ch, ChannelsCntr, tb_dly, StartDlyF: integer;
  cntrs : array of byte;
  vals : array of word;
begin
  SetLength(vals, num);
  SetLength(cntrs, 16);
  tb_dly := 0;
  for i:=0 to 15 do
    if ((value and (1 shl i))>0) then
      Inc(num_dly_ch);

  for StartDlyF := StartDly+1 to 15 do
  begin
    //Access board
    SetTestBoardFIFO(value, ch, 1, StartDlyF, alct_dly, tb_dly);
    SetFIFOReadWrite;
    ReadFIFO(vals,num,cntrs);

    //Check counters and increment
    ChannelsCntr := 0;
    for i:=0 to 15 do
      if (cntrs[i] = 0) and ((value and (1 shl i))>0) then
        Inc(ChannelsCntr);

     //Check if time bin is found
    if(ChannelsCntr = num_dly_ch) then
    begin
      StartDlyF_f := StartDlyF;
      break;
    end;
  end;//End of time bin for loop

  if(ChannelsCntr = num_dly_ch) then
    Result := true
  else
    Result := false;
end;


function FindStartDlyF(var StartDlyF_f: integer; value: integer; ch: integer; alct_dly: integer; num: integer): boolean;
var
  size, i, num_dly_ch, ChannelsCntr, tb_dly, StartDlyF: integer;
  cntrs : array of byte;
  vals : array of word;
begin
  SetLength(vals, num);
  SetLength(cntrs, 16);
  ChannelsCntr := 0;
  tb_dly := 0;
  for i:=0 to 15 do
    if ((value and (1 shl i))>0) then
      Inc(num_dly_ch);

  for StartDlyF := StartDlyF_f-1 downto 2 do
  begin
    //Access board
    SetTestBoardFIFO(value, ch, 1, StartDlyF, alct_dly, tb_dly);
    SetFIFOReadWrite;
    ReadFIFO(vals,num,cntrs);

    //Check counters and increment
    ChannelsCntr := 0;
    for i:=0 to 15 do
      if (cntrs[i] <> 0) and ((value and (1 shl i))>0) then
        Inc(ChannelsCntr);

    //Check if time bin is found
    if(ChannelsCntr = num_dly_ch) then
    begin
      StartDlyF_f := StartDlyF;
      break;
    end;
  end;//End of time bin for loop

  if(ChannelsCntr = num_dly_ch) then
    Result := true
  else
    Result := false;
end;
}
end.
