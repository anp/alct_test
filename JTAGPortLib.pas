unit JTAGPortLib;


interface
{$IFDEF LINUX}
uses SysUtils,
    libjtagio,

Classes;

function openJTAG(PortNum: byte): integer;
procedure closeJTAG;
procedure setchainJTAG(Chain: byte);
procedure resetJTAG;
procedure enableJTAG;
procedure idleJTAG;
procedure rtiJTAG;
procedure oneclockJTAG;
procedure lightsJTAG;
procedure jtagioJTAG(const TMSvalue: byte; const TDIvalue: byte; out TDOvalue: byte);

const jtagdrivername = '/dev/jtag';
{$ENDIF}
implementation
{$IFDEF LINUX}

var
    JTAGPort  : integer;

function openJTAG( PortNum: byte): integer;
begin
    JTAGPort := openJTAGPort(PChar(jtagdrivername + IntToStr(PortNum)));
    if JTAGPort > 0 then
    begin
         idleJTAG;
    end;
    Result := JTAGPort;
end;

procedure closeJTAG;
begin
    closeJTAGPort(JTAGPort);
end;

procedure setchainJTAG(Chain: byte);
begin
    setchainJTAGPort(JTAGPort, Chain);
end;

procedure resetJTAG;
begin
    resetJTAGPort(JTAGPort);
end;

procedure enableJTAG;
begin
    enableJTAGPort(JTAGPort);
end;

procedure idleJTAG;
begin
    idleJTAGPort(JTAGPort);
end;

procedure rtiJTAG;
begin
    rtiJTAGPort(JTAGPort);
end;

procedure oneclockJTAG;
begin
    oneclockJTAGPort(JTAGPort);
end;

procedure lightsJTAG;
begin
    lightsJTAGPort(JTAGPort);
end;

procedure jtagioJTAG( const TMSvalue: byte; const TDIvalue: byte; out TDOvalue: byte);
begin
    jtagioJTAGPort( JTAGPort, TMSvalue, TDIvalue, TDOvalue);
end;
{$ENDIF}
end.


