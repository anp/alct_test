unit FirmwareLoader;

interface
uses SysUtils, Classes;

function SVFLoad(Filename: TFileName): integer;

const SVFInstrs: array[0..10] of string = ('HIR', 'HDR', 'TIR', 'TDR', 'SIR', 'SDR', 'TRST', 'ENDIR', 'ENDDR', 'STATE', 'RUNTEST');

implementation

type
	TSVFOp = record
	length:	Cardinal;
	TDI:	string;
	TDO:	string;
	MASK:	string;
	SMASK:	string;
	end;

type TSVFOpType = (ShIR, ShDR);

type TOps		= (HIRop, HDRop, TIRop, TDRop, SIRop, SDRop, TRSTop, ENDIRop, ENDDRop, STATEop, RUNTESTop);

function ParseSVFLine(const cmd: string;  out SVFOp: TSVFOp): integer;
var
	i,j : Integer;
	params: array of string;
	tmpstr: string;
	flag: Boolean;
begin
	i := 1;
	j := 0;
	flag := true;
	while (not IsDelimiter(';', cmd, i)) or (i>Length(cmd)) do
	begin
	  if IsDelimiter(' ()',cmd,i) then
		flag := true
	  else
		begin
			if flag then
			begin
				Inc(j);
				SetLength(params, j);
				flag := false;
			end;
			params[j-1] := params[j-1] + cmd[i];
		end;
	  Inc(i);
	end;
	Result := j;
end;

function SVFLoad(Filename: TFileName): integer;
var
	SVFFile : TFileStream;
	Errs	: integer;
//	Data	: TStringList;
	HIR, HDR, TIR, TDR, SIR, SDR: TSVFOp;
	i,j, cnt	: Integer;
	tmpstr, cmd		: string;
	Data	: TStringStream;
begin
	Errs := 0;
{	Data := TStringList.Create;
	Data.LoadFromFile(Filename);

	cnt := 0;
	tmpstr := '';
	cmd := '';
	for i:= 0 to Data.Count-1 do
		if Pos( '//', Trim(Data.Strings[i])) = 1  then
			begin
				tmpstr := Trim(Data.Strings[i]);
				Delete(tmpstr, Pos( '//', tmpstr), 2);
			end
		else
		begin
//			cmd := cmd + Trim(Data.Strings[i]);
			if Pos(';', Trim(Data.Strings[i])) <> 0 then
			begin
				for j:= Low(SVFInstrs) to High(SVFInstrs) do
					if Pos(SVFInstrs[j], cmd) = 1 then
					begin
						case TOps(j) of
							HIRop: ParseSVFLine(cmd, HIR);
							HDRop: ParseSVFLine(cmd, HDR);
							TIRop: ParseSVFLine(cmd, TIR);
							TDRop: ParseSVFLine(cmd, TDR);
							SIRop: ParseSVFLine(cmd, SIR);
//							SDRop: ParseSVFLine(cmd, SDR);
						else
						end;
						Inc(cnt);
						break;
					end;
				cmd := '';
			end
		end;
}
	SVFFile := TFileStream.Create(string (Filename), fmOpenRead);
	Data := TStringStream.Create('');
	Data.CopyFrom(SVFFile, SVFFile.Size);
	Errs := SVFFile.Size;
	Data.Free;
	SVFFile.Free;
	Result := Errs;
end;

end.
