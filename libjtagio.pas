{-------------------------------------------------------------------------}
{									  }
{   Copyright (C) 1992-2000  Michel Catudal <bbcat@netonecom.net>         }
{   libjtagio.pas- Kylix Runtime Library forJ TAG via LPT port support    }
{									  }
{   This program is free software; you can redistribute it and/or modify  }
{   it under the terms of the GNU General Public License as published by  }
{   the Free Software Foundation; either version 2 of the License, or     }
{   (at your option) any later version.					  }
{									  }
{   This program is distributed in the hope that it will be useful,       }
{   but WITHOUT ANY WARRANTY; without even the implied warranty of	  }
{   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	  }
{   GNU General Public License for more details.			  }
{									  }
{   You should have received a copy of the GNU General Public License     }
{   along with this program; if not, write to the Free Software		  }
{   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.		  }
{									  }
{      Last edited 10-24-01 by Victor Barashko				  }
{									  }
{-------------------------------------------------------------------------}

unit libjtagio;

interface

function openJTAGPort(portN : PChar) : Integer; cdecl;
procedure closeJTAGPort(prtPort : Integer); cdecl;
function rtiJTAGPort(prtPort : Integer) : Integer; cdecl;
function enableJTAGPort(prtPort : Integer) : Integer; cdecl;
function idleJTAGPort(prtPort : Integer) : Integer; cdecl;
function resetJTAGPort(prtPort : Integer) : Integer; cdecl;
function setchainJTAGPort(prtPort : Integer; Chain : Integer) : Integer; cdecl;
function lightsJTAGPort(prtPort : Integer) : Integer; cdecl;
function oneclockJTAGPort(prtPort : Integer) : Integer; cdecl;
function jtagioJTAGPort(prtPort : Integer; TMSBit: byte; TDIbit: byte; out TDObit: byte) : Integer; cdecl;

implementation

{ A Delay in uSecs.  }

function openJTAGPort(portN : PChar) : Integer; cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM openJTAGPort}

procedure closeJTAGPort(prtPort : Integer); cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM closeJTAGPort}

function rtiJTAGPort(prtPort : Integer) : Integer; cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM rtiJTAGPort}

function enableJTAGPort(prtPort : Integer) : Integer; cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM enableJTAGPort}

function idleJTAGPort(prtPort : Integer) : Integer; cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM idleJTAGPort}

function resetJTAGPort(prtPort : Integer) : Integer; cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM resetJTAGPort}

function setchainJTAGPort(prtPort : Integer; Chain : Integer) : Integer; cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM setchainJTAGPort}

function lightsJTAGPort(prtPort : Integer) : Integer; cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM lightsJTAGPort}

function oneclockJTAGPort(prtPort : Integer) : Integer; cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM oneclockJTAGPort}

function jtagioJTAGPort(prtPort : Integer; TMSBit: byte; TDIbit: byte; out TDObit: byte) : Integer; cdecl; external 'libjtagio.so.1';
{$EXTERNALSYM jtagioJTAGPort}

end.

