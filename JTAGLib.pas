unit JTAGLib;

interface

uses SysUtils, Classes,
{$IFDEF LINUX}
JTAGPortLib,
{$ENDIF}
LPTJTAGLib;


const IR_REG = 0;
const DR_REG = 1;
const modulename = '"ppdev"';
const jtagdevice = '/dev/jtag';

var
   jtag_debug: Boolean;
   driverjtag: Boolean;
   ActiveHW : boolean;
   nPort    : integer;

{$IFDEF MSWINDOWS}
procedure open_jtag(Owner: TComponent);
{$ENDIF}
{$IFDEF LINUX}
procedure open_jtag(port: byte; drvJTAG: boolean);
{$ENDIF}
procedure close_jtag;
procedure set_chain(Chan: byte);
procedure reset_jtag;
procedure enable_jtag;
procedure idle_jtag;
procedure rti_jtag;
procedure oneclock_jtag;
procedure lights_jtag;

procedure TMS_High;
procedure TMS_Low;
procedure jtag_io( tms_: byte; tdi_: byte; var tdo_: byte);

function  WriteIR(IR: string; IRSize: Integer): string; overload;
function  WriteDR(DR: string; DRSize: Integer): string; overload;
function  ReadDR(DR: string; DRSize: Integer): string; overload;

function  WriteIR(IR: longword; IRSize: Integer): longword; overload;
function  WriteDR(DR: longword; DRSize: Integer): longword; overload;
function  ReadDR(DR: longword; DRSize: Integer): longword; overload;


procedure StartIRShift();
procedure StartDRShift();
procedure ExitIRShift();
procedure ExitDRShift();
function  ShiftData(Data: Longword; DataSize: byte; sendtms: boolean): Longword;
function  IOExchange(Send: array of byte; out Recv: array of byte; const Size : longint; const RegType: byte): longint;

implementation

{$IFDEF MSWINDOWS}
procedure open_jtag(Owner: TComponent);
begin
	 if not ActiveHW then
	 begin
		 if not driverJTAG then
		 begin
				nPort := openLPTJTAG(Owner);
			if nPort = -1 then  ActiveHW := false
			else ActiveHW := true;
		end;
	 end;
end;
{$ENDIF}

{$IFDEF LINUX}
procedure open_jtag(port: byte; drvJTAG: boolean);
begin
	 if not ActiveHW then
	 begin
		 if drvJTAG then
            nPort := openJTAG(port)
         else
			nPort := openLPTJTAG(port);
         driverjtag := drvJTAG;
		 if nPort = -1 then  ActiveHW := false
		 else ActiveHW := true;
	 end;
end;
{$ENDIF}

procedure close_jtag;
begin
     if ActiveHW then
     begin
        {$IFDEF LINUX}
        if driverJTAG then
            closeJTAG
        else
        {$ENDIF}
            closeLPTJTAG;
        nPort := -1;
        ActiveHW := false;
     end;
end;

procedure set_chain(Chan: byte);
begin
     if ActiveHW then
     begin
        {$IFDEF LINUX}
        if driverJTAG then
            setchainJTAG(Chan)
        else
        {$ENDIF}
            setchainLPTJTAG(Chan);
     end;
end;

procedure reset_jtag;
begin
     if ActiveHW then
     begin
        {$IFDEF LINUX}
        if driverJTAG then
            resetJTAG
        else
        {$ENDIF}
            resetLPTJTAG;
     end;
end;

procedure enable_jtag;
begin
     if ActiveHW then
     begin
        {$IFDEF LINUX}
        if driverJTAG then
            enableJTAG
        else
        {$ENDIF}
            enableLPTJTAG;
     end;
end;

procedure idle_jtag;
begin
     if ActiveHW then
     begin
        {$IFDEF LINUX}
        if driverJTAG then
            idleJTAG
        else
        {$ENDIF}
            idleLPTJTAG;
     end;
end;

procedure rti_jtag;
begin
	 if ActiveHW then
     begin
        {$IFDEF LINUX}
        if driverJTAG then
            rtiJTAG
        else
        {$ENDIF}
            rtiLPTJTAG;
     end;
end;

procedure oneclock_jtag;
begin
     if ActiveHW then
     begin
        {$IFDEF LINUX}
        if driverJTAG then
            oneclockJTAG
        else
        {$ENDIF}        
            oneclockLPTJTAG;
     end;
end;

procedure TMS_High;
begin
     if ActiveHW then
     begin
		TMSHighLPTJTAG;
     end;
end;

procedure TMS_Low;
begin
     if ActiveHW then
     begin
        TMSLowLPTJTAG;
     end;
end;

procedure lights_jtag;
begin
     if ActiveHW then
     begin
        {$IFDEF LINUX}
        if driverJTAG then
            lightsJTAG
        else
        {$ENDIF}
            lightsLPTJTAG;
     end;
end;

procedure jtag_io( tms_: byte; tdi_: byte; var tdo_: byte);
begin
     if ActiveHW then
	 begin
        {$IFDEF LINUX}
        if driverJTAG then
            jtagioJTAG(tms_, tdi_, tdo_)
        else
        {$ENDIF}
            jtagioLPTJTAG(tms_, tdi_, tdo_);
     end;
end;


function WriteIR(IR: string; IRSize: integer): string; overload;
var
   i : byte;
   arIR : array of byte;
   tmpstr : string;
begin

	SetLength(arIR, ((IRSize-1) div 8)+1);
	tmpstr := '';
	tmpstr := Trim(IR);

	if Length(tmpstr) < 2*Length(arIR) then
	begin
		for I := 1 to (2*Length(arIR) - Length(tmpstr)) do
			tmpstr := '0' + tmpstr;
	end;

	for I := Low(arIR) to High(arIR) do
		arIR[I] := StrToInt('$'+Copy(tmpstr,Length(tmpstr)-I*2-1,2));

	IOExchange(arIR, arIR, IRSize, IR_REG);

	tmpstr := '';
	for I := Low(arIR) to High(arIR) do tmpstr := IntToHex(arIR[I], 2 ) + tmpstr;
	Result := Copy(tmpstr, Length(tmpstr)-((IRSize-1) div 4), ((IRSize-1) div 4) + 1);
end;

function WriteIR(IR: longword; IRSize: integer): longword; overload;
begin
	Result := StrToInt('$'+WriteIR(IntToHex(IR, sizeof(IR)), IRSize));
end;


function WriteDR(DR: string; DRSize: integer): string; overload;
var
   i : byte;
   arDR : array of byte;
   tmpstr : string;
begin

	SetLength(arDR, ((DRSize-1) div 8)+1);
	tmpstr := '';
	tmpstr := Trim(DR);

	if Length(tmpstr) < 2*Length(arDR) then
	begin
		for I := 1 to (2*Length(arDR) - Length(tmpstr)) do
			tmpstr := '0' + tmpstr;
	end;

	for I := Low(arDR) to High(arDR) do
		arDR[I] := StrToInt('$'+Copy(tmpstr,Length(tmpstr) - I*2-1,2));

	IOExchange(arDR, arDR, DRSize, DR_REG);

	tmpstr := '';
	for I := Low(arDR) to High(arDR) do tmpstr := IntToHex(arDR[I], 2 ) + tmpstr;
	Result := Copy(tmpstr, Length(tmpstr)-((DRSize-1) div 4), ((DRSize-1) div 4) + 1);
//    Result := tmpstr;

end;

function WriteDR(DR: longword; DRSize: integer): longword; overload;
begin
	Result := StrToInt('$'+WriteDR(IntToHex(DR, sizeof(DR)), DRSize));
end;


function ReadDR(DR: string; DRSize: integer): string; overload;
var
   i : byte;
   arDR : array of byte;
   tmpstr : string;
begin

	SetLength(arDR, ((DRSize-1) div 8)+1);
	tmpstr := '';
	tmpstr := Trim(DR);

	if Length(tmpstr) < 2*Length(arDR) then
	begin
		for I := 1 to (2*Length(arDR) - Length(tmpstr)) do
			tmpstr := '0' + tmpstr;
	end;

	for I := Low(arDR) to High(arDR) do
		arDR[I] := StrToInt('$'+Copy(tmpstr,Length(tmpstr) - I*2 -1 ,2));

	IOExchange(arDR, arDR, DRSize, DR_REG);

	tmpstr := '';
	for I := Low(arDR) to High(arDR) do tmpstr := IntToHex(arDR[I], 2 ) + tmpstr;
	Result := Copy(tmpstr, Length(tmpstr)-((DRSize-1) div 4), ((DRSize-1) div 4) + 1);
//    Result := tmpstr;
end;

function ReadDR(DR: longword; DRSize: integer): longword; overload;
begin
	Result := StrToInt('$'+ReadDR(IntToHex(DR, sizeof(DR)), DRSize));
end;

function  ShiftData(Data: Longword; DataSize: byte; sendtms: boolean): Longword;
var
   i:   byte;
   tdo: byte;
   tmp: Longword;
begin
	if ActiveHW and (DataSize > 0) then
	begin
		if (DataSize > 32) then Result:= 0
		else
		begin
				tmp := 0;

				for i:=1 to DataSize do
				begin
//                        for j:= 0 to 10000 do begin end;
						jtag_io( byte ((i = DataSize) and sendtms), byte (Data and $01), tdo);
						tmp := tmp or ( (Longword (tdo and $01)) shl (i-1) );
						Data := Data shr 1;
				end;

				Result := tmp;
		end;
	end
	else Result := 0;
end;

function IOExchange(Send: array of byte; out Recv: array of byte; const Size : longint; const RegType: byte): longint;
const
	ChunkSz = 8;
var
	I:  longint;
begin
	if ActiveHW and (size > 0) then
	begin

		if RegType = IR_REG then StartIRShift
		else StartDRShift;

		for I:= Low(Send) to High(Send) do
		begin
			if Size > ChunkSz*I then
			begin
				if (Size - ChunkSz*I) > ChunkSz then
					Recv[I] := byte(ShiftData(Send[I], ChunkSz, false))
				else
					Recv[I] := byte(ShiftData(Send[I], Size - ChunkSz*I, true));
			end;
		end;

		if RegType = IR_REG then ExitIRShift
		else ExitDRShift;

		Result := size;
    end
    else Result := 0;
end;

procedure StartIRShift();
var
    tdo:    byte;
begin
    jtag_io(0, 0, tdo);
    jtag_io(1, 0, tdo);
    jtag_io(1, 0, tdo);
    jtag_io(0, 0, tdo);
    jtag_io(0, 0, tdo);
end;

procedure StartDRShift();
var
    tdo:    byte;
begin
    jtag_io(0, 0, tdo);
    jtag_io(1, 0, tdo);
    jtag_io(0, 0, tdo);
    jtag_io(0, 0, tdo);
end;

procedure ExitIRShift();
var
    tdo:    byte;
begin
    jtag_io(1, 0, tdo);
    jtag_io(0, 0, tdo);
    jtag_io(0, 0, tdo);
    jtag_io(0, 0, tdo);
end;

procedure ExitDRShift();
var
    tdo:    byte;
begin
    jtag_io(1, 0, tdo);
    jtag_io(0, 0, tdo);
    jtag_io(0, 0, tdo);
    jtag_io(0, 0, tdo);
end;

end.


