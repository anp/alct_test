unit OperProgress;

interface

uses
  Types, Classes, QGraphics, QControls, QForms, QDialogs, SysUtils,
  QStdCtrls, QButtons, QComCtrls, QTypes, QExtCtrls;


type
  TfrOperProgress = class(TForm)
	pbOper: TProgressBar;
	btClose: TBitBtn;
	btCancel: TBitBtn;
	lbOper: TLabel;
	sbProg: TStatusBar;
	procedure FormShow(Sender: TObject);
	procedure btCancelClick(Sender: TObject);
	procedure btCloseClick(Sender: TObject);
  private
	{ Private declarations }
	fCloseOnFinish: boolean;
	fEnableCancel: boolean;
	OperType : Integer;
	fn : TFileName;
	R : integer;
	procedure SetCloseOnExit(const flag: boolean);
	procedure SetEnableCancel(const flag: boolean);
	procedure SetOperationType(const optype: integer);
	procedure SetFileName(const filename: TFileName);
  public
	{ Public declarations }
	function GetFileName: TFileName;
	function IsCloseOnFinish: boolean;
	function DoOper(const opertype: integer; filename: TFileName; fclose: boolean; fcancel :boolean; out Res: integer): integer;
	procedure SetRes(const res: integer);
	function GetRes: integer;
  end;

type
	TOperThread = class(TThread)
	private
		FOperForm:		TfrOperProgress;
		FProgress:		Integer;
		FFileName:		TFileName;
		procedure DoVisual;
	protected
		procedure Execute; override;
		procedure Visual(position: integer);
		procedure Oper; virtual; abstract;

	public
		constructor Create(Form: TfrOperProgress; filename: TFileName);
end;

type TOperSCBlankCheckThread = class(TOperThread)
	protected
		procedure Oper; override;
end;

type TOperV600BlankCheckThread = class(TOperThread)
	protected
		procedure Oper; override;
end;

type TOperVBlankCheckEPROM1Thread = class(TOperThread)
	protected
		procedure Oper; override;
end;

type TOperVBlankCheckEPROM2Thread = class(TOperThread)
	protected
		procedure Oper; override;
end;

type TOperSCLoadEPROMThread = class(TOperThread)
	protected
		procedure Oper; override;
end;

type TOperSCLoadFPGAThread = class(TOperThread)
	protected
		procedure Oper; override;
end;

type TOperV600LoadEPROMThread = class(TOperThread)
	protected
		procedure Oper; override;
end;

type TOperVLoadEPROM1Thread = class(TOperThread)
	protected
		procedure Oper; override;
end;

type TOperVLoadEPROM2Thread = class(TOperThread)
	protected
		procedure Oper; override;
end;

type TOperVLoadFPGAThread = class(TOperThread)
	protected
		procedure Oper; override;
end;


var
  frOperProgress: TfrOperProgress;
  OperThread: TOperThread;

implementation
uses ALCT, JTAGLib, JTAGUtils;

{$R *.xfm}

constructor TOperThread.Create(Form: TfrOperProgress; filename : TFileName);
begin
	FOperForm := Form;
	FFileName := filename;
	FOperForm.pbOper.Position := FOperForm.pbOper.Min;
	FreeOnTerminate := True;
	inherited Create(False);
end;

procedure TOperThread.Execute;
begin
	Oper;
end;


procedure TOperSCBlankCheckThread.Oper;
const
	len = 16385;
	blocks = 64;
var
	i,p: 	longint;
	data: longword;
	errs: longint;
begin
	FOperForm.sbProg.SimpleText := 'Processing';
		if	(StrToInt64('$'+SCReadEPROMID) and PROG_SC_EPROM_ID_MASK) = PROG_SC_EPROM_ID then
		begin
			WriteIR('7E8',11);
			WriteDR('34',7);
			WriteIR('7E5',11);
			for p:=1 to 10000 do begin end;

			errs := 0;
			for i:=0 to blocks-1 do
			begin
				StartDRShift;
				for p:= 0 to (((len-1) div 32)) do
				begin
					data := 0;
					if (len - 32*p) > 32 then
					begin
						data := ShiftData($FFFFFFFF, 32, false);
						if data <> $FFFFFFFF then
							begin
								Inc(errs);
							end;
					end
					else
					begin
						data := ShiftData($FFFFFFFF, len - 32*p, true);
						if data <> ($FFFFFFFF shr (32-(len - 32*p))) then
							begin
								Inc(errs);
							end;
					end;
					Visual((i*len + p*32)  div ((len*blocks) div 100));
//					FOperForm.sbProg.SimpleText := 'Processing... ' +IntToStr(i*len + p*32) + ' of ' + IntToStr(len*blocks) + ' bits';
				end;
				ExitDRShift;
				FOperForm.sbProg.SimpleText := 'Processing... ' +IntToStr(i*len + p*32) + ' of ' + IntToStr(len*blocks) + ' bits';
				if Terminated then Exit;
				if errs >0 then break
			end;
			WriteIR('7F0',11);
			for p:=1 to 1100000 do begin end;
			WriteIR('7FF',11);
			WriteIR('7F0',11);
			for p:=1 to 1100000 do begin end;
			WriteIR('7FF',11);
			WriteIR('7F0',11);
			for p:=1 to 1100000 do begin end;
			WriteIR('7FF',11);
			ReturnValue := errs;
			FOperForm.SetRes(ReturnValue);
		end
		else
		begin
			ReturnValue := -1;
			FOperForm.SetRes(ReturnValue);
		end;
	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;

procedure TOperVBlankCheckEPROM1Thread.Oper;
const
	len = 16385;
	blocks = 256;
var
	i,p: 	longint;
	data: longword;
	errs: longint;
begin
	FOperForm.sbProg.SimpleText := 'Processing';
		if	((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM1_ID_MASK) = PROG_V_EPROM1_ID) or
                        ((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM1_ID_MASK) = PROG_V_EPROM1_ID2) then
		begin
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FFF',13);
			WriteIR('1FFE',13);
			WriteDR('00ffffffff',33);
			WriteIR('1FFF',13);
			WriteIR('1FE8',13);
			WriteDR('34',7);
			WriteIR('1FE5',13);
			errs := 0;

			for i:= 0 to blocks-1 do
			begin
				StartDRShift;
				for p:= 0 to (((len-1) div 32)) do
				begin
					data := 0;
					if (len - 32*p) > 32 then
					begin
						data := ShiftData($FFFFFFFF, 32, false);
						if data <> $FFFFFFFF then
						begin
							Inc(errs);
						end;
					end
					else
					begin
						data := ShiftData($FFFFFFFF, len - 32*p, true);
						if data <> ($FFFFFFFF shr (32-(len - 32*p))) then
						begin
							Inc(errs);
						end;
					end;
					Visual((i*len + p*32)  div ((len*blocks) div 100));
//					FOperForm.sbProg.SimpleText := 'Processing... ' +IntToStr(i*len + p*32) + ' of ' + IntToStr(len*blocks) + ' bits';
				end;
				ExitDRShift;
				FOperForm.sbProg.SimpleText := 'Processing... ' +IntToStr(i*len + p*32) + ' of ' + IntToStr(len*blocks) + ' bits';
				if Terminated then Exit;
				if errs >0 then break
			end;
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			ReturnValue := errs;
			FOperForm.SetRes(ReturnValue);
		end
		else
		begin
			ReturnValue := -1;
			FOperForm.SetRes(ReturnValue);
		end;
	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;

procedure TOperVBlankCheckEPROM2Thread.Oper;
const
	len = 16385;
	blocks = 256;
var
	i,p: 	longint;
	data: longword;
	errs: longint;
begin
	FOperForm.sbProg.SimpleText := 'Processing';
		if	((StrToInt64('$'+VReadEPROMID2) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID) or
                        ((StrToInt64('$'+VReadEPROMID2) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID2) then
		begin
			WriteIR('1FFFF0',21);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFFFF',21);
			WriteIR('1FFFFF',21);
			WriteIR('1FFFFE',21);
			WriteDR('00ffffffff',33);
			WriteIR('1FFFFF',21);
			WriteIR('1FFFE8',21);
			WriteDR('34',7);
			WriteIR('1FFFE5',21);
			errs := 0;

			for i:= 0 to blocks-1 do
			begin
				StartDRShift;
				for p:= 0 to (((len-1) div 32)) do
				begin
					data := 0;
					if (len - 32*p) > 32 then
					begin
						data := ShiftData($FFFFFFFF, 32, false);
						if data <> $FFFFFFFF then
						begin
							Inc(errs);
						end;
					end
					else
					begin
						data := ShiftData($FFFFFFFF, len - 32*p, true);
						if data <> ($FFFFFFFF shr (32-(len - 32*p))) then
						begin
							Inc(errs);
						end;
					end;
					Visual((i*len + p*32)  div ((len*blocks) div 100));
//					FOperForm.sbProg.SimpleText := 'Processing... ' +IntToStr(i*len + p*32) + ' of ' + IntToStr(len*blocks) + ' bits';
				end;
				ExitDRShift;
				FOperForm.sbProg.SimpleText := 'Processing... ' +IntToStr(i*len + p*32) + ' of ' + IntToStr(len*blocks) + ' bits';
				if Terminated then Exit;
				if errs >0 then break
			end;
			WriteIR('1FFFF0',21);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFFFF',21);
			WriteIR('1FFFF0',21);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFFFF',21);
			WriteIR('1FFFF0',21);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFFFF',21);
			ReturnValue := errs;
			FOperForm.SetRes(ReturnValue);
		end
		else
		begin
			ReturnValue := -1;
			FOperForm.SetRes(ReturnValue);
		end;
	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;



procedure TOperV600BlankCheckThread.Oper;
const
	len = 16385;
	blocks = 256;
var
	i,p: 	longint;
	data: longword;
	errs: longint;
begin
	FOperForm.sbProg.SimpleText := 'Processing';
		if	((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID) or
                        ((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID2) then
		begin
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FFF',13);
			WriteIR('1FFE',13);
			WriteDR('00ffffffff',33);
			WriteIR('1FFF',13);
			WriteIR('1FE8',13);
			WriteDR('34',7);
			WriteIR('1FE5',13);
			errs := 0;

			for i:= 0 to blocks-1 do
			begin
				StartDRShift;
				for p:= 0 to (((len-1) div 32)) do
				begin
					data := 0;
					if (len - 32*p) > 32 then
					begin
						data := ShiftData($FFFFFFFF, 32, false);
						if data <> $FFFFFFFF then
						begin
							Inc(errs);
						end;
					end
					else
					begin
						data := ShiftData($FFFFFFFF, len - 32*p, true);
						if data <> ($FFFFFFFF shr (32-(len - 32*p))) then
						begin
							Inc(errs);
						end;
					end;
					Visual((i*len + p*32)  div ((len*blocks) div 100));
//					FOperForm.sbProg.SimpleText := 'Processing... ' +IntToStr(i*len + p*32) + ' of ' + IntToStr(len*blocks) + ' bits';
				end;
				ExitDRShift;
				FOperForm.sbProg.SimpleText := 'Processing... ' +IntToStr(i*len + p*32) + ' of ' + IntToStr(len*blocks) + ' bits';
				if Terminated then Exit;
				if errs >0 then break
			end;
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			WriteIR('1FF0',13);
			for p:=1 to 1100000 do begin end;
			WriteIR('1FFF',13);
			ReturnValue := errs;
			FOperForm.SetRes(ReturnValue);
		end
		else
		begin
			ReturnValue := -1;
			FOperForm.SetRes(ReturnValue);
		end;
	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;


procedure TOperSCLoadEPROMThread.Oper;
const
	Chunk = 2048;
type
	TMCSHexFile = record
		bytecount:	BYTE;
		hexaddr:	WORD;
		rectype:BYTE;
		hdata:	array of BYTE;
		chksum:	BYTE;
	end;
var
	bfiledlg	: TOpenDialog;
	bfile		: TFileStream;
	hexdata		: TStringList;
	offs		: WORD;
	v, data		: BYTE;
	i,j,p,len,cnt	: longint;
	value		: string;
	mcs			: TMCSHexFile;
	hexstr		: string;
begin
	FOperForm.sbProg.SimpleText := 'Processing';
  if	(StrToInt64('$'+SCReadEPROMID) and PROG_SC_EPROM_ID_MASK) = PROG_SC_EPROM_ID then
  begin
	hexdata  := TStringList.Create;
	hexdata.LoadFromFile(FFileName);
	cnt := 0;
	for i:=0 to hexdata.Count-1 do
	begin
		hexstr := hexdata.Strings[i];
		if Pos(':', hexstr) = 1 then
		begin
			with mcs do
			begin
				bytecount := StrToInt('$'+ Copy(hexstr,2 , 2));
				hexaddr   := StrToInt('$'+ Copy(hexstr,4 , 4));
				rectype   := StrToInt('$'+ Copy(hexstr,8 , 2));
				SetLength(hdata, bytecount);
				for p:= Low(hdata) to High(hdata) do
					hdata[p]	  := StrToInt('$'+Copy(hexstr, 10+p*2, 2));
				chksum	  := StrToInt('$'+ Copy(hexstr,10+bytecount*2, 2));
				case rectype of
				00: begin
						if bytecount > 0 then
						begin
							for j:= 0 to bytecount-1 do
							begin
								Inc(cnt);
								data := hdata[j];
								if cnt = 1 then
								begin
									// Start Programming EPROM
									WriteIR('7F0',11);
									for p:=1 to 1000000 do begin end;
									WriteIR('7FF',11);
									WriteIR('7FF',11);
									WriteIR('7FE',11);
									ReadDR('1fffffffe',33);
									WriteIR('7FF',11);
									WriteIR('7E8',11);
									WriteDR('4',7);
									WriteIR('7EB',11);
									WriteDR('1',17);
									WriteIR('7EC',11);
									for p:=1 to 1000000 do begin end;
									for p:=1 to 1000000 do begin end;
									WriteIR('7F0',11);
									for p:=1 to 1000000 do begin end;
									for p:=1 to 1100000 do begin end;
									WriteIR('7FF',11);
									WriteDR('0',2);
									WriteIR('7F0',11);
									for p:=1 to 1100000 do begin end;
									WriteIR('7FF',11);
									WriteIR('7FF',11);
									WriteIR('7FF',11);
									WriteIR('7FE',11);
									ReadDR('1fffffffe',33);
									WriteIR('7FF',11);
									WriteIR('7E8',11);
									WriteDR('4',7);
									WriteIR('7ED',11);
									StartDRShift;
								end;
								if ((cnt mod 256) = 1) and (cnt > 1) then
								begin
									ShiftData(0,1,true);
									ExitDRShift;
									if (cnt div 256) = 1 then
									begin
										WriteIR('7EB',11);
										WriteDR('0',17);
									end;
									WriteIR('7EA',11);
									for p:=0 to 1000000 do begin end;
									WriteIR('7ED',11);
									StartDRShift;
								end;
								ShiftData(Longword(data),8, false);
								Visual(i  div (hexdata.Count div 100));
								if Terminated then Exit;
							end;
						end;
					end;
					01: begin
							for p:=1 to (256 - (cnt mod 256)) do
								ShiftData($FF,8, false);
							ShiftData(0,1, true);
							ExitDRShift;
							WriteIR('7EA',11);
							for p:=0 to 1000000 do begin end;
							WriteIR('7EB',11);
							WriteDR('1',17);
							WriteIR('70A',11);
							for p:=0 to 1000000 do begin end;
							WriteIR('7F0',11);
							WriteIR('7E8',11);
							WriteDR('4',7);
							WriteIR('7EB',11);
							WriteDR('4000',17);
							for p:=0 to 1000000 do begin end;
							WriteIR('7F3',11);
							WriteDR('3D',7);
							WriteIR('7EA',11);
							for p:=0 to 1000000 do begin end;
							WriteIR('7F0',11);
							for p:=0 to 1000000 do begin end;
							WriteIR('7FF',11);
							WriteIR('7F0',11);
							for p:=0 to 1000000 do begin end;
							WriteIR('7FF',11);
							break;
						end;
					02: continue;
					03: continue;
					04: continue;
					end;
				end;
			end
		end;
	hexdata.Free;
	hexdata := nil;
	ReturnValue := 0;
	FOperForm.SetRes(ReturnValue);
  end
  else
  begin
	ReturnValue := -1;
	FOperForm.SetRes(ReturnValue);
  end;

	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;

procedure TOperSCLoadFPGAThread.Oper;
var
	offs		: WORD;
	v, data		: BYTE;
	i,p, len	: longint;
	value		: string;
//	bfile		: TFileStream;
	bfile		: TMemoryStream;
	Res, dlgRes : integer;
begin
	FOperForm.sbProg.SimpleText := 'Processing';

  if(StrToInt64('$'+SCReadFPGAID) and PROG_SC_FPGA_ID_MASK) = PROG_SC_FPGA_ID then
  begin
//	bfile := TFileStream.Create(string(FFileName),fmOpenRead);
	bfile := TMemoryStream.Create;
	bfile.LoadFromFile(FFileName);
	bfile.Seek(0, soFromBeginning);
	bfile.Read(Offs,2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);
	bfile.Read(Offs,2);
	Offs := swap(Offs);

	bfile.Read(v,1);
	assert( v = $61);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);

	bfile.Read(v,1);
	assert( v = $62);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);

	bfile.Read(v,1);
	assert( v = $63);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);
	bfile.Read(v,1);
	assert( v = $64);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);

	bfile.Read(v,1);
	assert( v = $65);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	value := IntToHex(Offs, 4);
	bfile.Read(Offs, 2);
	Offs := swap(Offs);
	value := value + IntToHex(Offs, 4);
	len := StrToInt('$'+value);

	// Start Programming FPGA
	WriteIR('7FF',11);
	WriteIR('6FF',11);
	WriteDR('01fffffffe',33);
	WriteIR('7FF',11);
	WriteIR('5FF',11);
	for i:=1 to 1000000 do begin p:= i; end;
	StartDRShift;
	for i:=1 to len do
	begin
		bfile.Read(v,1);
		ShiftData(Longword(FlipByte(v)),8, i=len);
		Visual(i div (len div 100));
	end;
	ExitDRShift;
	WriteIR('7FF',11);
	// Finish
	bfile.Free;
	bfile := nil;
	ReturnValue := 0;
	FOperForm.SetRes(ReturnValue);
  end
  else
  begin
	ReturnValue := -1;
	FOperForm.SetRes(ReturnValue);
  end;

	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;

procedure TOperV600LoadEPROMThread.Oper;
const
	Chunk = 4096;
type
	TMCSHexFile = record
		bytecount:	BYTE;
		hexaddr:	WORD;
		rectype:BYTE;
		hdata:	array of BYTE;
		chksum:	BYTE;
	end;
var
//	bfiledlg	: TOpenDialog;
//	bfile		: TFileStream;
	hexdata		: TStringList;
	offs		: WORD;
	v, data		: BYTE;
	i,j,p,len,cnt	: longint;
	value		: string;
	mcs			: TMCSHexFile;
	hexstr		: string;
begin
	FOperForm.sbProg.SimpleText := 'Processing';
  if	((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID) or
        ((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID2) then
  begin
	hexdata  := TStringList.Create;
	hexdata.LoadFromFile(FFileName);
	cnt := 0;
	for i:=0 to hexdata.Count-1 do
	begin
		hexstr := hexdata.Strings[i];
		if Pos(':', hexstr) = 1 then
		begin
			with mcs do
			begin
				bytecount := StrToInt('$'+ Copy(hexstr,2 , 2));
				hexaddr   := StrToInt('$'+ Copy(hexstr,4 , 4));
				rectype   := StrToInt('$'+ Copy(hexstr,8 , 2));
				SetLength(hdata, bytecount);
				for p:= Low(hdata) to High(hdata) do
					hdata[p]	  := StrToInt('$'+Copy(hexstr, 10+p*2, 2));
				chksum	  := StrToInt('$'+ Copy(hexstr,10+bytecount*2, 2));
				case rectype of
				00: begin
						if bytecount > 0 then
						begin
							for j:= 0 to bytecount-1 do
							begin
								Inc(cnt);
								data := hdata[j];
								if cnt = 1 then
								begin
									// Start Programming EPROM
									WriteIR('1FF0',13);
									for p:=1 to 1000000 do begin end;
									WriteIR('1FFF',13);
									WriteIR('1FFF',13);
									WriteIR('1FFE',13);
									ReadDR('0',33);
									WriteIR('1FFF',13);
									WriteIR('1FE8',13);
									WriteDR('4',7);
									WriteIR('1FEB',13);
									WriteDR('1',17);
									WriteIR('1FEC',13);
									for p:=1 to 2000000 do begin end;
									for p:=1 to 2000000 do begin end;
									WriteIR('1FF0',13);
									for p:=1 to 1000000 do begin end;
									for p:=1 to 1100000 do begin end;
									WriteIR('1FFF',13);
									WriteDR('0',2);
									WriteIR('1FF0',13);
									for p:=1 to 1000000 do begin end;
									for p:=1 to 1100000 do begin end;
									WriteIR('1FFF',13);
									WriteIR('1FFF',13);
									WriteIR('1FFF',13);
									WriteIR('1FFE',13);
									ReadDR('00ffffffff',33);
									WriteIR('1FFF',13);
									WriteIR('1FE8',13);
									WriteDR('4',7);
									WriteIR('1FED',13);
									StartDRShift;
								end;
								if ((cnt mod 512) = 1) and (cnt > 1) then
								begin
									ShiftData(0,1,true);
									ExitDRShift;
									if (cnt div 512) = 1 then
									begin
										WriteIR('1FEB',13);
										WriteDR('0',17);
										for p:=0 to 1000 do begin end;
									end;
									WriteIR('1FEA',13);
									for p:=0 to 1000000 do begin end;
									WriteIR('1FED',13);
									StartDRShift;
								end;
								ShiftData(Longword(data),8, false);
								Visual(i  div (hexdata.Count div 100));
								if Terminated then Exit;
							end;
						end;
					end;
					01: begin
							for p:=1 to (512 - (cnt mod 512)) do
								ShiftData($FF,8, false);
							ShiftData(0,1, true);
							ExitDRShift;
							WriteIR('1FEA',13);
							for p:=0 to 1000000 do begin end;
							WriteIR('1FEB',13);
							WriteDR('1',17);
							WriteIR('1F0A',13);
							for p:=0 to 1000000 do begin end;
							WriteIR('1FE8',13);
							WriteDR('4',7);
							WriteIR('1FEB',13);
							WriteDR('8000',17);
							WriteIR('1FF3',13);
							WriteDR('5',4);
							WriteIR('1FEA',13);
							for p:=0 to 1000000 do begin end;
							WriteIR('1FF0',13);
							for p:=0 to 1000000 do begin end;
							for p:=0 to 1000000 do begin end;
							WriteIR('1FFF',13);
							WriteIR('1FF0',13);
							for p:=0 to 1000000 do begin end;
							for p:=0 to 1000000 do begin end;
							WriteIR('1FFF',13);
							break;
						end;
					02: continue;
					03: continue;
					04: continue;
					end;
				end;
			end
		end;
	hexdata.Free;
	hexdata := nil;
	ReturnValue := 0;
	FOperForm.SetRes(ReturnValue);
  end
  else
  begin
	ReturnValue := -1;
	FOperForm.SetRes(ReturnValue);
  end;

	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;

procedure TOperVLoadEPROM1Thread.Oper;
const
	Chunk = 4096;
type
	TMCSHexFile = record
		bytecount:	BYTE;
		hexaddr:	WORD;
		rectype:BYTE;
		hdata:	array of BYTE;
		chksum:	BYTE;
	end;
var
	bfiledlg	: TOpenDialog;
	bfile		: TFileStream;
	hexdata		: TStringList;
	offs		: WORD;
	v, data		: BYTE;
	i,j,p,len,cnt	: longint;
	value		: string;
	mcs			: TMCSHexFile;
	hexstr		: string;
begin
	FOperForm.sbProg.SimpleText := 'Processing';
  if	((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM1_ID_MASK) = PROG_V_EPROM1_ID) or
        ((StrToInt64('$'+VReadEPROMID1) and PROG_V_EPROM1_ID_MASK) = PROG_V_EPROM1_ID2) then
  begin
	hexdata  := TStringList.Create;
	hexdata.LoadFromFile(FFileName);
	cnt := 0;
	for i:=0 to hexdata.Count-1 do
	begin
		hexstr := hexdata.Strings[i];
		if Pos(':', hexstr) = 1 then
		begin
			with mcs do
			begin
				bytecount := StrToInt('$'+ Copy(hexstr,2 , 2));
				hexaddr   := StrToInt('$'+ Copy(hexstr,4 , 4));
				rectype   := StrToInt('$'+ Copy(hexstr,8 , 2));
				SetLength(hdata, bytecount);
				for p:= Low(hdata) to High(hdata) do
					hdata[p]	  := StrToInt('$'+Copy(hexstr, 10+p*2, 2));
				chksum	  := StrToInt('$'+ Copy(hexstr,10+bytecount*2, 2));
				case rectype of
				00: begin
						if bytecount > 0 then
						begin
							for j:= 0 to bytecount-1 do
							begin
								Inc(cnt);
								data := hdata[j];
								if cnt = 1 then
								begin
									// Start Programming EPROM
									WriteIR('1FFFFF',21);
									WriteIR('1FF0',13);
									for p:=1 to 1000000 do begin end;
									WriteIR('1FFF',13);
									WriteIR('1FFF',13);
									WriteIR('1FFE',13);
									ReadDR('1fffffffe',34);
// 									Log.Lines.Add('EPROM ID Code: ' + ReadDR('00ffffffff',33));
									WriteIR('1FFF',13);
									WriteIR('1FE8',13);
									WriteDR('8',8);
									WriteIR('1FEB',13);
									WriteDR('2',18);
									WriteIR('1FEC',13);
									for p:=1 to 1000000 do begin end;
									for p:=1 to 1000000 do begin end;
									WriteIR('1FF0',13);
									for p:=1 to 1000000 do begin end;
									for p:=1 to 1100000 do begin end;
									WriteIR('1FFF',13);
									WriteDR('0',3);
									WriteIR('1FF0',13);
									for p:=1 to 1100000 do begin end;
									WriteIR('1FFF',13);
									WriteIR('1FFF',13);
									WriteIR('1FFF',13);
									WriteIR('1FFE',13);
									ReadDR('1fffffffe',34);
// 									Log.Lines.Add('EPROM ID Code: ' + ReadDR('00ffffffff',33));
									WriteIR('1FFF',13);
									WriteIR('1FE8',13);
									WriteDR('08',8);
									WriteIR('1FED',13);
									StartDRShift;
								end;
								if ((cnt mod 512) = 1) and (cnt > 1) then
								begin
									ShiftData(0,2,true);
									ExitDRShift;
									if (cnt div 512) = 1 then
									begin
										WriteIR('1FEB',13);
										WriteDR('0',18);
									end;
									WriteIR('1FEA',13);
									for p:=0 to 1000000 do begin end;
									WriteIR('1FED',13);
									StartDRShift;
								end;
								ShiftData(Longword(data),8, false);
								Visual(i  div (hexdata.Count div 100));
								if Terminated then Exit;
							end;
						end;
					end;
					01: begin
							for p:=1 to (512 - (cnt mod 512)) do
								ShiftData($FF,8, false);
							ShiftData(0,2, true);
							ExitDRShift;
							WriteIR('1FEA',13);
							for p:=0 to 1000000 do begin end;
							WriteIR('1FEB',13);
							WriteDR('2',18);
							WriteIR('1F0A',13);
							for p:=0 to 1000000 do begin end;
							WriteIR('1FE8',13);
							WriteDR('8',8);
							WriteIR('1FEB',13);
							WriteDR('10000',18);
							WriteIR('1FF3',13);
							WriteDR('a',5);
							WriteIR('1FEA',13);
							for p:=0 to 1000000 do begin end;
							WriteIR('1FF0',13);
							for p:=0 to 1000000 do begin end;
							for p:=0 to 1000000 do begin end;
							WriteIR('1FFF',13);
							WriteIR('1FF0',13);
							for p:=0 to 1000000 do begin end;
							for p:=0 to 1000000 do begin end;
							WriteIR('1FFF',13);
							break;
						end;
					02: continue;
					03: continue;
					04: continue;
					end;
				end;
			end
		end;
	hexdata.Free;
	hexdata := nil;
	ReturnValue := 0;
	FOperForm.SetRes(ReturnValue);
  end
  else
  begin
	ReturnValue := -1;
	FOperForm.SetRes(ReturnValue);
  end;

	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;

procedure TOperVLoadEPROM2Thread.Oper;
const
	Chunk = 4096;
type
	TMCSHexFile = record
		bytecount:	BYTE;
		hexaddr:	WORD;
		rectype:BYTE;
		hdata:	array of BYTE;
		chksum:	BYTE;
	end;
var
	bfiledlg	: TOpenDialog;
	bfile		: TFileStream;
	hexdata		: TStringList;
	offs		: WORD;
	v, data		: BYTE;
	i,j,p,len,cnt	: longint;
	value		: string;
	mcs			: TMCSHexFile;
	hexstr		: string;
begin
	FOperForm.sbProg.SimpleText := 'Processing';
  if	((StrToInt64('$'+VReadEPROMID2) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID) or
        ((StrToInt64('$'+VReadEPROMID2) and PROG_V_EPROM2_ID_MASK) = PROG_V_EPROM2_ID2) then
  begin
	hexdata  := TStringList.Create;
	hexdata.LoadFromFile(FFileName);
	cnt := 0;
	for i:=0 to hexdata.Count-1 do
	begin
		hexstr := hexdata.Strings[i];
		if Pos(':', hexstr) = 1 then
		begin
			with mcs do
			begin
				bytecount := StrToInt('$'+ Copy(hexstr,2 , 2));
				hexaddr   := StrToInt('$'+ Copy(hexstr,4 , 4));
				rectype   := StrToInt('$'+ Copy(hexstr,8 , 2));
				SetLength(hdata, bytecount);
				for p:= Low(hdata) to High(hdata) do
					hdata[p]	  := StrToInt('$'+Copy(hexstr, 10+p*2, 2));
				chksum	  := StrToInt('$'+ Copy(hexstr,10+bytecount*2, 2));
				case rectype of
				00: begin
						if bytecount > 0 then
						begin
							for j:= 0 to bytecount-1 do
							begin
								Inc(cnt);
								data := hdata[j];
								if cnt = 1 then
								begin
									// Start Programming EPROM
									WriteIR('1FFFFF',21);
									WriteIR('1FFFF0',21);
									for p:=1 to 1000000 do begin end;
									WriteIR('1FFFFF',21);
									WriteIR('1FFFFF',21);
									WriteIR('1FFFFE',21);
									ReadDR('1fffffffe',34);
// 									Log.Lines.Add('EPROM ID Code: ' + ReadDR('00ffffffff',33));
									WriteIR('1FFFFF',21);
									WriteIR('1FFFE8',21);
									WriteDR('4',8);
									WriteIR('1FFFEB',21);
									WriteDR('1',18);
									WriteIR('1FFFEC',21);
									for p:=1 to 1000000 do begin end;
									for p:=1 to 1000000 do begin end;
									WriteIR('1FFFF0',21);
									for p:=1 to 1000000 do begin end;
									for p:=1 to 1100000 do begin end;
									WriteIR('1FFFFF',21);
									WriteDR('0',3);
									WriteIR('1FFFF0',21);
									for p:=1 to 1100000 do begin end;
									WriteIR('1FFFFF',21);
									WriteIR('1FFFFF',21);
									WriteIR('1FFFFF',21);
									WriteIR('1FFFFE',21);
									ReadDR('1fffffffe',34);
// 									Log.Lines.Add('EPROM ID Code: ' + ReadDR('00ffffffff',33));
									WriteIR('1FFFFF',21);
									WriteIR('1FFFE8',21);
									WriteDR('04',8);
									WriteIR('1FFFED',21);
									StartDRShift;
								end;
								if ((cnt mod 512) = 1) and (cnt > 1) then
								begin
									ShiftData(0,2,true);
									ExitDRShift;
									if (cnt div 512) = 1 then
									begin
										WriteIR('1FFFEB',21);
										WriteDR('0',18);
									end;
									WriteIR('1FFFEA',21);
									for p:=0 to 1000000 do begin end;
									WriteIR('1FFFED',21);
									StartDRShift;
								end;
								ShiftData(Longword(data),8, false);
								Visual(i  div (hexdata.Count div 100));
								if Terminated then Exit;
							end;
						end;
					end;
					01: begin
							for p:=1 to (512 - (cnt mod 512)) do
							ShiftData($FF,8, false);
							ShiftData(0,2, true);
							ExitDRShift;
							WriteIR('1FFFEA',21);
							for p:=0 to 1000000 do begin end;
							WriteIR('1FFFEB',21);
							WriteDR('1',18);
							WriteIR('1FFF0A',21);
							for p:=0 to 1000000 do begin end;
							WriteIR('1FFFE8',21);
							WriteDR('4',8);
							WriteIR('1FFFEB',21);
							WriteDR('8000',18);
							WriteIR('1FFFF3',21);
							WriteDR('5',5);
							WriteIR('1FFFEA',21);
							for p:=0 to 1000000 do begin end;
							WriteIR('1FFFF0',21);
							for p:=0 to 1000000 do begin end;
							for p:=0 to 1000000 do begin end;
							WriteIR('1FFFFF',21);
							WriteIR('1FFFF0',21);
							for p:=0 to 1000000 do begin end;
							for p:=0 to 1000000 do begin end;
							WriteIR('1FFFFF',21);
							break;
						end;
					02: continue;
					03: continue;
					04: continue;
					end;
				end;
			end
		end;
	hexdata.Free;
	hexdata := nil;
	ReturnValue := 0;
	FOperForm.SetRes(ReturnValue);
  end
  else
  begin
	ReturnValue := -1;
	FOperForm.SetRes(ReturnValue);
  end;

	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;


procedure TOperVLoadFPGAThread.Oper;
var
	offs		: WORD;
	v, data		: BYTE;
	i,p, len	: longint;
	value		: string;
//	bfile		: TFileStream;
	bfile		: TMemoryStream;
	Res, dlgRes : integer;
begin
	FOperForm.sbProg.SimpleText := 'Processing';

  if ((StrToInt64('$'+VReadFPGAID) and PROG_V_FPGA_ID_MASK) = PROG_V_FPGA_ID) or ((StrToInt64('$'+VReadFPGAID) and PROG_V_FPGA600_ID_MASK) = PROG_V_FPGA600_ID) then
  begin
//	bfile := TFileStream.Create(string(FFileName),fmOpenRead);
	bfile := TMemoryStream.Create;
	bfile.LoadFromFile(FFileName);
	bfile.Seek(0, soFromBeginning);
	bfile.Read(Offs,2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);
	bfile.Read(Offs,2);
	Offs := swap(Offs);

	bfile.Read(v,1);
	assert( v = $61);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);

	bfile.Read(v,1);
	assert( v = $62);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);

	bfile.Read(v,1);
	assert( v = $63);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);
	bfile.Read(v,1);
	assert( v = $64);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	bfile.Seek(Offs, soFromCurrent);

	bfile.Read(v,1);
	assert( v = $65);

	bfile.Read(Offs, 2);
	Offs := swap(Offs);

	value := IntToHex(Offs, 4);
	bfile.Read(Offs, 2);
	Offs := swap(Offs);
	value := value + IntToHex(Offs, 4);
	len := StrToInt('$'+value);

	// Start Programming FPGA
	WriteIR('1FFF',13);
	WriteIR('9FF',13);
	WriteDR('1fffffffe',33);
	WriteIR('1FFF',13);
	WriteIR('5FF',13);
	WriteDR('00000000e00000008001000ca00000008001000cb4fd05008004800c',224);
	WriteIR('CFF',13);
	WriteDR('0',13);
	WriteIR('5FF',13);

	StartDRShift;
	ShiftData($8001000c,32,false);
	ShiftData($10000000,32,false);
	for i:=1 to len do
	begin
		bfile.Read(v,1);
		ShiftData(Longword(FlipByte(v)),8, i=len);
		Visual(i div (len div 100));
	end;
	ExitDRShift;

	WriteIR('CFF',13);
	WriteDR('0',13);
	WriteIR('5FF',13);
	WriteDR('0000000080070014',64);
	WriteIR('4FF',13);
	WriteDR('0000000000',33);
	WriteIR('1FFF',13);
	WriteDR('0',2);
	WriteDR('0',2);
	// Finish
	bfile.Free;
	bfile := nil;
	ReturnValue := 0;
	FOperForm.SetRes(ReturnValue);
  end
  else
  begin
	ReturnValue := -1;
	FOperForm.SetRes(ReturnValue);
  end;

	FOperForm.sbProg.SimpleText := 'Finished';
	FOperForm.btClose.Enabled := true;
	if FOperForm.IsCloseOnFinish then
		FOperForm.btClose.Click;
end;


procedure TOperThread.Visual(position: integer);
begin
	FProgress := position;
	DoVisual;
end;

procedure TOperThread.DoVisual;
begin
	with FOperForm do
	begin
		pbOper.Position := FProgress;
	end;
end;
//function TfrOperProgress.DoOper(func: function:boolean; filename: TFileName; fclose: boolean; fcancel :boolean): integer;
function TfrOperProgress.DoOper(const opertype: integer; filename: TFileName; fclose: boolean; fcancel :boolean; out Res: integer): integer;
begin
	SetRes(0);
	lbOper.Caption := OperDescr[opertype];
	SetCloseOnExit(fclose);
	SetEnableCancel(fcancel);
	SetOperationType(opertype);
	SetFileName(filename);
	Result := ShowModal;
	Res := GetRes;
end;

function TfrOperProgress.IsCloseOnFinish: boolean;
begin
	Result := fCloseOnFinish;
end;

procedure TfrOperProgress.SetCloseOnExit(const flag: boolean);
begin
	fCloseOnFinish := flag;
end;

procedure TfrOperProgress.SetEnableCancel(const flag: boolean);
begin
	fEnableCancel := flag;
end;

procedure TfrOperProgress.SetOperationType(const optype: integer);
begin
	OperType := optype;
end;

procedure TfrOperProgress.SetFileName(const filename: TFileName);
begin
	fn := FileName;
end;
procedure TfrOperProgress.SetRes(const res: integer);
begin
	R := res;
end;

function TfrOperProgress.GetRes: integer;
begin
	Result := R;
end;

function TfrOperProgress.GetFileName: TFileName;
begin
	Result := fn;
end;


procedure TfrOperProgress.FormShow(Sender: TObject);
begin
	btClose.Enabled := False; // not fCloseOnFinish;
	btCancel.Enabled := fEnableCancel;
//	pbOper.Position := pbOper.Min;
	case OperType of
	0: 	OperThread := TOperSCBlankCheckThread.Create(frOperProgress,'');
	1:  OperThread := TOperVBlankCheckEPROM1Thread.Create(frOperProgress,'');
	2:  OperThread := TOperVBlankCheckEPROM2Thread.Create(frOperProgress,'');
	3:  OperThread := TOperV600BlankCheckThread.Create(frOperProgress,'');
	4:  OperThread := TOperSCLoadEPROMThread.Create(frOperProgress,GetFileName);
	5:  OperThread := TOperSCLoadFPGAThread.Create(frOperProgress,GetFileName);
	6:  OperThread := TOperV600LoadEPROMThread.Create(frOperProgress,GetFileName);
	8:  OperThread := TOperVLoadEPROM1Thread.Create(frOperProgress,GetFileName);
	9:  OperThread := TOperVLoadEPROM2Thread.Create(frOperProgress,GetFileName);
	7,10:  OperThread := TOperVLoadFPGAThread.Create(frOperProgress,GetFileName);
	else
//		OperThread := TOperThread.Create(frOperProgress);
	end;
//	OperThread.OnTerminate := ThreadDone;
end;

procedure TfrOperProgress.btCancelClick(Sender: TObject);
begin
	OperThread.Terminate;
end;

procedure TfrOperProgress.btCloseClick(Sender: TObject);
begin
	OperThread.Terminate;
end;

end.
